% Choose Participant Try 
clear all 
close all

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
input_folder=fullfile(subject_path,'3_Output\');
output_folder=fullfile(subject_path,'3_Output\');
if (isequal(input_folder, 0))
    return;
end
if (isequal(output_folder, 0))
    return;
end
%%
load(fullfile(input_folder,'StraitAllDataLeft.mat'))
answer = 'Yes';

while (strcmp(answer,'Yes'))
%Choose File
[file, Stride_path]=uigetfile({'*.mat'},'Select Stride File',input_folder);
if (isequal(file, 0))
    return;
end 
load(fullfile(Stride_path,file));
if  strcmp(char(Strides.Indices.LOLI(1,1)),'LeadIn')==1
    P0cc=FindCoContraction(Strides.Left.P0.NormAmp);
    P_1cc=FindCoContraction(Strides.Left.P_1.NormAmp);
    P_2cc=FindCoContraction(Strides.Left.P_2.NormAmp);
else 
    P0cc=FindCoContraction(Strides.Right.P0.NormAmp);
    P_1cc=FindCoContraction(Strides.Right.P_1.NormAmp);
    P_2cc=FindCoContraction(Strides.Right.P_2.NormAmp);
end
Name={'LTA/LGL';'RTA/RGL';'LGlu/LAdd';'RGlu/RAdd';'LTA/RTA';'LGL/RGL';'Ladd/RAdd';'LGlu/RGlu'};
CoContraction=mean([P0cc,P_1cc,P_2cc],2);

    answer = questdlg('Analyze another file ?', 'Analyse another file', 'Yes', 'No', 'Yes');
% end
close all
end 