function [trail_name,Data_trial_ref] = getvariables(Data_trial)
%Fonction pour extraire variable LogVS
%  
[lg,sz]=size(Data_trial);
if sz==8
       frame_num=cell2mat(Data_trial(:,2));
    frame_time=str2double(Data_trial(:,3));
    PP_mod=strip(Data_trial(:,4),'left','[');PP_mod=strip(PP_mod,'right',']');
    PP=split(PP_mod,',');PP=str2double(PP);
    Player_posX=PP(:,1);Player_posY=PP(:,2);Player_posZ=PP(:,3);
    PR_mod=strip(Data_trial(:,5),'left','[');PR_mod=strip(PR_mod,'right',']');
    PR=split(PR_mod,',');PR=str2double(PR);
    Player_rotX=PR(:,1);Player_rotY=PR(:,2);Player_rotZ=PR(:,3);
    VA_posX=zeros(lg,1);VA_posY=zeros(lg,1);VA_posZ=zeros(lg,1);VA_rotX=...
    zeros(lg,1);VA_rotY=zeros(lg,1);VA_rotZ=zeros(lg,1);
    time=str2double(Data_trial(:,6));
    trail_name=Data_trial(1,8);
    Data_trial_ref=[time,frame_num,frame_time,Player_posX,Player_posY,...
        Player_posZ,Player_rotX,Player_rotY,Player_rotZ,VA_posX,VA_posY,...
        VA_posZ,VA_rotX,VA_rotY,VA_rotZ];
        % Resample
    a=0;
for i=1:size(Data_trial_ref,1)
a=[a;a(i,1)+(Data_trial_ref(i,3))];
end
time=a/1000;
TimeOculus=time;
AxOculus=(Data_trial_ref(:,5))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,4)/100); 
AzOculus=(Data_trial_ref(:,6))/100;
AoriXoculus=(Data_trial_ref(:,7));
AoriYoculus=-(Data_trial_ref(:,8));
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,9)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(2:end,1),90.023);
Ppx=filtrateTraj(ResampO,1/10,1)-1.71;
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(2:end,1),90.023);
Ppy=filtrateTraj(ResampO,1/10,1)+4.57;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(2:end,1),90.023);
Ppz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(2:end,1),90.023);
Pox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(2:end,1),90.023);
Poy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(2:end,1),90.023);
Poz=filtrateTraj(ResampO,1/10,1);

[lg,sz]=size(Poz);
Apx=zeros(lg,1);Apy=zeros(lg,1);Apz=zeros(lg,1);Aox=...
    zeros(lg,1);Aoy=zeros(lg,1);Aoz=zeros(lg,1);

elseif sz==10
    frame_num=cell2mat(Data_trial(:,2));
    frame_time=str2double(Data_trial(:,3));
    PP_mod=strip(Data_trial(:,4),'left','[');PP_mod=strip(PP_mod,'right',']');
    PP=split(PP_mod,',');PP=str2double(PP);
    Player_posX=PP(:,1);Player_posY=PP(:,2);Player_posZ=PP(:,3);
    PR_mod=strip(Data_trial(:,5),'left','[');PR_mod=strip(PR_mod,'right',']');
    PR=split(PR_mod,',');PR=str2double(PR);
    Player_rotX=PR(:,1);Player_rotY=PR(:,2);Player_rotZ=PR(:,3);
    VAP_mod=strip(Data_trial(:,6),'left','[');VAP_mod=strip(VAP_mod,'right',']');
    VAP=split(VAP_mod,',');VAP=str2double(VAP);
    VA_posX=VAP(:,1);VA_posY=VAP(:,2);VA_posZ=VAP(:,3);
    VAR_mod=strip(Data_trial(:,7),'left','[');VAR_mod=strip(VAR_mod,'right',']');
    VAR=split(VAR_mod,',');VAR=str2double(VAR);
    VA_rotX=VAR(:,1);VA_rotY=VAR(:,2);VA_rotZ=VAR(:,3);
    time=str2double(Data_trial(:,8));
    trail_name=Data_trial(1,10);
    Data_trial_ref=[time,frame_num,frame_time,Player_posX,Player_posY,...
        Player_posZ,Player_rotX,Player_rotY,Player_rotZ,VA_posX,VA_posY,...
        VA_posZ,VA_rotX,VA_rotY,VA_rotZ];
        % Resample
    a=0;
for i=1:size(Data_trial_ref,1)
a=[a;a(i,1)+(Data_trial_ref(i,3))];
end
time=a/1000;
TimeOculus=time;
AxOculus=(Data_trial_ref(:,5))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,4)/100); 
AzOculus=(Data_trial_ref(:,6))/100;
AoriXoculus=(Data_trial_ref(:,7));
AoriYoculus=-(Data_trial_ref(:,8));
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,9)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(2:end,1),90.023);
Ppx=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(2:end,1),90.023);
Ppy=filtrateTraj(ResampO,1/10,1)+5;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(2:end,1),90.023);
Ppz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(2:end,1),90.023);
Pox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(2:end,1),90.023);
Poy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(2:end,1),90.023);
Poz=filtrateTraj(ResampO,1/10,1);

AxOculus=(Data_trial_ref(:,11))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,10)/100); 
AzOculus=Data_trial_ref(:,12)/100;
AoriXoculus=Data_trial_ref(:,13);
AoriYoculus=-Data_trial_ref(:,14);
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,15)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(2:end,1),90.023);
Apx=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(2:end,1),90.023);
Apy=filtrateTraj(ResampO,1/10,1)+5;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(2:end,1),90.023);
Apz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(2:end,1),90.023);
Aox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(2:end,1),90.023);
Aoy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(2:end,1),90.023);
Aoz=filtrateTraj(ResampO,1/10,1);
end
[Ppx,Ppy,Ppz,Apx,Apy,Apz] = trigrotate(Ppx,Ppy,Ppz,Apx,Apy,Apz,-45);
Data_trial_ref=[Ppx,Ppy,Ppz,Pox,Poy,Poz,Apx,Apy,Apz,Aox,Aoy,Aoz];


