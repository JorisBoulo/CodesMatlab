function [Ppx,Ppy,Ppz,Pox,Poy,Poz,Apx,Apy,Apz,Aox,Aoy,Aoz]=ResampleDataQuest(DataQuest)
a=0;
for i=1:size(DataQuest,1)
a=[a;a(i,1)+DataQuest(i,2)];
end
time=a/1000;
TimeOculus=time;
AxOculus=(DataQuest(:,5))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=-(DataQuest(:,4)/100); 
AzOculus=DataQuest(:,6)/100;
AoriXoculus=DataQuest(:,7);
AoriYoculus=-DataQuest(:,8);
for center=1:size(DataQuest,1)
    if DataQuest(center,9)>0
        DataQuest(center,9)=DataQuest(center,9)-180;
    else
        DataQuest(center,9)=DataQuest(center,9)+180;
    end
end
 AoriZoculus=(DataQuest(:,9)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(2:end,1),90.023);
Ppx=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(2:end,1),90.023);
Ppy=filtrateTraj(ResampO,1/10,1)-5;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(2:end,1),90.023);
Ppz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(2:end,1),90.023);
Pox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(2:end,1),90.023);
Poy=filtrateTraj(ResampO,1/10,1)-5;
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(2:end,1),90.023);
Poz=filtrateTraj(ResampO,1/10,1);

AxOculus=(DataQuest(:,10))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=-(DataQuest(:,11)/100); 
AzOculus=DataQuest(:,12)/100;
AoriXoculus=DataQuest(:,13);
AoriYoculus=-DataQuest(:,14);
for center=1:size(DataQuest,1)
    if DataQuest(center,9)>0
        DataQuest(center,9)=DataQuest(center,9)-180;
    else
        DataQuest(center,9)=DataQuest(center,9)+180;
    end
end
 AoriZoculus=(DataQuest(:,15)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(2:end,1),90.023);
Apx=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(2:end,1),90.023);
Apy=filtrateTraj(ResampO,1/10,1)-5;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(2:end,1),90.023);
Apz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(2:end,1),90.023);
Aox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(2:end,1),90.023);
Aoy=filtrateTraj(ResampO,1/10,1)-5;
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(2:end,1),90.023);
Aoz=filtrateTraj(ResampO,1/10,1);
