function [PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = trigrotate(PPx,PPy,PPz,VAPx,VAPy,VAPz,angle)
%Function to rotate data in order to align it to the world X axis

sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);

%Changing Player position 
for n=1:sz
    PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
    PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
    PPzmod(n,1) = PPz(n,1);
end

%Changing VA position
for o=1:sz
    VAPxmod(o,1) = VAPx(o,1)*sinagl - VAPy(o,1)*cosagl;
    VAPymod(o,1) = VAPx(o,1)*cosagl + VAPy(o,1)*sinagl;
    VAPzmod(o,1) = VAPz(o,1);
end

end

