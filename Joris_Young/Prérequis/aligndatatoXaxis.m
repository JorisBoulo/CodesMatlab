function [PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = aligndatatoXaxis(Data_to_format)
%Takes the positional data and rotate it around the player's position when VA is trig
%in order to align the VA starting point with the X axis
%   Classify data into XYZ system for each variable
PPxo=Data_to_format(:,1); PPyo=Data_to_format(:,2); PPz=Data_to_format(:,3);
VAPxo=Data_to_format(:,7); VAPyo=Data_to_format(:,8); VAPz=Data_to_format(:,9);

%% Put system origin on VA trig point 
Tx=VAPxo(1,1); Ty=VAPyo(1,1);
Trigtimex=find(VAPxo(:,1)==Tx,1,'last');Trigtimex=Trigtimex+1;
Trigtimey=find(VAPyo(:,1)==Ty,1,'last');Trigtimey=Trigtimey+1;
if Trigtimex<Trigtimey
    Trigtime=Trigtimex;
elseif Trigtimey<Trigtimex
    Trigtime=Trigtimey;
elseif Trigtimex==Trigtimey
    Trigtime=Trigtimex;
end
TPx=PPxo(Trigtime,1);TPy=PPyo(Trigtime,1);
PPx=PPxo-TPx;PPy=PPyo-TPy;VAPx=VAPxo-TPx;VAPy=VAPyo-TPy;
%% Get angle needed to rotate to position
Distance_X=VAPx(1,1);
Distance_Y=VAPy(1,1); 
dist=Distance_Y/Distance_X;
angle=atand(dist); 
%WHY les 2 sont neg????
if VAPy<0
    angle=-angle;
else
    angle=-angle;
end
%% Rotate data by variable
[PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = trigrotate(PPx,PPy,PPz,VAPx,VAPy,VAPz,angle)
%% Export Data into 1 variable
Data_formated=[PPxmod PPymod PPzmod VAPxmod VAPymod VAPzmod];
end

