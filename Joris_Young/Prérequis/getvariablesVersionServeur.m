function [trail_name,Data_trial_ref] = getvariablesVersionServeur(Data_trial)
%Fonction pour extraire variable LogVS
%  
[lg,sz]=size(Data_trial);
if sz==19
    frame_num=str2double(Data_trial(:,3));
    frame_time=str2double(Data_trial(:,4));
    Player_posX=str2double(Data_trial(:,5));Player_posY=str2double(Data_trial(:,6));Player_posZ=str2double(Data_trial(:,7));
    Player_rotX=str2double(Data_trial(:,8));Player_rotY=str2double(Data_trial(:,9));Player_rotZ=str2double(Data_trial(:,10));
    VA_posX=str2double(Data_trial(:,11));VA_posY=str2double(Data_trial(:,12));VA_posZ=str2double(Data_trial(:,13));
    VA_rotX=str2double(Data_trial(:,14));VA_rotY=str2double(Data_trial(:,15));VA_rotZ=str2double(Data_trial(:,16));
    time=str2double(Data_trial(:,17));
    trail_name=Data_trial(1,19);
    Data_trial_ref=[time,frame_num,frame_time,Player_posX,Player_posY,...
    Player_posZ,Player_rotX,Player_rotY,Player_rotZ,VA_posX,VA_posY,...
    VA_posZ,VA_rotX,VA_rotY,VA_rotZ];
        % Resample

TimeOculus=time-time(1,1);
AxOculus=(Data_trial_ref(:,5))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,4)/100); 
AzOculus=(Data_trial_ref(:,6))/100;
AoriXoculus=(Data_trial_ref(:,7));
AoriYoculus=-(Data_trial_ref(:,8));
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,9)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(1:end,1),90.023);
Ppx=filtrateTraj(ResampO,1/10,1)-1.71;
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(1:end,1),90.023);
Ppy=filtrateTraj(ResampO,1/10,1)+4.57;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(1:end,1),90.023);
Ppz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(1:end,1),90.023);
Pox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(1:end,1),90.023);
Poy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(1:end,1),90.023);
Poz=filtrateTraj(ResampO,1/10,1);

AxOculus=(Data_trial_ref(:,11))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,10)/100); 
AzOculus=Data_trial_ref(:,12)/100;
AoriXoculus=Data_trial_ref(:,13);
AoriYoculus=-Data_trial_ref(:,14);
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,15)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(1:end,1),90.023);
Apx=filtrateTraj(ResampO,1/10,1)-1.71;
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(1:end,1),90.023);
Apy=filtrateTraj(ResampO,1/10,1)+4.57;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(1:end,1),90.023);
Apz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(1:end,1),90.023);
Aox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(1:end,1),90.023);
Aoy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(1:end,1),90.023);
Aoz=filtrateTraj(ResampO,1/10,1);
end

%%
if sz==18
    frame_num=str2double(Data_trial(:,2));
    frame_time=str2double(Data_trial(:,3));
    Player_posX=str2double(Data_trial(:,4));Player_posY=str2double(Data_trial(:,5));Player_posZ=str2double(Data_trial(:,6));
    Player_rotX=str2double(Data_trial(:,7));Player_rotY=str2double(Data_trial(:,8));Player_rotZ=str2double(Data_trial(:,9));
    VA_posX=str2double(Data_trial(:,10));VA_posY=str2double(Data_trial(:,11));VA_posZ=str2double(Data_trial(:,12));
    VA_rotX=str2double(Data_trial(:,13));VA_rotY=str2double(Data_trial(:,14));VA_rotZ=str2double(Data_trial(:,15));
    time=str2double(Data_trial(:,16));
    trail_name=Data_trial(1,18);
    Data_trial_ref=[time,frame_num,frame_time,Player_posX,Player_posY,...
    Player_posZ,Player_rotX,Player_rotY,Player_rotZ,VA_posX,VA_posY,...
    VA_posZ,VA_rotX,VA_rotY,VA_rotZ];
        % Resample

TimeOculus=time-time(1,1);
AxOculus=(Data_trial_ref(:,5))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,4)/100); 
AzOculus=(Data_trial_ref(:,6))/100;
AoriXoculus=(Data_trial_ref(:,7));
AoriYoculus=-(Data_trial_ref(:,8));
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,9)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(1:end,1),90.023);
Ppx=filtrateTraj(ResampO,1/10,1)-1.71;
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(1:end,1),90.023);
Ppy=filtrateTraj(ResampO,1/10,1)+4.57;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(1:end,1),90.023);
Ppz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(1:end,1),90.023);
Pox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(1:end,1),90.023);
Poy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(1:end,1),90.023);
Poz=filtrateTraj(ResampO,1/10,1);

AxOculus=(Data_trial_ref(:,11))/100; %1:(AllsizeV{ses,k})-50)
AyOculus=(Data_trial_ref(:,10)/100); 
AzOculus=Data_trial_ref(:,12)/100;
AoriXoculus=Data_trial_ref(:,13);
AoriYoculus=-Data_trial_ref(:,14);
for center=1:size(Data_trial_ref,1)
    if Data_trial_ref(center,9)>0
        Data_trial_ref(center,9)=Data_trial_ref(center,9)-180;
    else
        Data_trial_ref(center,9)=Data_trial_ref(center,9)+180;
    end
end
 AoriZoculus=(Data_trial_ref(:,15)+90);
[ResampO,TimeRes]=resample(AxOculus,TimeOculus(1:end,1),90.023);
Apx=filtrateTraj(ResampO,1/10,1)-1.71;
[ResampO,TimeRes]=resample(AyOculus,TimeOculus(1:end,1),90.023);
Apy=filtrateTraj(ResampO,1/10,1)+4.57;
[ResampO,TimeRes]=resample(AzOculus,TimeOculus(1:end,1),90.023);
Apz=filtrateTraj(ResampO,1/10,1);
%Rotation
[ResampO,TimeRes]=resample(AoriXoculus,TimeOculus(1:end,1),90.023);
Aox=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriYoculus,TimeOculus(1:end,1),90.023);
Aoy=filtrateTraj(ResampO,1/10,1);
[ResampO,TimeRes]=resample(AoriZoculus,TimeOculus(1:end,1),90.023);
Aoz=filtrateTraj(ResampO,1/10,1);
end
[Ppx,Ppy,Ppz,Apx,Apy,Apz] = trigrotate(Ppx,Ppy,Ppz,Apx,Apy,Apz,-45);
Data_trial_ref=[Ppx,Ppy,Ppz,Pox,Poy,Poz,Apx,Apy,Apz,Aox,Aoy,Aoz];


