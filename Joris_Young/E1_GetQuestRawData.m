clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
[file, path]=uigetfile({'*.xlsx'},'Select Quest File',inputpath);
if (isequal(file, 0))
    return;
end
%%
file_path=fullfile(inputpath,file);

Data_raw=readtable(file_path);


%%
DataString =string(table2cell(Data_raw));
DataString=regexprep(DataString,{'{"avatar_trigger":false','{"avatar_trigger":','experiment_id:','frame_number:','frame_time:','participant_location:[',']','participant_rotation:[','side_step_character_position:[','side_step_character_rotation:[','time:','trial_id:','"','trial_name:','}'},...
    {'','','','','','','','','','','','','','',''});

First={'avatar_trigger','experiment_id','frame_number:','frame_time:','p_location X','p_location Y','p_location Z','p_rotation X','p_rotation Y','p_rotation Z','a_position X','a_position Y','a_position Z','a_rotation X','a_rotation Y','a_rotation Z','time:','trial_id:','trial_name:'};
DataString=[string(First);DataString];

for i=2:size(DataString,1)
    if DataString(i,14)==''
        DataString(i,17:19)=DataString(i,11:13);
        DataString(i,11:16)=zeros(1,6);
    end 
end 
        %%
DataQuestRaw=cellstr(DataString);
outputfilepath=fullfile(inputpath,'DataQuestRaw');
save(outputfilepath,'DataQuestRaw')