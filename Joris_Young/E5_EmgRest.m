% This script allows you to create the mean and std of muscle inactivity of each muscle for each participant  
%% *Open files*
% Choose Participant 
subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
input_folder=fullfile(subject_path,'2_TransformData\');
output_folder=fullfile(subject_path,'3_Output\');
if (isequal(input_folder, 0))
    return;
end
if (isequal(output_folder, 0))
    return;
end
% Choose a EMG File
[filecatch,path]=uigetfile('*.mat','Select 5 catch trials','MultiSelect','on',subject_path);

if numel(filecatch)~=5
    warndlg=('You didn''t select five catch trials');
    return
end
%%1700
filecatch1=cell2mat(fullfile(path,filecatch(1)));filecatch2=cell2mat(fullfile(path,filecatch(2)));
filecatch3=cell2mat(fullfile(path,filecatch(3)));filecatch4=cell2mat(fullfile(path,filecatch(4)));
filecatch5=cell2mat(fullfile(path,filecatch(5)));
catchdata1=load(filecatch1); catchdata2=load(filecatch2);catchdata3=load(filecatch3);
catchdata4=load(filecatch4);catchdata5=load(filecatch5);
catchEMGtxt=catchdata1.DataEMG(1,:);

catchdata1=catchdata1.DataEMG(2:end,1:13);catchdata2=catchdata2.DataEMG(2:end,1:13);catchdata3=catchdata3.DataEMG(2:end,1:13);
catchdata4=catchdata4.DataEMG(2:end,1:13);catchdata5=catchdata5.DataEMG(2:end,1:13);

catchdata1=cell2mat(catchdata1);catchdata2=cell2mat(catchdata2);catchdata3=cell2mat(catchdata3);
catchdata4=cell2mat(catchdata4);catchdata5=cell2mat(catchdata5);

Acc1=catchdata1(:,12:13);Acc2=catchdata2(2:end,12:13);Acc3=catchdata3(2:end,12:13);
Acc4=catchdata4(2:end,12:13);Acc5=catchdata5(2:end,12:13);

catchdata1=FiltrateEMG(catchdata1,4,20,450);catchdata2=FiltrateEMG(catchdata2,4,20,450);catchdata3=FiltrateEMG(catchdata3,4,20,450);
catchdata4=FiltrateEMG(catchdata4,4,20,450);catchdata5=FiltrateEMG(catchdata5,4,20,450);


catchdata1=movmean(abs(catchdata1-mean(catchdata1,1)),100);catchdata2=movmean(abs(catchdata2-mean(catchdata2,1)),100);catchdata3=movmean(abs(catchdata3-mean(catchdata3,1)),100);
catchdata4=movmean(abs(catchdata4-mean(catchdata4,1)),100);catchdata5=movmean(abs(catchdata5-mean(catchdata5,1)),100);

[~,HeelStrikeL1]=findpeaks(-Acc1(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL2]=findpeaks(-Acc2(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL3]=findpeaks(-Acc3(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL4]=findpeaks(-Acc4(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL5]=findpeaks(-Acc5(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);



Timeline=(1:1:size(catchdata1(HeelStrikeL1(2,1):HeelStrikeL1(3,1),:)));
Data1=resample(catchdata1(HeelStrikeL1(2,1):HeelStrikeL1(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata2(HeelStrikeL2(2,1):HeelStrikeL2(3,1),:)));
Data2=resample(catchdata2(HeelStrikeL2(2,1):HeelStrikeL2(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata3(HeelStrikeL3(2,1):HeelStrikeL3(3,1),:)));
Data3=resample(catchdata3(HeelStrikeL3(2,1):HeelStrikeL3(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata4(HeelStrikeL4(2,1):HeelStrikeL4(3,1),:)));
Data4=resample(catchdata4(HeelStrikeL4(2,1):HeelStrikeL4(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata5(HeelStrikeL5(2,1):HeelStrikeL5(3,1),:)));
Data5=resample(catchdata5(HeelStrikeL5(2,1):HeelStrikeL5(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));


%% Select Rest period  ,

Extract=[];
for i=1:1:11
    f1 = figure;
    hold on 
    title(catchEMGtxt(1,i))
    plot(Data1(:,i));
    plot(Data2(:,i));
    plot(Data3(:,i));
    plot(Data4(:,i));
    plot(Data5(:,i));
    [x1, ~]=ginput(1);
    [x2, ~]=ginput(1);
    x1=round(x1);
    x2=round(x2);
    DataExtrct=[(mean(Data1(x1:x2,i))+mean(Data2(x1:x2,i))+mean(Data3(x1:x2,i))+mean(Data4(x1:x2,i)))/4;...
        (std(Data1(x1:x2,i))+std(Data2(x1:x2,i))+std(Data3(x1:x2,i))+std(Data4(x1:x2,i)))/4];
    Extract=[Extract,DataExtrct];
    close(f1);clear f1; 
end
outputfilepath=fullfile(output_folder,'MeanAndSdRest.mat');
save(outputfilepath,'Extract')

