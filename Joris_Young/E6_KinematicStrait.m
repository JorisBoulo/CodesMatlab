%% 
% The goal of this scrpit is to produced a mean Strait trial curve that will 
% be used to guide visually the identification of the two deviation points 
% and reorientation sequencies. 
% 
% *Note that some part of the script is "write in french". 
% 
% *This script is need to be run before the main code to produce the "mean5Straittrials" 
% file that is needed to start the main code.*
% 
% *Open files*

clear;clc;
subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 1\Participants\','Select the participant file');
subject_path=(fullfile(subject_path));

%% Output Folder
output_folder = uigetdir(subject_path, 'Select Results Output Folder');
if (isequal(output_folder, 0))
    return; 
end

[fileStrait,path]=uigetfile('*.mat','Select 5 Strait trials','MultiSelect','on',subject_path);


if numel(fileStrait)~=5
    warndlg=('You didn''t select five Strait trials')
    return
end
%%
fileStrait1=cell2mat(fullfile(path,fileStrait(1)));fileStrait2=cell2mat(fullfile(path,fileStrait(2)));
fileStrait3=cell2mat(fullfile(path,fileStrait(3)));fileStrait4=cell2mat(fullfile(path,fileStrait(4)));
fileStrait5=cell2mat(fullfile(path,fileStrait(5)));
file_EMG1=load(strrep(fileStrait1,'MM.mat','EMG.mat'));file_EMG2=load(strrep(fileStrait2,'MM.mat','EMG.mat'));file_EMG3=load(strrep(fileStrait3,'MM.mat','EMG.mat'));file_EMG4=load(strrep(fileStrait4,'MM.mat','EMG.mat'));
file_EMG5=load(strrep(fileStrait5,'MM.mat','EMG.mat'));
% file_Quest1=load(strrep(fileStrait1,'MM.mat','Quest.mat'));file_Quest2=load(strrep(fileStrait2,'MM.mat','Quest.mat'));file_Quest3=load(strrep(fileStrait3,'MM.mat','Quest.mat'));file_Quest4=load(strrep(fileStrait4,'MM.mat','Quest.mat'));
% file_Quest5=load(strrep(fileStrait5,'MM.mat','Quest.mat'));
% file_Quest1=load(strrep(fileStrait1,'MM.mat','Quest.mat'));file_Quest2=load(strrep(fileStrait2,'MM.mat','Quest.mat'));file_Quest3=load(strrep(fileStrait3,'MM.mat','Quest.mat'));file_Quest4=load(strrep(fileStrait4,'MM.mat','Quest.mat'));
% file_Quest5=load(strrep(fileStrait5,'MM.mat','Quest.mat'));
% Participant_Quest_Position1= GetAvatarVariables(file_Quest1);Participant_Quest_Position2= GetAvatarVariables(file_Quest2);Participant_Quest_Position3= GetAvatarVariables(file_Quest3);Participant_Quest_Position4= GetAvatarVariables(file_Quest4);Participant_Quest_Position5= GetAvatarVariables(file_Quest5);

[~,HeelLFramesEMG1]=findpeaks(cell2mat(file_EMG1.DataEMG(2:end,end)),'MinPeakHeight',0,'MinPeakDistance',1700);
[~,HeelRFramesEMG1]=findpeaks(cell2mat(file_EMG1.DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelLFramesEMG2]=findpeaks(cell2mat(file_EMG2.DataEMG(2:end,end)),'MinPeakHeight',0,'MinPeakDistance',1700);
[~,HeelRFramesEMG2]=findpeaks(cell2mat(file_EMG2.DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelLFramesEMG3]=findpeaks(cell2mat(file_EMG3.DataEMG(2:end,end)),'MinPeakHeight',0,'MinPeakDistance',1700);
[~,HeelRFramesEMG3]=findpeaks(cell2mat(file_EMG3.DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelLFramesEMG4]=findpeaks(cell2mat(file_EMG4.DataEMG(2:end,end)),'MinPeakHeight',0,'MinPeakDistance',1700);
[~,HeelRFramesEMG4]=findpeaks(cell2mat(file_EMG4.DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelLFramesEMG5]=findpeaks(cell2mat(file_EMG5.DataEMG(2:end,end)),'MinPeakHeight',0,'MinPeakDistance',1700);
[~,HeelRFramesEMG5]=findpeaks(cell2mat(file_EMG5.DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);

HeelLFrames1=round((HeelLFramesEMG1/2250)*90);
HeelRFrames1=round((HeelRFramesEMG1/2250)*90);
HeelLFrames2=round((HeelLFramesEMG2/2250)*90);
HeelRFrames2=round((HeelRFramesEMG2/2250)*90);
HeelLFrames3=round((HeelLFramesEMG3/2250)*90);
HeelRFrames3=round((HeelRFramesEMG3/2250)*90);
HeelLFrames4=round((HeelLFramesEMG4/2250)*90);
HeelRFrames4=round((HeelRFramesEMG4/2250)*90);
HeelLFrames5=round((HeelLFramesEMG5/2250)*90);
HeelRFrames5=round((HeelRFramesEMG5/2250)*90);

Straitdata1=load(fileStrait1); Straitdata2=load(fileStrait2);Straitdata3=load(fileStrait3);
Straitdata4=load(fileStrait4);Straitdata5=load(fileStrait5);

[NameRow, colInd] = find(strcmp(Straitdata1.MMData(:,1), 'Frame #'));

%%
Straittxt=(Straitdata1.MMData(NameRow,:));
%Cut the files to keep only the number
Straitdata1=Straitdata1.MMData(10:end,:);Straitdata2=Straitdata2.MMData(10:end,:);Straitdata3=Straitdata3.MMData(10:end,:);
Straitdata4=Straitdata4.MMData(10:end,:);Straitdata5=Straitdata5.MMData(10:end,:);
% Straitdata1=strrep(string(Straitdata1),'-1.#IND00','0');Straitdata2=strrep(string(Straitdata2),'-1.#IND00','0');Straitdata3=strrep(string(Straitdata3),'-1.#IND00','0');
% Straitdata4=strrep(string(Straitdata4),'-1.#IND00','0');Straitdata5=strrep(string(Straitdata5),'-1.#IND00','0');


Straitdata1=str2double(string(Straitdata1));
Straitdata2=str2double(string(Straitdata2));
Straitdata3=str2double(string(Straitdata3));
Straitdata4=str2double(string(Straitdata4));
Straitdata5=str2double(string(Straitdata5));


%%
[n_line, n_column]=size(Straitdata1);
Title_line=1;

error=1;
for i=1:n_column
    if ~isempty(strfind(Straittxt{Title_line,i},'Head_Pos_X'))
    Head_X_pos_Ind=i;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Head_Pos_Y'))
    Head_Y_pos_Ind=i;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Head_X_Vel'))
    Head_X_vel_Ind=i;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Head_Y_Vel'))
    Head_Y_vel_Ind=i;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Head_Vel_Z'))
    Head_Z_vel_Ind=17;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Angle_Thorax_Y'))
    Head_Rot_Y_G_Ind=i;
% elseif ~isempty(strfind(Straittxt{Title_line,i},'Head_Rot_Y_G'));
    Head_Rot_Y_G_Ind=31;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Angle_Pelvis_Y'))
   Pelvis_Rot_Y_G_Ind=i;
elseif ~isempty(strfind(Straittxt{Title_line,i},'Angle_Thorax_Z'))
%     Head_Rot_Z_G_Ind=i;
% elseif ~isempty(strfind(Title{1,i},'Head_Pos_X'))
%     Pos_Head_X=MMData(:,i); 
% elseif ~isempty(strfind(Title{1,i},'Head_Pos_Y'))
%     Pos_Head_Y=MMData(:,i); 
% elseif ~isempty(strfind(Title{1,i},'Head_Pos_Z'))
%     Pos_Head_Z=MMData(:,i);
    end
end
Straitdata1=Straitdata1(HeelLFrames1(1,1):HeelLFrames1(4,1),:);Straitdata2=Straitdata2(HeelLFrames2(1,1):HeelLFrames2(4,1),:);Straitdata3=Straitdata3(HeelLFrames3(1,1):HeelLFrames3(4,1),:);Straitdata4=Straitdata4(HeelLFrames4(1,1):HeelLFrames4(4,1),:);Straitdata5=Straitdata5(HeelLFrames5(1,1):HeelLFrames5(4,1),:);
[s1 i1]=size(Straitdata1);[s2 i2]=size(Straitdata2);[s3 i3]=size(Straitdata3);[s4 i4]=size(Straitdata4);[s5 i5]=size(Straitdata5);
coupure=[s1,s2,s3,s4,s5];coupure=min(coupure);
Straitdata1=Straitdata1(1:coupure,:);Straitdata2=Straitdata2(1:coupure,:);Straitdata3=Straitdata3(1:coupure,:);Straitdata4=Straitdata4(1:coupure,:);Straitdata5=Straitdata5(1:coupure,:);
%% 
% *Display of the 250 first frameof the 5 Straits trials *Verify if it is representative 
% trials, ideally, all start with the same feet to average something and not do 
% two flat lines *
figure
plot(1:size(Straitdata1,1),Straitdata1(:,Head_X_pos_Ind))
hold on
plot(1:size(Straitdata1,1),Straitdata2(:,Head_X_pos_Ind))
plot(1:size(Straitdata1,1),Straitdata3(:,Head_X_pos_Ind))
plot(1:size(Straitdata1,1),Straitdata4(:,Head_X_pos_Ind))
plot(1:size(Straitdata1,1),Straitdata5(:,Head_X_pos_Ind))
title 'Strait trials M-L COM Head position'
title 'Head ML Position Straits'
legend('1','2','3','4','5')
hold off
answer=questdlg('Are the 5 Strait trials seem representative?','Verify Strait trials','Yes','No','Yes')
switch answer
    case 'Yes'
        representative=1;
    case 'No'
        representative=0;
end

if isequal(representative,0)
    return
end



% *Mean Strait trial vector and 2 SD curves*
XHeadStrait=[Straitdata1(:,Head_X_pos_Ind),Straitdata2(:,Head_X_pos_Ind),Straitdata3(:,Head_X_pos_Ind),Straitdata4(:,Head_X_pos_Ind),Straitdata5(:,Head_X_pos_Ind)];
% f_cutoff = 9; % Cutoff frequency (Hz)
% [b,a] = butter(4, f_cutoff/(90/2), 'low'); % 4th order Butterworth filter
% XHeadStrait= filtfilt(b,a,XHeadStrait);
meanXHead=mean(XHeadStrait')';
sdXHead=abs(std(XHeadStrait')');
sdXHead_up=meanXHead+2*sdXHead;sdXHead_low=meanXHead-2*sdXHead;
YHeadStrait=[Straitdata1(:,Head_Y_pos_Ind),Straitdata2(:,Head_Y_pos_Ind),Straitdata3(:,Head_Y_pos_Ind),Straitdata4(:,Head_Y_pos_Ind),Straitdata5(:,Head_Y_pos_Ind)];
% YHeadStrait= filtfilt(b,a,YHeadStrait);
meanYHead=mean(YHeadStrait')';
sdYHead=abs(std(YHeadStrait')');
sdYHead_up=meanYHead+2*sdYHead;sdYHead_low=meanYHead-2*sdYHead;

%% 
XHead_Vel_Strait=[Straitdata1(:,Head_X_vel_Ind),Straitdata2(:,Head_X_vel_Ind),Straitdata3(:,Head_X_vel_Ind),Straitdata4(:,Head_X_vel_Ind),Straitdata5(:,Head_X_vel_Ind)];
% XHead_Vel_Strait= filtfilt(b,a,XHead_Vel_Strait);
meanHead_Vel_X=mean(XHead_Vel_Strait')';
sdXHead_Vel=(std(XHead_Vel_Strait')');sdXHead_Vel=abs(sdXHead_Vel);
sdXHead_Vel_up=meanHead_Vel_X+2*sdXHead_Vel;sdXHead_Vel_low=meanHead_Vel_X-2*sdXHead_Vel;
YHead_Vel_Strait=[Straitdata1(:,Head_Y_vel_Ind),Straitdata2(:,Head_Y_vel_Ind),Straitdata3(:,Head_Y_vel_Ind),Straitdata4(:,Head_Y_vel_Ind),Straitdata5(:,Head_Y_vel_Ind)];
% YHead_Vel_Strait= filtfilt(b,a,YHead_Vel_Strait);
meanHead_Vel_Y=mean(YHead_Vel_Strait')';
sdYHead_Vel=abs(std(YHead_Vel_Strait')');
sdYHead_Vel_up=meanHead_Vel_Y+2*sdYHead_Vel;sdYHead_Vel_low=meanHead_Vel_Y-2*sdYHead_Vel;
% ZHead_Vel_Strait=[Straitdata1(:,Head_Z_vel_Ind),Straitdata2(:,Head_Z_vel_Ind),Straitdata3(:,Head_Z_vel_Ind),Straitdata4(:,Head_Z_vel_Ind),Straitdata5(:,Head_Z_vel_Ind)];
% % ZHead_Vel_Strait= filtfilt(b,a,ZHead_Vel_Strait);
% meanHead_Vel_Z=mean(ZHead_Vel_Strait')';
% sdZHead_Vel=abs(std(ZHead_Vel_Strait')');

%%
% Head_Rot_Z_Strait=[Straitdata1(:,Head_Rot_Z_G_Ind),Straitdata2(:,Head_Rot_Z_G_Ind),Straitdata3(:,Head_Rot_Z_G_Ind),Straitdata4(:,Head_Rot_Z_G_Ind),Straitdata5(:,Head_Rot_Z_G_Ind)];
% meanHead_Rot_Z=mean(Head_Rot_Z_Strait')';
% sdHead_Rot_Z=std(Head_Rot_Z_Strait')';
% sdHead_Rot_Z_up=meanHead_Rot_Z+2*sdHead_Rot_Z;sdHead_Rot_Z_low=meanHead_Rot_Z-2*sdHead_Rot_Z;
% Head_Rot_Y_Strait=[Straitdata1(:,Head_Rot_Y_G_Ind),Straitdata2(:,Head_Rot_Y_G_Ind),Straitdata3(:,Head_Rot_Y_G_Ind),Straitdata4(:,Head_Rot_Y_G_Ind),Straitdata5(:,Head_Rot_Y_G_Ind)];
% meanHead_Rot_Y=mean(Head_Rot_Y_Strait')';
% sdHead_Rot_Y=std(Head_Rot_Y_Strait')';
% sdHead_Rot_Y_up=meanHead_Rot_Y+2*sdHead_Rot_Y;sdHead_Rot_Y_low=meanHead_Rot_Y-2*sdHead_Rot_Y;
% 

% Head_Rot_Z_Strait=[Straitdata1(:,Head_Rot_Z_G_Ind),Straitdata2(:,Head_Rot_Z_G_Ind),Straitdata3(:,Head_Rot_Z_G_Ind),Straitdata4(:,Head_Rot_Z_G_Ind),Straitdata5(:,Head_Rot_Z_G_Ind)];
% meanHead_Rot_Z=mean(Head_Rot_Z_Strait')';
% sdHead_Rot_Z=std(Head_Rot_Z_Strait')';
% sdHead_Rot_Z_up=meanHead_Rot_Z+2*sdHead_Rot_Z;sdHead_Rot_Z_low=meanHead_Rot_Z-2*sdHead_Rot_Z;
% Head_Rot_Y_Strait=[Straitdata1(:,Head_Rot_Y_G_Ind),Straitdata2(:,Head_Rot_Y_G_Ind),Straitdata3(:,Head_Rot_Y_G_Ind),Straitdata4(:,Head_Rot_Y_G_Ind),Straitdata5(:,Head_Rot_Y_G_Ind)];
% meanHead_Rot_Y=mean(Head_Rot_Y_Strait')';
% sdHead_Rot_Y=std(Head_Rot_Y_Strait')';
% sdHead_Rot_Y_up=meanHead_Rot_Y+2*sdHead_Rot_Y;sdHead_Rot_Y_low=meanHead_Rot_Y-2*sdHead_Rot_Y;


% Pelvis_Rot_Z_Strait=[Straitdata1(:,Pelvis_Rot_Z_G_Ind),Straitdata2(:,Pelvis_Rot_Z_G_Ind),Straitdata3(:,Pelvis_Rot_Z_G_Ind),Straitdata4(:,Pelvis_Rot_Z_G_Ind),Straitdata5(:,Pelvis_Rot_Z_G_Ind)];
% meanPelvis_Rot_Z=mean(Pelvis_Rot_Z_Strait')';
% sdPelvis_Rot_Z=std(Pelvis_Rot_Z_Strait')';
% sdPelvis_Rot_Z_up=meanPelvis_Rot_Z+2*sdPelvis_Rot_Z;sdPelvis_Rot_Z_low=meanPelvis_Rot_Z-2*sdPelvis_Rot_Z;
% Pelvis_Rot_Y_Strait=[Straitdata1(:,Pelvis_Rot_Y_G_Ind),Straitdata2(:,Pelvis_Rot_Y_G_Ind),Straitdata3(:,Pelvis_Rot_Y_G_Ind),Straitdata4(:,Pelvis_Rot_Y_G_Ind),Straitdata5(:,Pelvis_Rot_Y_G_Ind)];
% meanPelvis_Rot_Y=mean(Pelvis_Rot_Y_Strait')';
% sdPelvis_Rot_Y=std(Pelvis_Rot_Y_Strait')';
% sdPelvis_Rot_Y_up=meanPelvis_Rot_Y+2*sdPelvis_Rot_Y;sdPelvis_Rot_Y_low=meanPelvis_Rot_Y-2*sdPelvis_Rot_Y;

% Scalar_Vel_Strait=[sqrt(Straitdata1(:,Head_X_vel_Ind).^2+Straitdata1(:,Head_Z_vel_Ind).^2+Straitdata1(:,Head_Z_vel_Ind).^2)...
%     ,sqrt(Straitdata2(:,Head_X_vel_Ind).^2+Straitdata2(:,Head_Y_vel_Ind).^2+Straitdata2(:,Head_Z_vel_Ind).^2) ...
% ,sqrt(Straitdata3(:,Head_X_vel_Ind).^2+Straitdata3(:,Head_Y_vel_Ind).^2+Straitdata3(:,Head_Z_vel_Ind).^2) ...
% ,sqrt(Straitdata4(:,Head_X_vel_Ind).^2+Straitdata4(:,Head_Y_vel_Ind).^2+Straitdata4(:,Head_Z_vel_Ind).^2) ...
% ,sqrt(Straitdata5(:,Head_X_vel_Ind).^2+Straitdata5(:,Head_Y_vel_Ind).^2+Straitdata5(:,Head_Z_vel_Ind).^2)];
% mean_Scalar_Vel=mean(Scalar_Vel_Strait')';
% SD_Scalar_Vel=std(Scalar_Vel_Strait')';
% SD_Scalar_Vel_up=mean_Scalar_Vel+2*SD_Scalar_Vel;SD_Scalar_Vel_low=mean_Scalar_Vel-2*SD_Scalar_Vel;

%% Graph
% *Graphic representation to verify the Strait trial average curve for the three critics variables*
% graph=figure
% plot(meanYHead,meanXHead,'m-')
% hold on
% plot(YHeadStrait(:,1),XHeadStrait(:,1))
% plot(YHeadStrait(:,2),XHeadStrait(:,2))
% plot(YHeadStrait(:,3),XHeadStrait(:,3))
% plot(YHeadStrait(:,4),XHeadStrait(:,4))
% plot(YHeadStrait(:,5),XHeadStrait(:,5))
% title 'Head COM M-L displacement during Strait trials'
% xlabel 'Longitudinal Head displacement(m)'
% ylabel 'ML Head displacement (m)'
% legend('mean','1','2','3','4','5')
% figure_name=fullfile(output_folder,'5Strait_Head_X');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanYHead,meanHead_Vel_X,'m-')
% hold on
% plot(YHeadStrait(:,1),XHead_Vel_Strait(:,1))
% plot(YHeadStrait(:,2),XHead_Vel_Strait(:,2))
% plot(YHeadStrait(:,3),XHead_Vel_Strait(:,3))
% plot(YHeadStrait(:,4),XHead_Vel_Strait(:,4))
% plot(YHeadStrait(:,5),XHead_Vel_Strait(:,5))
% title 'Head M-L velocity during Strait trials'
% xlabel 'Longitudinal Head displacement(m)'
% ylabel 'ML velocity (m/s)'
% legend('mean','1','2','3','4','5')
% figure_name=fullfile(output_folder,'5Strait_Head_VelX');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanYHead,meanHead_Rot_Z,'m-')
% hold on
% plot(YHeadStrait(:,1),Head_Rot_Z_Strait(:,1))
% plot(YHeadStrait(:,2),Head_Rot_Z_Strait(:,2))
% plot(YHeadStrait(:,3),Head_Rot_Z_Strait(:,3))
% plot(YHeadStrait(:,4),Head_Rot_Z_Strait(:,4))
% plot(YHeadStrait(:,5),Head_Rot_Z_Strait(:,5))
% title 'Head Yaw during Strait trials'
% xlabel 'Longitudinal Head displacement(m)'
% ylabel 'Head yaw (�)'
% legend('mean','1','2','3','4','5')
% figure_name=fullfile(output_folder,'5Strait_Head_Rot_Z');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanYHead,meanHead_Rot_Y,'m-')
% hold on
% plot(YHeadStrait(:,1),Head_Rot_Y_Strait(:,1))
% plot(YHeadStrait(:,2),Head_Rot_Y_Strait(:,2))
% plot(YHeadStrait(:,3),Head_Rot_Y_Strait(:,3))
% plot(YHeadStrait(:,4),Head_Rot_Y_Strait(:,4))
% plot(YHeadStrait(:,5),Head_Rot_Y_Strait(:,5))
% title 'Head Roll during Strait trials'
% xlabel 'Longitudinal Head displacement(m)'
% ylabel 'Head Roll (�)'
% legend('mean','1','2','3','4','5')
% figure_name=fullfile(output_folder,'5Strait_Head_Rot_Y');
% saveas(graph,figure_name,'png')
% hold off
%% 
% *Courbe moyenne avec les SD (2SD affich�e)*
graph=figure
plot(meanYHead,meanXHead)
hold on
plot(meanYHead,sdXHead_up,'--')
plot(meanYHead,sdXHead_low,'--')
title 'Mean Head COM M-L displacement during Strait trials'
xlabel 'Longitudinal Head displacement(m)'
ylabel 'ML Head displacemen (m)'
figure_name=fullfile(output_folder,'Strait_Head_X');
saveas(graph,figure_name,'png')
hold off

% graph=figure
% plot(meanYHead)
% hold on
% plot(sdYHead_up,'--')
% plot(sdYHead_low,'--')
% title 'Mean Head COM longitudinal displacement during Strait trials'
% xlabel 'Frame'
% ylabel 'Longitudinal Head displacement(m)'
% figure_name=fullfile(output_folder,'Strait_Head_Y');
% saveas(graph,figure_name,'png')
% hold off


graph=figure
plot(meanYHead,meanHead_Vel_X)
hold on
plot(meanYHead,sdXHead_Vel_up,'--')
plot(meanYHead,sdXHead_Vel_low,'--')
title 'Mean Head COM M-L velocity during Strait trials'
xlabel 'Longitudinal Head displacement(m)'
ylabel 'ML Head velocity(m/s)'
figure_name=fullfile(output_folder,'Strait_Head_VelX');
saveas(graph,figure_name,'png')
hold off
% 
% graph=figure
% plot(meanHead_Vel_Y)
% hold on
% plot(sdYHead_Vel_up,'--')
% plot(sdYHead_Vel_low,'--')
% title 'Mean Head longitudinal velocity during Strait trials'
% xlabel 'Frame'
% ylabel 'Longitudinal Head velocity(m/s)'
% figure_name=fullfile(output_folder,'Strait_Head_VelY');
% saveas(graph,figure_name,'png')
% hold off

% graph=figure
% plot(meanHead_Rot_Z)
% hold on
% plot(sdHead_Rot_Z_up,'--')
% plot(sdHead_Rot_Z_low,'--')
% title 'Mean Head Yaw during Strait trials'
% xlabel 'Frame'
% ylabel 'Head Yaw (�)'
% figure_name=fullfile(output_folder,'Strait_Head_Yaw');
% saveas(graph,figure_name,'png')
% hold off

% graph=figure
% plot(meanHead_Rot_Y)
% hold on
% plot(sdHead_Rot_Y_up,'--')
% plot(sdHead_Rot_Y_low,'--')
% title 'Mean Head Roll during Strait trials'
% xlabel 'Frame'
% ylabel 'Head Roll (�)'
% figure_name=fullfile(output_folder,'Strait_Head_Roll');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanHead_Rot_Z)
% hold on
% plot(sdHead_Rot_Z_up,'--')
% plot(sdHead_Rot_Z_low,'--')
% title 'Mean Head Yaw during Strait trials'
% xlabel 'Frame'
% ylabel 'Head Yaw (�)'
% figure_name=fullfile(output_folder,'Strait_Head_Yaw');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanHead_Rot_Y)
% hold on
% plot(sdHead_Rot_Y_up,'--')
% plot(sdHead_Rot_Y_low,'--')
% title 'Mean Head Roll during Strait trials'
% xlabel 'Frame'
% ylabel 'Head Roll (�)'
% figure_name=fullfile(output_folder,'Strait_Head_Roll');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanPelvis_Rot_Z)
% hold on
% plot(sdPelvis_Rot_Z_up,'--')
% plot(sdPelvis_Rot_Z_low,'--')
% title 'Mean Pelvis Yaw during Strait trials'
% xlabel 'Frame'
% ylabel 'Pelvis Yaw (�)'
% figure_name=fullfile(output_folder,'Strait_Pelvis_Yaw');
% saveas(graph,figure_name,'png')
% hold off
% 
% graph=figure
% plot(meanPelvis_Rot_Y)
% hold on
% plot(sdPelvis_Rot_Y_up,'--')
% plot(sdPelvis_Rot_Y_low,'--')
% title 'Mean Pelvis Roll during Strait trials'
% xlabel 'Frame'
% ylabel 'Pelvis Roll (�)'
% figure_name=fullfile(output_folder,'Strait_Pelvis_Roll');
% saveas(graph,figure_name,'png')
% hold off
% 
% 
% graph=figure
% plot(mean_Scalar_Vel)
% hold on
% plot(SD_Scalar_Vel_up,'--')
% plot(SD_Scalar_Vel_low,'--')
% title 'Mean scalar velocity'
% xlabel 'Frame'
% ylabel 'Velocity (m/s)'
% figure_name=fullfile(output_folder,'Strait_Scalar_Velocity');
% saveas(graph,figure_name,'png')
% hold off
filename='Mean_5_StraitTrials.mat';
filename=fullfile(output_folder,filename);
save(filename,'meanXHead','meanYHead','sdXHead','sdXHead_low','sdXHead_up','sdXHead_Vel_up','sdXHead_Vel_low','sdYHead_up','sdYHead_low','meanHead_Vel_X','meanHead_Vel_Y','sdYHead_Vel_up','sdYHead_Vel_low')%,'meanHead_Rot_Z','sdHead_Rot_Z_up', 'sdHead_Rot_Z_low', 'meanHead_Rot_Y', 'sdHead_Rot_Y_up', 'sdHead_Rot_Y_low','meanHead_Rot_Z','sdHead_Rot_Z_up','sdHead_Rot_Z_low','meanHead_Rot_Y','sdHead_Rot_Y_up','sdHead_Rot_Y_low','meanPelvis_Rot_Z','sdPelvis_Rot_Z_up','sdPelvis_Rot_Z_low','meanPelvis_Rot_Y','sdPelvis_Rot_Y_up','sdPelvis_Rot_Y_low','mean_Scalar_Vel','SD_Scalar_Vel_up','SD_Scalar_Vel_low','Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Head_Rot_Y_Mean','Value_Head_Rot_Y_SD','Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Head_Rot_Y_Mean','Value_Head_Rot_Y_SD','Value_Pelvis_Rot_Z_Mean','Value_Pelvis_Rot_Z_SD','Value_Pelvis_Rot_Y_Mean','Value_Pelvis_Rot_Y_SD');
% 

%% Calculs de variables 

%Value: mean and SD of each variables calculated 
% Value_Head_Rot_Z_Mean=mean(mean(Head_Rot_Z_Strait));
% Value_Head_Rot_Z_SD=mean(std(Head_Rot_Z_Strait));
% Value_Head_Rot_Y_Mean=mean(mean(Head_Rot_Y_Strait));
% Value_Head_Rot_Y_SD=mean(std(Head_Rot_Y_Strait));
% 
% Value_Head_Rot_Z_Mean=mean(mean(Head_Rot_Z_Strait));
% Value_Head_Rot_Z_SD=mean(std(Head_Rot_Z_Strait));
% Value_Head_Rot_Y_Mean=mean(mean(Head_Rot_Y_Strait));
% Value_Head_Rot_Y_SD=mean(std(Head_Rot_Y_Strait));
% 
% Value_Pelvis_Rot_Z_Mean=mean(mean(Pelvis_Rot_Z_Strait));
% Value_Pelvis_Rot_Z_SD=mean(std(Pelvis_Rot_Z_Strait));
% Value_Pelvis_Rot_Y_Mean=mean(mean(Pelvis_Rot_Y_Strait));
% Value_Pelvis_Rot_Y_SD=mean(std(Pelvis_Rot_Y_Strait));
% 
% Value_Mean_Scalar_Velocity=mean(mean(Scalar_Vel_Strait));
% Value_SD_Scalar_Velocity=std(mean(Scalar_Vel_Strait));
% 
% Value_Out=[Value_Head_Rot_Z_Mean,Value_Head_Rot_Z_SD,Value_Head_Rot_Y_Mean,Value_Head_Rot_Y_SD,Value_Head_Rot_Z_Mean,Value_Head_Rot_Z_SD... 
%     Value_Head_Rot_Y_Mean,Value_Head_Rot_Y_SD,Value_Pelvis_Rot_Z_Mean,Value_Pelvis_Rot_Z_SD,Value_Pelvis_Rot_Y_Mean,Value_Pelvis_Rot_Y_SD];
% Value_Out_title={'Head_Rot_Z_Mean','Head_Rot_Z_SD','Head_Rot_Y_Mean','Head_Rot_Y_SD','Head_Rot_Z_Mean'... 
%     ,'Head_Rot_Z_SD','Head_Rot_Y_Mean','Head_Rot_Y_SD','Pelvis_Rot_Z_Mean','Pelvis_Rot_Z_SD','Pelvis_Rot_Y_Mean','Pelvis_Rot_Y_SD'};
% value_sheet=[Value_Out_title;num2cell(Value_Out)];
% %data
% mean_SD_curve=[meanXHead,sdXHead_up,sdXHead_low,meanYHead,sdYHead_up,sdYHead_low,...
%     meanHead_Vel_X,sdXHead_Vel_up,sdXHead_Vel_low,meanHead_Vel_Y,sdYHead_Vel_up,sdYHead_Vel_low...
%     meanHead_Rot_Z,sdHead_Rot_Z_up,sdHead_Rot_Z_low,meanHead_Rot_Y,sdHead_Rot_Y_up,sdHead_Rot_Y_low...
%     meanHead_Rot_Z,sdHead_Rot_Z_up,sdHead_Rot_Z_low,meanHead_Rot_Y,sdHead_Rot_Y_up,sdHead_Rot_Y_low...
%     meanPelvis_Rot_Z,sdPelvis_Rot_Z_up,sdPelvis_Rot_Z_low,meanPelvis_Rot_Y,sdPelvis_Rot_Y_up,sdPelvis_Rot_Y_low ...
%     mean_Scalar_Vel,SD_Scalar_Vel_up,SD_Scalar_Vel_low]
% mean_SD_curve_title={'meanXHead','2SD_up','2SD_low','meanYHead','2SD_up','2SD_low'...
%     'meanHead_Vel_X','2SD_up','2SD_low','meanHead_Vel_Y','2SD_up','2SD_low'...
%     'meanHead_Rot_Z','2SD_up','2SD_low','meanHead_Rot_Y','2SD_up','2SD_low'...
%     'meanHead_Rot_Z','2SD_up','2SD_low','meanHead_Rot_Y','2SD_up','2SD_low'...
%     'meanPelvis_Rot_Z','2SD_up','2SD_low','meanPelvis_Rot_Y','2SD_up','2SD_low','mean_Scalar_Vel','2SD_up','2SD_low'}
% data_sheet=[mean_SD_curve_title;num2cell(mean_SD_curve)];
%% 
% *This section is about a theorical contact point. It calculates where the 
% avatar and the participant would contact if nothing was done by them to avoid 
% the contact. This was not used for the calcul of variables, but I kept this 
% calcul there just in case.*
% 
% Cr�ation de 2 �quations lin�

% 
% VYHead1=Straitdata1(:,Head_Y_vel_Ind);VYHead2=Straitdata2(:,Head_Y_vel_Ind);VYHead3=Straitdata3(:,Head_Y_vel_Ind);VYHead4=Straitdata4(:,Head_Y_vel_Ind);VYHead5=Straitdata5(:,Head_Y_vel_Ind);
% VYHeadStrait=[VYHead1,VYHead2,VYHead3,VYHead4,VYHead5]';meanVYHead=mean(VYHeadStrait)';meanVYHead=mean(meanVYHead)
% x=(1:500)/90; %repr�sente le temps, 90 car 90Hz d'acquisition. Le 500 est arbitraire. Le but est d'allonger la trajectoire pour qu'il y ait un point de rencontre
% eqpositionYHead=meanVYHead*x-3.8;
% a=inputdlg('Entrer la vitesse programm�e de l avatar, mettre la valeure positive, en m/s: ','Vitesse du vampire');
% a=str2num(a{1})
% eqavatarX=-a*x+2.9; 
% dif=eqavatarX-eqpositionYHead;
% Tcontact=find(dif<0,1)
% c1=eqpositionYHead(Tcontact)
% c2=eqavatarX(Tcontact)
% theorical_contact=(c1+c2)/2
% %% 
% % *Enregistrement des variables*
% msgbox('Note which Strait trials were used')
%%
%fichier excel
% Value=cell2table(value_sheet);
% Data=cell2table(data_sheet);
% excelValue_filename=fullfile(output_folder,'mean5StraitTrialsValue.xlsx');
% excelData_filename=fullfile(output_folder,'mean5StraitTrialsData.xlsx');
% % writetable(Value,excelValue_filename);
% writetable(Data,excelData_filename);

%fichier matlab
% filename='Mean_5_StraitTrials.mat';
% filename=fullfile(output_folder,filename);
% save(filename,'meanXHead','meanYHead','sdXHead','sdXHead_low','sdXHead_up','sdXHead_Vel_up','sdXHead_Vel_low','theorical_contact','Tcontact','sdYHead_up','sdYHead_low','meanHead_Vel_X','meanHead_Vel_Y','sdYHead_Vel_up','sdYHead_Vel_low')%,'meanHead_Rot_Z','sdHead_Rot_Z_up', 'sdHead_Rot_Z_low', 'meanHead_Rot_Y', 'sdHead_Rot_Y_up', 'sdHead_Rot_Y_low','meanHead_Rot_Z','sdHead_Rot_Z_up','sdHead_Rot_Z_low','meanHead_Rot_Y','sdHead_Rot_Y_up','sdHead_Rot_Y_low','meanPelvis_Rot_Z','sdPelvis_Rot_Z_up','sdPelvis_Rot_Z_low','meanPelvis_Rot_Y','sdPelvis_Rot_Y_up','sdPelvis_Rot_Y_low','mean_Scalar_Vel','SD_Scalar_Vel_up','SD_Scalar_Vel_low','Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Head_Rot_Y_Mean','Value_Head_Rot_Y_SD','Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Head_Rot_Y_Mean','Value_Head_Rot_Y_SD','Value_Pelvis_Rot_Z_Mean','Value_Pelvis_Rot_Z_SD','Value_Pelvis_Rot_Y_Mean','Value_Pelvis_Rot_Y_SD');
% % 
% %% Adding EMG Signals 
% 
% file_EMGStrait1=strrep(fileStrait1,'MM.mat','EMG.mat');
% file_EMGStrait2=strrep(fileStrait2,'MM.mat','EMG.mat');
% file_EMGStrait3=strrep(fileStrait3,'MM.mat','EMG.mat');
% file_EMGStrait4=strrep(fileStrait4,'MM.mat','EMG.mat');
% file_EMGStrait5=strrep(fileStrait5,'MM.mat','EMG.mat');
% 
% StraitdataEMG1=load(file_EMGStrait1); StraitdataEMG2=load(file_EMGStrait2);StraitdataEMG3=load(file_EMGStrait3);
% StraitdataEMG4=load(file_EMGStrait4);StraitdataEMG5=load(file_EMGStrait5);
% StraitEMGtxt=(StraitdataEMG1.DataEMG(1,:));
% StraitdataEMG1=StraitdataEMG1.DataEMG; StraitdataEMG2=StraitdataEMG2.DataEMG;StraitdataEMG3=StraitdataEMG3.DataEMG;
% StraitdataEMG4=StraitdataEMG4.DataEMG;StraitdataEMG5=StraitdataEMG5.DataEMG;
% 
% StraitdataEMG1=cell2mat(StraitdataEMG1(2:end,:)); StraitdataEMG2=cell2mat(StraitdataEMG2(2:end,:));StraitdataEMG3=cell2mat(StraitdataEMG3(2:end,:));
% StraitdataEMG4=cell2mat(StraitdataEMG4(2:end,:));StraitdataEMG5=cell2mat(StraitdataEMG5(2:end,:));
% %%
% for i=1:n_column
% if ~isempty(strfind(Straittxt{1,i},'Left_Heel_Z'))
%     Left_Heel_Z1=GetHeelStrike(Straitdata1(:,i));
%     Left_Heel_Z2=GetHeelStrike(Straitdata2(:,i));
%     Left_Heel_Z3=GetHeelStrike(Straitdata3(:,i));
%     Left_Heel_Z4=GetHeelStrike(Straitdata4(:,i));
%     Left_Heel_Z5=GetHeelStrike(Straitdata5(:,i));
% elseif ~isempty(strfind(Straittxt{1,i},'Right_Heel_Z'))
%     Right_Heel_Z1=GetHeelStrike(Straitdata1(:,i));
%     Right_Heel_Z2=GetHeelStrike(Straitdata2(:,i));
%     Right_Heel_Z3=GetHeelStrike(Straitdata3(:,i));
%     Right_Heel_Z4=GetHeelStrike(Straitdata4(:,i));
%     Right_Heel_Z5=GetHeelStrike(Straitdata5(:,i));
% end
% end
% 
% StraitdataEMG1=FiltrateEMG(StraitdataEMG1,2,30,450);
% StraitdataEMG2=FiltrateEMG(StraitdataEMG2,2,30,450);
% StraitdataEMG3=FiltrateEMG(StraitdataEMG3,2,30,450);
% StraitdataEMG4=FiltrateEMG(StraitdataEMG4,2,30,450);
% StraitdataEMG5=FiltrateEMG(StraitdataEMG5,2,30,450);
% 
% Strides.Stride1_Left(1).data=StraitdataEMG1((Left_Heel_Z1(1,1)/90)*2250:(Left_Heel_Z1(2,1)/90)*2250,:);
% Strides.Stride1_Left(2).data=StraitdataEMG2((Left_Heel_Z2(1,1)/90)*2250:(Left_Heel_Z2(2,1)/90)*2250,:);
% Strides.Stride1_Left(3).data=StraitdataEMG3((Left_Heel_Z3(1,1)/90)*2250:(Left_Heel_Z3(2,1)/90)*2250,:);
% Strides.Stride1_Left(4).data=StraitdataEMG4((Left_Heel_Z4(1,1)/90)*2250:(Left_Heel_Z4(2,1)/90)*2250,:);
% Strides.Stride1_Left(5).data=StraitdataEMG5((Left_Heel_Z5(1,1)/90)*2250:(Left_Heel_Z5(2,1)/90)*2250,:);
% 
% Strides.Stride1_Right(1).data=StraitdataEMG1((Right_Heel_Z1(1,1)/90)*2250:(Right_Heel_Z1(2,1)/90)*2250,:);
% Strides.Stride1_Right(2).data=StraitdataEMG2((Right_Heel_Z2(1,1)/90)*2250:(Right_Heel_Z2(2,1)/90)*2250,:);
% Strides.Stride1_Right(3).data=StraitdataEMG3((Right_Heel_Z3(1,1)/90)*2250:(Right_Heel_Z3(2,1)/90)*2250,:);
% Strides.Stride1_Right(4).data=StraitdataEMG4((Right_Heel_Z4(1,1)/90)*2250:(Right_Heel_Z4(2,1)/90)*2250,:);
% Strides.Stride1_Right(5).data=StraitdataEMG5((Right_Heel_Z5(1,1)/90)*2250:(Right_Heel_Z5(2,1)/90)*2250,:);
% 
% Strides.Stride2_Left(1).data=StraitdataEMG1((Left_Heel_Z1(2,1)/90)*2250:(Left_Heel_Z1(3,1)/90)*2250,:);
% Strides.Stride2_Left(2).data=StraitdataEMG2((Left_Heel_Z2(2,1)/90)*2250:(Left_Heel_Z2(3,1)/90)*2250,:);
% Strides.Stride2_Left(3).data=StraitdataEMG3((Left_Heel_Z3(2,1)/90)*2250:(Left_Heel_Z3(3,1)/90)*2250,:);
% Strides.Stride2_Left(4).data=StraitdataEMG4((Left_Heel_Z4(2,1)/90)*2250:(Left_Heel_Z4(3,1)/90)*2250,:);
% Strides.Stride2_Left(5).data=StraitdataEMG5((Left_Heel_Z5(2,1)/90)*2250:(Left_Heel_Z5(3,1)/90)*2250,:);
% 
% Strides.Stride2_Right(1).data=StraitdataEMG1((Right_Heel_Z1(2,1)/90)*2250:(Right_Heel_Z1(3,1)/90)*2250,:);
% Strides.Stride2_Right(2).data=StraitdataEMG2((Right_Heel_Z2(2,1)/90)*2250:(Right_Heel_Z2(3,1)/90)*2250,:);
% Strides.Stride2_Right(3).data=StraitdataEMG3((Right_Heel_Z3(2,1)/90)*2250:(Right_Heel_Z3(3,1)/90)*2250,:);
% Strides.Stride2_Right(4).data=StraitdataEMG4((Right_Heel_Z4(2,1)/90)*2250:(Right_Heel_Z4(3,1)/90)*2250,:);
% Strides.Stride2_Right(5).data=StraitdataEMG5((Right_Heel_Z5(2,1)/90)*2250:(Right_Heel_Z5(3,1)/90)*2250,:);
% %%
% Strides.Mean.Left1=zeros(100,1);
% Strides.Mean.Left2=zeros(100,1);
% Strides.Mean.Right1=zeros(100,1);
% Strides.Mean.Right2=zeros(100,1);
% for i=1:5
%     A=(2250/size(Strides.Stride1_Left(i).data,1))*100;
%     B=floor(A);
%     Strides.Stride1_Left(i).abs=abs(Strides.Stride1_Left(i).data);
%     Strides.Stride1_Left(i).envelope=envelope(Strides.Stride1_Left(i).data,200,'rms');
%     Strides.Resample(i).Left1=resample(Strides.Stride1_Left(i).data,B,2250);
%     Strides.Mean.Left1=Strides.Mean.Left1+Strides.Resample(i).Left1; %
%     
%     A=(2250/size(Strides.Stride2_Left(i).data,1))*100;
%     B=floor(A);
%     Strides.Stride2_Left(i).abs=abs(Strides.Stride2_Left(i).data);
%      Strides.Stride2_Left(i).envelope=envelope(Strides.Stride2_Left(i).data,200,'rms');
%     Strides.Resample(i).Left2=resample(Strides.Stride2_Left(i).data,B,2250);
%     Strides.Mean.Left2=Strides.Mean.Left2+Strides.Resample(i).Left2; %
%     
%     A=(2250/size(Strides.Stride1_Right(i).data,1))*100;
%     B=floor(A);
%     Strides.Stride1_Right(i).abs=abs(Strides.Stride1_Right(i).data);
%     Strides.Stride1_Right(i).envelope=envelope(Strides.Stride1_Right(i).data,200,'rms');
%     Strides.Resample(i).Right1=resample(Strides.Stride1_Right(i).data,B,2250);
%     Strides.Mean.Right1=Strides.Mean.Right1+Strides.Resample(i).Right1; %
%     
%     A=(2250/size(Strides.Stride2_Right(i).data,1))*100;
%     B=floor(A);
%     Strides.Stride2_Right(i).abs=abs(Strides.Stride2_Right(i).data);
%     Strides.Stride2_Right(i).envelope=envelope(Strides.Stride2_Right(i).data,200,'rms');
%     Strides.Resample(i).Right2=resample(Strides.Stride2_Right(i).data,B,2250);
%     Strides.Mean.Right2=Strides.Mean.Right2+Strides.Resample(i).Right2; %
% end
%     
% Strides.Mean.Left1=Strides.Mean.Left1./5;
% Strides.Mean.Left2=Strides.Mean.Left2./5;
% Strides.Mean.Right1=Strides.Mean.Right1./5;
% Strides.Mean.Right2=Strides.Mean.Right2./5;
% %%
% FilepathName=fullfile(subject_path,'Stride_NA_Condition');
% save(FilepathName,'Strides')
