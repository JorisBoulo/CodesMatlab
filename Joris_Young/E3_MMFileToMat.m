clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
outputpath=fullfile(subject_path,'2_TransformData');


Condition = {'P28_NA','P28_RA'};%Changer le nom du participant
Rep={'01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15';'16'};
Sensor={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
Error=1; 
A=1;
%%
for i=1:2
    for j=1:16
        Names{i,j}=sprintf('%s%s.xlsx',Condition{i},Rep{j});
        inputfilepath=fullfile(inputpath,Names{i,j});
        if exist(inputfilepath, 'file')==2
        MMData=readcell(inputfilepath);
        name=sprintf('%s%sMM.mat',Condition{i},Rep{j});
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'MMData')
        else 
            Error= Error+1; 
        end 
    end 
end 
            
       