clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
outputpath=fullfile(subject_path,'2_TransformData');

Condition = {'P28_NA','P28_RA'};%Changer le nom du participant
Rep={'01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15';'16';'17';'18';'19';'20'};
Sensor={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
Error=1; 
A=1;
for i=1:2
    for j=1:16
        Names{i,j}=sprintf('%s%s.c3d',Condition{i},Rep{j});
        inputfilepath=fullfile(inputpath,Names{i,j});
        if exist(inputfilepath, 'file')==2
        Fc3d = btkReadAcquisition(Names{i,j});
        Emg=btkGetAnalogs(Fc3d);
        EmgName={'SternoLeft','ErSpRight','ObliqRight','GluMedRight','GluMedLeft','AddRight','AddLeft','GasMedRight','GasMedLeft','TibAntRight','TibAntLeft','AccZLeftTA','AccZLeftTA'};
        for k=1:11
            EMG(k).Sensor = sprintf ('Emg.Sensor_%d_EMG%d',k,k);  
            EMG(k).Data=eval(EMG(k).Sensor);
        end
        HeelAceleration=[Emg.Sensor_10_ACCZ10,Emg.Sensor_11_ACCZ11]; 
        A=EMG(1).Data;B=EMG(2).Data;C=EMG(3).Data;D=EMG(4).Data;E=EMG(5).Data;F=EMG(6).Data;G=EMG(7).Data;H=EMG(8).Data;I=EMG(9).Data;J=EMG(10).Data;K=EMG(11).Data;
        EmgData=[A B C D E F G H I J K HeelAceleration];
        EmgData=num2cell(EmgData);
        DataEMG=[EmgName;EmgData];
        name=sprintf('%s%sEMG.mat',Condition{i},Rep{j});
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'DataEMG')
        else 
            Error= Error+1; 
        end 
    end 
end 
            
       