 load('StraitAllDataLeft.mat')
for k=1:15
figure (k)
subplot(3,4,12)
Mean=mean(AllDataLeft.TibAntL.FullData,2);Std=std(AllDataLeft.TibAntL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std/2)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,11)
Mean=mean(AllDataLeft.TibAntR.FullData,2);Std=std(AllDataLeft.TibAntR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,10)
Mean=mean(AllDataLeft.GasLatL.FullData,2);Std=std(AllDataLeft.GasLatL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,9)
Mean=mean(AllDataLeft.GasLatR.FullData,2);Std=std(AllDataLeft.GasLatR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,8)
Mean=mean(AllDataLeft.AdductL.FullData,2);Std=std(AllDataLeft.AdductL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,7)
Mean=mean(AllDataLeft.AdductR.FullData,2);Std=std(AllDataLeft.AdductR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,6)
Mean=mean(AllDataLeft.GlumedL.FullData,2);Std=std(AllDataLeft.GlumedL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,5)
Mean=mean(AllDataLeft.GlumedR.FullData,2);Std=std(AllDataLeft.GlumedR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,3)
Mean=mean(AllDataLeft.ObliquR.FullData,2);Std=std(AllDataLeft.ObliquR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,2)
Mean=mean(AllDataLeft.ErSpinR.FullData,2);Std=std(AllDataLeft.ErSpinR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,1)
Mean=mean(AllDataLeft.SternoL.FullData,2);Std=std(AllDataLeft.SternoL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
end

PasDeFichier=0;
aRevoir=[];
input_folder='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 2\Participants\P01\3_Output\';
output_folder='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 2\Participants\P01\4_Figures\';
Condi={'RA'};
Rep={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
for i=2
    for j=1:15
        Names{i,j}=fullfile(input_folder,(sprintf('P01_%s%sKinematic.mat',Condi{i},Rep{j})));
%         NamesTraj{i,j}=(sprintf('%s%sTraj.mat',Condi{i},Rep{j}));
        if exist(Names{i,j}, 'file')==2
            hold on
            load (Names{i,j});
            Timeline=(0:1/90:(size(Strides.Indices.Traj,1))/90.025);
            TajV2=resample(Strides.Indices.Traj,Timeline,(1/(Timeline(end,end)/3000))); 
            TajV2=TajV2-TajV2(1,:);
            for k= 1:3
            figure(j)
            subplot(3,4,k) %+1
            
            ttle=cell2char(Strides.Indices.NameEMG(1,k));
            title(ttle)
%             plot(Strides.LOLI.P.Brust(:,k),'k')
            plot(Strides.Left.P.NormAmp(:,k),'r','LineWidth',2)
            plot(Strides.Left.P.Brust(:,k),'k','LineWidth',2)
            yyaxis left
            plot(TajV2(:,1),'g')
            plot(ones(3001,1)*1000,(0:01:3000),':k')
            plot(ones(3001,1)*2000,(0:01:3000),':k')
                try
                plot(ones(3001,1)*Strides.Indices.CrossingReample(1,1),(0:01:3000),'y')
                catch
                    b='prout';
                end
            xlim([50,2980])
            ylim([0,2])
            end
            fig = gcf;
            fig.Position=[1,1,1440,800];
            legend ('Moyenne Ligne Droite','ET','Contournement','CDM ML','Contact talon G','Contact talon G','Croisement de l Agent')
            Lgnd = legend('show');
            Lgnd.Position(1) = 0.765843252609705;
            Lgnd.Position(2) = 0.745977443437847;
            for k= 4:11
            figure(j)
            subplot(3,4,k+1) %+1
            hold on
            ttle=cell2char(Strides.Indices.NameEMG(1,k));
            title(ttle)
%             plot(Strides.LOLI.P.Brust(:,k),'k')
            plot(Strides.Left.P.NormAmp(:,k),'r','LineWidth',1)
            plot(Strides.Left.P.Brust(:,k),'k','LineWidth',2)
            yyaxis left
            plot(TajV2(:,1),'g')
            plot(ones(3001,1)*1000,(0:01:3000),':k')
            plot(ones(3001,1)*2000,(0:01:3000),':k')
                try
                plot(ones(3001,1)*Strides.Indices.CrossingReample(1,1),(0:01:3000),'y')
                catch
                    b='prout';
                end
                        xlim([50,2980])
            ylim([0,2])
            end
            % Question 
            answer = questdlg('Do you want to save the figure ?', ' Save the figure', 'Yes', 'No', 'Yes');
            switch answer 
                case 'Yes'
                    name=strcat(erase(Names{i,j},'Strides.mat'),'AllEMG.png');
                    FilepathName=fullfile(output_folder,FilepathName);
                    saveas(figure(fig),name)
                    close (fig)
                case 'No'
                    aRevoir=[aRevoir,j];
                    close (fig)
            end
            
       else
            PasDeFichier=PasDeFichier+1;
        end
    end
    
end

