% This script allows you to find the sqare root of muscle
% activity during the catch trials 
%% *Open files*
% Choose Participant 
clear all 
close all
subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
input_folder=fullfile(subject_path,'2_TransformData\');
output_folder=fullfile(subject_path,'3_Output\');
if (isequal(input_folder, 0))
    return;
end
if (isequal(output_folder, 0))
    return;
end
%%
% Choose a EMG File
Condi={'P28_NA','RA','NR'}; % CHANGER LE NOM 
Rep={'01','02','03','04','05','06','07','8','9','10'};
MaxTot=zeros(1,15);
A=1;
EmgCompilLeft=zeros(1000,11);
EmgCompilLeftFull=zeros(3000,11);% PASNorm; % trouver une solution pour normaliser les sternocleidomastoidiens 
AllDataLeft.SternoL.Data=[];
AllDataLeft.ErSpinR.Data=[];
AllDataLeft.ObliquR.Data=[];
AllDataLeft.GlumedR.Data=[];
AllDataLeft.GlumedL.Data=[];
AllDataLeft.AdductR.Data=[];
AllDataLeft.AdductL.Data=[];
AllDataLeft.GasLatR.Data=[];
AllDataLeft.GasLatL.Data=[];
AllDataLeft.TibAntR.Data=[];
AllDataLeft.TibAntL.Data=[];
AllDataRight.SternoL.Data=[];
AllDataRight.ErSpinR.Data=[];
AllDataRight.ObliquR.Data=[];
AllDataRight.GlumedR.Data=[];
AllDataRight.GlumedL.Data=[];
AllDataRight.AdductR.Data=[];
AllDataRight.AdductL.Data=[];
AllDataRight.GasLatR.Data=[];
AllDataRight.GasLatL.Data=[];
AllDataRight.TibAntR.Data=[];
AllDataRight.TibAntL.Data=[];

AllDataLeft.SternoL.Brust=[];
AllDataLeft.ErSpinR.Brust=[];
AllDataLeft.ObliquR.Brust=[];
AllDataLeft.GlumedR.Brust=[];
AllDataLeft.GlumedL.Brust=[];
AllDataLeft.AdductR.Brust=[];
AllDataLeft.AdductL.Brust=[];
AllDataLeft.GasLatR.Brust=[];
AllDataLeft.GasLatL.Brust=[];
AllDataLeft.TibAntR.Brust=[];
AllDataLeft.TibAntL.Brust=[];
AllDataRight.SternoL.Brust=[];
AllDataRight.ErSpinR.Brust=[];
AllDataRight.ObliquR.Brust=[];
AllDataRight.GlumedR.Brust=[];
AllDataRight.GlumedL.Brust=[];
AllDataRight.AdductR.Brust=[];
AllDataRight.AdductL.Brust=[];
AllDataRight.GasLatR.Brust=[];
AllDataRight.GasLatL.Brust=[];
AllDataRight.TibAntR.Brust=[];
AllDataRight.TibAntL.Brust=[];

%

AllDataLeft.SternoL.FullData=[];
AllDataLeft.ErSpinR.FullData=[];
AllDataLeft.ObliquR.FullData=[];
AllDataLeft.GlumedR.FullData=[];
AllDataLeft.GlumedL.FullData=[];
AllDataLeft.AdductR.FullData=[];
AllDataLeft.AdductL.FullData=[];
AllDataLeft.GasLatR.FullData=[];
AllDataLeft.GasLatL.FullData=[];
AllDataLeft.TibAntR.FullData=[];
AllDataLeft.TibAntL.FullData=[];
AllDataRight.SternoL.FullData=[];
AllDataRight.ErSpinR.FullData=[];
AllDataRight.ObliquR.FullData=[];
AllDataRight.GlumedR.FullData=[];
AllDataRight.GlumedL.FullData=[];
AllDataRight.AdductR.FullData=[];
AllDataRight.AdductL.FullData=[];
AllDataRight.GasLatR.FullData=[];
AllDataRight.GasLatL.FullData=[];
AllDataRight.TibAntR.FullData=[];
AllDataRight.TibAntL.FullData=[];
AllDataLeft.SternoL.FullBrust=[];
AllDataLeft.ErSpinR.FullBrust=[];
AllDataLeft.ObliquR.FullBrust=[];
AllDataLeft.GlumedR.FullBrust=[];
AllDataLeft.GlumedL.FullBrust=[];
AllDataLeft.AdductR.FullBrust=[];
AllDataLeft.AdductL.FullBrust=[];
AllDataLeft.GasLatR.FullBrust=[];
AllDataLeft.GasLatL.FullBrust=[];
AllDataLeft.TibAntR.FullBrust=[];
AllDataLeft.TibAntL.FullBrust=[];
AllDataRight.SternoL.FullBrust=[];
AllDataRight.ErSpinR.FullBrust=[];
AllDataRight.ObliquR.FullBrust=[];
AllDataRight.GlumedR.FullBrust=[];
AllDataRight.GlumedL.FullBrust=[];
AllDataRight.AdductR.FullBrust=[];
AllDataRight.AdductL.FullBrust=[];
AllDataRight.GasLatR.FullBrust=[];
AllDataRight.GasLatL.FullBrust=[];
AllDataRight.TibAntR.FullBrust=[];
AllDataRight.TibAntL.FullBrust=[];
%%Brust
AllDataLeft.SternoL.P_2Brust=[];
AllDataLeft.ErSpinR.P_2Brust=[];
AllDataLeft.ObliquR.P_2Brust=[];
AllDataLeft.GlumedR.P_2Brust=[];
AllDataLeft.GlumedL.P_2Brust=[];
AllDataLeft.AdductR.P_2Brust=[];
AllDataLeft.AdductL.P_2Brust=[];
AllDataLeft.GasLatR.P_2Brust=[];
AllDataLeft.GasLatL.P_2Brust=[];
AllDataLeft.TibAntR.P_2Brust=[];
AllDataLeft.TibAntL.P_2Brust=[];
AllDataRight.SternoL.P_2Brust=[];
AllDataRight.ErSpinR.P_2Brust=[];
AllDataRight.ObliquR.P_2Brust=[];
AllDataRight.GlumedR.P_2Brust=[];
AllDataRight.GlumedL.P_2Brust=[];
AllDataRight.AdductR.P_2Brust=[];
AllDataRight.AdductL.P_2Brust=[];
AllDataRight.GasLatR.P_2Brust=[];
AllDataRight.GasLatL.P_2Brust=[];
AllDataRight.TibAntR.P_2Brust=[];
AllDataRight.TibAntL.P_2Brust=[];

AllDataLeft.SternoL.P_1Brust=[];
AllDataLeft.ErSpinR.P_1Brust=[];
AllDataLeft.ObliquR.P_1Brust=[];
AllDataLeft.GlumedR.P_1Brust=[];
AllDataLeft.GlumedL.P_1Brust=[];
AllDataLeft.AdductR.P_1Brust=[];
AllDataLeft.AdductL.P_1Brust=[];
AllDataLeft.GasLatR.P_1Brust=[];
AllDataLeft.GasLatL.P_1Brust=[];
AllDataLeft.TibAntR.P_1Brust=[];
AllDataLeft.TibAntL.P_1Brust=[];
AllDataRight.SternoL.P_1Brust=[];
AllDataRight.ErSpinR.P_1Brust=[];
AllDataRight.ObliquR.P_1Brust=[];
AllDataRight.GlumedR.P_1Brust=[];
AllDataRight.GlumedL.P_1Brust=[];
AllDataRight.AdductR.P_1Brust=[];
AllDataRight.AdductL.P_1Brust=[];
AllDataRight.GasLatR.P_1Brust=[];
AllDataRight.GasLatL.P_1Brust=[];
AllDataRight.TibAntR.P_1Brust=[];
AllDataRight.TibAntL.P_1Brust=[];

AllDataLeft.SternoL.P0Brust=[];
AllDataLeft.ErSpinR.P0Brust=[];
AllDataLeft.ObliquR.P0Brust=[];
AllDataLeft.GlumedR.P0Brust=[];
AllDataLeft.GlumedL.P0Brust=[];
AllDataLeft.AdductR.P0Brust=[];
AllDataLeft.AdductL.P0Brust=[];
AllDataLeft.GasLatR.P0Brust=[];
AllDataLeft.GasLatL.P0Brust=[];
AllDataLeft.TibAntR.P0Brust=[];
AllDataLeft.TibAntL.P0Brust=[];
AllDataRight.SternoL.P0Brust=[];
AllDataRight.ErSpinR.P0Brust=[];
AllDataRight.ObliquR.P0Brust=[];
AllDataRight.GlumedR.P0Brust=[];
AllDataRight.GlumedL.P0Brust=[];
AllDataRight.AdductR.P0Brust=[];
AllDataRight.AdductL.P0Brust=[];
AllDataRight.GasLatR.P0Brust=[];
AllDataRight.GasLatL.P0Brust=[];
AllDataRight.TibAntR.P0Brust=[];
AllDataRight.TibAntL.P0Brust=[];
NameDataRest=fullfile(output_folder,'MeanAndSdRest.mat');
load (NameDataRest);
Threshold=((Extract(1,1:11))+(2*(Extract(2,1:11))));
minFrameUnnormalized = 112.5;

%%
for i=1
    for j=[1,2,3,4,5]
        NamesEMG{i,j}=fullfile(input_folder,(sprintf('%s%sEMG.mat',Condi{i},Rep{j})));
        if exist(NamesEMG{i,j}, 'file')==2
            load (NamesEMG{i,j});
            emgnames=(DataEMG(1,:));
            RAWemg=cell2mat(DataEMG(2:end,:));
            RAWemg=FiltrateEMG(RAWemg,4,20,450);
            Extract=[];
            for k=1:size(RAWemg,2)
             rawemg=RAWemg(:,k)-mean(RAWemg(:,k));
             EmgRect = abs(rawemg(:,1));
             EmgSmooth=movmean(EmgRect,100);
             Max=max(EmgSmooth);
             if MaxTot(1,k)<Max
                 MaxTot(1,k)=Max;
             end
             end
            % En d�coupant les cycles 
            file_MM=strrep(NamesEMG{i,j},'EMG.mat','MM.mat');
            [Head_position,Heels_position,Body_position,Body_Vel,Probed_point_contact,title_probed_point_contact,...
            Reorientation_var,title_reorientation_var]...
            = GetMotionMonitorVariables(file_MM);
            [~,HeelLFrames]=findpeaks(cell2mat(DataEMG(2:end,end)),'MinPeakHeight',-3,'MinPeakDistance',1700);
            [~,HeelRFrames]=findpeaks(cell2mat(DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);
%             HeelLFrames=HeelLFrames+(2000*ones(size(HeelLFrames,2),1));
%             HeelRFrames=HeelLFrames+(2000*ones(size(HeelLFrames,2),1));
            EmgCompilSmooth=[];
            EMGRectif=abs(RAWemg-mean(RAWemg,1));
            EMGSmooth=movmean(EMGRectif,100);
            %% Plot heelstrike to know if the detection is correct 
            figure
            findpeaks(cell2mat(DataEMG(2:end,end)),'MinPeakHeight',-3,'MinPeakDistance',1700);
            hold on 
            findpeaks(cell2mat(DataEMG(2:end,12)),'MinPeakHeight',-3,'MinPeakDistance',1700);
            fig = gcf;
            fig.Position=[1,1,1440,500];
            % Question 
                        answer = questdlg('Do you want to continu ?', 'Continu the script', 'Yes', 'Find For left(BLue)','Find For Right(Orange)','Yes');
                        switch answer 
                            case 'Yes'
                                close (fig)
                            case 'Find For left(BLue)'
                                [x,y]=ginput(2);
                                [value,Row]=max(cell2mat(DataEMG(round(x(1,1)):round(x(2,1)),end)));
                                HeelLFrames=[HeelLFrames;round(x(1,1))+Row];
                                HeelLFrames=sort(HeelLFrames);
                                close (fig)
                            case 'Find For Right(Orange)'
                                [x,y]=ginput(2);
                                [Row,value]=max(cell2mat(DataEMG(round(x(1,1)):round(x(2,1)),12)));
                                HeelRFrames=[HeelRFrames;round(x(1,1))+Row];
                                HeelRFrames=sort(HeelRFrames);
                                close (fig)
                           
                        end
                
                            %%____________________________________________________________________________________________
             for l=1:3% En d�coupant les cycles 
                 %Left
                PASLeft=EMGSmooth(HeelLFrames(l,1):HeelLFrames(l+1,1),:);
                Timeline=(1:1:size(PASLeft,1));
                PASNormLeft=resample(PASLeft(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                
                AllDataLeft.SternoL.Data=[AllDataLeft.SternoL.Data,PASNormLeft(:,1)];
                AllDataLeft.ErSpinR.Data=[AllDataLeft.ErSpinR.Data,PASNormLeft(:,2)];
                AllDataLeft.ObliquR.Data=[AllDataLeft.ObliquR.Data,PASNormLeft(:,3)];
                AllDataLeft.GlumedR.Data=[AllDataLeft.GlumedR.Data,PASNormLeft(:,4)];
                AllDataLeft.GlumedL.Data=[AllDataLeft.GlumedL.Data,PASNormLeft(:,5)];
                AllDataLeft.AdductR.Data=[AllDataLeft.AdductR.Data,PASNormLeft(:,6)];
                AllDataLeft.AdductL.Data=[AllDataLeft.AdductL.Data,PASNormLeft(:,7)];
                AllDataLeft.GasLatR.Data=[AllDataLeft.GasLatR.Data,PASNormLeft(:,8)];
                AllDataLeft.GasLatL.Data=[AllDataLeft.GasLatL.Data,PASNormLeft(:,9)];
                AllDataLeft.TibAntR.Data=[AllDataLeft.TibAntR.Data,PASNormLeft(:,10)];
                AllDataLeft.TibAntL.Data=[AllDataLeft.TibAntL.Data,PASNormLeft(:,11)];
                
                % Compil the data to reduce peaks 
                EmgCompilLeft=EmgCompilLeft+PASNormLeft;
                                
                %Right 
                PASRight=EMGSmooth(HeelRFrames(l,1):HeelRFrames(l+1,1),:);
                Timeline=(1:1:size(PASRight,1));
                PASNormRight=resample(PASRight(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                
                AllDataRight.SternoL.Data=[AllDataRight.SternoL.Data,PASNormRight(:,1)];
                AllDataRight.ErSpinR.Data=[AllDataRight.ErSpinR.Data,PASNormRight(:,2)];
                AllDataRight.ObliquR.Data=[AllDataRight.ObliquR.Data,PASNormRight(:,3)];
                AllDataRight.GlumedR.Data=[AllDataRight.GlumedR.Data,PASNormRight(:,4)];
                AllDataRight.GlumedL.Data=[AllDataRight.GlumedL.Data,PASNormRight(:,5)];
                AllDataRight.AdductR.Data=[AllDataRight.AdductR.Data,PASNormRight(:,6)];
                AllDataRight.AdductL.Data=[AllDataRight.AdductL.Data,PASNormRight(:,7)];
                AllDataRight.GasLatR.Data=[AllDataRight.GasLatR.Data,PASNormRight(:,8)];
                AllDataRight.GasLatL.Data=[AllDataRight.GasLatL.Data,PASNormRight(:,9)];
                AllDataRight.TibAntR.Data=[AllDataRight.TibAntR.Data,PASNormRight(:,10)];
                AllDataRight.TibAntL.Data=[AllDataRight.TibAntL.Data,PASNormRight(:,11)];
                
                Traj=[Body_position];
                %%% _____________________________________________
                %% Brust
      
               
                AllDataLeft.SternoL.Brust=[AllDataLeft.SternoL.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,1),Threshold(:,1),(1:1:1000)')];
                AllDataLeft.ErSpinR.Brust=[AllDataLeft.ErSpinR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,2),Threshold(:,2),(1:1:1000)')];
                AllDataLeft.ObliquR.Brust=[AllDataLeft.ObliquR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,3),Threshold(:,3),(1:1:1000)')];
                AllDataLeft.GlumedR.Brust=[AllDataLeft.GlumedR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,4),Threshold(:,4),(1:1:1000)')];
                AllDataLeft.GlumedL.Brust=[AllDataLeft.GlumedL.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,5),Threshold(:,5),(1:1:1000)')];
                AllDataLeft.AdductR.Brust=[AllDataLeft.AdductR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,6),Threshold(:,6),(1:1:1000)')];
                AllDataLeft.AdductL.Brust=[AllDataLeft.AdductL.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,7),Threshold(:,7),(1:1:1000)')];
                AllDataLeft.GasLatR.Brust=[AllDataLeft.GasLatR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,8),Threshold(:,8),(1:1:1000)')];
                AllDataLeft.GasLatL.Brust=[AllDataLeft.GasLatL.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,9),Threshold(:,9),(1:1:1000)')];
                AllDataLeft.TibAntR.Brust=[AllDataLeft.TibAntR.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,10),Threshold(:,10),(1:1:1000)')];
                AllDataLeft.TibAntL.Brust=[AllDataLeft.TibAntL.Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,11),Threshold(:,11),(1:1:1000)')];

                AllDataRight.SternoL.Brust=[AllDataRight.SternoL.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,1),Threshold(:,1),(1:1:1000)')];
                AllDataRight.ErSpinR.Brust=[AllDataRight.ErSpinR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,2),Threshold(:,2),(1:1:1000)')];
                AllDataRight.ObliquR.Brust=[AllDataRight.ObliquR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,3),Threshold(:,3),(1:1:1000)')];
                AllDataRight.GlumedR.Brust=[AllDataRight.GlumedR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,4),Threshold(:,4),(1:1:1000)')];
                AllDataRight.GlumedL.Brust=[AllDataRight.GlumedL.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,5),Threshold(:,5),(1:1:1000)')];
                AllDataRight.AdductR.Brust=[AllDataRight.AdductR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,6),Threshold(:,6),(1:1:1000)')];
                AllDataRight.AdductL.Brust=[AllDataRight.AdductL.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,7),Threshold(:,7),(1:1:1000)')];
                AllDataRight.GasLatR.Brust=[AllDataRight.GasLatR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,8),Threshold(:,8),(1:1:1000)')];
                AllDataRight.GasLatL.Brust=[AllDataRight.GasLatL.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,9),Threshold(:,9),(1:1:1000)')];
                AllDataRight.TibAntR.Brust=[AllDataRight.TibAntR.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,10),Threshold(:,10),(1:1:1000)')];
                AllDataRight.TibAntL.Brust=[AllDataRight.TibAntL.Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,11),Threshold(:,11),(1:1:1000)')];
                
             end
%_______________________________________________________________________________________________________________________________________________________________________________________________________
                % En ne d�coupant pas les cycles PASNormRight
                 %Left
                 FullresampleL=[];
                 for k=1:3
                PASLeft=EMGSmooth(HeelLFrames(k,1):HeelLFrames(k+1,1),:);
                Timeline=(1:1:size(PASLeft,1));
                PASNormLeft=resample(PASLeft(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                FullresampleL=[FullresampleL;PASNormLeft];
                 if k==1
                    AllDataLeft.SternoL.P_2Brust=[AllDataLeft.SternoL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,1),Threshold(:,1),(1:1:1000)')];
                    AllDataLeft.ErSpinR.P_2Brust=[AllDataLeft.ErSpinR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,2),Threshold(:,2),(1:1:1000)')];
                    AllDataLeft.ObliquR.P_2Brust=[AllDataLeft.ObliquR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,3),Threshold(:,3),(1:1:1000)')];
                    AllDataLeft.GlumedR.P_2Brust=[AllDataLeft.GlumedR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,4),Threshold(:,4),(1:1:1000)')];
                    AllDataLeft.GlumedL.P_2Brust=[AllDataLeft.GlumedL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,5),Threshold(:,5),(1:1:1000)')];
                    AllDataLeft.AdductR.P_2Brust=[AllDataLeft.AdductR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,6),Threshold(:,6),(1:1:1000)')];
                    AllDataLeft.AdductL.P_2Brust=[AllDataLeft.AdductL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,7),Threshold(:,7),(1:1:1000)')];
                    AllDataLeft.GasLatR.P_2Brust=[AllDataLeft.GasLatR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,8),Threshold(:,8),(1:1:1000)')];
                    AllDataLeft.GasLatL.P_2Brust=[AllDataLeft.GasLatL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,9),Threshold(:,9),(1:1:1000)')];
                    AllDataLeft.TibAntR.P_2Brust=[AllDataLeft.TibAntR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,10),Threshold(:,10),(1:1:1000)')];
                    AllDataLeft.TibAntL.P_2Brust=[AllDataLeft.TibAntL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,11),Threshold(:,11),(1:1:1000)')];                
                elseif k==2
                    AllDataLeft.SternoL.P_1Brust=[AllDataLeft.SternoL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,1),Threshold(:,1),(1001:1:2000)')];
                    AllDataLeft.ErSpinR.P_1Brust=[AllDataLeft.ErSpinR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,2),Threshold(:,2),(1001:1:2000)')];
                    AllDataLeft.ObliquR.P_1Brust=[AllDataLeft.ObliquR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,3),Threshold(:,3),(1001:1:2000)')];
                    AllDataLeft.GlumedR.P_1Brust=[AllDataLeft.GlumedR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,4),Threshold(:,4),(1001:1:2000)')];
                    AllDataLeft.GlumedL.P_1Brust=[AllDataLeft.GlumedL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,5),Threshold(:,5),(1001:1:2000)')];
                    AllDataLeft.AdductR.P_1Brust=[AllDataLeft.AdductR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,6),Threshold(:,6),(1001:1:2000)')];
                    AllDataLeft.AdductL.P_1Brust=[AllDataLeft.AdductL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,7),Threshold(:,7),(1001:1:2000)')];
                    AllDataLeft.GasLatR.P_1Brust=[AllDataLeft.GasLatR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,8),Threshold(:,8),(1001:1:2000)')];
                    AllDataLeft.GasLatL.P_1Brust=[AllDataLeft.GasLatL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,9),Threshold(:,9),(1001:1:2000)')];
                    AllDataLeft.TibAntR.P_1Brust=[AllDataLeft.TibAntR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,10),Threshold(:,10),(1001:1:2000)')];
                    AllDataLeft.TibAntL.P_1Brust=[AllDataLeft.TibAntL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,11),Threshold(:,11),(1001:1:2000)')];                
                elseif k==3
                    AllDataLeft.SternoL.P0Brust=[AllDataLeft.SternoL.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,1),Threshold(:,1),(2001:1:3000)')];
                    AllDataLeft.ErSpinR.P0Brust=[AllDataLeft.ErSpinR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,2),Threshold(:,2),(2001:1:3000)')];
                    AllDataLeft.ObliquR.P0Brust=[AllDataLeft.ObliquR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,3),Threshold(:,3),(2001:1:3000)')];
                    AllDataLeft.GlumedR.P0Brust=[AllDataLeft.GlumedR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,4),Threshold(:,4),(2001:1:3000)')];
                    AllDataLeft.GlumedL.P0Brust=[AllDataLeft.GlumedL.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,5),Threshold(:,5),(2001:1:3000)')];
                    AllDataLeft.AdductR.P0Brust=[AllDataLeft.AdductR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,6),Threshold(:,6),(2001:1:3000)')];
                    AllDataLeft.AdductL.P0Brust=[AllDataLeft.AdductL.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,7),Threshold(:,7),(2001:1:3000)')];
                    AllDataLeft.GasLatR.P0Brust=[AllDataLeft.GasLatR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,8),Threshold(:,8),(2001:1:3000)')];
                    AllDataLeft.GasLatL.P0Brust=[AllDataLeft.GasLatL.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,9),Threshold(:,9),(2001:1:3000)')];
                    AllDataLeft.TibAntR.P0Brust=[AllDataLeft.TibAntR.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,10),Threshold(:,10),(2001:1:3000)')];
                    AllDataLeft.TibAntL.P0Brust=[AllDataLeft.TibAntL.P0Brust,FindBrust(minFrameUnnormalized,PASNormLeft(:,11),Threshold(:,11),(2001:1:3000)')];                
                end
                end
                EmgCompilLeftFull=EmgCompilLeftFull+FullresampleL;

                AllDataLeft.SternoL.FullData=[AllDataLeft.SternoL.FullData,FullresampleL(:,1)];
                AllDataLeft.ErSpinR.FullData=[AllDataLeft.ErSpinR.FullData,FullresampleL(:,2)];
                AllDataLeft.ObliquR.FullData=[AllDataLeft.ObliquR.FullData,FullresampleL(:,3)];
                AllDataLeft.GlumedR.FullData=[AllDataLeft.GlumedR.FullData,FullresampleL(:,4)];
                AllDataLeft.GlumedL.FullData=[AllDataLeft.GlumedL.FullData,FullresampleL(:,5)];
                AllDataLeft.AdductR.FullData=[AllDataLeft.AdductR.FullData,FullresampleL(:,6)];
                AllDataLeft.AdductL.FullData=[AllDataLeft.AdductL.FullData,FullresampleL(:,7)];
                AllDataLeft.GasLatR.FullData=[AllDataLeft.GasLatR.FullData,FullresampleL(:,8)];
                AllDataLeft.GasLatL.FullData=[AllDataLeft.GasLatL.FullData,FullresampleL(:,9)];
                AllDataLeft.TibAntR.FullData=[AllDataLeft.TibAntR.FullData,FullresampleL(:,10)];
                AllDataLeft.TibAntL.FullData=[AllDataLeft.TibAntL.FullData,FullresampleL(:,11)];
                                
                %Right 
                FullresampleR=[];
                for k=1:3
                PASRight=EMGSmooth(HeelRFrames(k,1):HeelRFrames(k+1,1),:);
                Timeline=(1:1:size(PASRight,1));
                PASNormRight=resample(PASRight(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                FullresampleR=[FullresampleR;PASNormRight];
                 if k==1
                    AllDataRight.SternoL.P_2Brust=[AllDataRight.SternoL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,1),Threshold(:,1),(1:1:1000)')];
                    AllDataRight.ErSpinR.P_2Brust=[AllDataRight.ErSpinR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,2),Threshold(:,2),(1:1:1000)')];
                    AllDataRight.ObliquR.P_2Brust=[AllDataRight.ObliquR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,3),Threshold(:,3),(1:1:1000)')];
                    AllDataRight.GlumedR.P_2Brust=[AllDataRight.GlumedR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,4),Threshold(:,4),(1:1:1000)')];
                    AllDataRight.GlumedL.P_2Brust=[AllDataRight.GlumedL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,5),Threshold(:,5),(1:1:1000)')];
                    AllDataRight.AdductR.P_2Brust=[AllDataRight.AdductR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,6),Threshold(:,6),(1:1:1000)')];
                    AllDataRight.AdductL.P_2Brust=[AllDataRight.AdductL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,7),Threshold(:,7),(1:1:1000)')];
                    AllDataRight.GasLatR.P_2Brust=[AllDataRight.GasLatR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,8),Threshold(:,8),(1:1:1000)')];
                    AllDataRight.GasLatL.P_2Brust=[AllDataRight.GasLatL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,9),Threshold(:,9),(1:1:1000)')];
                    AllDataRight.TibAntR.P_2Brust=[AllDataRight.TibAntR.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,10),Threshold(:,10),(1:1:1000)')];
                    AllDataRight.TibAntL.P_2Brust=[AllDataRight.TibAntL.P_2Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,11),Threshold(:,11),(1:1:1000)')];                
                elseif k==2
                    AllDataRight.SternoL.P_1Brust=[AllDataRight.SternoL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,1),Threshold(:,1),(1001:1:2000)')];
                    AllDataRight.ErSpinR.P_1Brust=[AllDataRight.ErSpinR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,2),Threshold(:,2),(1001:1:2000)')];
                    AllDataRight.ObliquR.P_1Brust=[AllDataRight.ObliquR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,3),Threshold(:,3),(1001:1:2000)')];
                    AllDataRight.GlumedR.P_1Brust=[AllDataRight.GlumedR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,4),Threshold(:,4),(1001:1:2000)')];
                    AllDataRight.GlumedL.P_1Brust=[AllDataRight.GlumedL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,5),Threshold(:,5),(1001:1:2000)')];
                    AllDataRight.AdductR.P_1Brust=[AllDataRight.AdductR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,6),Threshold(:,6),(1001:1:2000)')];
                    AllDataRight.AdductL.P_1Brust=[AllDataRight.AdductL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,7),Threshold(:,7),(1001:1:2000)')];
                    AllDataRight.GasLatR.P_1Brust=[AllDataRight.GasLatR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,8),Threshold(:,8),(1001:1:2000)')];
                    AllDataRight.GasLatL.P_1Brust=[AllDataRight.GasLatL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,9),Threshold(:,9),(1001:1:2000)')];
                    AllDataRight.TibAntR.P_1Brust=[AllDataRight.TibAntR.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,10),Threshold(:,10),(1001:1:2000)')];
                    AllDataRight.TibAntL.P_1Brust=[AllDataRight.TibAntL.P_1Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,11),Threshold(:,11),(1001:1:2000)')];                
                elseif k==3
                    AllDataRight.SternoL.P0Brust=[AllDataRight.SternoL.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,1),Threshold(:,1),(2001:1:3000)')];
                    AllDataRight.ErSpinR.P0Brust=[AllDataRight.ErSpinR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,2),Threshold(:,2),(2001:1:3000)')];
                    AllDataRight.ObliquR.P0Brust=[AllDataRight.ObliquR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,3),Threshold(:,3),(2001:1:3000)')];
                    AllDataRight.GlumedR.P0Brust=[AllDataRight.GlumedR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,4),Threshold(:,4),(2001:1:3000)')];
                    AllDataRight.GlumedL.P0Brust=[AllDataRight.GlumedL.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,5),Threshold(:,5),(2001:1:3000)')];
                    AllDataRight.AdductR.P0Brust=[AllDataRight.AdductR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,6),Threshold(:,6),(2001:1:3000)')];
                    AllDataRight.AdductL.P0Brust=[AllDataRight.AdductL.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,7),Threshold(:,7),(2001:1:3000)')];
                    AllDataRight.GasLatR.P0Brust=[AllDataRight.GasLatR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,8),Threshold(:,8),(2001:1:3000)')];
                    AllDataRight.GasLatL.P0Brust=[AllDataRight.GasLatL.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,9),Threshold(:,9),(2001:1:3000)')];
                    AllDataRight.TibAntR.P0Brust=[AllDataRight.TibAntR.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,10),Threshold(:,10),(2001:1:3000)')];
                    AllDataRight.TibAntL.P0Brust=[AllDataRight.TibAntL.P0Brust,FindBrust(minFrameUnnormalized,PASNormRight(:,11),Threshold(:,11),(2001:1:3000)')];                
                end
                end
                AllDataRight.SternoL.FullData=[AllDataRight.SternoL.FullData,FullresampleR(:,1)];
                AllDataRight.ErSpinR.FullData=[AllDataRight.ErSpinR.FullData,FullresampleR(:,2)];
                AllDataRight.ObliquR.FullData=[AllDataRight.ObliquR.FullData,FullresampleR(:,3)];
                AllDataRight.GlumedR.FullData=[AllDataRight.GlumedR.FullData,FullresampleR(:,4)];
                AllDataRight.GlumedL.FullData=[AllDataRight.GlumedL.FullData,FullresampleR(:,5)];
                AllDataRight.AdductR.FullData=[AllDataRight.AdductR.FullData,FullresampleR(:,6)];
                AllDataRight.AdductL.FullData=[AllDataRight.AdductL.FullData,FullresampleR(:,7)];
                AllDataRight.GasLatR.FullData=[AllDataRight.GasLatR.FullData,FullresampleR(:,8)];
                AllDataRight.GasLatL.FullData=[AllDataRight.GasLatL.FullData,FullresampleR(:,9)];
                AllDataRight.TibAntR.FullData=[AllDataRight.TibAntR.FullData,FullresampleR(:,10)];
                AllDataRight.TibAntL.FullData=[AllDataRight.TibAntL.FullData,FullresampleR(:,11)];
             %% FULL
                AllDataLeft.SternoL.FullBrust=[AllDataLeft.SternoL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,1),Threshold(:,1),(1:1:3000)')];
                AllDataLeft.ErSpinR.FullBrust=[AllDataLeft.ErSpinR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,2),Threshold(:,2),(1:1:3000)')];
                AllDataLeft.ObliquR.FullBrust=[AllDataLeft.ObliquR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,3),Threshold(:,3),(1:1:3000)')];
                AllDataLeft.GlumedR.FullBrust=[AllDataLeft.GlumedR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,4),Threshold(:,4),(1:1:3000)')];
                AllDataLeft.GlumedL.FullBrust=[AllDataLeft.GlumedL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,5),Threshold(:,5),(1:1:3000)')];
                AllDataLeft.AdductR.FullBrust=[AllDataLeft.AdductR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,6),Threshold(:,6),(1:1:3000)')];
                AllDataLeft.AdductL.FullBrust=[AllDataLeft.AdductL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,7),Threshold(:,7),(1:1:3000)')];
                AllDataLeft.GasLatR.FullBrust=[AllDataLeft.GasLatR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,8),Threshold(:,8),(1:1:3000)')];
                AllDataLeft.GasLatL.FullBrust=[AllDataLeft.GasLatL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,9),Threshold(:,9),(1:1:3000)')];
                AllDataLeft.TibAntR.FullBrust=[AllDataLeft.TibAntR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,10),Threshold(:,10),(1:1:3000)')];
                AllDataLeft.TibAntL.FullBrust=[AllDataLeft.TibAntL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleL(:,11),Threshold(:,11),(1:1:3000)')];

                AllDataRight.SternoL.FullBrust=[AllDataRight.SternoL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,1),Threshold(:,1),(1:1:3000)')];
                AllDataRight.ErSpinR.FullBrust=[AllDataRight.ErSpinR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,2),Threshold(:,2),(1:1:3000)')];
                AllDataRight.ObliquR.FullBrust=[AllDataRight.ObliquR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,3),Threshold(:,3),(1:1:3000)')];
                AllDataRight.GlumedR.FullBrust=[AllDataRight.GlumedR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,4),Threshold(:,4),(1:1:3000)')];
                AllDataRight.GlumedL.FullBrust=[AllDataRight.GlumedL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,5),Threshold(:,5),(1:1:3000)')];
                AllDataRight.AdductR.FullBrust=[AllDataRight.AdductR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,6),Threshold(:,6),(1:1:3000)')];
                AllDataRight.AdductL.FullBrust=[AllDataRight.AdductL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,7),Threshold(:,7),(1:1:3000)')];
                AllDataRight.GasLatR.FullBrust=[AllDataRight.GasLatR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,8),Threshold(:,8),(1:1:3000)')];
                AllDataRight.GasLatL.FullBrust=[AllDataRight.GasLatL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,9),Threshold(:,9),(1:1:3000)')];
                AllDataRight.TibAntR.FullBrust=[AllDataRight.TibAntR.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,10),Threshold(:,10),(1:1:3000)')];
                AllDataRight.TibAntL.FullBrust=[AllDataRight.TibAntL.FullBrust,FindBrust(minFrameUnnormalized,FullresampleR(:,11),Threshold(:,11),(1:1:3000)')];
                
                
             
        end
        
    end
end
%%_______________________________________----------------------------------------------__________________________________________________________________________
%% Compil des donn�es 
MaxCompil=max(EmgCompilLeft./15);


AllDataLeft.SternoL.Data=[AllDataLeft.SternoL.Data./MaxCompil(1,1);AllDataLeft.SternoL.Data./MaxCompil(1,1);AllDataLeft.SternoL.Data./MaxCompil(1,1)];
AllDataLeft.ErSpinR.Data=[AllDataLeft.ErSpinR.Data./MaxCompil(1,2);AllDataLeft.ErSpinR.Data./MaxCompil(1,2);AllDataLeft.ErSpinR.Data./MaxCompil(1,2)];
AllDataLeft.ObliquR.Data=[AllDataLeft.ObliquR.Data./MaxCompil(1,3);AllDataLeft.ObliquR.Data./MaxCompil(1,3);AllDataLeft.ObliquR.Data./MaxCompil(1,3)];
AllDataLeft.GlumedR.Data=[AllDataLeft.GlumedR.Data./MaxCompil(1,4);AllDataLeft.GlumedR.Data./MaxCompil(1,4);AllDataLeft.GlumedR.Data./MaxCompil(1,4)];
AllDataLeft.GlumedL.Data=[AllDataLeft.GlumedL.Data./MaxCompil(1,5);AllDataLeft.GlumedL.Data./MaxCompil(1,5);AllDataLeft.GlumedL.Data./MaxCompil(1,5)];
AllDataLeft.AdductR.Data=[AllDataLeft.AdductR.Data./MaxCompil(1,6);AllDataLeft.AdductR.Data./MaxCompil(1,6);AllDataLeft.AdductR.Data./MaxCompil(1,6)];
AllDataLeft.AdductL.Data=[AllDataLeft.AdductL.Data./MaxCompil(1,7);AllDataLeft.AdductL.Data./MaxCompil(1,7);AllDataLeft.AdductL.Data./MaxCompil(1,7)];
AllDataLeft.GasLatR.Data=[AllDataLeft.GasLatR.Data./MaxCompil(1,8);AllDataLeft.GasLatR.Data./MaxCompil(1,8);AllDataLeft.GasLatR.Data./MaxCompil(1,8)];
AllDataLeft.GasLatL.Data=[AllDataLeft.GasLatL.Data./MaxCompil(1,9);AllDataLeft.GasLatL.Data./MaxCompil(1,9);AllDataLeft.GasLatL.Data./MaxCompil(1,9)];
AllDataLeft.TibAntR.Data=[AllDataLeft.TibAntR.Data./MaxCompil(1,10);AllDataLeft.TibAntR.Data./MaxCompil(1,10);AllDataLeft.TibAntR.Data./MaxCompil(1,10)];
AllDataLeft.TibAntL.Data=[AllDataLeft.TibAntL.Data./MaxCompil(1,11);AllDataLeft.TibAntL.Data./MaxCompil(1,11);AllDataLeft.TibAntL.Data./MaxCompil(1,11)];


AllDataRight.SternoL.Data=[AllDataRight.SternoL.Data./MaxCompil(1,1);AllDataRight.SternoL.Data./MaxCompil(1,1);AllDataRight.SternoL.Data./MaxCompil(1,1)];
AllDataRight.ErSpinR.Data=[AllDataRight.ErSpinR.Data./MaxCompil(1,2);AllDataRight.ErSpinR.Data./MaxCompil(1,2);AllDataRight.ErSpinR.Data./MaxCompil(1,2)];
AllDataRight.ObliquR.Data=[AllDataRight.ObliquR.Data./MaxCompil(1,3);AllDataRight.ObliquR.Data./MaxCompil(1,3);AllDataRight.ObliquR.Data./MaxCompil(1,3)];
AllDataRight.GlumedR.Data=[AllDataRight.GlumedR.Data./MaxCompil(1,4);AllDataRight.GlumedR.Data./MaxCompil(1,4);AllDataRight.GlumedR.Data./MaxCompil(1,4)];
AllDataRight.GlumedL.Data=[AllDataRight.GlumedL.Data./MaxCompil(1,5);AllDataRight.GlumedL.Data./MaxCompil(1,5);AllDataRight.GlumedL.Data./MaxCompil(1,5)];
AllDataRight.AdductR.Data=[AllDataRight.AdductR.Data./MaxCompil(1,6);AllDataRight.AdductR.Data./MaxCompil(1,6);AllDataRight.AdductR.Data./MaxCompil(1,6)];
AllDataRight.AdductL.Data=[AllDataRight.AdductL.Data./MaxCompil(1,7);AllDataRight.AdductL.Data./MaxCompil(1,7);AllDataRight.AdductL.Data./MaxCompil(1,7)];
AllDataRight.GasLatR.Data=[AllDataRight.GasLatR.Data./MaxCompil(1,8);AllDataRight.GasLatR.Data./MaxCompil(1,8);AllDataRight.GasLatR.Data./MaxCompil(1,8)];
AllDataRight.GasLatL.Data=[AllDataRight.GasLatL.Data./MaxCompil(1,9);AllDataRight.GasLatL.Data./MaxCompil(1,9);AllDataRight.GasLatL.Data./MaxCompil(1,9)];
AllDataRight.TibAntR.Data=[AllDataRight.TibAntR.Data./MaxCompil(1,10);AllDataRight.TibAntR.Data./MaxCompil(1,10);AllDataRight.TibAntR.Data./MaxCompil(1,10)];
AllDataRight.TibAntL.Data=[AllDataRight.TibAntL.Data./MaxCompil(1,11);AllDataRight.TibAntL.Data./MaxCompil(1,11);AllDataRight.TibAntL.Data./MaxCompil(1,11)];
%%
AllDataLeft.SternoL.Brust=[AllDataLeft.SternoL.Brust;AllDataLeft.SternoL.Brust;AllDataLeft.SternoL.Brust];
AllDataLeft.ErSpinR.Brust=[AllDataLeft.ErSpinR.Brust;AllDataLeft.ErSpinR.Brust;AllDataLeft.ErSpinR.Brust];
AllDataLeft.ObliquR.Brust=[AllDataLeft.ObliquR.Brust;AllDataLeft.ObliquR.Brust;AllDataLeft.ObliquR.Brust];
AllDataLeft.GlumedR.Brust=[AllDataLeft.GlumedR.Brust;AllDataLeft.GlumedR.Brust;AllDataLeft.GlumedR.Brust];
AllDataLeft.GlumedL.Brust=[AllDataLeft.GlumedL.Brust;AllDataLeft.GlumedL.Brust;AllDataLeft.GlumedL.Brust];
AllDataLeft.AdductR.Brust=[AllDataLeft.AdductR.Brust;AllDataLeft.AdductR.Brust;AllDataLeft.AdductR.Brust];
AllDataLeft.AdductL.Brust=[AllDataLeft.AdductL.Brust;AllDataLeft.AdductL.Brust;AllDataLeft.AdductL.Brust];
AllDataLeft.GasLatR.Brust=[AllDataLeft.GasLatR.Brust;AllDataLeft.GasLatR.Brust;AllDataLeft.GasLatR.Brust];
AllDataLeft.GasLatL.Brust=[AllDataLeft.GasLatL.Brust;AllDataLeft.GasLatL.Brust;AllDataLeft.GasLatL.Brust];
AllDataLeft.TibAntR.Brust=[AllDataLeft.TibAntR.Brust;AllDataLeft.TibAntR.Brust;AllDataLeft.TibAntR.Brust];
AllDataLeft.TibAntL.Brust=[AllDataLeft.TibAntL.Brust;AllDataLeft.TibAntL.Brust;AllDataLeft.TibAntL.Brust];

AllDataRight.SternoL.Brust=[AllDataRight.SternoL.Brust;AllDataRight.SternoL.Brust;AllDataRight.SternoL.Brust];
AllDataRight.ErSpinR.Brust=[AllDataRight.ErSpinR.Brust;AllDataRight.ErSpinR.Brust;AllDataRight.ErSpinR.Brust];
AllDataRight.ObliquR.Brust=[AllDataRight.ObliquR.Brust;AllDataRight.ObliquR.Brust;AllDataRight.ObliquR.Brust];
AllDataRight.GlumedR.Brust=[AllDataRight.GlumedR.Brust;AllDataRight.GlumedR.Brust;AllDataRight.GlumedR.Brust];
AllDataRight.GlumedL.Brust=[AllDataRight.GlumedL.Brust;AllDataRight.GlumedL.Brust;AllDataRight.GlumedL.Brust];
AllDataRight.AdductR.Brust=[AllDataRight.AdductR.Brust;AllDataRight.AdductR.Brust;AllDataRight.AdductR.Brust];
AllDataRight.AdductL.Brust=[AllDataRight.AdductL.Brust;AllDataRight.AdductL.Brust;AllDataRight.AdductL.Brust];
AllDataRight.GasLatR.Brust=[AllDataRight.GasLatR.Brust;AllDataRight.GasLatR.Brust;AllDataRight.GasLatR.Brust];
AllDataRight.GasLatL.Brust=[AllDataRight.GasLatL.Brust;AllDataRight.GasLatL.Brust;AllDataRight.GasLatL.Brust];
AllDataRight.TibAntR.Brust=[AllDataRight.TibAntR.Brust;AllDataRight.TibAntR.Brust;AllDataRight.TibAntR.Brust];
AllDataRight.TibAntL.Brust=[AllDataRight.TibAntL.Brust;AllDataRight.TibAntL.Brust;AllDataRight.TibAntL.Brust];
%% _______________________________________----------------------------------------------__________________________________________________________________________
%                                                       Full Brust    
MaxCompilFull=max(EmgCompilLeftFull./5);

AllDataLeft.SternoL.FullData=[AllDataLeft.SternoL.FullData./MaxCompilFull(:,1)];
AllDataLeft.ErSpinR.FullData=[AllDataLeft.ErSpinR.FullData./MaxCompilFull(:,2)];
AllDataLeft.ObliquR.FullData=[AllDataLeft.ObliquR.FullData./MaxCompilFull(:,3)];
AllDataLeft.GlumedR.FullData=[AllDataLeft.GlumedR.FullData./MaxCompilFull(:,4)];
AllDataLeft.GlumedL.FullData=[AllDataLeft.GlumedL.FullData./MaxCompilFull(:,5)];
AllDataLeft.AdductR.FullData=[AllDataLeft.AdductR.FullData./MaxCompilFull(:,6)];
AllDataLeft.AdductL.FullData=[AllDataLeft.AdductL.FullData./MaxCompilFull(:,7)];
AllDataLeft.GasLatR.FullData=[AllDataLeft.GasLatR.FullData./MaxCompilFull(:,8)];
AllDataLeft.GasLatL.FullData=[AllDataLeft.GasLatL.FullData./MaxCompilFull(:,9)];
AllDataLeft.TibAntR.FullData=[AllDataLeft.TibAntR.FullData./MaxCompilFull(:,10)];
AllDataLeft.TibAntL.FullData=[AllDataLeft.TibAntL.FullData./MaxCompilFull(:,11)];

AllDataRight.SternoL.FullData=[AllDataRight.SternoL.FullData./MaxCompilFull(:,1)];
AllDataRight.ErSpinR.FullData=[AllDataRight.ErSpinR.FullData./MaxCompilFull(:,2)];
AllDataRight.ObliquR.FullData=[AllDataRight.ObliquR.FullData./MaxCompilFull(:,3)];
AllDataRight.GlumedR.FullData=[AllDataRight.GlumedR.FullData./MaxCompilFull(:,4)];
AllDataRight.GlumedL.FullData=[AllDataRight.GlumedL.FullData./MaxCompilFull(:,5)];
AllDataRight.AdductR.FullData=[AllDataRight.AdductR.FullData./MaxCompilFull(:,6)];
AllDataRight.AdductL.FullData=[AllDataRight.AdductL.FullData./MaxCompilFull(:,7)];
AllDataRight.GasLatR.FullData=[AllDataRight.GasLatR.FullData./MaxCompilFull(:,8)];
AllDataRight.GasLatL.FullData=[AllDataRight.GasLatL.FullData./MaxCompilFull(:,9)];
AllDataRight.TibAntR.FullData=[AllDataRight.TibAntR.FullData./MaxCompilFull(:,10)];
AllDataRight.TibAntL.FullData=[AllDataRight.TibAntL.FullData./MaxCompilFull(:,11)];

% Left
AllDataLeft.SternoL.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.SternoL.P_2Brust,AllDataLeft.SternoL.FullData(1:1000,:));
AllDataLeft.ErSpinR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.ErSpinR.P_2Brust,AllDataLeft.ErSpinR.FullData(1:1000,:));
AllDataLeft.ObliquR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.ObliquR.P_2Brust,AllDataLeft.ObliquR.FullData(1:1000,:));
AllDataLeft.GlumedR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedR.P_2Brust,AllDataLeft.GlumedR.FullData(1:1000,:));
AllDataLeft.GlumedL.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedL.P_2Brust,AllDataLeft.GlumedL.FullData(1:1000,:));
AllDataLeft.AdductR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.AdductR.P_2Brust,AllDataLeft.AdductR.FullData(1:1000,:));
AllDataLeft.AdductL.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.AdductL.P_2Brust,AllDataLeft.AdductL.FullData(1:1000,:));
AllDataLeft.GasLatR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatR.P_2Brust,AllDataLeft.GasLatR.FullData(1:1000,:));
AllDataLeft.GasLatL.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatL.P_2Brust,AllDataLeft.GasLatL.FullData(1:1000,:));
AllDataLeft.TibAntR.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntR.P_2Brust,AllDataLeft.TibAntR.FullData(1:1000,:));
AllDataLeft.TibAntL.P_2Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntL.P_2Brust,AllDataLeft.TibAntL.FullData(1:1000,:));

AllDataLeft.SternoL.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.SternoL.P_1Brust,AllDataLeft.SternoL.FullData(1001:2000,:));
AllDataLeft.ErSpinR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.ErSpinR.P_1Brust,AllDataLeft.ErSpinR.FullData(1001:2000,:));
AllDataLeft.ObliquR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.ObliquR.P_1Brust,AllDataLeft.ObliquR.FullData(1001:2000,:));
AllDataLeft.GlumedR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedR.P_1Brust,AllDataLeft.GlumedR.FullData(1001:2000,:));
AllDataLeft.GlumedL.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedL.P_1Brust,AllDataLeft.GlumedL.FullData(1001:2000,:));
AllDataLeft.AdductR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.AdductR.P_1Brust,AllDataLeft.AdductR.FullData(1001:2000,:));
AllDataLeft.AdductL.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.AdductL.P_1Brust,AllDataLeft.AdductL.FullData(1001:2000,:));
AllDataLeft.GasLatR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatR.P_1Brust,AllDataLeft.GasLatR.FullData(1001:2000,:));
AllDataLeft.GasLatL.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatL.P_1Brust,AllDataLeft.GasLatL.FullData(1001:2000,:));
AllDataLeft.TibAntR.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntR.P_1Brust,AllDataLeft.TibAntR.FullData(1001:2000,:));
AllDataLeft.TibAntL.P_1Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntL.P_1Brust,AllDataLeft.TibAntL.FullData(1001:2000,:));

AllDataLeft.SternoL.P0Air=FindAreaUnderCurveStrait(AllDataLeft.SternoL.P0Brust,AllDataLeft.SternoL.FullData(2001:3000,:));
AllDataLeft.ErSpinR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.ErSpinR.P0Brust,AllDataLeft.ErSpinR.FullData(2001:3000,:));
AllDataLeft.ObliquR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.ObliquR.P0Brust,AllDataLeft.ObliquR.FullData(2001:3000,:));
AllDataLeft.GlumedR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedR.P0Brust,AllDataLeft.GlumedR.FullData(2001:3000,:));
AllDataLeft.GlumedL.P0Air=FindAreaUnderCurveStrait(AllDataLeft.GlumedL.P0Brust,AllDataLeft.GlumedL.FullData(2001:3000,:));
AllDataLeft.AdductR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.AdductR.P0Brust,AllDataLeft.AdductR.FullData(2001:3000,:));
AllDataLeft.AdductL.P0Air=FindAreaUnderCurveStrait(AllDataLeft.AdductL.P0Brust,AllDataLeft.AdductL.FullData(2001:3000,:));
AllDataLeft.GasLatR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatR.P0Brust,AllDataLeft.GasLatR.FullData(2001:3000,:));
AllDataLeft.GasLatL.P0Air=FindAreaUnderCurveStrait(AllDataLeft.GasLatL.P0Brust,AllDataLeft.GasLatL.FullData(2001:3000,:));
AllDataLeft.TibAntR.P0Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntR.P0Brust,AllDataLeft.TibAntR.FullData(2001:3000,:));
AllDataLeft.TibAntL.P0Air=FindAreaUnderCurveStrait(AllDataLeft.TibAntL.P0Brust,AllDataLeft.TibAntL.FullData(2001:3000,:));

%Right

AllDataRight.SternoL.P_2Air=FindAreaUnderCurveStrait(AllDataRight.SternoL.P_2Brust,AllDataRight.SternoL.FullData(1:1000,:));
AllDataRight.ErSpinR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.ErSpinR.P_2Brust,AllDataRight.ErSpinR.FullData(1:1000,:));
AllDataRight.ObliquR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.ObliquR.P_2Brust,AllDataRight.ObliquR.FullData(1:1000,:));
AllDataRight.GlumedR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.GlumedR.P_2Brust,AllDataRight.GlumedR.FullData(1:1000,:));
AllDataRight.GlumedL.P_2Air=FindAreaUnderCurveStrait(AllDataRight.GlumedL.P_2Brust,AllDataRight.GlumedL.FullData(1:1000,:));
AllDataRight.AdductR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.AdductR.P_2Brust,AllDataRight.AdductR.FullData(1:1000,:));
AllDataRight.AdductL.P_2Air=FindAreaUnderCurveStrait(AllDataRight.AdductL.P_2Brust,AllDataRight.AdductL.FullData(1:1000,:));
AllDataRight.GasLatR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.GasLatR.P_2Brust,AllDataRight.GasLatR.FullData(1:1000,:));
AllDataRight.GasLatL.P_2Air=FindAreaUnderCurveStrait(AllDataRight.GasLatL.P_2Brust,AllDataRight.GasLatL.FullData(1:1000,:));
AllDataRight.TibAntR.P_2Air=FindAreaUnderCurveStrait(AllDataRight.TibAntR.P_2Brust,AllDataRight.TibAntR.FullData(1:1000,:));
AllDataRight.TibAntL.P_2Air=FindAreaUnderCurveStrait(AllDataRight.TibAntL.P_2Brust,AllDataRight.TibAntL.FullData(1:1000,:));

AllDataRight.SternoL.P_1Air=FindAreaUnderCurveStrait(AllDataRight.SternoL.P_1Brust,AllDataRight.SternoL.FullData(1001:2000,:));
AllDataRight.ErSpinR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.ErSpinR.P_1Brust,AllDataRight.ErSpinR.FullData(1001:2000,:));
AllDataRight.ObliquR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.ObliquR.P_1Brust,AllDataRight.ObliquR.FullData(1001:2000,:));
AllDataRight.GlumedR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.GlumedR.P_1Brust,AllDataRight.GlumedR.FullData(1001:2000,:));
AllDataRight.GlumedL.P_1Air=FindAreaUnderCurveStrait(AllDataRight.GlumedL.P_1Brust,AllDataRight.GlumedL.FullData(1001:2000,:));
AllDataRight.AdductR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.AdductR.P_1Brust,AllDataRight.AdductR.FullData(1001:2000,:));
AllDataRight.AdductL.P_1Air=FindAreaUnderCurveStrait(AllDataRight.AdductL.P_1Brust,AllDataRight.AdductL.FullData(1001:2000,:));
AllDataRight.GasLatR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.GasLatR.P_1Brust,AllDataRight.GasLatR.FullData(1001:2000,:));
AllDataRight.GasLatL.P_1Air=FindAreaUnderCurveStrait(AllDataRight.GasLatL.P_1Brust,AllDataRight.GasLatL.FullData(1001:2000,:));
AllDataRight.TibAntR.P_1Air=FindAreaUnderCurveStrait(AllDataRight.TibAntR.P_1Brust,AllDataRight.TibAntR.FullData(1001:2000,:));
AllDataRight.TibAntL.P_1Air=FindAreaUnderCurveStrait(AllDataRight.TibAntL.P_1Brust,AllDataRight.TibAntL.FullData(1001:2000,:));

AllDataRight.SternoL.P0Air=FindAreaUnderCurveStrait(AllDataRight.SternoL.P0Brust,AllDataRight.SternoL.FullData(2001:3000,:));
AllDataRight.ErSpinR.P0Air=FindAreaUnderCurveStrait(AllDataRight.ErSpinR.P0Brust,AllDataRight.ErSpinR.FullData(2001:3000,:));
AllDataRight.ObliquR.P0Air=FindAreaUnderCurveStrait(AllDataRight.ObliquR.P0Brust,AllDataRight.ObliquR.FullData(2001:3000,:));
AllDataRight.GlumedR.P0Air=FindAreaUnderCurveStrait(AllDataRight.GlumedR.P0Brust,AllDataRight.GlumedR.FullData(2001:3000,:));
AllDataRight.GlumedL.P0Air=FindAreaUnderCurveStrait(AllDataRight.GlumedL.P0Brust,AllDataRight.GlumedL.FullData(2001:3000,:));
AllDataRight.AdductR.P0Air=FindAreaUnderCurveStrait(AllDataRight.AdductR.P0Brust,AllDataRight.AdductR.FullData(2001:3000,:));
AllDataRight.AdductL.P0Air=FindAreaUnderCurveStrait(AllDataRight.AdductL.P0Brust,AllDataRight.AdductL.FullData(2001:3000,:));
AllDataRight.GasLatR.P0Air=FindAreaUnderCurveStrait(AllDataRight.GasLatR.P0Brust,AllDataRight.GasLatR.FullData(2001:3000,:));
AllDataRight.GasLatL.P0Air=FindAreaUnderCurveStrait(AllDataRight.GasLatL.P0Brust,AllDataRight.GasLatL.FullData(2001:3000,:));
AllDataRight.TibAntR.P0Air=FindAreaUnderCurveStrait(AllDataRight.TibAntR.P0Brust,AllDataRight.TibAntR.FullData(2001:3000,:));
AllDataRight.TibAntL.P0Air=FindAreaUnderCurveStrait(AllDataRight.TibAntL.P0Brust,AllDataRight.TibAntL.FullData(2001:3000,:));
% Co-contraction 
% 
% AllDataLeft.CoContraction=FindCoContraction(EMGSmooth(HeelLFrames(1,1):HeelLFrames(3,1),:));
% 
% AllDataRight.CoContraction=FindCoContraction(EMGSmooth(HeelRFrames(1,1):HeelRFrames(3,1),:));



%% _______________________________________----------------------------------------------__________________________________________________________________________
%                                                       Plot Data    

figure (1)
subplot(3,4,12)
Mean=mean(AllDataRight.TibAntL.FullData,2);Std=std(AllDataRight.TibAntL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std/2)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,11)
Mean=mean(AllDataRight.TibAntR.FullData,2);Std=std(AllDataRight.TibAntR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,10)
Mean=mean(AllDataRight.GasLatL.FullData,2);Std=std(AllDataRight.GasLatL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,9)
Mean=mean(AllDataRight.GasLatR.FullData,2);Std=std(AllDataRight.GasLatR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,8)
Mean=mean(AllDataRight.AdductL.FullData,2);Std=std(AllDataRight.AdductL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,7)
Mean=mean(AllDataRight.AdductR.FullData,2);Std=std(AllDataRight.AdductR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,6)
Mean=mean(AllDataRight.GlumedL.FullData,2);Std=std(AllDataRight.GlumedL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,5)
Mean=mean(AllDataRight.GlumedR.FullData,2);Std=std(AllDataRight.GlumedR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,3)
Mean=mean(AllDataRight.ObliquR.FullData,2);Std=std(AllDataRight.ObliquR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,2)
Mean=mean(AllDataRight.ErSpinR.FullData,2);Std=std(AllDataRight.ErSpinR.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);
subplot(3,4,1)
Mean=mean(AllDataRight.SternoL.FullData,2);Std=std(AllDataRight.SternoL.FullData,0,2); plot(Mean);hold on;Shaded_region=patch([1:3000,flip(1:3000)],[Mean+Std;flip(Mean-Std)],[0.6  0.7  0.8]);alpha(Shaded_region,0.5);


outputfilepath=fullfile(output_folder,'StraitAllDataLeft.mat');
save(outputfilepath,'AllDataLeft')
outputfilepath=fullfile(output_folder,'StraitAllDataRight.mat');
save(outputfilepath,'AllDataRight')
outputfilepath=fullfile(output_folder,'StraitMax.mat');
save(outputfilepath,'MaxCompil')
