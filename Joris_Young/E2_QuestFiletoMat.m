%% Ouvrir le Fichier
clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
outputpath=fullfile(subject_path,'2_TransformData');
[file, path]=uigetfile({'*.mat'},'Select Quest File',inputpath);
if (isequal(file, 0))
    return;
end
file_path=fullfile(inputpath,file);
%%
Data_raw=load(file_path);
Data_raw=cell2table(Data_raw.DataQuestRaw(2:end,:));

%% S�paration des donn�es
if size(Data_raw,2)==9
trial_id=Data_raw(:,8);trial_id=table2array(trial_id);trial_id=split(trial_id,"-");trial_names=trial_id(:,1);trial_id=trial_id(:,2);trial_id=str2double (trial_id);
SizeDataRaw=size(trial_id,1); first=trial_id(1,1);last=trial_id(SizeDataRaw,1);

for n=first:last
    FirstF=find(trial_id(:,1)==n,1,'first');
    LastF=find(trial_id(:,1)==n,1,'last');
    Data_trial=Data_raw(FirstF:LastF,:);Data_trial=rmmissing(Data_trial,2);
    Data_trial=table2cell(Data_trial);
    [~,Data_trial_ref] = getvariablesVersionServeur(Data_trial);
    trial_name=trial_names(FirstF,1);
    trial_name=char(trial_name);
    trial_name=sprintf('%sQuest.mat',trial_name);
    DataQuest = Data_trial_ref;
    outputfilepath=fullfile(outputpath,trial_name);
    save(outputfilepath,'DataQuest')
end
end
%% Si c'est avec un agent..
if size(Data_raw,2)==19
trial_id=Data_raw(:,18);trial_id=table2array(trial_id);trial_id=split(trial_id,"-");trial_names=trial_id(:,1);trial_id=trial_id(:,2);trial_id=str2double (trial_id);
SizeDataRaw=size(trial_id,1); first=trial_id(1,1);last=trial_id(SizeDataRaw,1);

for n=28:last
    FirstF=find(trial_id(:,1)==n,1,'first');
    LastF=find(trial_id(:,1)==n,1,'last');
    Data_trial=Data_raw(FirstF:LastF,:);Data_trial=rmmissing(Data_trial,2);
    Data_trial=table2cell(Data_trial);
    [~,Data_trial_ref] = getvariablesVersionServeur(Data_trial);
    trial_name=trial_names(FirstF,1);
    trial_name=char(trial_name);
    trial_name=sprintf('%sQuest.mat',trial_name);
    DataQuest = Data_trial_ref;
    outputfilepath=fullfile(outputpath,trial_name);
    save(outputfilepath,'DataQuest')
end
end