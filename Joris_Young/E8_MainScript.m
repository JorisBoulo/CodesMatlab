
%% Introduction: 
% In this script, the goal is to calulate dependant variables with MotionMonitor and
% EMG files from Nexus C3D and compare them with the quest files. Before using this matlab Code: 
%   
%   1 -  The EXP files have to be converted with the RenameMMFiles.vbs
%   program and translate in .m format with MMFiletoMat.m for a quicker response of this code 
%  
%   2 -  The Quest Files have to be converted from the .csv origin file. A
%   convertion of the first colon and a xlsx save is mandatory.
%   The QuestFiletoMat.m script needs to be use to transcript the .xlsx to .m.
%   This allows to cut and convert trys into easear and quicker files. 
%  
%   3 - To obtain the EMG files the use of EmgFiletoMat.m is mandatory. 
%  
%   4 - To obtain data of the control condition (Without Agent) you need to use Produce CatchTrials.m 
%  
%   5 - To obtain a normalisation of the EMG data you need to use EmgfindMAX.m
% 
%   6 - TO obtain Brust you need to use EmgFindMeanSd.m
% 
% clearvars;clc;
%% *Open files*
% Choose Participant 
clear all 
close all

% subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 1\Participants\','Select Participant');
% if isequal(subject_path,0)
%     return;
% end
% input_folder=fullfile(subject_path,'2_TransformData\');
% output_folder=fullfile(subject_path,'3_Output\');
% if (isequal(input_folder, 0))
%     return;
% end
% if (isequal(output_folder, 0))
%     return;
% end
% %%
% answer = 'Yes';
% 
% while (strcmp(answer,'Yes'))
% Choose File
% [file, Quest_path]=uigetfile({'*.mat'},'Select Quest File',input_folder);
% if (isequal(file, 0))
%     return;
% end
output_file='C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 2\Participants\MainResultsEtude1.xlsx';
subject_path='C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 2\Participants\';
Parti={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28'};
Condi={'RA'};
Rep={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
XlsName = {'P01','P02','P03','P04','P05','P06','P07','P08','P09','P10','P11','P12','P13','P14','P15','P16','P17','P18','P19','P20','P21','P22','P23','P24','P25','P26','P27','P28','P29','P30'};
Input='\2_TransformData\';
Output='\3_Output\';

VariablesPapier=readtable('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 2\Participants\VariablesPapiers.xlsx');
Actif=table2cell(VariablesPapier(:,12));
for t=15
    for i=1
        for j=4
            fullNames_Quest{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_RA%sQuest.mat',Parti{t},Rep{j}))));
            fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_RA%sMM.mat',Parti{t},Rep{j}))));
            fullNames_EMG{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_RA%sEMG.mat',Parti{t},Rep{j}))));
            if exist(fullNames_Quest{j,t}, 'file')==2 && exist(fullNames_MM{j,t}, 'file')==2 && exist(fullNames_EMG{j,t}, 'file')==2


             file_Quest=fullNames_Quest{j,t};
             file_MM=fullNames_MM{j,t};
             file_EMG=fullNames_EMG{j,t};

             input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
             output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);
load(file_EMG)
titleEMG=DataEMG(1,:);
DataEMG=cell2mat(DataEMG(2:end,:));


%% *Importing Variables*

[Head_position,Heels_position,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables(file_MM);
        
[Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position]...
            = GetAvatarVariables(file_Quest);

[istart,istop,~]=findsignal((Participant_Quest_Position(:,2)),Body_position(:,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
Participant_Quest_Position=Participant_Quest_Position(istart:istop,:);
Participant_Quest_Orientation=Participant_Quest_Orientation(istart:istop,:);
Avatar_Position=Avatar_Position(istart:istop,:);


% [Head_position,Body_position,Probed_point_contact,Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position]=...
%     AjustParticipantKinematic(Head_position,Body_position,Probed_point_contact,Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position);
%         
% % Rotate the Quest data  
% [Body_Vel,Body_position,Participant_Quest_Position,Avatar_Position,HeelsL_position,HeelsR_position,Probed_point_contact1,Probed_point_contact2]...
%      =TrigrotateV2(Body_position,Participant_Quest_Position,Avatar_Position,Heels_position,Probed_point_contact,100);


%% Kinematic Processing 
%*Find the crossing frame and synchronize the Quest data*
[CrossingFrame]=GetCrosssingFrame(Participant_Quest_Position,Avatar_Position);

%*Deviation points*
[Strides.Indices.Dev,frame_dev1, frame_dev2,Scalar_Velocity]=Calcul_Deviation2...
    (Head_position,Head_Vel,Avatar_Position,CrossingFrame,input_folder,DataEMG);

%*Find the clearance*
Strides.Indices.CleranceCrossing=norm((Probed_point_contact(CrossingFrame,1:2)-Avatar_Position(CrossingFrame,1:2)));
Strides.Indices.CleranceMin=min(sqrt(sum((Probed_point_contact(:,1:2)-Avatar_Position(:,1:2)).^2, 2)));

%%
% Foot Kinematic *

DataFiltrated=FiltrateEMG(DataEMG,4,20,450);
Strides.Indices.Crossing=CrossingFrame;
Strides.Indices.DistanceDev1=norm(Avatar_Position(frame_dev1,:)-Head_position(frame_dev1,:));
Strides.Indices.DistanceDev2=norm(Avatar_Position(frame_dev2,:)-Head_position(frame_dev2,:));
[~,Strides.Indices.HeelLFramesEMG]=findpeaks(DataEMG(2:end,12),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',500);
[~,Strides.Indices.HeelRFramesEMG]=findpeaks(DataEMG(2:end,end),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',500);
%% Plot heelstrike to know if the detection is correct 
figure
findpeaks(DataEMG(2:end,12),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',500);
hold on 
findpeaks(DataEMG(2:end,end),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',500);
fig = gcf;
fig.Position=[1,1,1440,500];
% Question 
            answer = questdlg('Do you want to continu ?', 'Continu the script', 'Yes', 'Find For left(BLue)','Find For Right(Orange)','Yes');
            switch answer 
                case 'Yes'
                    close (figure)
                case 'Find For left(BLue)'
                    [x,y]=ginput(2);
                    [value,Row]=max(DataEMG(round(x(1,1)):round(x(2,1)),end));
                    Strides.Indices.HeelLFramesEMG=[Strides.Indices.HeelLFramesEMG;round(x(1,1))+Row];
                    Strides.Indices.HeelLFramesEMG=sort(Strides.Indices.HeelLFramesEMG);
                    close (figure)
                case 'Find For Right(Orange)'
                    [x,y]=ginput(2);
                    [Row,value]=max(DataEMG(round(x(1,1)):round(x(2,1)),12));
                    Strides.Indices.HeelRFramesEMG=[Strides.Indices.HeelRFramesEMG;round(x(1,1))+Row];
                    Strides.Indices.HeelRFramesEMG=sort(Strides.Indices.HeelRFramesEMG);
                    close (figure)
                
            end

Strides.Indices.HeelLFrames=round((Strides.Indices.HeelLFramesEMG/2250)*90);
Strides.Indices.HeelRFrames=round((Strides.Indices.HeelRFramesEMG/2250)*90);
[~,Strides.Indices.ToeLFrames]=findpeaks(-Toe_Position(:,3),'MinPeakHeight',-0.01); 
[~,Strides.Indices.ToeRFrames]=findpeaks(-Toe_Position(:,6),'MinPeakHeight',-0.01); 
Left=Strides.Indices.HeelLFrames;
idx=Strides.Indices.HeelLFrames>Strides.Indices.Crossing;
Left(idx)=[];
Right=Strides.Indices.HeelRFrames;
idx=Strides.Indices.HeelRFrames>Strides.Indices.Crossing;
Right(idx)=[];
%%
if size(Left,1)==2
Strides.Indices.CrossingReample=[(((CrossingFrame/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))),(((CrossingFrame/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1),1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
Strides.Indices.Dev1Reample=[(((frame_dev1/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))),(((frame_dev1/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1),1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
Strides.Indices.Dev2Reample=[(((frame_dev2/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))),(((frame_dev2/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1),1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
elseif size(Left,1)==3
Strides.Indices.CrossingReample=[(((CrossingFrame/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))*3000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))),(((CrossingFrame/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
Strides.Indices.Dev1Reample=[(((frame_dev1/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))*3000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))),(((frame_dev1/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
Strides.Indices.Dev2Reample=[(((frame_dev2/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))*3000)/((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1))),(((frame_dev2/90)*2000-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))*2000)/(Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)-Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1))]; 
end

%% Lead Out/Lead In 
Strides.Indices.LOLI=GetLeadINOUT(Strides.Indices.HeelLFramesEMG,Strides.Indices.HeelRFramesEMG,Strides.Indices.Crossing,Left,Right);
%%
% CrossOver/Step Out 

if   islogical(Toe_Position(1:Strides.Indices.Crossing,1)>Toe_Position(1:Strides.Indices.Crossing,4))==0
    Strides.Indices.CoSo='CrossOver'; 
    A=find(Toe_Position(1:Strides.Indices.Crossing,1)>Toe_Position(1:Strides.Indices.Crossing,4));
    Strides.Indices.CoDistanceAgent=norm(Avatar_Position(A(1,1),1:2)-Head_position(A(1,1),1:2));
else
    Strides.Indices.CoSo='StepOut';
    Strides.Indices.CoDistanceAgent=0;
end

Strides.Indices.NameEMG={'SternoLeft' 'ErSpRight' 'ObliqRight' 'GluMedRight' 'GluMedLeft' 'AddRight' 'AddLeft' 'GasLatRight' 'GasLatLeft' 'TibAntRight' 'TibAntLeft'};



%% EMG Processing 

% Initiation des valeurs 
NameDataRest=fullfile(output_folder,'StraitMax.mat');
load (NameDataRest);
NameDataRest=fullfile(output_folder,'MeanAndSdRest.mat');
load (NameDataRest);
Threshold=((Extract(1,1:11)./MaxCompil)+(2*(Extract(2,1:11)./MaxCompil)));
minFrameUnmormalized = 112.5;

% Left Foot is the first contact after crossing :
if size(Left,1)>=3
%Stride-2
Strides.Left.P_2.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1):Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1),1:11);
Strides.Left.P_2.Rectif=abs(Strides.Left.P_2.RAW-mean(Strides.Left.P_2.RAW));
Strides.Left.P_2.Smooth=movmean(Strides.Left.P_2.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.P_2.Smooth));
Strides.Left.P_2.NormTim=resample(Strides.Left.P_2.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Left.P_2.NormAmp=(Strides.Left.P_2.NormTim./MaxCompil);
Strides.Left.P_2.Brust=FindBrust(minFrameUnmormalized,Strides.Left.P_2.NormAmp,Threshold,Strides.Left.P_2.Smooth);
Strides.Left.P_2.Air=FindAreaUnderCurve(Strides.Left.P_2.Brust,Strides.Left.P_2.NormAmp);
Strides.Left.P_2.CoContraction=FindCoContraction(Strides.Left.P_2.NormAmp);
try 
    Strides.Left.P_2.StepWide=[abs(Heels_position(size(Left,1)-2,4)-Heels_position(size(Right,1)-2,1)),abs(Heels_position(size(Right,1)-2,4)-Heels_position(size(Left,1)-1,1))];
    Strides.Left.P_2.StepLength=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-2,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-2,1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-1,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-2,1))];
catch 
    Strides.Left.P_2.StepWide='Pas de detection des pieds � ce moment';
    Strides.Left.P_2.StepLength='Pas de detection des pieds � ce moment';
end 
end

%Stride-1
Strides.Left.P_1.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1)-1,1):Strides.Indices.HeelLFramesEMG(size(Left,1),1),1:11);
Strides.Left.P_1.Rectif=abs(Strides.Left.P_1.RAW-mean(Strides.Left.P_1.RAW));
Strides.Left.P_1.Smooth=movmean(Strides.Left.P_1.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.P_1.Smooth));
Strides.Left.P_1.NormTim=resample(Strides.Left.P_1.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Left.P_1.NormAmp=(Strides.Left.P_1.NormTim./MaxCompil);
Strides.Left.P_1.Brust=FindBrust(minFrameUnmormalized,Strides.Left.P_1.NormAmp,Threshold,Strides.Left.P_1.Smooth);
Strides.Left.P_1.Air=FindAreaUnderCurve(Strides.Left.P_1.Brust,Strides.Left.P_1.NormAmp);
Strides.Left.P_1.CoContraction=FindCoContraction(Strides.Left.P_1.NormAmp);
Strides.Left.P_1.StepWide=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-1,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1),1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1))];
Strides.Left.P_1.StepLength=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-1,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1),1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1))];

%Stride0
Strides.Left.P0.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1),1):Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1),1:11);
Strides.Left.P0.Rectif=abs(Strides.Left.P0.RAW-mean(Strides.Left.P0.RAW));
Strides.Left.P0.Smooth=movmean(Strides.Left.P0.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.P0.Smooth));
Strides.Left.P0.NormTim=resample(Strides.Left.P0.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Left.P0.NormAmp=(Strides.Left.P0.NormTim./MaxCompil);
Strides.Left.P0.Brust=FindBrust(minFrameUnmormalized,Strides.Left.P0.NormAmp,Threshold,Strides.Left.P0.Smooth);
Strides.Left.P0.Air=FindAreaUnderCurve(Strides.Left.P0.Brust,Strides.Left.P0.NormAmp);
Strides.Left.P0.CoContraction=FindCoContraction(Strides.Left.P0.NormAmp);
Strides.Left.P0.StepWide=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1),1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1)+1,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1))];
Strides.Left.P0.StepLength=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1),1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1)+1,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1))];

if size(Left,1)>=3
%Full Strides
Strides.Left.P.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1):Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1),1:11);
Strides.Left.P.Rectif=abs(Strides.Left.P.RAW-mean(Strides.Left.P.RAW));
Strides.Left.P.Smooth=movmean(Strides.Left.P.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.P.Smooth));
Strides.Left.P.NormTim=resample(Strides.Left.P.Smooth,Timeline,(1/(Timeline(end,end)/3000)));
Strides.Left.P.NormAmp=(Strides.Left.P.NormTim./MaxCompil);
Strides.Left.P.Brust=FindBrust(minFrameUnmormalized,Strides.Left.P.NormAmp,Threshold,Strides.Left.P.Smooth);
Strides.Left.P.Air=FindAreaUnderCurve(Strides.Left.P.Brust,Strides.Left.P.NormAmp);
% Strides.Left.P.CoContraction=FindCoContraction(Strides.Left.P.NormAmp);
end 
if size(Left,1)>=4
%Stride-3
Strides.Left.P_3.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1)-3,1):Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1),1:11);
Strides.Left.P_3.Rectif=abs(Strides.Left.P_3.RAW-mean(Strides.Left.P_3.RAW));
Strides.Left.P_3.Smooth=movmean(Strides.Left.P_3.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.P_3.Smooth));
Strides.Left.P_3.NormTim=resample(Strides.Left.P_3.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Left.P_3.NormAmp=(Strides.Left.P_3.NormTim./MaxCompil);
Strides.Left.P_3.Brust=FindBrust(minFrameUnmormalized,Strides.Left.P_3.NormAmp,Threshold,Strides.Left.P_3.Smooth);
Strides.Left.P_3.Air=FindAreaUnderCurve(Strides.Left.P_3.Brust,Strides.Left.P_3.NormAmp);
% Strides.Left.P_3.CoContraction=FindCoContraction(Strides.Left.P_3.NormAmp);
try
    Strides.Left.P_3.StepWide=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-3,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-3,1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-2,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-3,1))];
    Strides.Left.P_3.StepLength=[Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-3,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-3,1)),Heels_position(Strides.Indices.HeelLFrames(size(Left,1)-2,1))-Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-3,1))];
catch 
    Strides.Left.P_3.StepWide='Pas de d�tection des pieds � ce moment';
    Strides.Left.P_3.StepLength='Pas de d�tection des pieds � ce moment';
end 

%Full Strides
Strides.Left.Px3.RAW=DataFiltrated(Strides.Indices.HeelLFramesEMG(size(Left,1)-3,1):Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1),1:11);
Strides.Left.Px3.Rectif=abs(Strides.Left.Px3.RAW-mean(Strides.Left.Px3.RAW));
Strides.Left.Px3.Smooth=movmean(Strides.Left.Px3.Rectif,112.5);
Timeline=(1:1:size(Strides.Left.Px3.Smooth));
Strides.Left.Px3.NormTim=resample(Strides.Left.Px3.Smooth,Timeline,(1/(Timeline(end,end)/3000)));
Strides.Left.Px3.NormAmp=(Strides.Left.Px3.NormTim./MaxCompil);
Strides.Left.Px3.Brust=FindBrust(minFrameUnmormalized,Strides.Left.Px3.NormAmp,Threshold,Strides.Left.Px3.Smooth);
Strides.Left.Px3.Air=FindAreaUnderCurve(Strides.Left.Px3.Brust,Strides.Left.Px3.NormAmp);
% Strides.Left.Px3.CoContraction=FindCoContraction(Strides.Left.Px3.NormAmp);
end
%___________________________________________________________________________________________________________________________________________________________________________________________________________________________________
% Right foot is the first contact after crossing : 
if size(Right,1)>=3
%Stride-2
Strides.Right.P_2.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1)-2,1):Strides.Indices.HeelRFramesEMG(size(Right,1)-1,1),1:11);
Strides.Right.P_2.Rectif=abs(Strides.Right.P_2.RAW-mean(Strides.Right.P_2.RAW));
Strides.Right.P_2.Smooth=movmean(Strides.Right.P_2.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.P_2.Smooth));
Strides.Right.P_2.NormTim=resample(Strides.Right.P_2.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Right.P_2.NormAmp=(Strides.Right.P_2.NormTim./MaxCompil);
Strides.Right.P_2.Brust=FindBrust(minFrameUnmormalized,Strides.Right.P_2.NormAmp,Threshold,Strides.Right.P_2.Smooth);
Strides.Right.P_2.Air=FindAreaUnderCurve(Strides.Right.P_2.Brust,Strides.Right.P_2.NormAmp);
% Strides.Right.P_2.CoContraction=FindCoContraction(Strides.Right.P_2.NormAmp);
try
    Strides.Right.P_2.StepWide=[abs(Heels_position(size(Right,1)-2,4)-Heels_position(size(Right,1)-2,1)),abs(Heels_position(size(Right,1)-2,4)-Heels_position(size(Right,1)-1,1))];
    Strides.Right.P_2.StepWide=[Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-2,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-2,1)),Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-2,1))];
catch 
    Strides.Right.P_2.CoContraction='Pas de detection des pieds � ce moment';
    Strides.Right.P_2.StepLength='Pas de detection des pieds � ce moment';
end 
end
%Stride-1
Strides.Right.P_1.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1)-1,1):Strides.Indices.HeelRFramesEMG(size(Right,1),1),1:11);
Strides.Right.P_1.Rectif=abs(Strides.Right.P_1.RAW-mean(Strides.Right.P_1.RAW));
Strides.Right.P_1.Smooth=movmean(Strides.Right.P_1.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.P_1.Smooth));
Strides.Right.P_1.NormTim=resample(Strides.Right.P_1.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Right.P_1.NormAmp=(Strides.Right.P_1.NormTim./MaxCompil);
Strides.Right.P_1.Brust=FindBrust(minFrameUnmormalized,Strides.Right.P_1.NormAmp,Threshold,Strides.Right.P_1.Smooth);
Strides.Right.P_1.Air=FindAreaUnderCurve(Strides.Right.P_1.Brust,Strides.Right.P_1.NormAmp);
% Strides.Right.P_1.CoContraction=FindCoContraction(Strides.Right.P_1.NormAmp);
Strides.Right.P_1.StepWide=[abs(Heels_position(size(Right,1)-1,4)-Heels_position(size(Right,1)-1,1)),abs(Heels_position(size(Right,1)-1,4)-Heels_position(size(Right,1),1))];
Strides.Right.P_1.StepWide=[Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-1,1)),Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-1,1))];


%Stride0
Strides.Right.P0.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1),1):Strides.Indices.HeelRFramesEMG(size(Right,1)+1,1),1:11);
Strides.Right.P0.Rectif=abs(Strides.Right.P0.RAW-mean(Strides.Right.P0.RAW));
Strides.Right.P0.Smooth=movmean(Strides.Right.P0.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.P0.Smooth));
Strides.Right.P0.NormTim=resample(Strides.Right.P0.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Right.P0.NormAmp=(Strides.Right.P0.NormTim./MaxCompil);
Strides.Right.P0.Brust=FindBrust(minFrameUnmormalized,Strides.Right.P0.NormAmp,Threshold,Strides.Right.P0.Smooth);
Strides.Right.P0.Air=FindAreaUnderCurve(Strides.Right.P0.Brust,Strides.Right.P0.NormAmp);
% Strides.Right.P0.CoContraction=FindCoContraction(Strides.Right.P0.NormAmp);
Strides.Right.P0.StepWide=[abs(Heels_position(size(Right,1),4)-Heels_position(size(Right,1),1)),abs(Heels_position(size(Right,1),4)-Heels_position(size(Right,1)+1,1))];
Strides.Right.P0.StepWide=[Heels_position(Strides.Indices.HeelRFrames(size(Right,1),1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1),1)),Heels_position(Strides.Indices.HeelRFrames(size(Right,1)+1,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1),1))];



%Full Strides
Strides.Right.P.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1)-2,1):Strides.Indices.HeelRFramesEMG(size(Right,1)+1,1),1:11);
Strides.Right.P.Rectif=abs(Strides.Right.P.RAW-mean(Strides.Right.P.RAW));
Strides.Right.P.Smooth=movmean(Strides.Right.P.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.P.Smooth));
Strides.Right.P.NormTim=resample(Strides.Right.P.Smooth,Timeline,(1/(Timeline(end,end)/3000)));
Strides.Right.P.NormAmp=(Strides.Right.P.NormTim./MaxCompil);
Strides.Right.P.Brust=FindBrust(minFrameUnmormalized,Strides.Right.P.NormAmp,Threshold,Strides.Right.P.Smooth);
Strides.Right.P.Air=FindAreaUnderCurve(Strides.Right.P.Brust,Strides.Right.P.NormAmp);
% Strides.Right.P.CoContraction=FindCoContraction(Strides.Right.P.NormAmp);

if size(Right,1)>=4
%Stride-3
Strides.Right.P_3.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1)-3,1):Strides.Indices.HeelRFramesEMG(size(Right,1)-2,1),1:11);
Strides.Right.P_3.Rectif=abs(Strides.Right.P_3.RAW-mean(Strides.Right.P_3.RAW));
Strides.Right.P_3.Smooth=movmean(Strides.Right.P_3.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.P_3.Smooth));
Strides.Right.P_3.NormTim=resample(Strides.Right.P_3.Smooth,Timeline,(1/(Timeline(end,end)/1000)));
Strides.Right.P_3.NormAmp=(Strides.Right.P_3.NormTim./MaxCompil);
Strides.Right.P_3.Brust=FindBrust(minFrameUnmormalized,Strides.Right.P_3.NormAmp,Threshold,Strides.Right.P_3.Smooth);
Strides.Right.P_3.Air=FindAreaUnderCurve(Strides.Right.P_3.Brust,Strides.Right.P_3.NormAmp);
% Strides.Right.P_3.CoContraction=FindCoContraction(Strides.Right.P_3.NormAmp);
try
    Strides.Right.P_3.StepWide=[abs(Heels_position(size(Right,1)-3,4)-Heels_position(size(Right,1)-3,1)),abs(Heels_position(size(Right,1)-3,4)-Heels_position(size(Right,1)-2,1))];
    Strides.Right.P_3.StepWide=[Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-2,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-2,1)),Heels_position(Strides.Indices.HeelRFrames(size(Right,1)-1,1))-Heels_position(Strides.Indices.HeelLFrames(size(Right,1)-2,1))];
catch 
    Strides.Right.P_3.CoContraction='Pas de detection des pieds � ce moment';
    Strides.Right.P_3.StepLength='Pas de detection des pieds � ce moment';
end 
%Full Strides
Strides.Right.Px3.RAW=DataFiltrated(Strides.Indices.HeelRFramesEMG(size(Right,1)-3,1):Strides.Indices.HeelRFramesEMG(size(Right,1)+1,1),1:11);
Strides.Right.Px3.Rectif=abs(Strides.Right.Px3.RAW-mean(Strides.Right.Px3.RAW));
Strides.Right.Px3.Smooth=movmean(Strides.Right.Px3.Rectif,112.5);
Timeline=(1:1:size(Strides.Right.Px3.Smooth));
Strides.Right.Px3.NormTim=resample(Strides.Right.Px3.Smooth,Timeline,(1/(Timeline(end,end)/3000)));
Strides.Right.Px3.NormAmp=(Strides.Right.Px3.NormTim./MaxCompil);
Strides.Right.Px3.Brust=FindBrust(minFrameUnmormalized,Strides.Right.Px3.NormAmp,Threshold,Strides.Right.Px3.Smooth);
Strides.Right.Px3.Air=FindAreaUnderCurve(Strides.Right.Px3.Brust,Strides.Right.Px3.NormAmp);
% Strides.Right.Px3.CoContraction=FindCoContraction(Strides.Right.Px3.NormAmp);
end

% Lead Out/Lead In 
if  strcmp(char(Strides.Indices.LOLI(1,1)),'LeadIn')==1
    try
        Strides.LOLI.P_2.NormAmp=Strides.Right.P_2.NormAmp;
        Strides.LOLI.P_2.Brust=Strides.Right.P_2.Brust;
        Strides.LOLI.P_2.Air=Strides.Right.P_2.Air;
        Strides.LOLI.P_2.CoContraction=Strides.Right.P_2.CoContraction;
    catch
        Strides.LOLI.P_2.NormAmp='Pas de donnees';
        Strides.LOLI.P_2.Brust='Pas de donnees';
        Strides.LOLI.P_2.Air='Pas de donnees';
        Strides.LOLI.P_2.CoContraction='Pas de donnees';
    end
    Strides.LOLI.P_1.NormAmp=Strides.Right.P_1.NormAmp;
    Strides.LOLI.P0.NormAmp=Strides.Right.P0.NormAmp;
    Strides.LOLI.P.NormAmp=Strides.Right.P.NormAmp;
    Strides.LOLI.P_1.Brust=Strides.Right.P_1.Brust;
    Strides.LOLI.P0.Brust=Strides.Right.P0.Brust;
    Strides.LOLI.P.Brust=Strides.Right.P.Brust;
    Strides.LOLI.P_1.Air=Strides.Right.P_1.Air;
    Strides.LOLI.P0.Air=Strides.Right.P0.Air;
    Strides.LOLI.P.Air=Strides.Right.P.Air;
%     Strides.LOLI.P_1.CoContraction=Strides.Right.P_1.CoContraction;
%     Strides.LOLI.P0.CoContraction=Strides.Right.P0.CoContraction;
%     Strides.LOLI.P.CoContraction=Strides.Right.P.CoContraction;
    Strides.Indices.Traj=[Head_position(round((Strides.Indices.HeelRFramesEMG(size(Right,1)-2,1)/2500)*90):round((Strides.Indices.HeelRFramesEMG(size(Right,1)+1,1)/2500)*90),1:2),Avatar_Position(round((Strides.Indices.HeelRFramesEMG(size(Right,1)-2,1)/2500)*90):round((Strides.Indices.HeelRFramesEMG(size(Right,1)+1,1)/2500)*90),1:2)];% end 

else 
    try
        Strides.LOLI.P_2.NormAmp=Strides.Left.P_2.NormAmp;
        Strides.LOLI.P_2.Brust=Strides.Left.P_2.Brust;
        Strides.LOLI.P_2.Air=Strides.Left.P_2.Air;
        Strides.LOLI.P_2.CoContraction=Strides.Left.P_2.CoContraction;
    catch
        Strides.LOLI.P_2.NormAmp='Pas de donnees';
        Strides.LOLI.P_2.Brust='Pas de donnees';
        Strides.LOLI.P_2.Air='Pas de donnees';
        Strides.LOLI.P_2.CoContraction='Pas de donnees';
    end
    Strides.LOLI.P_1.NormAmp=Strides.Left.P_1.NormAmp;
    Strides.LOLI.P0.NormAmp=Strides.Left.P0.NormAmp;
    Strides.LOLI.P.NormAmp=Strides.Left.P.NormAmp;
    Strides.LOLI.P_1.Brust=Strides.Left.P_1.Brust;
    Strides.LOLI.P0.Brust=Strides.Left.P0.Brust;
    Strides.LOLI.P.Brust=Strides.Left.P.Brust;
    Strides.LOLI.P_1.Air=Strides.Left.P_1.Air;
    Strides.LOLI.P0.Air=Strides.Left.P0.Air;
    Strides.LOLI.P.Air=Strides.Left.P.Air;
%     Strides.LOLI.P_1.CoContraction=Strides.Left.P_1.CoContraction;
%     Strides.LOLI.P0.CoContraction=Strides.Left.P0.CoContraction;
%     Strides.LOLI.P.CoContraction=Strides.Left.P.CoContraction;
    try
    Strides.Indices.Traj=[Head_position(round((Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1)/2500)*90):round((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)/2500)*90),1:2),Avatar_Position(round((Strides.Indices.HeelLFramesEMG(size(Left,1)-2,1)/2500)*90):round((Strides.Indices.HeelLFramesEMG(size(Left,1)+1,1)/2500)*90),1:2)];% end 
    catch
    Strides.Indices.Traj='Ca saoul' ;
    end

end


%%
name=erase(file_MM,input_folder);
name=strcat(erase(name,'MM.mat'),'EMG.mat');
FilepathName=fullfile(output_folder,name);
save(FilepathName,'Strides')

answer = questdlg('Analyze another file ?', 'Analyse another file', 'Yes', 'No', 'Yes');
% end
close all
            end 
        end
    end
end


