function [Head_position,Body_position,Probed_point_contact,Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position]=AjustParticipantKinematic(Head_position,Body_position,Probed_point_contact,Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position)

[istart,istop,~]=findsignal((Participant_Quest_Position(:,2)),Body_position(:,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
Participant_Quest_Position=Participant_Quest_Position(istart:istop,:);
Participant_Quest_Orientation=Participant_Quest_Orientation(istart:istop,:);
Avatar_Position=Avatar_Position(istart:istop,:);


Dif=Participant_Quest_Position-Head_position; 
Head_position=Head_position-Dif;
Body_position=Body_position-Dif;
Probed_point_contact=Probed_point_contact(:,1:2)-Dif(:,1:2);

