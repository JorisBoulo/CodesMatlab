function [CrossingFrameQuest]=GetCrosssingFrame(Participant_Quest_Position,Avatar_Position)

CrossingFrameQuest=min(find(Avatar_Position(:,2)<Participant_Quest_Position(:,2)));
