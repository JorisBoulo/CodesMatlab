function Answer=FindAreaUnderCurveStrait(Brust,DataNorm)
for i=1:size(DataNorm,2)
    [~, A] = max(Brust(:,i),[],1);
    [~, B] = min(Brust(:,i),[],1);
if B<A 
    [~, B] =min(Brust(A:end,i),[],1);
    B=A+B;
end
Answer(i)=trapz(DataNorm(A:B,i));
end 
