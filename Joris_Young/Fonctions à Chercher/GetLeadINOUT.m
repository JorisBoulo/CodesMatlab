function [FootAfterCrossing]= GetLeadINOUT (StridesRightHeelLFrames,StridesRightHeelRFrames,FrameCrossing,Left,Right)
A=Left(end,1)-FrameCrossing;
B=Right(end,1)-FrameCrossing;
if A>B  %% Si le dernier pas avant le croisement �tait le gauche alors on a un lead out 
    Text=mat2str(StridesRightHeelRFrames(size(Right,1)+1,1));
    FootAfterCrossing={'LeadOut';Text};
else
    Text=mat2str(StridesRightHeelLFrames(size(Left,1)+1,1));
    FootAfterCrossing={'LeadIn';Text};
end 