% This script allows you to find the sqare root of muscle
% activity during the entire experiment of a participant 

%% *Open files*
% Choose Participant 
subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 1\Pilots\','Select Participant');
if isequal(subject_path,0)
    return;
end
input_folder=fullfile(subject_path,'2_TransformData\');
output_folder=fullfile(subject_path,'3_Output\');
if (isequal(input_folder, 0))
    return;
end
if (isequal(output_folder, 0))
    return;
end
% Choose a EMG File
Condi={'NA','RA','NR'};
Rep={'01','02','03','04','05','06','07','8','9','10'};
MaxTot=zeros(1,15); 
A=1;
EmgCompilLeft=zeros(1000,11);
EmgCompilRight=zeros(1000,11);% PASNorm; % trouver une solution pour normaliser les sternocleidomastoidiens 
AllDataLeft.SternoR=[];
AllDataLeft.ErSpinR=[];
AllDataLeft.ObliquR=[];
AllDataLeft.GlumedR=[];
AllDataLeft.GlumedL=[];
AllDataLeft.AdductR=[];
AllDataLeft.AdductL=[];
AllDataLeft.GasLatR=[];
AllDataLeft.GasLatL=[];
AllDataLeft.TibAntR=[];
AllDataLeft.TibAntL=[];
AllDataRight.SternoR=[];
AllDataRight.ErSpinR=[];
AllDataRight.ObliquR=[];
AllDataRight.GlumedR=[];
AllDataRight.GlumedL=[];
AllDataRight.AdductR=[];
AllDataRight.AdductL=[];
AllDataRight.GasLatR=[];
AllDataRight.GasLatL=[];
AllDataRight.TibAntR=[];
AllDataRight.TibAntL=[];
for i=1
    for j=1:5
        NamesEMG{i,j}=fullfile(input_folder,(sprintf('%s%sEMG.mat',Condi{i},Rep{j})));
        if exist(NamesEMG{i,j}, 'file')==2
            load (NamesEMG{i,j});
            emgnames=(DataEMG(1,:));
            RAWemg=cell2mat(DataEMG(2:end,:));
            RAWemg=FiltrateEMG(RAWemg,4,20,450);
            Extract=[];
            for k=1:size(RAWemg,2)
             rawemg=RAWemg(:,k)-mean(RAWemg(:,k));
             EmgRect = abs(rawemg(:,1));
             EmgSmooth=movmean(EmgRect,112.5);
             Max=max(EmgSmooth);
             if MaxTot(1,k)<Max
                 MaxTot(1,k)=Max;
             end
             end
            % En d�coupant les pas 
            file_MM=strrep(NamesEMG{i,j},'EMG.mat','MM.mat');
            [Head_position,Heels_position,Body_position,Body_Vel,Probed_point_contact,title_probed_point_contact,...
            Reorientation_var,title_reorientation_var]...
            = GetMotionMonitorVariables(file_MM);
            [~,HeelLFrames]=findpeaks(cell2mat(DataEMG(2:end,end)),'MinPeakDistance',2500);
            [~,HeelRFrames]=findpeaks(cell2mat(DataEMG(2:end,12)),'MinPeakDistance',2500);
            EmgCompilSmooth=[];
            EMGRectif=abs(RAWemg-mean(RAWemg,1));
            EMGSmooth=movmean(EMGRectif,112.5);
             for l=1:4
                 %Left
                PASLeft=EMGSmooth(HeelLFrames(l,1):HeelLFrames(l+1,1),:);
                Timeline=(1:1:size(PASLeft,1));
                PASNormLeft=resample(PASLeft(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                
                AllDataLeft.SternoR=[AllDataLeft.SternoR,PASNormLeft(:,1)];
                AllDataLeft.ErSpinR=[AllDataLeft.ErSpinR,PASNormLeft(:,2)];
                AllDataLeft.ObliquR=[AllDataLeft.ObliquR,PASNormLeft(:,3)];
                AllDataLeft.GlumedR=[AllDataLeft.GlumedR,PASNormLeft(:,4)];
                AllDataLeft.GlumedL=[AllDataLeft.GlumedL,PASNormLeft(:,5)];
                AllDataLeft.AdductR=[AllDataLeft.AdductR,PASNormLeft(:,6)];
                AllDataLeft.AdductL=[AllDataLeft.AdductL,PASNormLeft(:,7)];
                AllDataLeft.GasLatR=[AllDataLeft.GasLatR,PASNormLeft(:,8)];
                AllDataLeft.GasLatL=[AllDataLeft.GasLatL,PASNormLeft(:,9)];
                AllDataLeft.TibAntR=[AllDataLeft.TibAntR,PASNormLeft(:,10)];
                AllDataLeft.TibAntL=[AllDataLeft.TibAntL,PASNormLeft(:,11)];
                
                % Compil the data to reduce peaks 
                EmgCompilLeft=EmgCompilLeft+PASNormLeft;
                                
                %Right 
                PASRight=EMGSmooth(HeelRFrames(l,1):HeelRFrames(l+1,1),:);
                Timeline=(1:1:size(PASRight,1));
                PASNormRight=resample(PASRight(:,1:11),Timeline,(1/(Timeline(end,end)/1000)));
                
                AllDataRight.SternoR=[AllDataRight.SternoR,PASNormRight(:,1)];
                AllDataRight.ErSpinR=[AllDataRight.ErSpinR,PASNormRight(:,2)];
                AllDataRight.ObliquR=[AllDataRight.ObliquR,PASNormRight(:,3)];
                AllDataRight.GlumedR=[AllDataRight.GlumedR,PASNormRight(:,4)];
                AllDataRight.GlumedL=[AllDataRight.GlumedL,PASNormRight(:,5)];
                AllDataRight.AdductR=[AllDataRight.AdductR,PASNormRight(:,6)];
                AllDataRight.AdductL=[AllDataRight.AdductL,PASNormRight(:,7)];
                AllDataRight.GasLatR=[AllDataRight.GasLatR,PASNormRight(:,8)];
                AllDataRight.GasLatL=[AllDataRight.GasLatL,PASNormRight(:,9)];
                AllDataRight.TibAntR=[AllDataRight.TibAntR,PASNormRight(:,10)];
                AllDataRight.TibAntL=[AllDataRight.TibAntL,PASNormRight(:,11)];
                
                Traj=[Body_position];
                
             end
        end
    end
end
MaxCompil=max(EmgCompilLeft./20);
AllDataLeft.SternoR=[AllDataLeft.SternoR./MaxCompil(1,1);AllDataLeft.SternoR./MaxCompil(1,1)];
AllDataLeft.ErSpinR=[AllDataLeft.ErSpinR./MaxCompil(1,2);AllDataLeft.ErSpinR./MaxCompil(1,2)];
AllDataLeft.ObliquR=[AllDataLeft.ObliquR./MaxCompil(1,3);AllDataLeft.ObliquR./MaxCompil(1,3)];
AllDataLeft.GlumedR=[AllDataLeft.GlumedR./MaxCompil(1,4);AllDataLeft.GlumedR./MaxCompil(1,4)];
AllDataLeft.GlumedL=[AllDataLeft.GlumedL./MaxCompil(1,5);AllDataLeft.GlumedL./MaxCompil(1,5)];
AllDataLeft.AdductR=[AllDataLeft.AdductR./MaxCompil(1,6);AllDataLeft.AdductR./MaxCompil(1,6)];
AllDataLeft.AdductL=[AllDataLeft.AdductL./MaxCompil(1,7);AllDataLeft.AdductL./MaxCompil(1,7)];
AllDataLeft.GasLatR=[AllDataLeft.GasLatR./MaxCompil(1,8);AllDataLeft.GasLatR./MaxCompil(1,8)];
AllDataLeft.GasLatL=[AllDataLeft.GasLatL./MaxCompil(1,9);AllDataLeft.GasLatL./MaxCompil(1,9)];
AllDataLeft.TibAntR=[AllDataLeft.TibAntR./MaxCompil(1,10);AllDataLeft.TibAntR./MaxCompil(1,10)];
AllDataLeft.TibAntL=[AllDataLeft.TibAntL./MaxCompil(1,11);AllDataLeft.TibAntL./MaxCompil(1,11)];


AllDataRight.SternoR=[AllDataRight.SternoR./MaxCompil(1,1);AllDataRight.SternoR./MaxCompil(1,1)];
AllDataRight.ErSpinR=[AllDataRight.ErSpinR./MaxCompil(1,2);AllDataRight.ErSpinR./MaxCompil(1,2)];
AllDataRight.ObliquR=[AllDataRight.ObliquR./MaxCompil(1,3);AllDataRight.ObliquR./MaxCompil(1,3)];
AllDataRight.GlumedR=[AllDataRight.GlumedR./MaxCompil(1,4);AllDataRight.GlumedR./MaxCompil(1,4)];
AllDataRight.GlumedL=[AllDataRight.GlumedL./MaxCompil(1,5);AllDataRight.GlumedL./MaxCompil(1,5)];
AllDataRight.AdductR=[AllDataRight.AdductR./MaxCompil(1,6);AllDataRight.AdductR./MaxCompil(1,6)];
AllDataRight.AdductL=[AllDataRight.AdductL./MaxCompil(1,7);AllDataRight.AdductL./MaxCompil(1,7)];
AllDataRight.GasLatR=[AllDataRight.GasLatR./MaxCompil(1,8);AllDataRight.GasLatR./MaxCompil(1,8)];
AllDataRight.GasLatL=[AllDataRight.GasLatL./MaxCompil(1,9);AllDataRight.GasLatL./MaxCompil(1,9)];
AllDataRight.TibAntR=[AllDataRight.TibAntR./MaxCompil(1,10);AllDataRight.TibAntR./MaxCompil(1,10)];
AllDataRight.TibAntL=[AllDataRight.TibAntL./MaxCompil(1,11);AllDataRight.TibAntL./MaxCompil(1,11)];



%%
outputfilepath=fullfile(output_folder,'StraitAllDataLeft.mat');
save(outputfilepath,'AllDataLeft')
outputfilepath=fullfile(output_folder,'StraitAllDataRight.mat');
save(outputfilepath,'AllDataRight')
outputfilepath=fullfile(output_folder,'StraitMax.mat');
save(outputfilepath,'MaxCompil')
