function DataBrust=FindBrust(minFrameUnmormalized,Strides,Threshold,StrideSize)
aboveThreshold=(Strides>=Threshold);
MinframeNorm=round((1000*minFrameUnmormalized)/size(StrideSize,1));
Brust=[];
for i=1:size(aboveThreshold,2)
Try= bwareaopen(aboveThreshold(:,i), MinframeNorm);
Brust=[Brust,Try];
end 
BehindThreshold=(Brust==0);
Brust2=[];
for i=1:size(aboveThreshold,2)
Try= bwareaopen(BehindThreshold(:,i), MinframeNorm);
Brust2=[Brust2,Try];
end 
DataBrust=(Brust2==0);