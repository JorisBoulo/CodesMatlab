function [clearance, side_circumvention] = Calcul_Clearance(Trunk_position,Avatar_Position,probed_point_contact,frame_at_crossing,sex)
if sex==1
    av_rayon=0.25;
elseif sex==2
    av_rayon=0.23;
elseif sex==3
    av_rayon=0.25; %homme �g� a 50 cm de largeur
elseif sex==4
    av_rayon=0.23; %femme �g�e a 46 cm de largeur
end

L_Acr_AVshoulder=sqrt((probed_point_contact(:,5)-(Avatar_Position(:,1)+av_rayon)).^2+(probed_point_contact(:,6)-Avatar_Position(:,2)).^2);
R_Acr_AVshoulder=sqrt((probed_point_contact(:,7)-(Avatar_Position(:,1)-av_rayon)).^2+(probed_point_contact(:,8)-Avatar_Position(:,2)).^2);
L_Hand_AVshoulder=sqrt((probed_point_contact(:,1)-(Avatar_Position(:,1)+av_rayon)).^2+(probed_point_contact(:,2)-Avatar_Position(:,2)).^2);
R_Hand_AVshoulder=sqrt((probed_point_contact(:,3)-(Avatar_Position(:,1)-av_rayon)).^2+(probed_point_contact(:,4)-Avatar_Position(:,2)).^2);

%identification d'un contact entre le VICON et l'Avatar
if L_Acr_AVshoulder<=0 | R_Acr_AVshoulder<=0 | L_Hand_AVshoulder<=0 | R_Hand_AVshoulder<=0
    contact=1;
    warndlg('There was a contact between the participant and the avatar. Look at the VIVE file to see if a verbal feedback was sent to the participant','Warning')
else
    contact=0;
end

%Clearance shoulder-shoulder calcul 
cut=frame_at_crossing+1
%if circumvent to the right side
[Min_Clearance_AcrG, frame_clearance1]=min(L_Acr_AVshoulder(1:cut));frame_clearance1=frame_clearance1-1;
Distance_at_crossing_Acr=L_Acr_AVshoulder(frame_at_crossing+1); %because frame start at 0 but cases of matlab at 1, we need to ad one to the frame to get the good number

%if circumvent to the left side
[Min_Clearance_AcrD, frame_clearance2]=min(R_Acr_AVshoulder(1:cut));frame_clearance2=frame_clearance2-1;
Distance_at_crossing_Acr=R_Acr_AVshoulder(frame_at_crossing+1);

%True clearance
if Min_Clearance_AcrG<Min_Clearance_AcrD
    side_circumvention='Right';
    Min_Clearance_Acr=Min_Clearance_AcrG;
    frame_clearance=frame_clearance1;
    Side=1
else 
    side_circumvention='Left'
    Min_Clearance_Acr=Min_Clearance_AcrD;
    frame_clearance=frame_clearance2;
    Side=2
end

%clearance CoM to CoM 
dist_CoM_center=sqrt((Trunk_position(:,1)-Avatar_Position(:,1)).^2+(Trunk_position(:,2)-Avatar_Position(:,2)).^2);
[Min_Clearrance_CoM, frame_clearance_com]=min(dist_CoM_center);frame_clearance_com=frame_clearance_com-1;
Distance_at_crossing_CoM=dist_CoM_center(frame_at_crossing+1);

clearance={'Contact','Side_Circumvention','Min_Clearance_Acr','frame_clearance','Min_Clearrance_CoM','frame_clearance_com','Distance_at_crossing_Acr','Distance_at_crossing_CoM','frame_at_crossing';contact,Side,Min_Clearance_Acr,frame_clearance,Min_Clearrance_CoM,frame_clearance_com,Distance_at_crossing_Acr,Distance_at_crossing_CoM,frame_at_crossing};
end

