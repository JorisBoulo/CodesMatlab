% inputpath='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Pilots\Pilot 3\1_RawData';
% outputpath='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Pilots\Pilot 3\2_TransformData';
Parti = {'P1'};%Changer le nom du participant
Rep={'01';'02';'03';'04';'05';'06';'07';'8';'09';'10';'11';'12';'13';'14';'15';'16';'17';'18';'19';'20'};
NumPart=1;%Changer le nom du participant dans l'excel
Conditions=table2cell(readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Conditions.xlsx'));
Conditions=strrep(Conditions,'Agent','1');
Conditions=strrep(Conditions,'No 1','2');
Conditions=strrep(Conditions,'Reacting 1','3');
Conditions=str2double(Conditions);
A=1;
B=1;
C=1;
%%
for i=1
    for j=1:20
        NamesEMG{i,j}=sprintf('%s%sEMG.mat',Parti{i},Rep{j});
        NamesMM{i,j}=sprintf('%s%sMM.mat',Parti{i},Rep{j});
        NamesQuest{i,j}=sprintf('%s%sQuest.mat',Parti{i},Rep{j});
        if exist(NamesQuest{i,j}, 'file')==2
        if Conditions(j,NumPart)==1
            Condi='NR';
            NewNameEMG{i,j}=sprintf('%s_%s%d_EMG.mat',Parti{i},Condi,A);
            NewNameMM{i,j}=sprintf('%s_%s%d_MM.mat',Parti{i},Condi,A);
            NewNameQuest{i,j}=sprintf('%s_%s%d_Quest.mat',Parti{i},Condi,A);
            A=A+1;
        elseif Conditions(j,NumPart)==2
            Condi='NA';
            NewNameEMG{i,j}=sprintf('%s_%s%d_EMG.mat',Parti{i},Condi,B);
            NewNameMM{i,j}=sprintf('%s_%s%d_MM.mat',Parti{i},Condi,B);
            NewNameQuest{i,j}=sprintf('%s_%s%d_Quest.mat',Parti{i},Condi,B);
            B=B+1;
        elseif Conditions(j,NumPart)==3
            Condi='RA'; 
            NewNameEMG{i,j}=sprintf('%s_%s%d_EMG.mat',Parti{i},Condi,B);
            NewNameMM{i,j}=sprintf('%s_%s%d_MM.mat',Parti{i},Condi,B);
            NewNameQuest{i,j}=sprintf('%s_%s%d_Quest.mat',Parti{i},Condi,B);
            C=C+1;
        end

        movefile (NamesEMG{i,j},NewNameEMG{i,j},'f')
        movefile (NamesMM{i,j},NewNameMM{i,j},'f')
        movefile (NamesQuest{i,j},NewNameQuest{i,j},'f') 
        end
    end
end