acq= btkReadAcquisition('Calib04.c3d')
app=btkGetMarkers(acq);
[x,n]= struct2mat(app);
angle=-215;
for i=1:size(x,2)
    PPx=x(1:38,i);
    PPy=x(39:76,i);
    PPz=x(76:114,i);
    sz=size(PPx);sz=sz(1,1);
    sinagl=sind(angle);
    cosagl=cosd(angle);
    %Changing Player position 
    for n=1:sz
        PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
        PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
        PPzmod(n,1) = PPz(n,1);
    end
    x(:,i)=[PPxmod;PPymod;PPzmod];
end
btkSetMarkersValues(acq, x)
btkWriteAcquisition(acq, 'Test.c3d')