function [Body_Vel,Body_position,Participant_Quest_Position,Avatar_Position,HeelsL_position,HeelsR_position,Probed_point_contact1,Probed_point_contact2]...
    =TrigrotateV2(Body_position,Participant_Quest_Position,Avatar_Position,Heels_position,Probed_point_contact,angle)
%Function to rotate data in order to align it to the world X axis
% angle=100;
PPx=Body_position(:,1);
PPy=Body_position(:,2);
PPz=Body_position(:,3);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
% for n=1:sz
%     PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
%     PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
%     PPzmod(n,1) = PPz(n,1);
% end
% Body_position=[PPxmod,PPymod,PPzmod];

% Body_Vel=[derivate(PPxmod,1/90.023),derivate(PPymod,1/90.023),derivate(PPzmod,1/90.023)];
%%
% PPx=Participant_Quest_Position(:,1);
% PPy=Participant_Quest_Position(:,2);
% PPz=Participant_Quest_Position(:,3);
% sz=size(PPx);sz=sz(1,1);
% sinagl=sind(angle);
% cosagl=cosd(angle);
% %Changing Player position 
% for n=1:sz
%     PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
%     PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
%     PPzmod(n,1) = PPz(n,1);
% end
Participant_Quest_Position=[PPxmod,PPymod,PPzmod];

%%
PPx=Avatar_Position(:,1);
PPy=Avatar_Position(:,2);
PPz=Avatar_Position(:,3);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
for n=1:sz
    PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
    PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
    PPzmod(n,1) = PPz(n,1);
end
Avatar_Position=[PPxmod,PPymod,PPzmod];
%%
PPx=Heels_position(:,1);
PPy=Heels_position(:,2);
PPz=Heels_position(:,3);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
for n=1:sz
    PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
    PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
    PPzmod(n,1) = PPz(n,1);
end
HeelsL_position=[PPxmod,PPymod,PPzmod];
%%
PPx=Heels_position(:,4);
PPy=Heels_position(:,5);
PPz=Heels_position(:,6);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
for n=1:sz
    PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
    PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
    PPzmod(n,1) = PPz(n,1);
end
HeelsR_position=[PPxmod,PPymod,PPzmod];%,Ppxmod,Ppymod,Ppzmod];
%%
PPx=Probed_point_contact(:,1);
PPy=Probed_point_contact(:,2);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
for n=1:sz
    PPxmod(n,1) = PPx(n,1)*sinagl - PPy(n,1)*cosagl;
    PPymod(n,1) = PPx(n,1)*cosagl + PPy(n,1)*sinagl;
end
Probed_point_contact1=[PPxmod,PPymod];%,Ppxmod,Ppymod];

%%
Ppx=Probed_point_contact(:,3);
Ppy=Probed_point_contact(:,4);
sz=size(PPx);sz=sz(1,1);
sinagl=sind(angle);
cosagl=cosd(angle);
%Changing Player position 
for n=1:sz
    PPxmod(n,1) = Ppx(n,1)*sinagl - Ppy(n,1)*cosagl;
    PPymod(n,1) = Ppx(n,1)*cosagl + Ppy(n,1)*sinagl;
end
Probed_point_contact2=[PPxmod,PPymod];%,Ppxmod,Ppymod];


end

