function resultf=TheLast10(ClearanceMin)
for i = 1:28
for j = 1:16
if isempty(ClearanceMin{i, j})
% Replace the empty cell with zero
ClearanceMin{i, j} = 0;
end
end
end

%% §§§§§ A FINIR §§§§ 
% Extract the last 10 non-zero numbers from each column
ClearanceMat=cell2mat(ClearanceMin);
result = zeros(size(ClearanceMat, 1),10);
for row = 1:size(ClearanceMat,1)
    row_data = ClearanceMat(row,:);
    non_zero_indices = find(row_data ~= 0);
    if  size(non_zero_indices,2)<10
       a=1;
    else
        last_10_indices = non_zero_indices(1:10);
        result(row,:) = row_data(last_10_indices);
    end
end

resultf = reshape(result.', [], 1);

