function [Participant_Quest_Position,Participant_Quest_Orientation,Avatar_Position] = GetAvatarVariables(file_Quest)
Quest_data=load(file_Quest);
Participant_Quest_Position=Quest_data.DataQuest(:,1:3);
Participant_Quest_Orientation=Quest_data.DataQuest(:,4:6);
Avatar_Position=Quest_data.DataQuest(:,7:9);
end