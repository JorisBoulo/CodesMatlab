function [reorientation,reorientation_Magnitude] = Calcul_seq_rot(reorientation_var,title_reorientation_var,frame_dev1,frame_at_crossing,output_folder,file,subject_path,side_circumvention,Gaze_Hit,Trunk_position)


addpath(subject_path)
load('Mean_5_CatchTrials.mat')
load('Mean_5_Catch_Gaze.mat')
%% Define Variable

Head_Rot_Z_G=reorientation_var(:,1);
Head_Angular_Vel_Z=reorientation_var(:,7);
Trunk_Rot_Z_G=reorientation_var(:,2);
Trunk_Angular_Vel_Z=reorientation_var(:,8);
Pelvis_Rot_Z_G=reorientation_var(:,3);
Pelvis_Angular_Vel_Z=reorientation_var(:,9);
Head_Rot_Y_G=reorientation_var(:,4);
Trunk_Rot_Y_G=reorientation_var(:,5);
Pelvis_Rot_Y_G=reorientation_var(:,6);
Gaze_Hit_X=Gaze_Hit(:,1);
Trunk_Angular_Vel_Y=reorientation_var(:,10);


[frame ~]=size(Head_Rot_Z_G);frame=(1:frame)';

%% Standardized right side of circumvention 
%Onset identification will always seem to be right sided circumvention to
%facilitate coding and visual confirmation. After that, you will see the
%real side in the graph.

if isequal(side_circumvention,'Right')
   
    STDR_Head_Rot_Z_G=Head_Rot_Z_G;
    STDR_Head_Angular_Vel_Z=Head_Angular_Vel_Z;
    STDR_Trunk_Rot_Z_G=Trunk_Rot_Z_G;
    STDR_Trunk_Angular_Vel_Z=Trunk_Angular_Vel_Z;
    STDR_Pelvis_Rot_Z_G=Pelvis_Rot_Z_G;
    STDR_Pelvis_Angular_Vel_Z=Pelvis_Angular_Vel_Z;
    STDR_Head_Rot_Y_G=Head_Rot_Y_G;
    STDR_Trunk_Rot_Y_G=Trunk_Rot_Y_G;
    STDR_Pelvis_Rot_Y_G=Pelvis_Rot_Y_G;
    STDR_Gaze_Hit_X=Gaze_Hit_X;
    STDR_Trunk_Angular_Vel_Y=Trunk_Angular_Vel_Y;
else
    STDR_Head_Rot_Z_G=Head_Rot_Z_G*-1;
    STDR_Head_Angular_Vel_Z=Head_Angular_Vel_Z*-1;
    STDR_Trunk_Rot_Z_G=Trunk_Rot_Z_G*-1;
    STDR_Trunk_Angular_Vel_Z=Trunk_Angular_Vel_Z*-1;
    STDR_Pelvis_Rot_Z_G=Pelvis_Rot_Z_G*-1;
    STDR_Pelvis_Angular_Vel_Z=Pelvis_Angular_Vel_Z*-1;
    STDR_Head_Rot_Y_G=Head_Rot_Y_G*-1;
    STDR_Trunk_Rot_Y_G=Trunk_Rot_Y_G*-1;
    STDR_Pelvis_Rot_Y_G=Pelvis_Rot_Y_G*-1;
    STDR_Gaze_Hit_X=Gaze_Hit_X*-1;
    STDR_Trunk_Angular_Vel_Y=Trunk_Angular_Vel_Y*-1;
end



%% Head rotation onset

graph3=figure;

%  if abs(min(STDR_Head_Rot_Z_G))>=abs(Value_Head_Rot_Z_Mean)+2*Value_Head_Rot_Z_SD
    %si le minimum de rotation vers la gauche (essais standardis�s pour
    %�tre tous dans le m�me sens) n'a pas un minimum allant au-del� de 2 �cart-types de la moyenne 
    % de l'amplitude des catchs, la courbe n'affichera tout simplement pas.
response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Head_Rot_Z_G,'-')
    hold on
    title('Head')
    plot(frame,STDR_Head_Angular_Vel_Z,':')
    plot(frame(frame_dev1),Head_Rot_Z_G(frame_dev1),'o')
    plot(sdHead_Rot_Z_low,'--')
    plot(sdHead_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_head_onset=find(STDR_Head_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_head_onset=frame_head_onset+b1;
    if isempty(frame_head_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_head_onset= 'NaN'
                
                  case 'Yes'
                  frame_head_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end        
    end

hold off
break
end


%  else
%      frame_head_onset='NaN'
%  end
%% Trunk rotation onset
%  if abs(min(STDR_Trunk_Rot_Z_G))>=abs(Value_Trunk_Rot_Z_Mean)+2*Value_Trunk_Rot_Z_SD
    %si le minimum de rotation vers la gauche (essais standardis�s pour
    %�tre tous dans le m�me sens) n'a pas un minimum allant au-del� de 2 �cart-types de la moyenne 
    % de l'amplitude des catchs, la courbe n'affichera tout simplement pas.
response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Trunk_Rot_Z_G,'-')
    hold on
    title('Trunk')
    plot(frame,STDR_Trunk_Angular_Vel_Z,':')
    plot(frame(frame_dev1),Trunk_Rot_Z_G(frame_dev1),'o')
    plot(sdTrunk_Rot_Z_low,'--')
    plot(sdTrunk_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_trunk_onset=find(STDR_Trunk_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_trunk_onset=frame_trunk_onset+b1;
    if isempty(frame_trunk_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_trunk_onset= 'NaN'
                  case 'Yes'
                  frame_trunk_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end


%  else
%      frame_trunk_onset='NaN'
% end

%% Plevis Yaw onset
% if abs(min(STDR_Pelvis_Rot_Z_G))>=abs(Value_Pelvis_Rot_Z_Mean)+2*Value_Pelvis_Rot_Z_SD
    %si le minimum de rotation vers la gauche (essais standardis�s pour
    %�tre tous dans le m�me sens) n'a pas un minimum allant au-del� de 2 �cart-types de la moyenne 
    % de l'amplitude des catchs, la courbe n'affichera tout simplement pas.
response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Pelvis_Rot_Z_G,'-')
    hold on
    title('Pelvis')
    plot(frame,STDR_Pelvis_Angular_Vel_Z,':')
    plot(frame(frame_dev1),Pelvis_Rot_Z_G(frame_dev1),'o')
    plot(sdPelvis_Rot_Z_low,'--')
    plot(sdPelvis_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_pelvis_onset=find(STDR_Pelvis_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_pelvis_onset=frame_pelvis_onset+b1;
    if isempty(frame_pelvis_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_pelvis_onset= 'NaN'
                  case 'Yes'
                  frame_pelvis_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end


% else
%     frame_pelvis_onset='NaN'
% end
%% Gaze Hit
%if abs(max(STDR_Gaze_Hit_X))>=abs(Value_mean_Gaz_Hit_X)+2*Value_SD_Gaz_Hit_X
    %Le maximum (essais standardis�s pour
    %�tre tous dans le m�me sens, positif � D) doit aller au-del� de 2 �cart-types de la moyenne 
    % de l'amplitude des catchs, la courbe n'affichera tout simplement pas si ce n'est pas le cas.
response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Gaze_Hit_X,'-')
    hold on
    title('Gaze')
    plot(frame(frame_dev1),STDR_Gaze_Hit_X(frame_dev1),'o')
    plot(std_Gaz_Hit_X_low,'--')
    plot(std_Gaz_Hit_X_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_gaze_onset=find(islocalmin(STDR_Gaze_Hit_X(b1:b2,1)),1,'last');
    frame_gaze_onset=frame_gaze_onset+b1;
    if isempty(frame_gaze_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_gaze_onset= 'NaN'
                  case 'Yes'
                  frame_gaze_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end

% else
%     frame_gaze_onset='NaN'
% end


%% Roll axis 

%Head and pelvis has been remove because there no consistencies in this
%behaviour found in preliminary analysis. I still plot the curve further to
%look at it qualitatively. 

%trunk roll
if abs(min(STDR_Trunk_Rot_Y_G))>=abs(Value_Trunk_Rot_Y_Mean)+2*Value_Trunk_Rot_Y_SD
    %si le minimum de rotation vers la gauche (essais standardis�s pour
    %�tre tous dans le m�me sens) n'a pas un minimum allant au-del� de 2 �cart-types de la moyenne 
    % de l'amplitude des catchs, la courbe n'affichera tout simplement pas.
response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Trunk_Rot_Y_G,'-')
    hold on
    title('Trunk')
    plot(frame,STDR_Trunk_Angular_Vel_Y,':')
    plot(frame(frame_dev1),Trunk_Rot_Y_G(frame_dev1),'o')
    plot(sdTrunk_Rot_Y_low,'--')
    plot(sdTrunk_Rot_Y_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    [~ , frame_trunk_roll_onset]=max(STDR_Trunk_Rot_Y_G(b1:b2));
    frame_trunk_roll_onset=frame_trunk_roll_onset+b1;
    if isempty(frame_trunk_roll_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_trunk_roll_onset= 'NaN'
                  case 'Yes'
                  frame_trunk_roll_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end

else
    frame_trunk_roll_onset='NaN'
end

close all
%% Figure
%Principal variable figure: display head/trunk/pelvis yaw, gaze, ML-CoM
%displacement and trunk roll 
rot_graph=figure
subplot(2,3,1)
plot(frame,Head_Rot_Z_G,'-')
hold on
plot(frame,Head_Angular_Vel_Z,':')
plot(frame(frame_dev1),Head_Rot_Z_G(frame_dev1),'o')
if isnumeric(frame_head_onset)==1
   plot(frame(frame_head_onset),Head_Rot_Z_G(frame_head_onset),'*')
end
title('Head Yaw')
plot(sdHead_Rot_Z_low,'--')
plot(sdHead_Rot_Z_up,'--')
%legend({'position','velocity','COM onset','Rot Onset','catch','catch'})
hold off


subplot(2,3,2)
plot(frame,Trunk_Rot_Z_G,'-')
hold on
plot(frame,Trunk_Angular_Vel_Z,':')
plot(frame(frame_dev1),Trunk_Rot_Z_G(frame_dev1),'o')
if isnumeric(frame_trunk_onset)==1
   plot(frame(frame_trunk_onset),Trunk_Rot_Z_G(frame_trunk_onset),'*')
end
title('Trunk Yaw')
plot(sdTrunk_Rot_Z_low,'--')
plot(sdTrunk_Rot_Z_up,'--')
hold off

subplot(2,3,3)
plot(frame,Pelvis_Rot_Z_G,'-')
hold on
plot(frame,Pelvis_Angular_Vel_Z,':')
plot(frame(frame_dev1),Pelvis_Rot_Z_G(frame_dev1),'o')
plot(frame(frame_pelvis_onset),Pelvis_Rot_Z_G(frame_pelvis_onset),'*')
if isnumeric(frame_pelvis_onset)==1
   plot(frame(frame_pelvis_onset),Trunk_Rot_Z_G(frame_pelvis_onset),'*')
end
title('Pelvis Yaw')
plot(sdPelvis_Rot_Z_low,'--')
plot(sdPelvis_Rot_Z_up,'--')
hold off

subplot(2,3,4)
plot(frame,Gaze_Hit_X,'-')
hold on
plot(frame(frame_dev1),Gaze_Hit_X(frame_dev1),'o')
plot(frame(frame_gaze_onset),Gaze_Hit_X(frame_gaze_onset),'*')
title('Gaze Hit')
plot(std_Gaz_Hit_X_low,'--')
plot(std_Gaz_Hit_X_up,'--')
hold off

subplot(2,3,5)
plot(frame,Trunk_Rot_Y_G,'-')
hold on
plot(frame,Trunk_Angular_Vel_Y,':')
plot(frame(frame_dev1),Trunk_Rot_Y_G(frame_dev1),'o')
if isnumeric(frame_trunk_roll_onset)==1
   plot(frame(frame_trunk_roll_onset),Trunk_Rot_Y_G(frame_trunk_roll_onset),'*')
end
title('Trunk Roll')
plot(sdTrunk_Rot_Y_low,'--')
plot(sdTrunk_Rot_Y_up,'--')
hold off

subplot(2,3,6)
plot(frame,Trunk_position(:,1))
hold on
plot(frame(frame_dev1),Trunk_position(frame_dev1,1),'o')
title('Trunk Displacement')
plot(sdXtrunk_low,'--')
plot(sdXtrunk_up,'--')
hold off


figure_reorientation_file=fullfile(output_folder,strrep(file, '.xlsx', '_rot_Yaw'));
saveas(rot_graph,figure_reorientation_file,'png')



%Roll axis figure (I kept this part, but not used anymore. No event are
%identified in roll axis, so nothing is displayed.)

roll_graph=figure
subplot(3,1,1)
plot(frame,Head_Rot_Y_G,'-')
hold on
plot(frame(frame_dev1),Head_Rot_Y_G(frame_dev1),'o')
title('Head roll')
hold off
subplot(3,1,2)
plot(frame,Trunk_Rot_Y_G,'-')
hold on
plot(frame(frame_dev1),Trunk_Rot_Y_G(frame_dev1),'o')
title('Trunk roll')
hold off
subplot(3,1,3)
plot(frame,Pelvis_Rot_Y_G,'-')
hold on
plot(frame(frame_dev1),Pelvis_Rot_Y_G(frame_dev1),'o')
title('Pelvis roll')
hold off

figure_reorientation_file=fullfile(output_folder,strrep(file, '.xlsx', '_rot_Roll'));
saveas(roll_graph,figure_reorientation_file,'png')
%% Onset relative to the time of crossing
if isnumeric(frame_head_onset)==1
    onset_time_head_yaw=(frame_at_crossing-frame_head_onset)/90;
else
    onset_time_head_yaw='NaN'
end

if isnumeric(frame_trunk_onset)==1
    onset_time_trunk_yaw=(frame_at_crossing-frame_trunk_onset)/90;
else
   onset_time_trunk_yaw='NaN'
end

if isnumeric(frame_pelvis_onset)==1    
    onset_time_pelvis_yaw=(frame_at_crossing-frame_pelvis_onset)/90;
else
    onset_time_pelvis_yaw='NaN'
end

if isnumeric(frame_gaze_onset)==1
onset_time_gaze=(frame_at_crossing-frame_gaze_onset)/90;
else
    onset_time_gaze='NaN'
end

trunk_ML_COM_onset=(frame_at_crossing-frame_dev1)/90;
frame_trunk_ML_COM_onset=frame_dev1; 

if isnumeric(frame_trunk_roll_onset)==1
    onset_time_trunk_roll=(frame_at_crossing-frame_trunk_roll_onset)/90;
else
   onset_time_trunk_roll='NaN'
end


%% Magnitude 


magnitude=[Head_Rot_Z_G,Trunk_Rot_Z_G,Pelvis_Rot_Z_G,Head_Rot_Y_G,Trunk_Rot_Y_G,Pelvis_Rot_Y_G];
cut_magnitude=magnitude(1:frame_at_crossing,:);

mean_rot=mean(abs(cut_magnitude));
mean_gaze_location=mean(abs(Gaze_Hit_X(1:frame_at_crossing)));

if isequal(side_circumvention,'Right')
 %min correspond � tourner � droite et pour les autres variables, le min
 %correspond � la FLG (roll c�t� oppos� yaw donc �a marche)
    [max_rot frame_max_rot]=min(cut_magnitude);
    [max_gaze_location max_gaze_frame]=max(Gaze_Hit_X);
    range_gaze=abs(max(Gaze_Hit_X)-min(Gaze_Hit_X(1:max_gaze_frame)));
    range_rot_head_yaw=abs(min(cut_magnitude(:,1))-max(cut_magnitude(1:frame_max_rot(1),1)));
    range_rot_trunk_yaw=abs(min(cut_magnitude(:,2))-max(cut_magnitude(1:frame_max_rot(2),2)));
    range_rot_pelvis_yaw=abs(min(cut_magnitude(:,3))-max(cut_magnitude(1:frame_max_rot(3),3)));
    range_rot_head_roll=abs(min(cut_magnitude(:,4))-max(cut_magnitude(1:frame_max_rot(4),4)));
    range_rot_trunk_roll=abs(min(cut_magnitude(:,5))-max(cut_magnitude(1:frame_max_rot(5),5)));
    range_rot_pelvis_roll=abs(min(cut_magnitude(:,6))-max(cut_magnitude(1:frame_max_rot(6),6)));
    range_rot=[range_rot_head_yaw,range_rot_trunk_yaw,range_rot_pelvis_yaw,range_rot_head_roll,range_rot_trunk_roll,range_rot_pelvis_roll,range_gaze];
    
else 
    max_rot=max(cut_magnitude)*-1; %*-1 pour standardiser comme si le contournement avait lieu � droite.
    [~ , frame_max_rot]=max(cut_magnitude);
    max_gaze_location=min(Gaze_Hit_X)*-1;
    [~ , max_gaze_frame]=min(Gaze_Hit_X);
    range_gaze=abs(min(Gaze_Hit_X)-max(Gaze_Hit_X(1:max_gaze_frame)));
    range_rot_head_yaw=abs(max(cut_magnitude(:,1))-min(cut_magnitude(1:frame_max_rot(1),1)));
    range_rot_trunk_yaw=abs(max(cut_magnitude(:,2))-min(cut_magnitude(1:frame_max_rot(2),2)));
    range_rot_pelvis_yaw=abs(max(cut_magnitude(:,3))-min(cut_magnitude(1:frame_max_rot(3),3)));
    range_rot_head_roll=abs(max(cut_magnitude(:,4))-min(cut_magnitude(1:frame_max_rot(4),4)));
    range_rot_trunk_roll=abs(max(cut_magnitude(:,5))-min(cut_magnitude(1:frame_max_rot(5),5)));
    range_rot_pelvis_roll=abs(max(cut_magnitude(:,6))-min(cut_magnitude(1:frame_max_rot(6),6)));
    range_rot=[range_rot_head_yaw,range_rot_trunk_yaw,range_rot_pelvis_yaw,range_rot_head_roll,range_rot_trunk_roll,range_rot_pelvis_roll,range_gaze];
end

%range: range from beginning of the trial to
%frame_at_crossing. Idea of the range is mostly for the head. If the
%participant have a different zero head yaw from the calibration, it could
%create an offset.


%% Export to main code
reorientation_Magnitude={'Max_Head_Yaw','Mean_Abs_Head_Yaw','Max_Trunk_Yaw','Mean_Abs_Trunk_Yaw',...
    'Max_Pelvis_Yaw','Mean_Abs_Pelvis_Yaw','Max_Head_Roll','Mean_Abs_Head_Roll','Max_Trunk_Roll',...
    'Mean_Abs_Trunk_Roll','Max_Pelvis_Roll','Mean_Abs_Pelvis_Roll','mean_gaze_location','max_gaze_location',...
    'range_rot_head_yaw','range_rot_trunk_yaw','range_rot_pelvis_yaw','range_rot_head_roll','range_rot_trunk_roll','range_rot_pelvis_roll','range_gaze';...
    max_rot(1),mean_rot(1),...
    max_rot(2),mean_rot(2),max_rot(3),mean_rot(3),max_rot(4),mean_rot(4),max_rot(5)...
    ,mean_rot(5),max_rot(6),mean_rot(6),mean_gaze_location,max_gaze_location,range_rot(1),range_rot(2)...
    ,range_rot(3),range_rot(4),range_rot(5),range_rot(6), range_rot(7)};


reorientation={'onset_time_head_yaw','onset_time_trunk_yaw','onset_time_pelvis_yaw','trunk_ML_COM_onset','onset_time_trunk_roll','onset_time_gaze','frame_head_yaw_onset','frame_trunk_yaw_onset','frame_pelvis_yaw_onset','frame_trunk_ML_COM_onset,','frame_trunk_roll_onset','frame_gaze_onset'...
               ;onset_time_head_yaw,onset_time_trunk_yaw,onset_time_pelvis_yaw,trunk_ML_COM_onset,onset_time_trunk_roll,onset_time_gaze,           frame_head_onset,      frame_trunk_onset,     frame_pelvis_onset,frame_trunk_ML_COM_onset          ,frame_trunk_roll_onset,frame_gaze_onset};
end

