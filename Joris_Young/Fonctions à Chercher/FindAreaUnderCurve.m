function Answer=FindAreaUnderCurve(Brust,DataNorm)
for i=1:size(DataNorm,2)
%     % Find the indices of the onset and offset of the burst
%     [~,onset] = find(Brust(1,i), 1, 'first');
%     [~,offset] = find(Brust(1:i), 1, 'last');
%     % Extract the EMG signal for the current muscle
    emg_signal = DataNorm(:,i);
    emg_signal=emg_signal(Brust(:,i))
    % Calculate the area under the curve using the trapezoidal rule
    Answer(i)=trapz(emg_signal);
end 
