function [frame_dev1, frame_dev2] = Calcul_Deviation(Body_position,Body_Vel,Avatar_Position,CrossingFrame,subject_path,output_folder,file,DataEMG)
%% Deviation points identification
close all
addpath(subject_path)
load('Mean_5_CatchTrials.mat')
n=numel(Body_position(:,1));zero=zeros(n,1);
response = 'Do it again';
[~,HeelLFramesEMG]=findpeaks(DataEMG(2:end,end),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',200);
[~,HeelRFramesEMG]=findpeaks(DataEMG(2:end,12),'MinPeakHeight',-3,'MinPeakDistance',1700,'MaxPeakWidth',200);
HeelLFrames=round((HeelLFramesEMG/2250)*90);
HeelRFrames=round((HeelRFramesEMG/2250)*90);
%  while (strcmp(response,'Do it again'));
   plot(Body_position(HeelLFrames(1,1):HeelLFrames(4,1),2),Body_position(HeelLFrames(1,1):HeelLFrames(4,1),1))
   hold on
   plot(meanYtrunk,sdXtrunk_up,'--')
   plot(meanYtrunk,sdXtrunk_low,'--')
   plot(Avatar_Position(:,2),Avatar_Position (:,1),':r')
   [x,y]=ginput(2);
   b1=find(Body_position(:,2)>x(1));
   b2=find(Body_position(:,2)<x(2));b2=b2(end);
   cut_Body_position=Body_position(b1(1,1):b2,:);  
   c1=find(meanYtrunk(:,1)>x(1),1);
   c2=find(meanYtrunk(:,1)<x(2));c2=c2(end);
   cut_sdXtrunk_up=sdXtrunk_up(c1:c2,:);   
   [xi,yi] = polyxpoly(cut_Body_position(:,1),[1:1:size(cut_Body_position)], cut_sdXtrunk_up,[1:1:size(cut_sdXtrunk_up)]);
   frame_dev1=round(yi)+b1(1,1);
   plot(Body_position(frame_dev1,2),Body_position(frame_dev1,1),'*');
   %%
   close all
   plot([1:1:(HeelLFrames(4,1)-HeelLFrames(1,1))+1],Body_Vel(HeelLFrames(1,1):HeelLFrames(4,1),1))
   hold on
   plot([1:1:size(sdXtrunk_Vel_up)],sdXtrunk_Vel_up,'--')
   plot([1:1:size(sdXtrunk_Vel_low)],sdXtrunk_Vel_low,'--')
   plot(Avatar_Position(:,2),Avatar_Position (:,1),':r')
   [x,y]=ginput(2);
%    b1=find(Body_Vel(:,2)>x(1));
%    b2=find(Body_Vel(:,2)<x(2));b2=b2(end);
   cut_Body_vel=Body_Vel(round(x(1,1)):round(x(2,1)),:);  
%    c1=find(meanYtrunk(:,1)>x(1),1);
%    c2=find(meanYtrunk(:,1)<x(2));c2=c2(end);
   cut_sdXtrunkVel_up=sdXtrunk_Vel_up(round(x(1,1)):round(x(2,1)),:); 
   [xi,yi] = polyxpoly(cut_Body_vel(:,1),[1:1:size(cut_Body_vel)], cut_sdXtrunkVel_up,[1:1:size(cut_sdXtrunkVel_up)]);
   frame_dev2=round(yi)+round(x(1,1));
%    plot([1:1:size(Body_Vel)],Body_Vel(frame_dev2,1),'*');
   %%
%    
%    
%    
%    
%    if cut_Body_position(end,1)==Trunk_min
%      response = questdlg('The minimum detected is equal to the second click. Would you like to to do it again or select points on trunk velocity graph?','error','Do it again','Velocity graph','Do it again'); 
%      if response == 'Velocity graph'
%          plot(Body_position(:,2),Body_Vel(:,1))
%          hold on
%          plot(meanYtrunk,sdXtrunk_Vel_up,'--')
%          plot(meanYtrunk,sdXtrunk_Vel_low,'--')
%          plot(Body_position(:,2),zero,':r')
%          [x, ~]=ginput(2);
%          b1=find(Body_position(:,2)>x(2),1);
%          b2=find(Body_position(:,2)<x(1));b2=b2(end);
%          cut_Body_Vel=Body_Vel(b1:b2,:);
%          [A, B]=min(cut_Body_Vel(:,1));
%          if A<0
%              [x, ~]=ginput(2);
%              b1=find(Body_position(:,2)>x(2),1);
%              b2=find(Body_position(:,2)<x(1));b2=b2(end);
%              cut_Body_Vel=Body_Vel(b1:b2,:);
%              B=find(cut_Body_Vel(:,1)>=0,1,'first');
%          end
%          hold off
%          frame_dev1=(b1+B);
%      
%     
%      end 
%   
%    end
%%
subplot(2,1,1)
plot(Body_position(:,2),Body_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(Body_position(:,2),zero,':r')
plot(Body_position(frame_dev1,2),Body_position(frame_dev1,1),'*');
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Body_position(:,2),Body_Vel(:,1))
plot(Body_position(:,2),zero,':r')
plot(Body_position(frame_dev2,2),Body_Vel(frame_dev2,1),'*')
hold off
% %%

graph2=figure;
plot(Body_position(:,2),Body_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Body_position(:,2),zero,':r')
Identify the window within you want the program to search the ML velocity
local min
[x, ~]=ginput(2);
b1=find(Body_position(:,2)>x(2),1);
b2=find(Body_position(:,2)<x(1));b2=b2(end);
cut_Body_Vel=Body_Vel(b1:b2,:);
Max_vel=max(Body_Vel(:,1));
[B,~]=find(islocalmin(cut_Body_Vel(:,1),'MinProminence',0.05*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_Body_Vel(:,1)),1,'last')
end
%
Local min must have a proeminence of 5% of max ML velocity to make sure
it is not only a lack of fluidity
A=cut_Body_Vel(B,1);
if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 title('if local min is negative, take the first point it become positive')
 b1=find(Body_position(:,2)>x(2),1);
 b2=find(Body_position(:,2)<x(1));b2=b2(end);
 cut_Body_Vel=Body_Vel(b1:b2,:);
 B=find(cut_Body_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all


    inverse_Body_position=[Body_position(:,1)*-1,Body_position(:,2:3)];
    inverse_Body_Vel=[Body_Vel(:,1)*-1,Body_Vel(:,2:3)];
    
  response = 'Do it again';
while (strcmp(response,'Do it again'));
   plot(inverse_Body_position(:,2),inverse_Body_position(:,1))
   hold on
   plot(meanYtrunk,sdXtrunk_up,'--')
   plot(meanYtrunk,sdXtrunk_low,'--')
   plot(inverse_Body_position(:,2),zero,':r')
   [x, ~]=ginput(2);
   b1=find(inverse_Body_position(:,2)>x(2),1);
   b2=find(inverse_Body_position(:,2)<x(1));b2=b2(end);
   cut_inverse_Body_position=inverse_Body_position(b1:b2,:);  
   [Trunk_min, frame_dev1]=min(cut_inverse_Body_position(:,1));
   frame_dev1=frame_dev1+b1;
   plot(inverse_Body_position(frame_dev1,2),inverse_Body_position(frame_dev1,1),'*');
   graph1=figure;
   if cut_inverse_Body_position(1,1)==Trunk_min;
     response = questdlg('The minimum detected is equal to the second click. Would you like to to do it again or select points on trunk velocity graph?','error','Do it again','Velocity graph','Do it again'); 
     if response == 'Velocity graph'
         plot(inverse_Body_position(:,2),inverse_Body_Vel(:,1))
         hold on
         plot(meanYtrunk,sdXtrunk_Vel_up,'--')
         plot(meanYtrunk,sdXtrunk_Vel_low,'--')
         plot(inverse_Body_position(:,2),zero,':r')
         [x, ~]=ginput(2);
         b1=find(inverse_Body_position(:,2)>x(2),1);
         b2=find(inverse_Body_position(:,2)<x(1));b2=b2(end);
         cut_inverse_Body_Vel=inverse_Body_Vel(b1:b2,:);
         [A, B]=min(cut_inverse_Body_Vel(:,1));
         if A<0
             [x, ~]=ginput(2);
             b1=find(inverse_Body_position(:,2)>x(2),1);
             b2=find(inverse_Body_position(:,2)<x(1));b2=b2(end);
             cut_inverse_Body_Vel=inverse_Body_Vel(b1:b2,:);
             B=find(cut_inverse_Body_Vel(:,1)>=0,1,'first');
         end
         hold off
         frame_dev1=(b1+B);
     
     end
   else break 
   end 
end
subplot(2,1,1)
plot(inverse_Body_position(:,2),inverse_Body_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(inverse_Body_position(:,2),zero,':r')
plot(inverse_Body_position(frame_dev1,2),inverse_Body_position(frame_dev1,1),'*');
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Body_position(:,2),Body_Vel(:,1))
plot(inverse_Body_position(:,2),zero,':r')
plot(Body_position(frame_dev1,2),Body_Vel(frame_dev1,1),'*')
hold off
 
 
graph2=figure;
plot(inverse_Body_position(:,2),inverse_Body_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(inverse_Body_position(:,2),zero,':r')
%Identify the window within you want the program to search the ML velocity
%local min
[x, ~]=ginput(2);
b1=find(inverse_Body_position(:,2)>x(2),1);
b2=find(inverse_Body_position(:,2)<x(1));b2=b2(end);
cut_inverse_Body_Vel=inverse_Body_Vel(b1:b2,:);
Max_vel=max(inverse_Body_Vel(:,1));
[B,~]=find(islocalmin(cut_inverse_Body_Vel(:,1),'MinProminence',0.1*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_inverse_Body_Vel(:,1)),1,'last')
end

%Local min must have a proeminence of 5% of max ML velocity to make sure
%it is not only a lack of fluidity
A=cut_inverse_Body_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(inverse_Body_position(:,2)>x(2),1);
 b2=find(inverse_Body_position(:,2)<x(1));b2=b2(end);
cut_inverse_Body_Vel=inverse_Body_Vel(b1:b2,:);
 B=find(cut_inverse_Body_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all
   



graph_dev=figure
subplot(2,1,1)
plot(Body_position(:,2),Body_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(Body_position(:,2),zero,':r')
plot(Body_position(frame_dev1,2),Body_position(frame_dev1,1),'*')
plot(Body_position(frame_dev2,2),Body_position(frame_dev2,1),'o')
title('Trunk Position')
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Body_position(:,2),Body_Vel(:,1))
plot(Body_position(:,2),zero,':r')
plot(Body_position(frame_dev1,2),Body_position(frame_dev1,1),'*')
plot(Body_position(frame_dev2,2),Body_Vel(frame_dev2,1),'o')
title('Trunk Velocity')
hold off
figure_dev_file=fullfile(output_folder,strrep(file, '.xlsx', '_dev'));
saveas(graph_dev,figure_dev_file,'png')
waitfor(graph_dev)
 

% Deviation point calculated relatived to the avatar position

distance_dev1_avatar=Avatar_Position(frame_dev1,2)-Body_position(frame_dev1,2);
distance_dev2_avatar=Avatar_Position(frame_dev2,2)-Body_position(frame_dev2,2);
Time_dev1_passing=(CrossingFrame-frame_dev1)/90;
Time_dev2_passing=(CrossingFrame-frame_dev2)/90;
frameEXP_dev1=frame_dev1-1; %to get the real frame of EXP file that start to 0
frameEXP_dev2=frame_dev2-1; %to get the real frame of EXP file that start to 0

% Complementary variables: Maximal velocity 


dev1_Trunk_X_vel=Body_Vel(frame_dev1:frame_dev2,1);
dev2_Trunk_X_vel=Body_Vel(frame_dev2:CrossingFrame,1);

absolute value to help to compare R and L circumv trials
vel_at_onset_X_dev1=abs(Body_Vel(frame_dev1,1));vel_at_onset_X_dev2=abs(Body_Vel(frame_dev2,1));
rate_of_ML_avoidance=abs(mean(Body_Vel(frame_dev1:CrossingFrame,1))); 



max_dev1_X_vel=max(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
max_dev2_X_vel=max(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
min_dev1_X_vel=min(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
min_dev2_X_vel=min(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);



% Velocity calcul 
Scalar_Velocity=sqrt((Body_Vel(:,1)).^2+(Body_Vel(:,2)).^2+(Body_Vel(:,3)).^2);
mean_scalar_vel_dev1_crossing=mean(Scalar_Velocity(frame_dev1:CrossingFrame));
mean_scalar_vel_dev1_dev2=mean(Scalar_Velocity(frame_dev1:frame_dev2));
mean_scalar_vel_dev2_crossing=mean(Scalar_Velocity(frame_dev2:CrossingFrame));

velocity 1.5m (pour enlever la phase d'acc�l�ration) to crossing
frame_start=find(Body_position(:,2)>=-3.3,1);
mean_scalar_vel_one_p_five_cros= mean(Scalar_Velocity(frame_start:CrossingFrame));
max_scalar_vel_one_p_five_cros= max(Scalar_Velocity(frame_start:CrossingFrame));
min_scalar_vel_one_p_five_cros= min(Scalar_Velocity(frame_start:CrossingFrame));

% Range of deviation
pour un contournement � droite
Max_dev=max(Body_position(frame_dev1:CrossingFrame))-min(Body_position(frame_dev1:CrossingFrame));
% Exporter les variables
dev={'FrameEXP_dev1','Dev1_longitudinal_axis','Distance_dev1_avatar','Dev1_onset_VelocityX',...
    'Max_dev1_X_vel','Min_dev1_X_vel','Mean_dev1_X_vel','Time_dev1_passing','FrameEXP_dev2','Dev2_axe_longitudinal',...
    'Distance_dev2_avatar','Dev2_onset_VelocityX','Max_dev2_X_vel','Min_dev2_X_vel','Mean_dev2_X_vel','Time_dev2_passing',...
    'Rate_ML_avoidance','mean_scalar_vel_dev1_crossing','mean_scalar_vel_dev1_dev2','mean_scalar_vel_dev2_crossing','frame_dev1','frame_dev2'...
    'mean_scalar_vel_1.5_crossing','max_scalar_vel_1.5_crossing','min_scalar_vel_1.5_crossing','Max_dev(range)',;...
    frameEXP_dev1,Body_position(frame_dev1,2),distance_dev1_avatar,vel_at_onset_X_dev1,max_dev1_X_vel,min_dev1_X_vel,...
    mean_dev1_X_vel,Time_dev1_passing,frameEXP_dev2,Body_position(frame_dev2,2),distance_dev2_avatar,...
    vel_at_onset_X_dev2,max_dev2_X_vel,min_dev2_X_vel,mean_dev2_X_vel,Time_dev2_passing,rate_of_ML_avoidance,...
    mean_scalar_vel_dev1_crossing,mean_scalar_vel_dev1_dev2,mean_scalar_vel_dev2_crossing,frame_dev1,frame_dev2,...
    mean_scalar_vel_one_p_five_cros,max_scalar_vel_one_p_five_cros,min_scalar_vel_one_p_five_cros,Max_dev};
excel_file = fullfile(strrep(file_EXP, '.xlsx', '_dev.xlsx'));
xlswrite(excel_file,dev);

end

