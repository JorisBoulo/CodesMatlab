function Answer=FindCoContraction(DataBrut)
LTA=DataBrut(:,11);
RTA=DataBrut(:,10);
LGL=DataBrut(:,9);
RGL=DataBrut(:,8);
LAdd=DataBrut(:,7);
RAdd=DataBrut(:,6);
LGlu=DataBrut(:,5);
RGlu=DataBrut(:,4);
Name={'LTA/LGL';'RTA/RGL';'LGlu/LAdd';'RGlu/RAdd';'LTA/RTA';'LGL/RGL';'Ladd/RAdd';'LGlu/RGlu'};
t = linspace(0, 1, 1000);
Data=[(trapz(t,min(LTA,LGL))/(trapz(t,LTA)+trapz(t,LGL)))*100; (trapz(t,min(RTA,RGL))/(trapz(t,RTA)+trapz(t,RGL)))*100;(trapz(t,min(LGlu,LAdd))/(trapz(t,LGL)+trapz(t,LAdd)))*100;(trapz(t,min(RGlu,RAdd))/(trapz(t,RGlu)+trapz(t,RAdd)))*100;...
    (trapz(t,min(LTA,RTA))/(trapz(t,LTA)+trapz(t,RTA)))*100;(trapz(t,min(LGL,RGL))/(trapz(t,LGL)+trapz(t,RGL)))*100;(trapz(t,min(LAdd,RAdd))/(trapz(t,LAdd)+trapz(t,RAdd)))*100;(trapz(t,min(RGlu,LGlu))/(trapz(t,RGlu)+trapz(t,LGlu)))*100];
Answer=Data;

