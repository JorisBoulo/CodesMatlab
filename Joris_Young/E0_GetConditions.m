clear all
A = [ones(1,8),2.*ones(1,6),3.*ones(1,13)];
A_shuffled = A(randperm(numel(A))); 
A_shuffled =string(A_shuffled);
Conditions=strrep(A_shuffled,'1','NA');
Conditions=strrep(Conditions,'2','TA');
Conditions=strrep(Conditions,'3','RA');
Conditions=Conditions';
    a=1;
    b=1;
    c=1;
for x=1:size(Conditions,1) 
   if Conditions(x,1)== 'NA'
      Conditions(x,1)=strcat('NA',string(a)); 
      a=a+1;
   end
   if Conditions(x,1)== 'TA'
      Conditions(x,1)=strcat('TA',string(b)); 
      b=b+1;
   end
   if Conditions(x,1)== 'RA'
      Conditions(x,1)=strcat('RA',string(c)); 
      c=c+1;
   end
end 