clear all
output_file='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\MainResultsEtude1.xlsx';
subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\';
Parti={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26'};
Condi={'RA'};
Rep={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
XlsName = {'P01','P02','P03','P04','P05','P06','P07','P08','P09','P10','P11','P12','P13','P14','P15','P16','P17','P18','P19','P20','P21','P22','P23','P24','P25','P26','P27','P28','P29','P30'};
Output='\3_Output\';
%              
LoLi={};
CoSo={};
Clearance=[];
Dev1=[];
Dev2=[];
error=0;
VariablesPapier=readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 1\Participants\VariablesPapiers.xlsx');
Actif=table2cell(VariablesPapier(:,12));
for t=1:26
    for i=1
        for j=1:15
            FullName{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Output,(sprintf('P%s_RA%sStrides.mat',Parti{t},Rep{j}))));
            if exist(FullName{j,t}, 'file')==2
                load(FullName{j,t});
%                 [NameRow, colInd] = find(strcmp(Data.MMData(:,1), 'Frame #'));
%                 Straittxt=(Data.MMData(NameRow,:));
                
%                 Data=str2double(string(Data.MMData(NameRow+1:end,:)));
%                 [n_line, n_column]=size(Data);
%                 %
%                 Title_line=1;
%                 for i=1:n_column
%                 if ~isempty(strfind(Straittxt{Title_line,i},'Body_X'))
%                 Body_X_pos_Ind=i;
%                 elseif ~isempty(strfind(Straittxt{Title_line,i},'Body_Y'))
%                 Body_Y_pos_Ind=i;
%                 end
%              end
                if strcmp(Actif(t,1),'Actif')==1
                    try
                        %% Co-contrcction 
                        if  strcmp(char(Strides.Indices.LOLI(1,1)),'LeadIn')==1
                            P0cc=FindCoContraction(Strides.Left.P0.NormAmp);
                            P_1cc=FindCoContraction(Strides.Left.P_1.NormAmp);
                            P_2cc=FindCoContraction(Strides.Left.P_2.NormAmp);
                        else 
                            P0cc=FindCoContraction(Strides.Right.P0.NormAmp);
                            P_1cc=FindCoContraction(Strides.Right.P_1.NormAmp);
                            P_2cc=FindCoContraction(Strides.Right.P_2.NormAmp);
                        end
                        Name={'LTA/LGL';'RTA/RGL';'LGlu/LAdd';'RGlu/RAdd';'LTA/RTA';'LGL/RGL';'Ladd/RAdd';'LGlu/RGlu'};
                        CoContraction=mean([P0cc,P_1cc,P_2cc],2);
                        figure(1)
                        hold on
                        scatter (1,CoContraction(1,1),'r')
                        LTA_GL{t,j}=CoContraction(1,1);
                        RTA_GL{t,j}=CoContraction(2,1);
                        LGLu_Add{t,j}=CoContraction(3,1);
                        RGLu_Add{t,j}=CoContraction(4,1);
%                       %%  Max Accélération LAtérale 
%                         DataBody=Data(50:400,Body_X_pos_Ind);
%                         [b,a]=butter(2,1/90,'low');
%                         lowpassed=filtfilt(b,a,DataBody);
%                         VDevML{t,j}=max(derivate(lowpassed,1/90));
                        %%
%                        figure(4)
%                        fig = gcf;
%                        fig.Position=[1,1,1440,800];
%                        hold on
%                        plot(lowpassed,'r')
                       
                    catch
                        a=1;
                    end
                else 
                    try
                        %% Co-contrcction 
                        if  strcmp(char(Strides.Indices.LOLI(1,1)),'LeadIn')==1
                            P0cc=FindCoContraction(Strides.Left.P0.NormAmp);
                            P_1cc=FindCoContraction(Strides.Left.P_1.NormAmp);
                            P_2cc=FindCoContraction(Strides.Left.P_2.NormAmp);
                        else 
                            P0cc=FindCoContraction(Strides.Right.P0.NormAmp);
                            P_1cc=FindCoContraction(Strides.Right.P_1.NormAmp);
                            P_2cc=FindCoContraction(Strides.Right.P_2.NormAmp);
                        end
                        Name={'LTA/LGL';'RTA/RGL';'LGlu/LAdd';'RGlu/RAdd';'LTA/RTA';'LGL/RGL';'Ladd/RAdd';'LGlu/RGlu'};
                        CoContraction=mean([P0cc,P_1cc,P_2cc],2);
                        figure(1)
                        hold on
                        scatter (2,CoContraction(1,1),'b')
                        LTA_GL{t,j}=CoContraction(1,1);
                        RTA_GL{t,j}=CoContraction(2,1);
                        LGLu_Add{t,j}=CoContraction(3,1);
                        RGLu_Add{t,j}=CoContraction(4,1);
%                     figure(1)
%                     hold on
%                     plot(Strides.Indices.Traj(:,1),'b')

%                     figure(2)
%                     hold on 
%                     plot(Strides.Indices.Traj(:,3),'b')
%                     A=Strides.Indices.Dev{2, 2};
%                     figure(3)
%                     hold on 
%                     plot(2,Strides.Indices.DistanceDev1,'O','MarkerEdgeColor','b')
%                         %% Max Accélération LAtérale  
%                        DataBody=Data(50:400,Body_X_pos_Ind);
%                         [b,a]=butter(2,1/90,'low');
%                         lowpassed=filtfilt(b,a,DataBody);
%                         
%                         VDevML{t,j}=max(derivate(lowpassed,1/90));
%                        figure(4)
%                        fig = gcf;
%                        fig.Position=[1,1,1440,800];
%                        hold on
%                        plot(lowpassed,'b')
                       

                    catch
                    a=1;
                    end
                end
            end
        end
    end
end




        