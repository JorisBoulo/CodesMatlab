%% 
% *In this script, the goal is to calculate dependant variables with EXP files 
% produced by motion monitor. Before using this matlab code, the EXP files have 
% to be converted in excel files et passed threw the Nick's program. The data 
% from Unreal are in the result file produced by Nick program and this data 
% is aligned to data from motion monitor. *
% 
% *Variables calculated in this code are:*
% 
% -Clearance shoulder to shoulder and center to center before crossing point
% 
% -Deviation point. The first one as the local minimum preceding the exit from 
% two standard deviation of catch trials and the second one as the local minimum 
% preceding the peak in the ML CoM trunk velocity
% 
% -Sequence of reorientation: The onset of head, trunk rotation and Dev-1 and 
% calculated in function of the time before crossing. The goal is to see if the 
% stratety is "en bloc" are not. Trunk roll and head roll are also
% investiguated. 
% 
% -Fluidity: Zero crossing method is applied in the window of Dev1 to crossing 
% point. Other variables such as the integrated jerk or maximal ML velocity/acc 
% are also calcultated for complementary analysis if needed. 
% 
% *This code imply many manual intervention or verification to make sure that 
% the good point are identified. Figures are saved to verify uniformity of point 
% identified further.  *
% 
% *Output: 1 excel file containing all the main variables will be produced. 
% Another one containing complementary variables will be produced for further 
% analysis. *

clearvars;clc;
%% 
% *Open files*

subject_path=uigetdir('C:\Data\Projet_Vampire_2.0');
if isequal(subject_path,0)
    return;
end

output_folder = uigetdir(subject_path, 'Select Results Output Folder');
if (isequal(output_folder, 0))
    return;
end

%Avatar have different width depending sexe and age, which impact the
%clearance
list={'Young Man','Young Female','Aged Man','Aged Woman'};
sex=listdlg('PromptString','Select the avatar:','SelectionMode','single','Name','Avatar','Liststring',list);
switch sex
    case 'Young Man'
        sex=1;
    case 'Young Female'
        sex=2;
    case 'Aged Man'
        sex=3;
    case 'Aged Woman'
        sex=4;
end

answer = 'Yes';

while (strcmp(answer,'Yes'))

%open motion monitor file
[file, EXP_path]=uigetfile({'*.xlsx';'Excel Files (*.xlsx)';'*.*'},'Select Motion Monitor File',subject_path);
if (isequal(file, 0))
    return;
end
file_EXP=fullfile(EXP_path,file);
file_Nick=strrep(file,'.xlsx', '_Results.xls');
file_Nick=fullfile(EXP_path,'results',file_Nick);

%open Nick's file
% [file_N, Nick_path]=uigetfile({'*.xls';'*.*'},'Select Result File',EXP_path);
% if (isequal(file, 0))
%     return;
% end
% file_Nick=fullfile(Nick_path,file_N);


%% 
% *Importing variable from motion monitor and vive files*


[Trunk_position,Trunk_Vel,Trunk_Acc,probed_point_contact,title_probed_point_contact,...
    reorientation_var,title_reorientation_var]...
    = GetMotionMonitorVariables(file_EXP);

[Avatar_Position,Gaze_Hit,Nick_Results] = GetAvatarVariables(file_Nick);

frame_at_crossing=cell2mat(Nick_Results(2,1));
[~, frame_at_crossing2]=min(abs(Trunk_position(:,2)-Avatar_Position(:,2)));frame_at_crossing2=frame_at_crossing2-1;
%comparing Nick input and this code input to make sure everything is fine
if frame_at_crossing~=frame_at_crossing2
    warndlg('Difference with the actual code and the frame at crossing calculated by Nick')
end
%% 
% *Clearance and contact identification*

[clearance, side_circumvention]= Calcul_Clearance(Trunk_position,Avatar_Position,probed_point_contact,frame_at_crossing,sex);
%% 
% *Deviation points*

[deviation_points,frame_dev1, frame_dev2,Scalar_Velocity]=Calcul_Deviation(Trunk_position,Trunk_Vel,Avatar_Position,frame_at_crossing,side_circumvention,subject_path,output_folder,file);
%% 
% *Sequence of reorientation *

%Le regard avec le gaze Hit

[reorientation,reorientation_Magnitude]=Calcul_seq_rot(reorientation_var,title_reorientation_var,frame_dev1,frame_at_crossing,output_folder,file,subject_path,side_circumvention,Gaze_Hit,Trunk_position);
%% 
% *Fluidity*

fluidity = Calcul_Fluidity(Trunk_position,Trunk_Vel,Trunk_Acc,frame_dev1,frame_dev2,frame_at_crossing,output_folder,file);
%% 
% *Export the results*

%%Big file with everything:
Variables_All=[clearance,deviation_points,reorientation,reorientation_Magnitude,fluidity];

Variables_Key=[clearance(:,[1:4]),deviation_points(:,[3,7,8,11,15,16,23:26]),reorientation(:,[1:6]),reorientation_Magnitude(:,[1,3,5,10,14,15:17,19,21]),fluidity(:,[1:6])];

Out_Data=[Trunk_position,Trunk_Vel,Trunk_Acc,probed_point_contact(:,5:8),reorientation_var(:,[1:3,5]),Avatar_Position,Gaze_Hit,Scalar_Velocity];

Out_Data= Out_Data(frame_dev1:frame_at_crossing,:);

Out_Data=[{'Trunk_X','Trunk_Y','Trunk_Z','Trunk_Vx','Trunk_Vy','Trunk_Vz','Trunk_Ax','Trunk_Ay','Trunk_Az'...
    ,'L_Acr_X','L_Acr_Y','R_Acr_X','R_Acr_Y','Head_Yaw_Z_G','Trunk_Yaw_Z_G','Pelvis_Yaw_Z_G',...
    'Trunk_Roll_Z_G','Avatar_Pos_X','Avatar_Pos_Y','Gaze_Hit_X','Gaze_Hit_Y','Gaze_Hit_Z'...
    ,'Scalar_Velocity'};num2cell(Out_Data)];

Variables_Key=cell2table(Variables_Key);
Variables_All=cell2table(Variables_All);
Out_Data=cell2table(Out_Data);

% output_file4= fullfile(output_folder,strrep(file,'.xlsx', '_output.xlsx'));
% writetable(Variables_Key,output_file4,'Sheet',1);
% writetable(Variables_All,output_file4,'Sheet',2);
% writetable(Out_Data,output_file4,'Sheet',3);

%% 
% *Process another file*

answer = questdlg('Analyze another file ?', 'Analyse another file', 'Yes', 'No', 'Yes');
end