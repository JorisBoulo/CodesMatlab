function [dev,frame_dev1, frame_dev2,Scalar_Velocity] = Calcul_Deviation(Trunk_position,Trunk_Vel,Avatar_position,frame_at_crossing,side_circumvention,subject_path,output_folder,file)

%% Deviation points identification
close all
addpath(subject_path)
load('Mean_5_CatchTrials.mat')
n=numel(Trunk_position(:,1));zero=zeros(n,1);

if isequal(side_circumvention,'Right')

response = 'Do it again';
while (strcmp(response,'Do it again'));
   plot(Trunk_position(:,2),Trunk_position(:,1))
   hold on
   plot(meanYtrunk,sdXtrunk_up,'--')
   plot(meanYtrunk,sdXtrunk_low,'--')
   plot(Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2);
   b1=find(Trunk_position(:,2)>x(2),1);
   b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
   cut_trunk_position=Trunk_position(b1:b2,:);  
   [Trunk_min, frame_dev1]=min(cut_trunk_position(:,1));
   frame_dev1=frame_dev1+b1;
   plot(Trunk_position(frame_dev1,2),Trunk_position(frame_dev1,1),'*');
   graph1=figure;
   if cut_trunk_position(1,1)==Trunk_min;
     response = questdlg('The minimum detected is equal to the second click. Would you like to to do it again or select points on trunk velocity graph?','error','Do it again','Velocity graph','Do it again'); 
     if response == 'Velocity graph'
         plot(Trunk_position(:,2),Trunk_Vel(:,1))
         hold on
         plot(meanYtrunk,sdXtrunk_Vel_up,'--')
         plot(meanYtrunk,sdXtrunk_Vel_low,'--')
         plot(Trunk_position(:,2),zero,':r')
         [x, ~]=ginput(2);
         b1=find(Trunk_position(:,2)>x(2),1);
         b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
         cut_trunk_Vel=Trunk_Vel(b1:b2,:);
         [A, B]=min(cut_trunk_Vel(:,1));
         if A<0
             [x, ~]=ginput(2);
             b1=find(Trunk_position(:,2)>x(2),1);
             b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
             cut_trunk_Vel=Trunk_Vel(b1:b2,:);
             B=find(cut_trunk_Vel(:,1)>=0,1,'first');
         end
         hold off
         frame_dev1=(b1+B);
     
     end
   else break 
   end 
end
subplot(2,1,1)
plot(Trunk_position(:,2),Trunk_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(Trunk_position(:,2),zero,':r')
plot(Trunk_position(frame_dev1,2),Trunk_position(frame_dev1,1),'*');
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),Trunk_Vel(:,1))
plot(Trunk_position(:,2),zero,':r')
plot(Trunk_position(frame_dev1,2),Trunk_Vel(frame_dev1,1),'*')
hold off


graph2=figure;
plot(Trunk_position(:,2),Trunk_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),zero,':r')
%Identify the window within you want the program to search the ML velocity
%local min
[x, ~]=ginput(2);
b1=find(Trunk_position(:,2)>x(2),1);
b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=Trunk_Vel(b1:b2,:);
Max_vel=max(Trunk_Vel(:,1));
[B,~]=find(islocalmin(cut_trunk_Vel(:,1),'MinProminence',0.05*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_trunk_Vel(:,1)),1,'last')
end

%Local min must have a proeminence of 5% of max ML velocity to make sure
%it is not only a lack of fluidity
A=cut_trunk_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(Trunk_position(:,2)>x(2),1);
 b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=Trunk_Vel(b1:b2,:);
 B=find(cut_trunk_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all

else
    inverse_Trunk_position=[Trunk_position(:,1)*-1,Trunk_position(:,2:3)];
    inverse_Trunk_Vel=[Trunk_Vel(:,1)*-1,Trunk_Vel(:,2:3)];
    
  response = 'Do it again';
while (strcmp(response,'Do it again'));
   plot(inverse_Trunk_position(:,2),inverse_Trunk_position(:,1))
   hold on
   plot(meanYtrunk,sdXtrunk_up,'--')
   plot(meanYtrunk,sdXtrunk_low,'--')
   plot(inverse_Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2);
   b1=find(inverse_Trunk_position(:,2)>x(2),1);
   b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
   cut_inverse_Trunk_position=inverse_Trunk_position(b1:b2,:);  
   [Trunk_min, frame_dev1]=min(cut_inverse_Trunk_position(:,1));
   frame_dev1=frame_dev1+b1;
   plot(inverse_Trunk_position(frame_dev1,2),inverse_Trunk_position(frame_dev1,1),'*');
   graph1=figure;
   if cut_inverse_Trunk_position(1,1)==Trunk_min;
     response = questdlg('The minimum detected is equal to the second click. Would you like to to do it again or select points on trunk velocity graph?','error','Do it again','Velocity graph','Do it again'); 
     if response == 'Velocity graph'
         plot(inverse_Trunk_position(:,2),inverse_Trunk_Vel(:,1))
         hold on
         plot(meanYtrunk,sdXtrunk_Vel_up,'--')
         plot(meanYtrunk,sdXtrunk_Vel_low,'--')
         plot(inverse_Trunk_position(:,2),zero,':r')
         [x, ~]=ginput(2);
         b1=find(inverse_Trunk_position(:,2)>x(2),1);
         b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
         cut_inverse_Trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
         [A, B]=min(cut_inverse_Trunk_Vel(:,1));
         if A<0
             [x, ~]=ginput(2);
             b1=find(inverse_Trunk_position(:,2)>x(2),1);
             b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
             cut_inverse_Trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
             B=find(cut_inverse_Trunk_Vel(:,1)>=0,1,'first');
         end
         hold off
         frame_dev1=(b1+B);
     
     end
   else break 
   end 
end
subplot(2,1,1)
plot(inverse_Trunk_position(:,2),inverse_Trunk_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(inverse_Trunk_position(:,2),zero,':r')
plot(inverse_Trunk_position(frame_dev1,2),inverse_Trunk_position(frame_dev1,1),'*');
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),Trunk_Vel(:,1))
plot(inverse_Trunk_position(:,2),zero,':r')
plot(Trunk_position(frame_dev1,2),Trunk_Vel(frame_dev1,1),'*')
hold off
 
 
graph2=figure;
plot(inverse_Trunk_position(:,2),inverse_Trunk_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(inverse_Trunk_position(:,2),zero,':r')
%Identify the window within you want the program to search the ML velocity
%local min
[x, ~]=ginput(2);
b1=find(inverse_Trunk_position(:,2)>x(2),1);
b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
cut_inverse_Trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
Max_vel=max(inverse_Trunk_Vel(:,1));
[B,~]=find(islocalmin(cut_inverse_Trunk_Vel(:,1),'MinProminence',0.1*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_inverse_Trunk_Vel(:,1)),1,'last')
end

%Local min must have a proeminence of 5% of max ML velocity to make sure
%it is not only a lack of fluidity
A=cut_inverse_Trunk_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(inverse_Trunk_position(:,2)>x(2),1);
 b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
cut_inverse_Trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
 B=find(cut_inverse_Trunk_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all
   
end


graph_dev=figure
subplot(2,1,1)
plot(Trunk_position(:,2),Trunk_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(Trunk_position(:,2),zero,':r')
plot(Trunk_position(frame_dev1,2),Trunk_position(frame_dev1,1),'*')
plot(Trunk_position(frame_dev2,2),Trunk_position(frame_dev2,1),'o')
title('Trunk Position')
hold off
subplot(2,1,2)
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
hold on
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),Trunk_Vel(:,1))
plot(Trunk_position(:,2),zero,':r')
plot(Trunk_position(frame_dev1,2),Trunk_position(frame_dev1,1),'*')
plot(Trunk_position(frame_dev2,2),Trunk_Vel(frame_dev2,1),'o')
title('Trunk Velocity')
hold off
figure_dev_file=fullfile(output_folder,strrep(file, '.xlsx', '_dev'));
saveas(graph_dev,figure_dev_file,'png')
%waitfor(graph_dev)


%% Deviation point calculated relatived to the avatar position

distance_dev1_avatar=Avatar_position(frame_dev1,2)-Trunk_position(frame_dev1,2);
distance_dev2_avatar=Avatar_position(frame_dev2,2)-Trunk_position(frame_dev2,2);
Time_dev1_passing=(frame_at_crossing-frame_dev1)/90;
Time_dev2_passing=(frame_at_crossing-frame_dev2)/90;
frameEXP_dev1=frame_dev1-1; %to get the real frame of EXP file that start to 0
frameEXP_dev2=frame_dev2-1; %to get the real frame of EXP file that start to 0

%% Complementary variables: Maximal velocity 


dev1_Trunk_X_vel=Trunk_Vel(frame_dev1:frame_dev2,1);
dev2_Trunk_X_vel=Trunk_Vel(frame_dev2:frame_at_crossing,1);

%absolute value to help to compare R and L circumv trials
vel_at_onset_X_dev1=abs(Trunk_Vel(frame_dev1,1));vel_at_onset_X_dev2=abs(Trunk_Vel(frame_dev2,1));
rate_of_ML_avoidance=abs(mean(Trunk_Vel(frame_dev1:frame_at_crossing,1))); 


if isequal(side_circumvention,'Right')
max_dev1_X_vel=max(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
max_dev2_X_vel=max(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
min_dev1_X_vel=min(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
min_dev2_X_vel=min(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
else %les vitesses minimales deviennent max et vice-versa du chgmt de sens de contournement
min_dev1_X_vel=max(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
min_dev2_X_vel=max(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
max_dev1_X_vel=min(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
max_dev2_X_vel=min(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
end


%% Velocity calcul 
Scalar_Velocity=sqrt((Trunk_Vel(:,1)).^2+(Trunk_Vel(:,2)).^2+(Trunk_Vel(:,3)).^2);
mean_scalar_vel_dev1_crossing=mean(Scalar_Velocity(frame_dev1:frame_at_crossing));
mean_scalar_vel_dev1_dev2=mean(Scalar_Velocity(frame_dev1:frame_dev2));
mean_scalar_vel_dev2_crossing=mean(Scalar_Velocity(frame_dev2:frame_at_crossing));

%velocity 1.5m (pour enlever la phase d'acc�l�ration) to crossing
frame_start=find(Trunk_position(:,2)>=-3.3,1);
mean_scalar_vel_one_p_five_cros= mean(Scalar_Velocity(frame_start:frame_at_crossing));
max_scalar_vel_one_p_five_cros= max(Scalar_Velocity(frame_start:frame_at_crossing));
min_scalar_vel_one_p_five_cros= min(Scalar_Velocity(frame_start:frame_at_crossing));

%% Range of deviation
%pour un contournement � droite
if isequal(side_circumvention,'Right')

Max_dev=max(Trunk_position(frame_dev1:frame_at_crossing))-min(Trunk_position(frame_dev1:frame_at_crossing));
else
%pour un contournement � gauche
Max_dev=min(Trunk_position(frame_dev1:frame_at_crossing))-max(Trunk_position(frame_dev1:frame_at_crossing));
Max_dev=abs(Max_dev);
end
%% Exporter les variables
dev={'FrameEXP_dev1','Dev1_longitudinal_axis','Distance_dev1_avatar','Dev1_onset_VelocityX',...
    'Max_dev1_X_vel','Min_dev1_X_vel','Mean_dev1_X_vel','Time_dev1_passing','FrameEXP_dev2','Dev2_axe_longitudinal',...
    'Distance_dev2_avatar','Dev2_onset_VelocityX','Max_dev2_X_vel','Min_dev2_X_vel','Mean_dev2_X_vel','Time_dev2_passing',...
    'Rate_ML_avoidance','mean_scalar_vel_dev1_crossing','mean_scalar_vel_dev1_dev2','mean_scalar_vel_dev2_crossing','frame_dev1','frame_dev2'...
    'mean_scalar_vel_1.5_crossing','max_scalar_vel_1.5_crossing','min_scalar_vel_1.5_crossing','Max_dev(range)',;...
    frameEXP_dev1,Trunk_position(frame_dev1,2),distance_dev1_avatar,vel_at_onset_X_dev1,max_dev1_X_vel,min_dev1_X_vel,...
    mean_dev1_X_vel,Time_dev1_passing,frameEXP_dev2,Trunk_position(frame_dev2,2),distance_dev2_avatar,...
    vel_at_onset_X_dev2,max_dev2_X_vel,min_dev2_X_vel,mean_dev2_X_vel,Time_dev2_passing,rate_of_ML_avoidance,...
    mean_scalar_vel_dev1_crossing,mean_scalar_vel_dev1_dev2,mean_scalar_vel_dev2_crossing,frame_dev1,frame_dev2,...
    mean_scalar_vel_one_p_five_cros,max_scalar_vel_one_p_five_cros,min_scalar_vel_one_p_five_cros,Max_dev};
%excel_file = fullfile(strrep(file_EXP, '.xlsx', '_dev.xlsx'));
%xlswrite(excel_file,dev);

end

