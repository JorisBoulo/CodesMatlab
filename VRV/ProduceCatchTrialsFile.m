%% 
% The goal of this scrpit is to produced a mean catch trial curve that will 
% be used to guide visually the identification of the two deviation points 
% and reorientation sequencies. 
% 
% *Note that some part of the script is "write in french". 
% 
% *This script is need to be run before the main code to produce the "mean5catchtrials" 
% file that is needed to start the main code.*
% 
% *Open files*

clear;clc;
subject_path=uigetdir('C:\','Select the participant file')
subject_path=(fullfile(subject_path));

%% Output Folder
output_folder = uigetdir(subject_path, 'Select Results Output Folder');
if (isequal(output_folder, 0))
    return;
end

[filecatch path]=uigetfile({'*.xlsx';'Excel Files (*.xlsx)';'*.*'},'Select 5 catch trials','MultiSelect','on',subject_path);


if numel(filecatch)~=5
    warndlg=('You didn''t select five catch trials')
    return
end

filecatch1=cell2mat(fullfile(path,filecatch(1)));filecatch2=cell2mat(fullfile(path,filecatch(2)));
filecatch3=cell2mat(fullfile(path,filecatch(3)));filecatch4=cell2mat(fullfile(path,filecatch(4)));
filecatch5=cell2mat(fullfile(path,filecatch(5)));
% filecatch1='VRV2_09_03.xlsx'; filecatch2='VRV2_09_21.xlsx'; filecatch3='VRV2_09_27.xlsx';
% filecatch4='VRV2_09_36.xlsx';filecatch5='VRV2_09_39.xlsx';

[catchdata1,catchtxt]=xlsread(filecatch1); catchdata2=xlsread(filecatch2);catchdata3=xlsread(filecatch3);
catchdata4=xlsread(filecatch4);catchdata5=xlsread(filecatch5);

%Cut the files to keep only the number
catchdata1=catchdata1(7:end,:);catchdata2=catchdata2(7:end,:);catchdata3=catchdata3(7:end,:);
catchdata4=catchdata4(7:end,:);catchdata5=catchdata5(7:end,:);

%Find wich column represent the Trunk CoM position in X and Y, because all
%catch trials are processed at the same time, we only need to search it for
%one of the catch trials.
[n_line n_column]=size(catchdata1);
Title_line=9;
for i=1:n_column
    if ~isempty(strfind(catchtxt{Title_line,i},'Trunk_X_position'));
    Trunk_X_pos_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_Y_position'));
    Trunk_Y_pos_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_X_velocity'));
    Trunk_X_vel_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_Y_velocity'));
    Trunk_Y_vel_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_Z_velocity'));
    Trunk_Z_vel_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_Rot_Y_G'));
    Trunk_Rot_Y_G_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Head_Rot_Y_G'));
    Head_Rot_Y_G_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Pelvis_Rot_Y_G'));
   Pelvis_Rot_Y_G_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Trunk_Rot_Z_G'));
    Trunk_Rot_Z_G_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Head_Rot_Z_G'));
    Head_Rot_Z_G_Ind=i;
elseif ~isempty(strfind(catchtxt{Title_line,i},'Pelvis_Rot_Z_G'));
   Pelvis_Rot_Z_G_Ind=i;
    end
end
%% 
% *Display of the 250 first frameof the 5 catchs trials *Verify if it is representative 
% trials, ideally, all start with the same feet to average something and not do 
% two flat lines *

plot(1:250,catchdata1(1:250,Trunk_X_pos_Ind))
hold on
plot(1:250,catchdata2(1:250,Trunk_X_pos_Ind))
plot(1:250,catchdata3(1:250,Trunk_X_pos_Ind))
plot(1:250,catchdata4(1:250,Trunk_X_pos_Ind))
plot(1:250,catchdata5(1:250,Trunk_X_pos_Ind))
title 'Catch trials M-L COM Trunk position'
legend('1','2','3','4','5')
hold off
answer=questdlg('Are the 5 catch trials seem representative?','Verify catch trials','Yes','No','Yes')
switch answer
    case 'Yes'
        representative=1
    case 'No'
        representative=0
end

if isequal(representative,0)
    return
end

%% 
% *Cut the trials to make sure every five trials have the same length*

Ytrunk1=catchdata1(:,Trunk_Y_pos_Ind);Ytrunk2=catchdata2(:,Trunk_Y_pos_Ind);Ytrunk3=catchdata3(:,Trunk_Y_pos_Ind);Ytrunk4=catchdata4(:,Trunk_Y_pos_Ind);Ytrunk5=catchdata5(:,Trunk_Y_pos_Ind);

minYtrunk1= min(Ytrunk1);minYtrunk2= min(Ytrunk2);minYtrunk3= min(Ytrunk3);minYtrunk4= min(Ytrunk4);minYtrunk5= min(Ytrunk5);
minYtrunk=[minYtrunk1,minYtrunk2,minYtrunk3,minYtrunk4,minYtrunk5];
[maxYtrunk ind]=max(minYtrunk)

if ind==1
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==2
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==3
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==4
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==5
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);1
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
end
[s1 i1]=size(catchdata1);[s2 i2]=size(catchdata2);[s3 i3]=size(catchdata3);[s4 i4]=size(catchdata4);[s5 i5]=size(catchdata5);
coupure=[s1,s2,s3,s4,s5];coupure=min(coupure)
catchdata1=catchdata1(1:coupure,:);catchdata2=catchdata2(1:coupure,:);catchdata3=catchdata3(1:coupure,:);catchdata4=catchdata4(1:coupure,:);catchdata5=catchdata5(1:coupure,:);
%% 
% *Mean catch trial vector and 2 SD curves*

XtrunkCatch=[catchdata1(:,Trunk_X_pos_Ind),catchdata2(:,Trunk_X_pos_Ind),catchdata3(:,Trunk_X_pos_Ind),catchdata4(:,Trunk_X_pos_Ind),catchdata5(:,Trunk_X_pos_Ind)];
meanXtrunk=mean(XtrunkCatch')';
sdXtrunk=std(XtrunkCatch')';
sdXtrunk_up=meanXtrunk+2*sdXtrunk;sdXtrunk_low=meanXtrunk-2*sdXtrunk;
YtrunkCatch=[catchdata1(:,Trunk_Y_pos_Ind),catchdata2(:,Trunk_Y_pos_Ind),catchdata3(:,Trunk_Y_pos_Ind),catchdata4(:,Trunk_Y_pos_Ind),catchdata5(:,Trunk_Y_pos_Ind)];
meanYtrunk=mean(YtrunkCatch')';
sdYtrunk=std(YtrunkCatch')';
sdYtrunk_up=meanYtrunk+2*sdYtrunk;sdYtrunk_low=meanYtrunk-2*sdYtrunk;


Xtrunk_Vel_Catch=[catchdata1(:,Trunk_X_vel_Ind),catchdata2(:,Trunk_X_vel_Ind),catchdata3(:,Trunk_X_vel_Ind),catchdata4(:,Trunk_X_vel_Ind),catchdata5(:,Trunk_X_vel_Ind)];
meantrunk_Vel_X=mean(Xtrunk_Vel_Catch')';
sdXtrunk_Vel=std(Xtrunk_Vel_Catch')';
sdXtrunk_Vel_up=meantrunk_Vel_X+2*sdXtrunk_Vel;sdXtrunk_Vel_low=meantrunk_Vel_X-2*sdXtrunk_Vel;
Ytrunk_Vel_Catch=[catchdata1(:,Trunk_Y_vel_Ind),catchdata2(:,Trunk_Y_vel_Ind),catchdata3(:,Trunk_Y_vel_Ind),catchdata4(:,Trunk_Y_vel_Ind),catchdata5(:,Trunk_Y_vel_Ind)];
meantrunk_Vel_Y=mean(Ytrunk_Vel_Catch')';
sdYtrunk_Vel=std(Ytrunk_Vel_Catch')';
sdYtrunk_Vel_up=meantrunk_Vel_Y+2*sdYtrunk_Vel;sdYtrunk_Vel_low=meantrunk_Vel_Y-2*sdYtrunk_Vel;
Ztrunk_Vel_Catch=[catchdata1(:,Trunk_Z_vel_Ind),catchdata2(:,Trunk_Z_vel_Ind),catchdata3(:,Trunk_Z_vel_Ind),catchdata4(:,Trunk_Z_vel_Ind),catchdata5(:,Trunk_Z_vel_Ind)];
meantrunk_Vel_Z=mean(Ztrunk_Vel_Catch')';
sdZtrunk_Vel=std(Ztrunk_Vel_Catch')';


Head_Rot_Z_Catch=[catchdata1(:,Head_Rot_Z_G_Ind),catchdata2(:,Head_Rot_Z_G_Ind),catchdata3(:,Head_Rot_Z_G_Ind),catchdata4(:,Head_Rot_Z_G_Ind),catchdata5(:,Head_Rot_Z_G_Ind)];
meanHead_Rot_Z=mean(Head_Rot_Z_Catch')';
sdHead_Rot_Z=std(Head_Rot_Z_Catch')';
sdHead_Rot_Z_up=meanHead_Rot_Z+2*sdHead_Rot_Z;sdHead_Rot_Z_low=meanHead_Rot_Z-2*sdHead_Rot_Z;
Head_Rot_Y_Catch=[catchdata1(:,Head_Rot_Y_G_Ind),catchdata2(:,Head_Rot_Y_G_Ind),catchdata3(:,Head_Rot_Y_G_Ind),catchdata4(:,Head_Rot_Y_G_Ind),catchdata5(:,Head_Rot_Y_G_Ind)];
meanHead_Rot_Y=mean(Head_Rot_Y_Catch')';
sdHead_Rot_Y=std(Head_Rot_Y_Catch')';
sdHead_Rot_Y_up=meanHead_Rot_Y+2*sdHead_Rot_Y;sdHead_Rot_Y_low=meanHead_Rot_Y-2*sdHead_Rot_Y;


Trunk_Rot_Z_Catch=[catchdata1(:,Trunk_Rot_Z_G_Ind),catchdata2(:,Trunk_Rot_Z_G_Ind),catchdata3(:,Trunk_Rot_Z_G_Ind),catchdata4(:,Trunk_Rot_Z_G_Ind),catchdata5(:,Trunk_Rot_Z_G_Ind)];
meanTrunk_Rot_Z=mean(Trunk_Rot_Z_Catch')';
sdTrunk_Rot_Z=std(Trunk_Rot_Z_Catch')';
sdTrunk_Rot_Z_up=meanTrunk_Rot_Z+2*sdTrunk_Rot_Z;sdTrunk_Rot_Z_low=meanTrunk_Rot_Z-2*sdTrunk_Rot_Z;
Trunk_Rot_Y_Catch=[catchdata1(:,Trunk_Rot_Y_G_Ind),catchdata2(:,Trunk_Rot_Y_G_Ind),catchdata3(:,Trunk_Rot_Y_G_Ind),catchdata4(:,Trunk_Rot_Y_G_Ind),catchdata5(:,Trunk_Rot_Y_G_Ind)];
meanTrunk_Rot_Y=mean(Trunk_Rot_Y_Catch')';
sdTrunk_Rot_Y=std(Trunk_Rot_Y_Catch')';
sdTrunk_Rot_Y_up=meanTrunk_Rot_Y+2*sdTrunk_Rot_Y;sdTrunk_Rot_Y_low=meanTrunk_Rot_Y-2*sdTrunk_Rot_Y;


Pelvis_Rot_Z_Catch=[catchdata1(:,Pelvis_Rot_Z_G_Ind),catchdata2(:,Pelvis_Rot_Z_G_Ind),catchdata3(:,Pelvis_Rot_Z_G_Ind),catchdata4(:,Pelvis_Rot_Z_G_Ind),catchdata5(:,Pelvis_Rot_Z_G_Ind)];
meanPelvis_Rot_Z=mean(Pelvis_Rot_Z_Catch')';
sdPelvis_Rot_Z=std(Pelvis_Rot_Z_Catch')';
sdPelvis_Rot_Z_up=meanPelvis_Rot_Z+2*sdPelvis_Rot_Z;sdPelvis_Rot_Z_low=meanPelvis_Rot_Z-2*sdPelvis_Rot_Z;
Pelvis_Rot_Y_Catch=[catchdata1(:,Pelvis_Rot_Y_G_Ind),catchdata2(:,Pelvis_Rot_Y_G_Ind),catchdata3(:,Pelvis_Rot_Y_G_Ind),catchdata4(:,Pelvis_Rot_Y_G_Ind),catchdata5(:,Pelvis_Rot_Y_G_Ind)];
meanPelvis_Rot_Y=mean(Pelvis_Rot_Y_Catch')';
sdPelvis_Rot_Y=std(Pelvis_Rot_Y_Catch')';
sdPelvis_Rot_Y_up=meanPelvis_Rot_Y+2*sdPelvis_Rot_Y;sdPelvis_Rot_Y_low=meanPelvis_Rot_Y-2*sdPelvis_Rot_Y;

Scalar_Vel_Catch=[sqrt(catchdata1(:,Trunk_X_vel_Ind).^2+catchdata1(:,Trunk_Y_vel_Ind).^2+catchdata1(:,Trunk_Z_vel_Ind).^2)...
    ,sqrt(catchdata2(:,Trunk_X_vel_Ind).^2+catchdata2(:,Trunk_Y_vel_Ind).^2+catchdata2(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata3(:,Trunk_X_vel_Ind).^2+catchdata3(:,Trunk_Y_vel_Ind).^2+catchdata3(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata4(:,Trunk_X_vel_Ind).^2+catchdata4(:,Trunk_Y_vel_Ind).^2+catchdata4(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata5(:,Trunk_X_vel_Ind).^2+catchdata5(:,Trunk_Y_vel_Ind).^2+catchdata5(:,Trunk_Z_vel_Ind).^2)];
mean_Scalar_Vel=mean(Scalar_Vel_Catch')';
SD_Scalar_Vel=std(Scalar_Vel_Catch')';
SD_Scalar_Vel_up=mean_Scalar_Vel+2*SD_Scalar_Vel;SD_Scalar_Vel_low=mean_Scalar_Vel-2*SD_Scalar_Vel;

%% Graph
% *Graphic representation to verify the catch trial average curve for the three critics variables*
graph=figure
plot(meanYtrunk,meanXtrunk,'m-')
hold on
plot(YtrunkCatch(:,1),XtrunkCatch(:,1))
plot(YtrunkCatch(:,2),XtrunkCatch(:,2))
plot(YtrunkCatch(:,3),XtrunkCatch(:,3))
plot(YtrunkCatch(:,4),XtrunkCatch(:,4))
plot(YtrunkCatch(:,5),XtrunkCatch(:,5))
title 'Trunk COM M-L displacement during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML Trunk displacement (m)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_X');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meantrunk_Vel_X,'m-')
hold on
plot(YtrunkCatch(:,1),Xtrunk_Vel_Catch(:,1))
plot(YtrunkCatch(:,2),Xtrunk_Vel_Catch(:,2))
plot(YtrunkCatch(:,3),Xtrunk_Vel_Catch(:,3))
plot(YtrunkCatch(:,4),Xtrunk_Vel_Catch(:,4))
plot(YtrunkCatch(:,5),Xtrunk_Vel_Catch(:,5))
title 'Trunk M-L velocity during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML velocity (m/s)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_VelX');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanHead_Rot_Z,'m-')
hold on
plot(YtrunkCatch(:,1),Head_Rot_Z_Catch(:,1))
plot(YtrunkCatch(:,2),Head_Rot_Z_Catch(:,2))
plot(YtrunkCatch(:,3),Head_Rot_Z_Catch(:,3))
plot(YtrunkCatch(:,4),Head_Rot_Z_Catch(:,4))
plot(YtrunkCatch(:,5),Head_Rot_Z_Catch(:,5))
title 'Head Yaw during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'Head yaw (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Head_Rot_Z');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanTrunk_Rot_Y,'m-')
hold on
plot(YtrunkCatch(:,1),Trunk_Rot_Y_Catch(:,1))
plot(YtrunkCatch(:,2),Trunk_Rot_Y_Catch(:,2))
plot(YtrunkCatch(:,3),Trunk_Rot_Y_Catch(:,3))
plot(YtrunkCatch(:,4),Trunk_Rot_Y_Catch(:,4))
plot(YtrunkCatch(:,5),Trunk_Rot_Y_Catch(:,5))
title 'Trunk Roll during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'Trunk Roll (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_Rot_Y');
saveas(graph,figure_name,'png')
hold off
%% 
% *Courbe moyenne avec les SD (2SD affich�e)*
graph=figure
plot(meanYtrunk,meanXtrunk)
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
title 'Mean Trunk COM M-L displacement during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML trunk displacemen (m)'
figure_name=fullfile(output_folder,'Catch_Trunk_X');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk)
hold on
plot(sdYtrunk_up,'--')
plot(sdYtrunk_low,'--')
title 'Mean Trunk COM longitudinal displacement during catch trials'
xlabel 'Frame'
ylabel 'Longitudinal trunk displacement(m)'
figure_name=fullfile(output_folder,'Catch_Trunk_Y');
saveas(graph,figure_name,'png')
hold off


graph=figure
plot(meanYtrunk,meantrunk_Vel_X)
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
title 'Mean Trunk COM M-L velocity during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML trunk velocity(m/s)'
figure_name=fullfile(output_folder,'Catch_Trunk_VelX');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meantrunk_Vel_Y)
hold on
plot(sdYtrunk_Vel_up,'--')
plot(sdYtrunk_Vel_low,'--')
title 'Mean Trunk longitudinal velocity during catch trials'
xlabel 'Frame'
ylabel 'Longitudinal trunk velocity(m/s)'
figure_name=fullfile(output_folder,'Catch_Trunk_VelY');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanHead_Rot_Z)
hold on
plot(sdHead_Rot_Z_up,'--')
plot(sdHead_Rot_Z_low,'--')
title 'Mean Head Yaw during catch trials'
xlabel 'Frame'
ylabel 'Head Yaw (�)'
figure_name=fullfile(output_folder,'Catch_Head_Yaw');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanHead_Rot_Y)
hold on
plot(sdHead_Rot_Y_up,'--')
plot(sdHead_Rot_Y_low,'--')
title 'Mean Head Roll during catch trials'
xlabel 'Frame'
ylabel 'Head Roll (�)'
figure_name=fullfile(output_folder,'Catch_Head_Roll');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanTrunk_Rot_Z)
hold on
plot(sdTrunk_Rot_Z_up,'--')
plot(sdTrunk_Rot_Z_low,'--')
title 'Mean Trunk Yaw during catch trials'
xlabel 'Frame'
ylabel 'Trunk Yaw (�)'
figure_name=fullfile(output_folder,'Catch_Trunk_Yaw');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanTrunk_Rot_Y)
hold on
plot(sdTrunk_Rot_Y_up,'--')
plot(sdTrunk_Rot_Y_low,'--')
title 'Mean Trunk Roll during catch trials'
xlabel 'Frame'
ylabel 'Trunk Roll (�)'
figure_name=fullfile(output_folder,'Catch_Trunk_Roll');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanPelvis_Rot_Z)
hold on
plot(sdPelvis_Rot_Z_up,'--')
plot(sdPelvis_Rot_Z_low,'--')
title 'Mean Pelvis Yaw during catch trials'
xlabel 'Frame'
ylabel 'Pelvis Yaw (�)'
figure_name=fullfile(output_folder,'Catch_Pelvis_Yaw');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanPelvis_Rot_Y)
hold on
plot(sdPelvis_Rot_Y_up,'--')
plot(sdPelvis_Rot_Y_low,'--')
title 'Mean Pelvis Roll during catch trials'
xlabel 'Frame'
ylabel 'Pelvis Roll (�)'
figure_name=fullfile(output_folder,'Catch_Pelvis_Roll');
saveas(graph,figure_name,'png')
hold off


graph=figure
plot(mean_Scalar_Vel)
hold on
plot(SD_Scalar_Vel_up,'--')
plot(SD_Scalar_Vel_low,'--')
title 'Mean scalar velocity'
xlabel 'Frame'
ylabel 'Velocity (m/s)'
figure_name=fullfile(output_folder,'Catch_Scalar_Velocity');
saveas(graph,figure_name,'png')
hold off

%% Calculs de variables 

%Value: mean and SD of each variables calculated 
Value_Head_Rot_Z_Mean=mean(mean(Head_Rot_Z_Catch));
Value_Head_Rot_Z_SD=mean(std(Head_Rot_Z_Catch));
Value_Head_Rot_Y_Mean=mean(mean(Head_Rot_Y_Catch));
Value_Head_Rot_Y_SD=mean(std(Head_Rot_Y_Catch));

Value_Trunk_Rot_Z_Mean=mean(mean(Trunk_Rot_Z_Catch));
Value_Trunk_Rot_Z_SD=mean(std(Trunk_Rot_Z_Catch));
Value_Trunk_Rot_Y_Mean=mean(mean(Trunk_Rot_Y_Catch));
Value_Trunk_Rot_Y_SD=mean(std(Trunk_Rot_Y_Catch));

Value_Pelvis_Rot_Z_Mean=mean(mean(Pelvis_Rot_Z_Catch));
Value_Pelvis_Rot_Z_SD=mean(std(Pelvis_Rot_Z_Catch));
Value_Pelvis_Rot_Y_Mean=mean(mean(Pelvis_Rot_Y_Catch));
Value_Pelvis_Rot_Y_SD=mean(std(Pelvis_Rot_Y_Catch));

Value_Mean_Scalar_Velocity=mean(mean(Scalar_Vel_Catch));
Value_SD_Scalar_Velocity=std(mean(Scalar_Vel_Catch));

Value_Out=[Value_Head_Rot_Z_Mean,Value_Head_Rot_Z_SD,Value_Head_Rot_Y_Mean,Value_Head_Rot_Y_SD,Value_Trunk_Rot_Z_Mean,Value_Trunk_Rot_Z_SD... 
    Value_Trunk_Rot_Y_Mean,Value_Trunk_Rot_Y_SD,Value_Pelvis_Rot_Z_Mean,Value_Pelvis_Rot_Z_SD,Value_Pelvis_Rot_Y_Mean,Value_Pelvis_Rot_Y_SD];
Value_Out_title={'Head_Rot_Z_Mean','Head_Rot_Z_SD','Head_Rot_Y_Mean','Head_Rot_Y_SD','Trunk_Rot_Z_Mean'... 
    ,'Trunk_Rot_Z_SD','Trunk_Rot_Y_Mean','Trunk_Rot_Y_SD','Pelvis_Rot_Z_Mean','Pelvis_Rot_Z_SD','Pelvis_Rot_Y_Mean','Pelvis_Rot_Y_SD'};
value_sheet=[Value_Out_title;num2cell(Value_Out)];
%data
mean_SD_curve=[meanXtrunk,sdXtrunk_up,sdXtrunk_low,meanYtrunk,sdYtrunk_up,sdYtrunk_low,...
    meantrunk_Vel_X,sdXtrunk_Vel_up,sdXtrunk_Vel_low,meantrunk_Vel_Y,sdYtrunk_Vel_up,sdYtrunk_Vel_low...
    meanHead_Rot_Z,sdHead_Rot_Z_up,sdHead_Rot_Z_low,meanHead_Rot_Y,sdHead_Rot_Y_up,sdHead_Rot_Y_low...
    meanTrunk_Rot_Z,sdTrunk_Rot_Z_up,sdTrunk_Rot_Z_low,meanTrunk_Rot_Y,sdTrunk_Rot_Y_up,sdTrunk_Rot_Y_low...
    meanPelvis_Rot_Z,sdPelvis_Rot_Z_up,sdPelvis_Rot_Z_low,meanPelvis_Rot_Y,sdPelvis_Rot_Y_up,sdPelvis_Rot_Y_low ...
    mean_Scalar_Vel,SD_Scalar_Vel_up,SD_Scalar_Vel_low]
mean_SD_curve_title={'meanXtrunk','2SD_up','2SD_low','meanYtrunk','2SD_up','2SD_low'...
    'meantrunk_Vel_X','2SD_up','2SD_low','meantrunk_Vel_Y','2SD_up','2SD_low'...
    'meanHead_Rot_Z','2SD_up','2SD_low','meanHead_Rot_Y','2SD_up','2SD_low'...
    'meanTrunk_Rot_Z','2SD_up','2SD_low','meanTrunk_Rot_Y','2SD_up','2SD_low'...
    'meanPelvis_Rot_Z','2SD_up','2SD_low','meanPelvis_Rot_Y','2SD_up','2SD_low','mean_Scalar_Vel','2SD_up','2SD_low'}
data_sheet=[mean_SD_curve_title;num2cell(mean_SD_curve)];
%% 
% *This section is about a theorical contact point. It calculates where the 
% avatar and the participant would contact if nothing was done by them to avoid 
% the contact. This was not used for the calcul of variables, but I kept this 
% calcul there just in case.*
% 
% Cr�ation de 2 �quations lin�


VYtrunk1=catchdata1(:,Trunk_Y_vel_Ind);VYtrunk2=catchdata2(:,Trunk_Y_vel_Ind);VYtrunk3=catchdata3(:,Trunk_Y_vel_Ind);VYtrunk4=catchdata4(:,Trunk_Y_vel_Ind);VYtrunk5=catchdata5(:,Trunk_Y_vel_Ind);
VYtrunkCatch=[VYtrunk1,VYtrunk2,VYtrunk3,VYtrunk4,VYtrunk5]';meanVYtrunk=mean(VYtrunkCatch)';meanVYtrunk=mean(meanVYtrunk)
x=(1:500)/90; %repr�sente le temps, 90 car 90Hz d'acquisition. Le 500 est arbitraire. Le but est d'allonger la trajectoire pour qu'il y ait un point de rencontre
eqpositionYtrunk=meanVYtrunk*x-3.8;
a=inputdlg('Entrer la vitesse programm�e de l avatar, mettre la valeure positive, en m/s: ','Vitesse du vampire');
a=str2num(a{1})
eqavatarX=-a*x+2.9; 
dif=eqavatarX-eqpositionYtrunk;
Tcontact=find(dif<0,1)
c1=eqpositionYtrunk(Tcontact)
c2=eqavatarX(Tcontact)
theorical_contact=(c1+c2)/2
%% 
% *Enregistrement des variables*
msgbox('Note which catch trials were used')

%fichier excel
Value=cell2table(value_sheet);
Data=cell2table(data_sheet);
excel_filename=fullfile(output_folder,'mean5CatchTrials.xlsx');
writetable(Value,excel_filename,'sheet',1);
writetable(Data,excel_filename,'sheet',2);

%fichier matlab
filename='Mean_5_CatchTrials.mat'
filename=fullfile(output_folder,filename);
save(filename,'meanXtrunk','meanYtrunk','sdXtrunk','sdXtrunk_low','sdXtrunk_up','sdXtrunk_Vel_up','sdXtrunk_Vel_low','theorical_contact','Tcontact','sdYtrunk_up','sdYtrunk_low','meantrunk_Vel_X','meantrunk_Vel_Y','sdYtrunk_Vel_up','sdYtrunk_Vel_low','meanHead_Rot_Z','sdHead_Rot_Z_up', 'sdHead_Rot_Z_low', 'meanHead_Rot_Y', 'sdHead_Rot_Y_up', 'sdHead_Rot_Y_low','meanTrunk_Rot_Z','sdTrunk_Rot_Z_up','sdTrunk_Rot_Z_low','meanTrunk_Rot_Y','sdTrunk_Rot_Y_up','sdTrunk_Rot_Y_low','meanPelvis_Rot_Z','sdPelvis_Rot_Z_up','sdPelvis_Rot_Z_low','meanPelvis_Rot_Y','sdPelvis_Rot_Y_up','sdPelvis_Rot_Y_low','mean_Scalar_Vel','SD_Scalar_Vel_up','SD_Scalar_Vel_low','Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Head_Rot_Y_Mean','Value_Head_Rot_Y_SD','Value_Trunk_Rot_Z_Mean','Value_Trunk_Rot_Z_SD','Value_Trunk_Rot_Y_Mean','Value_Trunk_Rot_Y_SD','Value_Pelvis_Rot_Z_Mean','Value_Pelvis_Rot_Z_SD','Value_Pelvis_Rot_Y_Mean','Value_Pelvis_Rot_Y_SD');

%% Adding ML Gaze Hit part

clearvars -except coupure mf1 mf2 mf3 mf4 mf5 path output_folder subject_path ind

[fileGaze path]=uigetfile({'*.xls';'Excel Files (*.xls)';'*.*'},'Select 5 catch trials','MultiSelect','on',subject_path);


if numel(fileGaze)~=5
    warndlg=('You didn''t select five catch trials')
    return
end

GazeFile1=cell2mat(fullfile(path,fileGaze(1)));GazeFile2=cell2mat(fullfile(path,fileGaze(2)));
GazeFile3=cell2mat(fullfile(path,fileGaze(3)));GazeFile4=cell2mat(fullfile(path,fileGaze(4)));
GazeFile5=cell2mat(fullfile(path,fileGaze(5)));

GazeFile1=readcell(GazeFile1,'Sheet','Data');
GazeFile2=readcell(GazeFile2,'Sheet','Data');
GazeFile3=readcell(GazeFile3,'Sheet','Data');
GazeFile4=readcell(GazeFile4,'Sheet','Data');
GazeFile5=readcell(GazeFile5,'Sheet','Data');

[n_line n_column]=size(GazeFile1);
for i=1:n_column
    if ~isempty(strfind(GazeFile1{1,i},'X_Gaze_hit'))
    X_Gaze_Hit_ind=i
    end
end

X_Gaz_Hit_1=cell2mat(GazeFile1(2:end,X_Gaze_Hit_ind));
X_Gaz_Hit_2=cell2mat(GazeFile2(2:end,X_Gaze_Hit_ind));
X_Gaz_Hit_3=cell2mat(GazeFile3(2:end,X_Gaze_Hit_ind));
X_Gaz_Hit_4=cell2mat(GazeFile4(2:end,X_Gaze_Hit_ind));
X_Gaz_Hit_5=cell2mat(GazeFile5(2:end,X_Gaze_Hit_ind));

%Couper l'essai avec les m�mes valeurs que tant�t 
if ind==1
X_Gaz_Hit_2=X_Gaz_Hit_2(mf2:end,:);
X_Gaz_Hit_3=X_Gaz_Hit_3(mf3:end,:);
X_Gaz_Hit_4=X_Gaz_Hit_4(mf4:end,:);
X_Gaz_Hit_5=X_Gaz_Hit_5(mf5:end,:);
elseif ind==2
X_Gaz_Hit_1=X_Gaz_Hit_1(mf1:end,:);
X_Gaz_Hit_3=X_Gaz_Hit_3(mf3:end,:);
X_Gaz_Hit_4=X_Gaz_Hit_4(mf4:end,:);
X_Gaz_Hit_5=X_Gaz_Hit_5(mf5:end,:);
elseif ind==3
X_Gaz_Hit_1=X_Gaz_Hit_1(mf1:end,:);
X_Gaz_Hit_2=X_Gaz_Hit_2(mf2:end,:);
X_Gaz_Hit_4=X_Gaz_Hit_4(mf4:end,:);
X_Gaz_Hit_5=X_Gaz_Hit_5(mf5:end,:);
elseif ind==4
X_Gaz_Hit_1=X_Gaz_Hit_1(mf1:end,:);
X_Gaz_Hit_2=X_Gaz_Hit_2(mf2:end,:);
X_Gaz_Hit_3=X_Gaz_Hit_3(mf3:end,:);
X_Gaz_Hit_5=X_Gaz_Hit_5(mf5:end,:);
elseif ind==5
X_Gaz_Hit_1=X_Gaz_Hit_1(mf1:end,:);
X_Gaz_Hit_2=X_Gaz_Hit_2(mf2:end,:);
X_Gaz_Hit_3=X_Gaz_Hit_3(mf3:end,:);
X_Gaz_Hit_4=X_Gaz_Hit_4(mf4:end,:);
end

X_Gaz_Hit_1=X_Gaz_Hit_1(1:coupure,:);X_Gaz_Hit_2=X_Gaz_Hit_2(1:coupure,:);X_Gaz_Hit_3=X_Gaz_Hit_3(1:coupure,:);X_Gaz_Hit_4=X_Gaz_Hit_4(1:coupure,:);X_Gaz_Hit_5=X_Gaz_Hit_5(1:coupure,:);

X_Gaz_Hit=[X_Gaz_Hit_1,X_Gaz_Hit_2,X_Gaz_Hit_3,X_Gaz_Hit_4,X_Gaz_Hit_5];


mean_Gaz_Hit_X=mean(X_Gaz_Hit')';
std_Gaz_Hit_X=std(X_Gaz_Hit')';
std_Gaz_Hit_X_up=mean_Gaz_Hit_X+2*std_Gaz_Hit_X ;std_Gaz_Hit_X_low=mean_Gaz_Hit_X-2*std_Gaz_Hit_X;

graph=figure
plot(mean_Gaz_Hit_X)
hold on
plot(std_Gaz_Hit_X_up,'--')
plot(std_Gaz_Hit_X_low,'--')
title 'Mean Gaze Hit during catch trials'
xlabel 'frame'
ylabel 'M-L gaze hit coordinate (m)'
figure_name=fullfile(output_folder,'Catch_Gaz_Hit_M-L');
saveas(graph,figure_name,'png')
hold off



graph=figure
plot(mean_Gaz_Hit_X,'m-')
hold on
plot(X_Gaz_Hit_1)
plot(X_Gaz_Hit_2)
plot(X_Gaz_Hit_3)
plot(X_Gaz_Hit_4)
plot(X_Gaz_Hit_5)
title 'Gaze Hit during catch trials'
xlabel 'Frame'
ylabel 'M-L Gaze Hit'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Gaze_Hit_X');
saveas(graph,figure_name,'png')
hold off

Value_mean_Gaz_Hit_X=mean(mean(X_Gaz_Hit));
Value_SD_Gaz_Hit_X=std(mean(X_Gaz_Hit));

%fichier matlab
filename='Mean_5_Catch_Gaze.mat'
filename=fullfile(output_folder,filename);
save(filename,'mean_Gaz_Hit_X','std_Gaz_Hit_X_up','std_Gaz_Hit_X_low','Value_mean_Gaz_Hit_X','Value_SD_Gaz_Hit_X')
