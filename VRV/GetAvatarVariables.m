function [Avatar_Position,Gaze_Hit,Nick_Results] = GetAvatarVariables(vive_file)
VIVE_data=readcell(vive_file,'Sheet','Data');
Nick_Results=readcell(vive_file,'Sheet','Distance');

[n_line n_column]=size(VIVE_data);
for i=1:n_column
    if ~isempty(strfind(VIVE_data{1,i},'X Avatar'))
    Av_X_Pos=cell2mat(VIVE_data(2:end,i));
    elseif ~isempty(strfind(VIVE_data{1,i},'Y Avatar'))
    Av_Y_Pos=cell2mat(VIVE_data(2:end,i));
    elseif ~isempty(strfind(VIVE_data{1,i},'X_Gaze_hit'))
    X_Gaze_Hit=cell2mat(VIVE_data(2:end,i));
    elseif ~isempty(strfind(VIVE_data{1,i},'Y_Gaze_hit'))
    Y_Gaze_Hit=cell2mat(VIVE_data(2:end,i));
    elseif ~isempty(strfind(VIVE_data{1,i},'Z_Gaze_hit'))
    Z_Gaze_Hit=cell2mat(VIVE_data(2:end,i));
    end
end
Avatar_Position=[Av_X_Pos Av_Y_Pos];
Gaze_Hit=[X_Gaze_Hit,Y_Gaze_Hit,Z_Gaze_Hit];
end

%Ajouter ici le gaze_hit pour la s�quence de r�orientation avec les donn�es
%align�es compl�tement avec VICON. Pour l'instant, mes pens�es sont de
%faire le code de donn�es du eye tracking � part except� l'orientation du
%regard. Il sera possible d'automatiser compl�tement l'analyse de ces
%donn�es � mon avis et si on modifie quelque chose, on n'aura pas besoin 
... de recommencer les identifications d'�v�nements. 