function fluidity = Calcul_Fluidity(Trunk_position,Trunk_Vel,Trunk_Acc,frame_dev1,frame_dev2,frame_at_crossing,output_folder,file)

%%cut variables to keep only between dev1 and the point of crossing

cut_trunk_position=Trunk_position(frame_dev1:frame_at_crossing,:);


cut_trunk_velocity=Trunk_Vel(frame_dev1:frame_at_crossing,:);

cut_trunk_acc=Trunk_Acc(frame_dev1:frame_at_crossing,:);
cut_trunk_X_acc=cut_trunk_acc(:,1);
cut_trunk_Y_acc=cut_trunk_acc(:,2);
cut_trunk_Z_acc=cut_trunk_acc(:,3);

n=numel(cut_trunk_position(:,1)); zero=zeros(n,1);
x=(1:n)';

%%Zero_Crossing calcul
%ML
%[peakmax1,loc_max1] = findpeaks(cut_trunk_velocity(:,1)); method changed
%here, verifiy that results are the same. The change in the function was
%done to be independent of the signal processing toolbox I kept this in comment in case I doesn't obtained. 
[peakmax, loc_max]=islocalmax(Trunk_Vel(frame_dev1:frame_at_crossing,1));
loc_max=find(peakmax==1);
[peakmin,loc_min] = islocalmin(Trunk_Vel(frame_dev1:frame_at_crossing,1));
loc_min=find(peakmin==1);
peak_ML=[loc_min;loc_max];peak_ML=sort(peak_ML);

Zero_crossing_ML_total=numel(peak_ML);
Zero_Crossing_ML_dev1=numel(find(peak_ML<=(frame_dev2-frame_dev1)));
Zero_Crossing_ML_dev2=numel(find(peak_ML>(frame_dev2-frame_dev1)));

Rate_of_ML_Avoidance=mean(Trunk_Vel(frame_dev1:frame_at_crossing,1));

%AP
[peakmax loc_max] = islocalmax(Trunk_Vel(frame_dev1:frame_at_crossing,2));
loc_max=find(peakmax==1);
[peakmin,loc_min] = islocalmin(Trunk_Vel(frame_dev1:frame_at_crossing,2));
loc_min=find(peakmin==1);
peak_AP=[loc_min;loc_max];peak_AP=sort(peak_AP);

Zero_crossing_AP_total=numel(peak_AP);
Zero_Crossing_AP_dev1=numel(find(peak_AP<=(frame_dev2-frame_dev1)));
Zero_Crossing_AP_dev2=numel(find(peak_AP>(frame_dev2-frame_dev1)));

%vertical
[peakmax,loc_max] = islocalmax(Trunk_Vel(frame_dev1:frame_at_crossing,3));
loc_max=find(peakmax==1)
[peakmin,loc_min] = islocalmin(Trunk_Vel(frame_dev1:frame_at_crossing,3));
loc_min=find(peakmin==1);
peak_V=[loc_min;loc_max];peak_V=sort(peak_V);

Zero_crossing_V_total=numel(peak_V);
Zero_Crossing_V_dev1=numel(find(peak_V<=(frame_dev2-frame_dev1)));
Zero_Crossing_V_dev2=numel(find(peak_V>(frame_dev2-frame_dev1)));

Zero_Crossing=[{'Zero_crossing_AP_total','Zero_Crossing_AP_dev1','Zero_Crossing_AP_dev2','Zero_crossing_ML_total','Zero_Crossing_ML_dev1','Zero_Crossing_ML_dev2','Zero_crossing_V_total','Zero_Crossing_V_dev1','Zero_Crossing_V_dev2';Zero_crossing_AP_total,Zero_Crossing_AP_dev1,Zero_Crossing_AP_dev2,Zero_crossing_ML_total,Zero_Crossing_ML_dev1,Zero_Crossing_ML_dev2,Zero_crossing_V_total,Zero_Crossing_V_dev1,Zero_Crossing_V_dev2}]

%%calcul complementary variables

%ML (I commented some lines, there are not usefull, so i cut them, but let
%them there is needed. 
%maxAccML_R=max(Trunk_Acc(frame_dev1:frame_at_crossing,1));
%maxAccML_L=abs(min(Trunk_Acc(frame_dev1:frame_at_crossing,1)));
maxAccML=max(abs(Trunk_Acc(frame_dev1:frame_at_crossing,1)));

%maxAccML_R_dev1=max(Trunk_Acc(frame_dev1:frame_dev2,1));
%maxAccML_L_dev1=abs(min(Trunk_Acc(frame_dev1:frame_dev2,1)));
maxAccML_dev1=max(abs(Trunk_Acc(frame_dev1:frame_dev2,1)));

%maxAccML_R_dev2=max(Trunk_Acc(frame_dev2:frame_at_crossing,1));
%maxAccML_L_dev2=abs(min(Trunk_Acc(frame_dev2:frame_at_crossing,1)));
maxAccML_dev2=max(abs(Trunk_Acc(frame_dev2:frame_at_crossing,1)));

%AP
maxAccAP=max(abs(Trunk_Acc(frame_dev1:frame_at_crossing,2)));
maxAccAP_dev1=max(abs(Trunk_Acc(frame_dev1:frame_dev2,2)));
maxAccAP_dev2=max(abs(Trunk_Acc(frame_dev2:frame_at_crossing,2)));

%Vertical
maxAccV=max(abs(Trunk_Acc(frame_dev1:frame_at_crossing,3)));
maxAccV_dev1=max(abs(Trunk_Acc(frame_dev1:frame_dev2,3)));
maxAccV_dev2=max(abs(Trunk_Acc(frame_dev2:frame_at_crossing,3)));

Acc_Max=[{'maxAccAP','maxAccAP_dev1','maxAccAP_dev2','maxAccML','maxAccML_dev1','maxAccML_dev2','maxAccV','maxAccV_dev1','maxAccV_dev2';maxAccAP,maxAccAP_dev1,maxAccAP_dev2,maxAccML,maxAccML_dev1,maxAccML_dev2,maxAccV,maxAccV_dev1,maxAccV_dev2}]

%%Root Mean Square (RMS): dispersion measure relative to zero, take into
%%accunt the amplitude od the acceleration

RMS=sqrt(sum(Trunk_Acc(frame_dev1:frame_at_crossing,:).^2)/numel(Trunk_Acc(frame_dev1:frame_at_crossing,1)));
RMS_Dev1=sqrt(sum(Trunk_Acc(frame_dev1:frame_dev2,:).^2)/numel(Trunk_Acc(frame_dev1:frame_dev2,1)));
RMS_Dev2=sqrt(sum(Trunk_Acc(frame_dev2:frame_at_crossing,:).^2)/numel(Trunk_Acc(frame_dev2:frame_at_crossing,1)));


RMS={'RMS_AP','RMS_AP_Dev1','RMS_AP_Dev2','RMS_ML','RMS_ML_Dev1','RMS_ML_Dev2','RMS_vertical','RMS_V_Dev1','RMS_V_Dev2';RMS(2),RMS_Dev1(2),RMS_Dev2(2),RMS(1),RMS_Dev1(1),RMS_Dev2(1),RMS(3),RMS_Dev1(3),RMS_Dev2(3)}


%%Jerk

%derivate to obtain jerk from acceleration
for j=2:numel(cut_trunk_X_acc)-1
jerk_ML(j-1,1)=((cut_trunk_X_acc(j+1,1)-cut_trunk_X_acc(j-1,1))/(2*1/90)); %le x2-x1, il y a 2 temps donc 2*(1/fr�quence) pour avoir le temps entre les 2
jerk_AP(j-1,1)=((cut_trunk_Y_acc(j+1,1)-cut_trunk_Y_acc(j-1,1))/(2*1/90));
jerk_V(j-1,1)=((cut_trunk_Z_acc(j+1,1)-cut_trunk_Z_acc(j-1,1))/(2*1/90));
end

ndev=frame_dev2-frame_dev1;
nt=numel(cut_trunk_acc(:,1));
time=linspace(0,(nt/90),nt)'; 
d_time=time(2:(nt-1));
Sqr_jerk_AP=jerk_AP.^2; Square_jerk_AP=trapz(d_time,Sqr_jerk_AP);
Sqr_jerk_AP_dev1=Sqr_jerk_AP(1:ndev);
Square_jerk_AP_dev1=trapz(d_time(1:ndev),Sqr_jerk_AP_dev1);
Sqr_jerk_AP_dev2=Sqr_jerk_AP(ndev:end);
Square_jerk_AP_dev2=trapz(d_time(ndev:end),Sqr_jerk_AP_dev2);

Sqr_jerk_ML=jerk_ML.^2; Square_jerk_ML=trapz(d_time,Sqr_jerk_ML);
Sqr_jerk_ML_dev1=Sqr_jerk_ML(1:ndev);
Square_jerk_ML_dev1=trapz(d_time(1:ndev),Sqr_jerk_ML_dev1);
Sqr_jerk_ML_dev2=Sqr_jerk_ML(ndev:end);
Square_jerk_ML_dev2=trapz(d_time(ndev:end),Sqr_jerk_ML_dev2);

Sqr_jerk_V=jerk_V.^2; Square_jerk_V=trapz(d_time,Sqr_jerk_V);
Sqr_jerk_V_dev1=Sqr_jerk_V(1:ndev);
Square_jerk_V_dev1=trapz(d_time(1:ndev),Sqr_jerk_V_dev1);
Sqr_jerk_V_dev2=Sqr_jerk_V(ndev:end);
Square_jerk_V_dev2=trapz(d_time(ndev:end),Sqr_jerk_V_dev2);

%There was an error in the previous code, the frame was used in the calcul  
%and not the time. It is not very sad because we didn't used this variable 
% finally and it was the same error for all participant and all the condition.
% Additionnaly, the diff function is not recommanded and the loop is better. 

Jerk=[{'Square_jerk_AP','Square_jerk_AP_dev1','Square_jerk_AP_dev2','Square_jerk_ML','Square_jerk_ML_dev1','Square_jerk_ML_dev2','Square_jerk_V','Square_jerk_V_dev1','Square_jerk_V_dev2';Square_jerk_AP,Square_jerk_AP_dev1,Square_jerk_AP_dev2,Square_jerk_ML,Square_jerk_ML_dev1,Square_jerk_ML_dev2,Square_jerk_V,Square_jerk_V_dev1,Square_jerk_V_dev2}]
 
graph2=figure
subplot(2,3,1)
plot(time,cut_trunk_Y_acc,time,zero,'--r',time(peak_AP),cut_trunk_Y_acc(peak_AP),'*')
title('AP Acc')
subplot(2,3,4)
plot(d_time,jerk_AP,time,zero,'--r')
title('AP Jerk')
subplot(2,3,2)
plot(time,cut_trunk_X_acc,time,zero,'--r',time(peak_ML),cut_trunk_X_acc(peak_ML),'*')
title('ML Acc')
subplot(2,3,5)
plot(d_time,jerk_ML,time,zero,'--r')
title('ML Jerk')
subplot(2,3,3)
plot(time,cut_trunk_Z_acc,time,zero,'--r',time(peak_V),cut_trunk_Z_acc(peak_V),'*')
title('Vertical Acc')
subplot(2,3,6)
plot(d_time,jerk_V,time,zero,'--r')
title('Vertical Jerk')
hold off
 
figure_fluidity_file=fullfile(output_folder,strrep(file, '.xlsx', '_fluidity'))
saveas(graph2,figure_fluidity_file,'png')

fluidity=[Zero_Crossing,RMS,Jerk,Acc_Max];

end

