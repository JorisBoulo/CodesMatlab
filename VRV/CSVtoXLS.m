%% Ouvrir le Fichier
clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Pr�cision Oculus\Data4');
if isequal(subject_path,0)
    return;
end

[file, Data_path]=uigetfile('.xlsx','Select Data File',subject_path);
if (isequal(file, 0))
    return;
end

file_Data=fullfile(Data_path,file);
%%
Data_raw=readtable(file);

%% S�paration des donn�es

trial_id=Data_raw(:,10);trial_id=table2array(trial_id);trial_id=split(trial_id,"-");trial_id=trial_id(:,2);trial_id=str2double (trial_id);
SizeDataRaw=size(trial_id,1); num=trial_id(SizeDataRaw,1);

for n=1:num
    FirstF=find(trial_id(:,1)==n,1,'first');
    LastF=find(trial_id(:,1)==n,1,'last');
    Data_trial=Data_raw(FirstF:LastF,:);Data_trial=rmmissing(Data_trial,2);
    Data_trial=table2cell(Data_trial);
    [trail_name,Data_trial_ref] = getvariables(Data_trial);
    % ajouter le butterworth filter
%     Data_reform=array2table(Data_trial_ref,'VariableNames',{'Time'...
%         'Frame_num' 'Frame_time' 'Player_posX' 'Player_posY' ...
%         'Player_posZ' 'Player_rotX' 'Player_rotY' 'Player_rotZ' 'VA_posX' ...
%         'VA_posY' 'VA_posZ' 'VA_rotX' 'VA_rotY' 'VA_rotZ' });
%     fname=[trail_name,"Quest.xlsx"];fname=join(fname);
%     writetable (Data_reform,fname)
    trail_name=char(trail_name);
    s1.Data = Data_trial_ref;
    save(trail_name,'-struct','s1')
end


