% clearvars;clc;

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Participant\';
Input='\2_TransformData\';
Output='\3_Output\';

Parti={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35'};
Condi={'01','02','03','04','05','06','08','09','10','11','12','14','15','16','17','18','19','20','23','24','25','26','27','28','29','30','31','32','34','35','36','37','38','39','40'};


excelInactiveFilePath = "C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\VariablesQuestionnaires.xlsx";
% Read the Excel sheets
Activity = readtable(excelInactiveFilePath);
conditionOrder = readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Conditions\ConditionOrder.xlsx');


% Define the path to the Excel sheet
excelFilePath = 'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\CrowdFullData.xlsx';

% Read the Excel sheet
raw = readtable(excelFilePath);
headers =  raw.Properties.VariableNames; % First row contains headers
data = table2struct(raw); % Remaining rows contain data

% Initialize variables outside the loop
letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
sequenceToLetterMap = containers.Map('KeyType', 'char', 'ValueType', 'char');
uniqueSequences = {};

% Function to convert a sequence to a string for easy comparison
sequenceToString = @(seq) sprintf('%d-', seq);
TrajectoryLenght = {};
%
for t=6
for h=1
for j=33
fullNames_Quest{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_Quest.mat',Parti{t},Condi{j}))));
fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_MM.mat',Parti{t},Condi{j}))));
% Check if the file exists
        fileExists = exist(fullNames_Quest{j}, 'file') == 2;
        
%         % Check if the cell is empty
%         cellIsEmpty = isempty(TrajectoryLenght{j,t});
%         
%         % Check if the cell contains only zeros
%         cellIsZero = TrajectoryLenght{j,t} == 0;
%         
        % Combine the conditions
 if fileExists %&(cellIsEmpty || cellIsZero)
%import data
 file_Quest=fullNames_Quest{j,t};
 try 
     file_EXP=fullNames_MM{j,t};
 catch
     Raison{j,t}='No C3D file';
 end

 input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
 output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);
 CrowdConditionName = conditionOrder{str2double (Condi{j}), 1}{1};
 xmlFilePath = fullfile('C:\UMANS Wouter\UMANS Wouter\UMANS\agents\', [CrowdConditionName, '.xml']);
 XlmAgentsPositions= xmlread(xmlFilePath);


%% % *Importing variable from motion monitor and Quest files*

%MotionMonitor variables 
try 
    [Head_position,~,~,Probed_point_contact,~,~,~]...
            = GetMotionMonitorVariables_Crowd(file_EXP);
catch
    Raison{j,t}='No C3D file';
end

%Quest variables
[HMD_position, HMD_rotation, ~, Gaze,HMD_position_raw,HMD_position_cut]=GetQuestVariables_Crowd(file_Quest, Head_position);
QuestPosition{j,t}=HMD_position;

%% Walking speed depending of the food stall 
if j >= 31 && j <= 35
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity=movmean(HMD_Velocity,8);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));
TimeToCrowd{j,t}=length(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd))/90;
Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));

STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));
TrajectoryLenght{j,t} = 0;
% Loop through each pair of consecutive points
for i = StartCrowd:find(HMD_position(:,2)>5,1,'first') - 1
    distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
    TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
end
end
%% Other 
[humanShoulders{j,t},percentage_goal_direction{j,t},percentage_shorter_edge{j,t},local_densities_voronoi{j,t},local_densities_circle{j,t},mean_walking_speed{j,t},local_density{j,t},ShoulderAngle{j,t},AgentsCrossed{j,t}, collision_countL{j,t},collision_countR{j,t},TrajectorySequence{j,t},Aperture_sizes{j,t},closestShoulder,translated_polygons,K,AgentsPositions,AgentName,ShoulderWidth{j,t},RelativeApertureSize{j,t}] =...
    FindClearance_Crowd(HMD_position_cut,Probed_point_contact, XlmAgentsPositions ,HMD_position,Head_position,HMD_Velocity_scalaire{j,t},CrowdConditionName,j);

Collisions{j,t}=collision_countL{j,t}+collision_countR{j,t};

%% Eye Tracking 
[output_gaze{j,t},DistanceParticipantAgent{j,t}] =FindGaze_Crowd(Gaze,AgentsPositions,HMD_position,StartCrowd,AgentName,CrowdConditionName,j);
Raison{j,t}='Good';

else
    Raison{j,t}='File';
 end
end
end
end


%% Mean Data
for t=1:27
    for j=31:35
        VariableTimeToCrowd(j,t)=ZeroEmptyToNaN(TimeToCrowd{j, t});
        VariableTrajectoryLenght(j,t)=ZeroEmptyToNaN(TrajectoryLenght{j, t}); 
        
        if isempty(percentage_shorter_edge{j, t})
            Variablepercentage_shorter_edge(j,t)=nan;
        else
            Variablepercentage_shorter_edge(j,t)=percentage_shorter_edge{j, t};
        end
        if isempty(percentage_goal_direction{j, t})
            Variablepercentage_goal_direction(j,t)=nan;
        else
            Variablepercentage_goal_direction(j,t)=percentage_goal_direction{j, t};
        end

        %Mean distance Focus 
        MeanFocus(j-30,t)=mean(DistanceParticipantAgent{j, t});
        %Mean Velocity in Crowd 
        MeanVelocityInCrowd(j-30,t)=mean(Velocity_inCrowd{j, t});
        %Mean Velocity before crowd  
        MeanVelocityBeforeCrowd(j-30,t)=mean(Velocity_BeforeCrowd{j, t});
        %Mean Accel in Crowd 
        MeanAccelInCrowd(j-30,t)=mean(abs(Accel_inCrowd{j, t}));
        
        % Shoulder Angle
        if isempty(ShoulderAngle{j, t})
            MeanShoulderAngle(j,t)=nan;
        else
            MeanShoulderAngle(j-30,t)=mean(abs(ShoulderAngle{j, t}));
        end
        
    end 
end
%%
mean(MeanFocus,'omitnan')
