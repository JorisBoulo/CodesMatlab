clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Universit� Laval\Documents\Doctorat\Etude 4\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
outputpath=fullfile(subject_path,'2_TransformData');
 
numParticipant=erase(subject_path,'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Participant\');
Rep={'01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15';'16';'17';'18';'19';'20';'21';'22';'23';'24';'25';'26';'27';'28';'29';'30';'31';'32';'33';'34';'35';'36';'37';'38';'39';'40'};
Sensor={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
Error=1; 
A=1;
%% for MM 
     for j=1:40
        NamesMM{1,j}=sprintf('%s_%s.xlsx',numParticipant,Rep{j});
        inputfilepath=fullfile(inputpath,NamesMM{1,j});
        if exist(inputfilepath, 'file')==2
        MMData=readcell(inputfilepath);
        name=sprintf('%s_%s_MM.mat',numParticipant,Rep{j});
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'MMData')
        else 
            Error= Error+1; 
        end 
     end 

%% for Quest
cd(inputpath);
fileList = dir('*.csv');
for j = 1:size(fileList, 1)
    % Read the file content as text
    fileContent = fileread(fileList(j).name);
    
    % Remove quotes and brackets
    cleanedContent = erase(fileContent, {'"', '[', ']'});

    % Write the cleaned content to a temporary file
    tempFileName = fullfile(tempdir, 'cleaned_temp.csv');
    fid = fopen(tempFileName, 'w');
    a=fwrite(fid, cleanedContent);
    fclose(fid);
    
    % Read the cleaned data
    DataQuest = readcell(tempFileName);
    
    % Generate the output file name
    Split = split(fileList(j).name, '-');
    Erase = erase(Split(1), '.json');
    name = strcat(Erase, '_Quest.mat');
    outputfilepath = fullfile(outputpath, name);
    
    % Save the processed data
    save(outputfilepath{1,1}, 'DataQuest');
end