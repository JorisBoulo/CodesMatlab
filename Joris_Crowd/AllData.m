clearvars;clc;

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Participant\';
Input='\2_TransformData\';
Output='\3_Output\';

Parti={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28'};
Low={'01','09','12','13','26','28','34','35'};
High={'01','02','03','05','07','08','09','10','11','12','13','15','16','17','18','19','20','21','23','24','25','26','27','28','29','31','32','33','34','35','36','37','38','39'};
Unobstructed={'04','06','14','22','30'};

Condi={High,Low};
CondiName={'High','Low'};
excelInactiveFilePath = "C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\VariablesQuestionnaires.xlsx";
% Read the Excel sheet
Activity = readtable(excelInactiveFilePath);

% Define the path to the Excel sheet
excelFilePath = 'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\CrowdFullData.xlsx';

% Read the Excel sheet
raw = readtable(excelFilePath);
headers =  raw.Properties.VariableNames; % First row contains headers
data = table2struct(raw); % Remaining rows contain data

% Initialize variables outside the loop
letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
sequenceToLetterMap = containers.Map('KeyType', 'char', 'ValueType', 'char');
uniqueSequences = {};

% Function to convert a sequence to a string for easy comparison
sequenceToString = @(seq) sprintf('%d-', seq);
%%
for t=1:4
for j=1:34
fullNames_Quest{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_Quest.mat',Parti{t},string(High{1,j})))));
if exist(fullNames_Quest{j,t}, 'file')==2 
    %import data
 file_Quest=fullNames_Quest{j,t};

 input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
 output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);

%% % *Importing variable from motion monitor and Quest files*

%Quest variables
[HMD_position, HMD_rotation, ~, Gaze, cutoff_vicon,HMD_position_raw]=Copy_of_GetQuestVariables_Crowd(file_Quest);

%% ExtractVariables
%% Walking speed and Maximal ML speed 
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5.5,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5.5,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));

Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));

STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5.5,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5.5,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));

%% Other 
[mean_walking_speed{j,t},local_density{j,t},ShoulderAngle{j,t},AgentsCrossed{j,t},clearanceL{j,t},clearanceR{j,t}, collision_countL{j,t},collision_countR{j,t},TrajectorySequence{j,t},Aperture_sizes{j,t},closestShoulder,translated_polygons,K,AgentsPositions,AgentName] =...
    Copy_of_FindClearance_Crowd(string(High{1,j}) ,HMD_position,HMD_Velocity_scalaire{j,t});

Collisions{j,t}=collision_countL{j,t}+collision_countR{j,t};

% %% PLOT
% if h==1
% if j==1
%     figure(1);
%     hold on;
%     colors = jet(length(translated_polygons)); 
%     % Plot polygons
%     for i = 1:length(translated_polygons)
%         polygon = translated_polygons{i};
%         trisurf(K{i},polygon(:, 1), polygon(:, 2), polygon(:, 3),'FaceColor', colors(i, :), 'EdgeColor',colors(i, :), 'FaceAlpha', 0.25);
%     end
% end
% 
%     if t==1
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','r','LineWidth',2);
%     end
%     end
%     if t==2
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','b','LineWidth',2);
%     end
%     end
%     if t==3
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','green','LineWidth',2);
%     end
%     end
%     if t==4
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','black','LineWidth',2);
%     end
%     end
%     
%     xlabel('X');
%     ylabel('Y');
%     zlabel('Z');
%     legend;
%     hold off;
% end
% %% Plot Low
% if h==2
% if j==1
%     figure(2);
%     hold on;
%     colors = jet(length(translated_polygons)); 
%     % Plot polygons
%     for i = 1:length(translated_polygons)
%         polygon = translated_polygons{i};
%         trisurf(K{i},polygon(:, 1), polygon(:, 2), polygon(:, 3),'FaceColor', colors(i, :), 'EdgeColor',colors(i, :), 'FaceAlpha', 0.25);
%     end
% end
% 
%     if t==1
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','r','LineWidth',2);
%     end
%     end
%     if t==2
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','b','LineWidth',2);
%     end
%     end
%     if t==3
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','green','LineWidth',2);
%     end
%     end
%     if t==4
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','black','LineWidth',2);
%     end
%     end
%     
%     xlabel('X');
%     ylabel('Y');
%     zlabel('Z');
%     legend;
%     hold off;
% end

%% Trajectory classifier 
% TrajectoryLenght{j,t} = 0;
% % Loop through each pair of consecutive points
% for i = StartCrowd:find(HMD_position(:,2)>5.5,1,'first') - 1
%     % Calculate the Euclidean distance between consecutive points
%     distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
%     
%     % Add the distance to the total trajectory length
%     TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
% end
% 
% % Trajectory Sequence 
% % Convert the current sequence to a string
% currentSequenceStr = sequenceToString(TrajectorySequence{j, t});
% 
% % Check if the current sequence is already in the uniqueSequences list
% if isKey(sequenceToLetterMap, currentSequenceStr)
%     % If it is, use the existing letter
%     categorizedTrajectorySequence{j,t} = sequenceToLetterMap(currentSequenceStr);
% else
%     % If it is not, add it to the uniqueSequences list and assign a new letter
%     uniqueSequences{end + 1} = currentSequenceStr;
%     newLetter = letters(length(uniqueSequences));
%     sequenceToLetterMap(currentSequenceStr) = newLetter;
%     categorizedTrajectorySequence{j,t} = newLetter;
% end
% 
% % disp(['Trial ', num2str(t), ', Condition ', num2str(h), ', Repetition ', num2str(j), ': Categorized TrajectorySequence = ', categorizedTrajectorySequence]);
% 
% %% Eye Tracking 
% [output_gaze,DistanceParticipantAgent{j,t}] =FindGaze_Crowd(Gaze,AgentsPositions,HMD_position,StartCrowd,AgentName);

%% Saving Data



% % Find the columns for participant, repetition, and condition
% participantCol = find(strcmp(headers, 'Participant'));
% repetitionCol = find(strcmp(headers, 'Repetition'));
% conditionCol = find(strcmp(headers, 'Condition'));
% % Define the participant, repetition, and condition
% participant = sprintf("P%s", Parti{t});
% repetition = j;
% condition = CondiName{h};
% % Find the row corresponding to the participant, repetition, and condition
% rowIdx = find(strcmp(table2array(raw(:, participantCol)), participant) & ...
%               table2array(raw(:, repetitionCol)) == repetition & ...
%               strcmp(table2array(raw(:, conditionCol)), condition));
% 
% % Save the variables into the appropriate cells
% raw(rowIdx, find(strcmp(headers, 'VelocityCrowd'))) = Velocity_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'VelocityBeforeCrowd'))) = Velocity_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'AccelCrowd'))) = Accel_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'AccelBeforeCrowd'))) = Accel_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'TrajectoryLenght'))) = TrajectoryLenght(j, t);
% raw(rowIdx, find(strcmp(headers, 'ApertureSequence'))) = categorizedTrajectorySequence(j,t);
% raw{rowIdx, find(strcmp(headers, 'NumbOfAperture'))} =size(TrajectorySequence(j, t),1);
% raw(rowIdx, find(strcmp(headers, 'CollisionCount'))) = Collisions(j, t);
% raw{rowIdx, find(strcmp(headers, 'MeanSizeOfAppertureDuringTrial'))} = mean(Aperture_sizes{j,t},1);
% raw{rowIdx, find(strcmp(headers, 'MeanShoulderOrientation'))} = mean(ShoulderAngle{j,t});
% raw(rowIdx, find(strcmp(headers, 'Std_Velocity_Crowd'))) = STD_Velocity_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Velocity_BeforeCrowd'))) = STD_Velocity_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Accel_Crowd'))) = STD_Accel_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Accel_BeforeCrowd'))) = STD_Accel_BeforeCrowd(j, t);
% raw{rowIdx, find(strcmp(headers, 'Std_NumbOf_Aperture'))} =std(Aperture_sizes{j,t});
% raw{rowIdx, find(strcmp(headers, 'DistanceParitcipantFocus'))} = mean(cell2mat(DistanceParticipantAgent(j, t)));
% raw{rowIdx, find(strcmp(headers, 'PersentGround'))} = output_gaze(1,1);
% raw{rowIdx, find(strcmp(headers, 'PercentGoal'))} = output_gaze(1,2);
% raw{rowIdx, find(strcmp(headers, 'PercentEnvironment'))} = output_gaze(1,3);
% raw{rowIdx, find(strcmp(headers, 'PercentCrowd'))} = output_gaze(1,4);
%%
%Extract the relevant data for the regression
% walking_speed_in_crowd = cell2mat(Velocity_inCrowd(:));
% mean_aperture_size = cellfun(@mean, Aperture_sizes(:));
% 
% % Perform non-parametric linear regression
% mdl = fitlm(mean_aperture_size, walking_speed_in_crowd, 'linear', 'RobustOpts', 'on');
% 
% % Display the regression results
% disp(mdl);
% 
% % Plot the regression
% figure;
% plot(mdl);
% xlabel('Mean Aperture Size');
% ylabel('Walking Speed in Crowd');
% title('Non-parametric Linear Regression: Walking Speed vs. Mean Aperture Size');

%% Calculate local density for each Delaunay triangle
active_status = table2array(Activity(:, 7));
mean_walking_speedActive=[];
crossed_local_densityActive=[];
ActiveIndex=find(string(active_status)=='Actif');
for j = size(ActiveIndex)
mean_walking_speedActive = [mean_walking_speedActive;cell2mat(mean_walking_speed(:,ActiveIndex(j)))];
crossed_local_densityActive = [crossed_local_densityActive;cell2mat(local_density(:,ActiveIndex(j)))];
end
mean_walking_speedInactive=[];
crossed_local_densityInactive=[];
InactiveIndex=find(string(active_status)=='Inactif');
for j= size(InactiveIndex)
mean_walking_speedInactive = [mean_walking_speedInactive;cell2mat(mean_walking_speed(:,InactiveIndex(j)))];
crossed_local_densityInactive = [crossed_local_densityInactive;cell2mat(local_density(:,InactiveIndex(j)))];
end
% Perform non-parametric linear regression for active participants
mdl_active = fitlm(crossed_local_densityActive, mean_walking_speedActive, 'linear', 'RobustOpts', 'on');
disp('Active Participants:');
disp(mdl_active);

% Perform non-parametric linear regression for inactive participants
mdl_inactive = fitlm(crossed_local_densityInactive, mean_walking_speedInactive, 'linear', 'RobustOpts', 'on');
disp('Inactive Participants:');
disp(mdl_inactive);

% Plot the regression for active participants
figure;
plot(mdl_active); % Plot active participants in blue
hold on;
plot(mdl_inactive); % Plot inactive participants in red
xlabel('Delaunay Triangle Size');
ylabel('Walking Speed in Crowd');
title('Non-parametric Linear Regression: Walking Speed vs. Mean Aperture Size');
hold off;
end
end
end
%% Write the data back to the Excel sheet
%  writetable(raw,excelFilePath);