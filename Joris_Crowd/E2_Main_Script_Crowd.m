% clearvars;clc;

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Participant\';
Input='\2_TransformData\';
Output='\3_Output\';

Parti={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35'};
Condi={'01','02','03','04','05','06','08','09','10','11','12','14','15','16','17','18','19','20','23','24','25','26','27','28','29','30','31','32','34','35','36','37','38','39','40'};


excelInactiveFilePath = "C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\VariablesQuestionnaires.xlsx";
% Read the Excel sheets
Activity = readtable(excelInactiveFilePath);
conditionOrder = readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Conditions\ConditionOrder.xlsx');


% Define the path to the Excel sheet
excelFilePath = 'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\CrowdFullData.xlsx';

% Read the Excel sheet
raw = readtable(excelFilePath);
headers =  raw.Properties.VariableNames; % First row contains headers
data = table2struct(raw); % Remaining rows contain data

% Initialize variables outside the loop
letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
sequenceToLetterMap = containers.Map('KeyType', 'char', 'ValueType', 'char');
uniqueSequences = {};

% Function to convert a sequence to a string for easy comparison
sequenceToString = @(seq) sprintf('%d-', seq);
%
for t=18
for h=1
for j=23
fullNames_Quest{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_Quest.mat',Parti{t},Condi{j}))));
fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_MM.mat',Parti{t},Condi{j}))));
% Check if the file exists
        fileExists = exist(fullNames_Quest{j,t}, 'file') == 2;
%         
%         % Check if the cell is empty
%         cellIsEmpty = isempty(TrajectoryLenght{j,t});
%         
%         % Check if the cell contains only zeros
%         cellIsZero = TrajectoryLenght{j,t} == 0;
        
        % Combine the conditions
 if fileExists %&(cellIsEmpty || cellIsZero)
%import data
 file_Quest=fullNames_Quest{j,t};
 try 
     file_EXP=fullNames_MM{j,t};
 catch
     Raison{j,t}='No C3D file';
 end

 input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
 output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);
 CrowdConditionName = conditionOrder{str2double (Condi{j}), 1}{1};
 xmlFilePath = fullfile('C:\UMANS Wouter\UMANS Wouter\UMANS\agents\', [CrowdConditionName, '.xml']);
 XlmAgentsPositions= xmlread(xmlFilePath);


%% % *Importing variable from motion monitor and Quest files*

%MotionMonitor variables 
try 
    [Head_position,~,~,Probed_point_contact,~,~,~]...
            = GetMotionMonitorVariables_Crowd(file_EXP);
catch
    Raison{j,t}='No C3D file';
end

%Quest variables
[HMD_position, HMD_rotation, ~, Gaze,HMD_position_raw,HMD_position_cut]=GetQuestVariables_Crowd(file_Quest, Head_position);
QuestPosition{j,t}=HMD_position;

%% Walking speed and Maximal ML speed depending of the food stall 
if j >= 1 && j <= 30
    if contains(CrowdConditionName,'(1)')||contains(CrowdConditionName,"2")  
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity=movmean(HMD_Velocity,8);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>3,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>3,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));

Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));
TimeToCrowd{j,t}=length(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd))/90;
STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>3,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>3,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));
TrajectoryLenght{j,t} = 0;
% Loop through each pair of consecutive points
for i = StartCrowd:find(HMD_position(:,2)>3,1,'first') - 1
    distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
    TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
end
    elseif contains(CrowdConditionName,'3')||contains(CrowdConditionName,"4")
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity=movmean(HMD_Velocity,8);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));
TimeToCrowd{j,t}=length(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd))/90;
Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));

STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));
TrajectoryLenght{j,t} = 0;
% Loop through each pair of consecutive points
for i = StartCrowd:find(HMD_position(:,2)>5,1,'first') - 1
    distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
    TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
end
    elseif contains(CrowdConditionName,'5')||contains(CrowdConditionName,"6")
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity=movmean(HMD_Velocity,8);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,1)>3,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,1)>3,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));

TimeToCrowd{j,t}=length(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd))/90;
Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));

STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,1)>3,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,1)>3,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));
TrajectoryLenght{j,t} = 0;
% Loop through each pair of consecutive points
for i = StartCrowd:find(HMD_position(:,1)>3,1,'first') - 1
    distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
    TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
end
    end 
else
StartCrowd=find(HMD_position(:,1)>-1,1,'first');
HMD_Velocity=diff(HMD_position)./(1/90);
HMD_Velocity=movmean(HMD_Velocity,8);
HMD_Velocity_scalaire{j,t}=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2);
HMD_Velocity_onset_cross{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_Velocity_scalaire{j,t}(20:end,1)>0.3,1,'first')+20:(StartCrowd)));

Velocity_inCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
Accel_inCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
Jerk_inCrowd{j,t}=mean(diff(Accel_inCrowd{j,t})./(1/90));

Velocity_BeforeCrowd{j,t}=mean(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
Accel_BeforeCrowd{j,t}=mean(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
Jerk_BeforeCrowd{j,t}=mean(diff(Accel_BeforeCrowd{j,t})./(1/90));

STD_Velocity_inCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')));
STD_Accel_inCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}((StartCrowd):find(HMD_position(:,2)>5,1,'first')))./(1/90));
STD_Jerk_inCrowd{j,t}=std(diff(Accel_inCrowd{j,t})./(1/90));

STD_Velocity_BeforeCrowd{j,t}=std(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd));
STD_Accel_BeforeCrowd{j,t}=std(diff(HMD_Velocity_scalaire{j,t}(find(HMD_position(:,1)>-3,1,'first'):StartCrowd)));
STD_Jerk_BeforeCrowd{j,t}=std(diff(Accel_BeforeCrowd{j,t})./(1/90));
TrajectoryLenght{j,t} = 0;
% Loop through each pair of consecutive points
for i = StartCrowd:find(HMD_position(:,2)>5,1,'first') - 1
    distance = norm(HMD_position(i+1, :) - HMD_position(i, :));
    TrajectoryLenght{j,t} = TrajectoryLenght{j,t} + distance;
end

end
%% Other 
[humanShoulders{j,t},percentage_goal_direction{j,t},percentage_shorter_edge{j,t},local_densities_voronoi{j,t},local_densities_circle{j,t},mean_walking_speed{j,t},local_density{j,t},ShoulderAngle{j,t},AgentsCrossed{j,t}, collision_countL{j,t},collision_countR{j,t},TrajectorySequence{j,t},Aperture_sizes{j,t},closestShoulder,translated_polygons,K,AgentsPositions,AgentName,ShoulderWidth{j,t},RelativeApertureSize{j,t}] =...
FindClearance_Crowd(HMD_position_cut,Probed_point_contact, XlmAgentsPositions ,HMD_position,Head_position,HMD_Velocity_scalaire{j,t},CrowdConditionName,j);

Collisions{j,t}=collision_countL{j,t}+collision_countR{j,t};

%% Eye Tracking 
[output_gaze{j,t},DistanceParticipantAgent{j,t}] =FindGaze_Crowd(Gaze,AgentsPositions,HMD_position,StartCrowd,AgentName,CrowdConditionName,j);
Raison{j,t}='Good';
%% Saving Data

% 
% 
% % Find the columns for participant, repetition, and condition
% participantCol = find(strcmp(headers, 'Participant'));
% repetitionCol = find(strcmp(headers, 'Repetition'));
% conditionCol = find(strcmp(headers, 'Condition'));
% % Define the participant, repetition, and condition
% participant = sprintf("P%s", Parti{t});
% repetition = j;
% condition = CondiName{h};
% % Find the row corresponding to the participant, repetition, and condition
% rowIdx = find(strcmp(table2array(raw(:, participantCol)), participant) & ...
%               table2array(raw(:, repetitionCol)) == repetition & ...
%               strcmp(table2array(raw(:, conditionCol)), condition));
% 
% % Save the variables into the appropriate cells
% raw(rowIdx, find(strcmp(headers, 'VelocityCrowd'))) = Velocity_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'VelocityBeforeCrowd'))) = Velocity_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'AccelCrowd'))) = Accel_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'AccelBeforeCrowd'))) = Accel_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'TrajectoryLenght'))) = TrajectoryLenght(j, t);
% raw(rowIdx, find(strcmp(headers, 'ApertureSequence'))) = categorizedTrajectorySequence(j,t);
% raw{rowIdx, find(strcmp(headers, 'NumbOfAperture'))} =size(TrajectorySequence(j, t),1);
% raw(rowIdx, find(strcmp(headers, 'CollisionCount'))) = Collisions(j, t);
% raw{rowIdx, find(strcmp(headers, 'MeanSizeOfAppertureDuringTrial'))} = mean(Aperture_sizes{j,t},1);
% raw{rowIdx, find(strcmp(headers, 'MeanShoulderOrientation'))} = mean(ShoulderAngle{j,t});
% raw(rowIdx, find(strcmp(headers, 'Std_Velocity_Crowd'))) = STD_Velocity_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Velocity_BeforeCrowd'))) = STD_Velocity_BeforeCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Accel_Crowd'))) = STD_Accel_inCrowd(j, t);
% raw(rowIdx, find(strcmp(headers, 'Std_Accel_BeforeCrowd'))) = STD_Accel_BeforeCrowd(j, t);
% raw{rowIdx, find(strcmp(headers, 'Std_NumbOf_Aperture'))} =std(Aperture_sizes{j,t});
% raw{rowIdx, find(strcmp(headers, 'DistanceParitcipantFocus'))} = mean(cell2mat(DistanceParticipantAgent(j, t)));
% raw{rowIdx, find(strcmp(headers, 'PersentGround'))} = output_gaze(1,1);
% raw{rowIdx, find(strcmp(headers, 'PercentGoal'))} = output_gaze(1,2);
% raw{rowIdx, find(strcmp(headers, 'PercentEnvironment'))} = output_gaze(1,3);
% raw{rowIdx, find(strcmp(headers, 'PercentCrowd'))} = output_gaze(1,4);

else
    Raison{j,t}='File';
 end
end
end
end
%% Write the data back to the Excel sheet
%  writetable(raw,excelFilePath);
% 
%% SPEED DENSITY §§§§§§§§§§§§§§§§§§§§§§§§§§
% Initialize variables
active_status = table2array(Activity(:, 7));
mean_walking_speedActive = [];
crossed_local_densityActive = [];
ActiveIndex = find(string(active_status) == 'Actif');
InactiveIndex = find(string(active_status) == 'Inactif');
figure
% Loop through each person


% Repeat for inactive participants
for idx = 1:length(InactiveIndex)
    t = InactiveIndex(idx); 
    all_local_densities_voronoi = [];
    all_local_densities_circle = [];
    all_HMD_Velocity_scalaire = [];
    
    for j = 1:36
        try
            nonInfIndicesVoronoi = find(~isinf(local_densities_voronoi{j, t}));
            nonInfIndicesCircle = find(~isinf(local_densities_circle{j, t}));
            
            % Aggregate data
            all_local_densities_voronoi = [all_local_densities_voronoi; local_densities_voronoi{j, t}(nonInfIndicesVoronoi(100):nonInfIndicesVoronoi(end-100), 1)];
            all_local_densities_circle = [all_local_densities_circle; local_densities_circle{j, t}(nonInfIndicesCircle(100):nonInfIndicesCircle(end-100), 1)];
            all_HMD_Velocity_scalaire = [all_HMD_Velocity_scalaire; HMD_Velocity_scalaire{j, t}(nonInfIndicesVoronoi(100):nonInfIndicesVoronoi(end-100), 1)];
        catch
            % Handle any errors
            continue;
        end
    end
    % Find indices where all_local_densities_voronoi is less than or equal to 4
    validIndices = all_local_densities_voronoi <= 5;
    
    % Keep only the valid elements
    all_local_densities_voronoi = all_local_densities_voronoi(validIndices);
    all_HMD_Velocity_scalaire = all_HMD_Velocity_scalaire(validIndices);
    all_local_densities_circle=all_local_densities_circle(validIndices);

    validIndices = all_HMD_Velocity_scalaire <= 4;
    
    % Keep only the valid elements
    all_local_densities_voronoi = all_local_densities_voronoi(validIndices);
    all_HMD_Velocity_scalaire = all_HMD_Velocity_scalaire(validIndices);
    all_local_densities_circle=all_local_densities_circle(validIndices);
    % Perform linear regression for the aggregated data
    try
        VororoiSpeed = fitlm(all_local_densities_voronoi, all_HMD_Velocity_scalaire, 'linear', 'RobustOpts', 'on');
        Intercept(t)=VororoiSpeed.Coefficients{1,1};
        x1(t)=VororoiSpeed.Coefficients{2,1};
        x_values = linspace(min(all_local_densities_voronoi), max(all_local_densities_voronoi), 100);
        y_values =  Intercept(t) + x1(t) * x_values;
        
        % Plot the original data points
        hold on ;
         scatter(all_local_densities_voronoi, all_HMD_Velocity_scalaire,"red",".",'LineWidth', .2);
        hold on;
        
        % Plot the regression line
%        plot(x_values, y_values, 'LineWidth', 2);


        CircleSpeed = fitlm(all_local_densities_circle, all_HMD_Velocity_scalaire, 'linear', 'RobustOpts', 'on');
        
    catch
        % Handle any errors
        continue;
    end
end

for idx = 1:length(ActiveIndex)
    t = ActiveIndex(idx); 
    all_local_densities_voronoi = [];
    all_local_densities_circle = [];
    all_HMD_Velocity_scalaire = [];
    
    for j = 1:30
        try
            nonInfIndicesVoronoi = find(~isinf(local_densities_voronoi{j, t}));
            nonInfIndicesCircle = find(~isinf(local_densities_circle{j, t}));
            
            % Aggregate data
            all_local_densities_voronoi = [all_local_densities_voronoi; local_densities_voronoi{j, t}(nonInfIndicesVoronoi(100):nonInfIndicesVoronoi(end-100), 1)];
            all_local_densities_circle = [all_local_densities_circle; local_densities_circle{j, t}(nonInfIndicesCircle(100):nonInfIndicesCircle(end-100), 1)];
            all_HMD_Velocity_scalaire = [all_HMD_Velocity_scalaire; HMD_Velocity_scalaire{j, t}(nonInfIndicesVoronoi(100):nonInfIndicesVoronoi(end-100), 1)];
        catch
            % Handle any errors
            disp error
        end
    end
    % Find indices where all_local_densities_voronoi is less than or equal to 4
    validIndices = all_local_densities_voronoi <= 5;
    
    % Keep only the valid elements
    all_local_densities_voronoi = all_local_densities_voronoi(validIndices);
    all_HMD_Velocity_scalaire = all_HMD_Velocity_scalaire(validIndices);
    all_local_densities_circle=all_local_densities_circle(validIndices);

    validIndices = all_HMD_Velocity_scalaire <= 4;
    
    % Keep only the valid elements
    all_local_densities_voronoi = all_local_densities_voronoi(validIndices);
    all_HMD_Velocity_scalaire = all_HMD_Velocity_scalaire(validIndices);
    all_local_densities_circle=all_local_densities_circle(validIndices);

    % Perform linear regression for the aggregated data
    try
        VororoiSpeed = fitlm(all_local_densities_voronoi, all_HMD_Velocity_scalaire, 'linear', 'RobustOpts', 'on');
        Intercept(t)=VororoiSpeed.Coefficients{1,1};
        
        x1(t)=VororoiSpeed.Coefficients{2,1};
        
        x_values = linspace(min(all_local_densities_voronoi), max(all_local_densities_voronoi), 100);
        y_values = Intercept(t) + x1(t) * x_values;
        
        % Plot the original data points
        hold on;
        scatter(all_local_densities_voronoi, all_HMD_Velocity_scalaire,"blue","+",'LineWidth', .0000001);
        hold on;
        
        % Plot the regression line
%          plot(x_values, y_values, 'LineWidth', 2);


        CircleSpeed = fitlm(all_local_densities_circle, all_HMD_Velocity_scalaire, 'linear', 'RobustOpts', 'on');
    catch
        % Handle any errors
        continue;
    end
end
%%
x1Active=mean(x1(ActiveIndex));
x1Inactive=mean(x1(InactiveIndex));
InterActive=mean(Intercept(ActiveIndex));
InterInactive=mean(Intercept(InactiveIndex));
y_valuesActive =  InterActive + x1Active * x_values;
hold on
plot(x_values, y_valuesActive, 'LineWidth', 2);
y_valuesInactive =  InterInactive + x1Inactive * x_values;
plot(x_values, y_valuesInactive, 'LineWidth', 2);

xlabel('Local Density');
ylabel('Walking Speed in Crowd');
title('inear Regression: Walking Speed vs. Local Density');
hold off;
%% ShoulderANgle 
% Initialize variables
active_status = table2array(Activity(:, 7));
mean_walking_speedActive = [];
crossed_local_densityActive = [];
ActiveIndex = find(string(active_status) == 'Actif');
InactiveIndex = find(string(active_status) == 'Inactif');
figure
All_ShoulderAngleActif=[];
All_Aperture_sizesActif=[];
All_ShoulderAngleInactif=[];
All_Aperture_sizesInactif=[];
% Loop through each person
for idx = 1:length(ActiveIndex)
    t = ActiveIndex(idx); 
    all_ShoulderAngle = [];
    all_Aperture_sizes = [];
    
    for j = 1:30
        try
            nonInfShoulderAgle = find(~isinf(local_densities_voronoi{j, t}));
            nonInfAperture_sizes = find(~isinf(RelativeApertureSize{j, t}));

            % Aggregate data
            all_ShoulderAngle = [all_ShoulderAngle; ShoulderAngle{j, t}];
            all_Aperture_sizes = [all_Aperture_sizes; RelativeApertureSize{j, t}];
        catch
            % Handle any errors
            disp error
        end
    end
    % Find indices where all_local_densities_voronoi is less than or equal to 4
    validIndices = all_ShoulderAngle <= 120;
    
    % Keep only the valid elements
    all_ShoulderAngle = all_ShoulderAngle(validIndices);
    all_Aperture_sizes=all_Aperture_sizes(validIndices);
    validIndices = all_Aperture_sizes <= 5;
    
    % Keep only the valid elements
    all_ShoulderAngle = all_ShoulderAngle(validIndices);
    all_Aperture_sizes=all_Aperture_sizes(validIndices);
    All_ShoulderAngleActif=[All_ShoulderAngleActif;all_ShoulderAngle];
    All_Aperture_sizesActif= [All_Aperture_sizesActif;all_Aperture_sizes];
    
    % Perform linear regression for the aggregated data
    try
        ShoulderAngleLinearReg = fitlm(all_Aperture_sizes, all_ShoulderAngle, 'linear', 'RobustOpts', 'on');
        InterceptShoulderAngle (t)=ShoulderAngleLinearReg.Coefficients{1,1};
        
        x1ShoulderAngle(t)=ShoulderAngleLinearReg.Coefficients{2,1};
        
        x_values = linspace(min(all_Aperture_sizes), max(all_Aperture_sizes), 100);
        y_values = InterceptShoulderAngle(t) + x1ShoulderAngle(t) * x_values;
        
        % Plot the original data points
        hold on;
        scatter(all_Aperture_sizes, all_ShoulderAngle,"blue","+",'LineWidth', .0000001);
        hold on;
        
        % Plot the regression line
         plot(x_values, y_values,'cyan', 'LineWidth', 2);


        CircleSpeed = fitlm(all_local_densities_circle, all_HMD_Velocity_scalaire, 'linear', 'RobustOpts', 'on');
    catch
        % Handle any errors
        continue;
    end
end
% Loop through each person
for idx = 1:length(InactiveIndex)
    t = InactiveIndex(idx); 
    all_ShoulderAngle = [];
    all_Aperture_sizes = [];
    
    for j = 1:30
        try
            nonInfShoulderAgle = find(~isinf(local_densities_voronoi{j, t}));
            nonInfAperture_sizes = find(~isinf(RelativeApertureSize{j, t}));

            % Aggregate data
            all_ShoulderAngle = [all_ShoulderAngle; ShoulderAngle{j, t}];
            all_Aperture_sizes = [all_Aperture_sizes; RelativeApertureSize{j, t}];
        catch
            % Handle any errors
            disp error
        end
    end
    % Find indices where all_local_densities_voronoi is less than or equal to 4
    validIndices = all_ShoulderAngle <= 120;
    
    % Keep only the valid elements
    all_ShoulderAngle = all_ShoulderAngle(validIndices);
    all_Aperture_sizes=all_Aperture_sizes(validIndices);
    validIndices = all_Aperture_sizes <= 5;
    
    % Keep only the valid elements
    all_ShoulderAngle = all_ShoulderAngle(validIndices);
    all_Aperture_sizes=all_Aperture_sizes(validIndices);

    All_ShoulderAngleInactif=[All_ShoulderAngleInactif;all_ShoulderAngle];
    All_Aperture_sizesInactif= [All_Aperture_sizesInactif;all_Aperture_sizes];
    % Perform linear regression for the aggregated data
    try
        ShoulderAngleLinearReg = fitlm(all_Aperture_sizes, all_ShoulderAngle, 'linear', 'RobustOpts', 'on');
        InterceptShoulderAngle (t)=ShoulderAngleLinearReg.Coefficients{1,1};
        
        x1ShoulderAngle(t)=ShoulderAngleLinearReg.Coefficients{2,1};
        
        x_values = linspace(min(all_Aperture_sizes), max(all_Aperture_sizes), 100);
        y_values = InterceptShoulderAngle(t) + x1ShoulderAngle(t) * x_values;
        
        % Plot the original data points
        hold on;
        scatter(all_Aperture_sizes, all_ShoulderAngle,"red","+",'LineWidth', .0000001);
        hold on;
        
        % Plot the regression line
        plot(x_values, y_values,'green', 'LineWidth', 2);
        % 


        catch
        % Handle any errors
        continue;
    end
end


%% Mean the results 
x1Active=mean(x1(ActiveIndex));
x1Inactive=mean(x1(InactiveIndex));
InterActive=mean(Intercept(ActiveIndex));
InterInactive=mean(Intercept(InactiveIndex));
y_valuesActive =  InterActive + x1Active * x_values;
hold on
plot(x_values, y_valuesActive, 'LineWidth', 2);
y_valuesInactive =  InterInactive + x1Inactive * x_values;
plot(x_values, y_valuesInactive, 'LineWidth', 2);

xlabel('Local Density');
ylabel('Walking Speed in Crowd');
title('Non-parametric Linear Regression: Walking Speed vs. Local Density');
hold off;
%%
for t=1:27
    for j=1:30
        VariableTimeToCrowd(j,t)=ZeroEmptyToNaN(TimeToCrowd{j, t});
        VariableTrajectoryLenght(j,t)=ZeroEmptyToNaN(TrajectoryLenght{j, t}); 
        
        if isempty(percentage_shorter_edge{j, t})
            Variablepercentage_shorter_edge(j,t)=nan;
        else
            Variablepercentage_shorter_edge(j,t)=percentage_shorter_edge{j, t};
        end
        if isempty(percentage_goal_direction{j, t})
            Variablepercentage_goal_direction(j,t)=nan;
        else
            Variablepercentage_goal_direction(j,t)=percentage_goal_direction{j, t};
        end
    end 
end
mean(VariableTrajectoryLenght,'omitnan')
%% PLOT,
% if h==1
% if j==1
%     figure(1);
%     hold on;
%     colors = jet(length(translated_polygons)); 
%     % Plot polygons
%     for i = 1:length(translated_polygons)
%         polygon = translated_polygons{i};
%         trisurf(K{i},polygon(:, 1), polygon(:, 2), polygon(:, 3),'FaceColor', colors(i, :), 'EdgeColor',colors(i, :), 'FaceAlpha', 0.25);
%     end
% end
% 
%     if t==1
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','r','LineWidth',2);
%     end
%     end
%     if t==2
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','b','LineWidth',2);
%     end
%     end
%     if t==3
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','green','LineWidth',2);
%     end
%     end
%     if t==4
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(1);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','black','LineWidth',2);
%     end
%     end
%     
%     xlabel('X');
%     ylabel('Y');
%     zlabel('Z');
%     legend;
%     hold off;
% end
% %% Plot Low
% if h==2
% if j==1
%     figure(2);
%     hold on;
%     colors = jet(length(translated_polygons)); 
%     % Plot polygons
%     for i = 1:length(translated_polygons)
%         polygon = translated_polygons{i};
%         trisurf(K{i},polygon(:, 1), polygon(:, 2), polygon(:, 3),'FaceColor', colors(i, :), 'EdgeColor',colors(i, :), 'FaceAlpha', 0.25);
%     end
% end
% 
%     if t==1
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','r','LineWidth',2);
%     end
%     end
%     if t==2
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','b','LineWidth',2);
%     end
%     end
%     if t==3
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','green','LineWidth',2);
%     end
%     end
%     if t==4
%     % Plot head trajectory
%     if ~isempty(HMD_position)
%         figure(2);
%         hold on
%         plot3(HMD_position(1:end-300, 1), HMD_position(1:end-300, 2), HMD_position(1:end-300, 3), 'DisplayName', 'Head Trajectory','Color','black','LineWidth',2);
%     end
%     end
%     
%     xlabel('X');
%     ylabel('Y');
%     zlabel('Z');
%     legend;
%     hold off;
% end

% %% Trajectory classifier 
% 
% 
% % Trajectory Sequence 
% % Convert the current sequence to a string
% currentSequenceStr = sequenceToString(TrajectorySequence{j, t});
% 
% % Check if the current sequence is already in the uniqueSequences list
% if isKey(sequenceToLetterMap, currentSequenceStr)
%     % If it is, use the existing letter
%     categorizedTrajectorySequence{j,t} = sequenceToLetterMap(currentSequenceStr);
% else
%     % If it is not, add it to the uniqueSequences list and assign a new letter
%     uniqueSequences{end + 1} = currentSequenceStr;
%     newLetter = letters(length(uniqueSequences));
%     sequenceToLetterMap(currentSequenceStr) = newLetter;
%     categorizedTrajectorySequence{j,t} = newLetter;
% end
% 
% % disp(['Trial ', num2str(t), ', Condition ', num2str(h), ', Repetition ', num2str(j), ': Categorized TrajectorySequence = ', categorizedTrajectorySequence]);
