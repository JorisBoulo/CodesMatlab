function [HMD_position, HMD_rotation, Agent_position, Gaze, cutoff_vicon,HMD_position_raw]=Copy_of_GetQuestVariables_Crowd(file_Quest)
%This function is used to extract the variables that are collected by the QuestPro in a .csv file 
%via the LogVS application 


%Open Quest csv file
data_quest=load(file_Quest);
sizeQuestData = find(string(data_quest.DataQuest(:,7))~='false'); % Initial loop size
for i = sizeQuestData
        disp 'Pouet'
        data_quest.DataQuest(i,:)=[];
        sizeQuestData=sizeQuestData-1;
end

%% Extract raw variable

Gaze=string(data_quest.DataQuest(2:end,18));
%%
% Resampling

frame_time=str2double(string(data_quest.DataQuest(2:end,14)));

a=0;
for i=1:size(frame_time,1)-1
    if isnan(frame_time(i,1))
        a=[a];
    else
    a=[a;a(i,1)+(frame_time(i+1,1)-frame_time(i,1))];
    end
end
TimeQuest=a;% Mettre en secondes 

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
HMDx=(str2double(string(data_quest.DataQuest(2:end,2))))/100; 
HMDy=(str2double(string(data_quest.DataQuest(2:end,1))))/100; 
HMDz=(str2double(string(data_quest.DataQuest(2:end,3))))/100; 
%v�rifier les angles de rotation, car Joris ne les a jamais pris. D'un autre c�t�, je ne suis pas suppos� en avoir besoin. 
HMDangx=(str2double(string(data_quest.DataQuest(2:end,4))));
HMDangy=-(str2double(string(data_quest.DataQuest(2:end,5))));
HMDangz=(str2double(string(data_quest.DataQuest(2:end,6))))+90;

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
Agent_position_x=(str2double(string(data_quest.DataQuest(2:end,2))))/100; 
Agent_position_y=(str2double(string(data_quest.DataQuest(2:end,1))))/100; 
Agent_position_z=(str2double(string(data_quest.DataQuest(2:end,3))))/100;

% Remove incoherent data
thresholdup = 10; 
thresholddown = -10;     % Define a threshold for incoherent data
incoherent_indices = HMDx > thresholdup | HMDy > thresholdup | HMDz > thresholdup |HMDx < thresholddown | HMDy < thresholddown | HMDz < thresholddown;

% Remove rows with incoherent data
HMDx(incoherent_indices) = [];
HMDy(incoherent_indices) = [];
HMDz(incoherent_indices) = [];
HMDangx(incoherent_indices) = [];
HMDangy(incoherent_indices) = [];
HMDangz(incoherent_indices) = [];
Agent_position_x(incoherent_indices) = [];
Agent_position_y(incoherent_indices) = [];
Agent_position_z(incoherent_indices) = [];
TimeQuest(incoherent_indices) = [];
Gaze(incoherent_indices) = [];
%% Filter les donn�es cin�matiques du casque
[Resamp_HMDx]=Filtrate(HMDx,2,9,90);
[Resamp_HMDy]=Filtrate(HMDy,2,9,90);
[Resamp_HMDz]=Filtrate(HMDz,2,9,90);

[Resamp_HMD_ang_x]=Filtrate(HMDangx,2,9,90);
[Resamp_HMD_ang_y]=Filtrate(HMDangy,2,9,90);
[Resamp_HMD_ang_z]=Filtrate(HMDangz,2,9,90);

[ResampAgent_x]=Filtrate(Agent_position_x,2,9,90);
[ResampAgent_y]=Filtrate(Agent_position_y,2,9,90);
[ResampAgent_z]=Filtrate(Agent_position_z,2,9,90);

%%Resample
[Resamp_HMDx,TimeRes]=resample(Resamp_HMDx,TimeQuest(:,1),90);
[Resamp_HMDy,TimeRes]=resample(Resamp_HMDy,TimeQuest(:,1),90);
[Resamp_HMDz,TimeRes]=resample(Resamp_HMDz,TimeQuest(:,1),90);

[Resamp_HMD_ang_x,TimeRes]=resample(Resamp_HMD_ang_x,TimeQuest(:,1),90);
[Resamp_HMD_ang_y,TimeRes]=resample(Resamp_HMD_ang_y,TimeQuest(:,1),90);
[Resamp_HMD_ang_z,TimeRes]=resample(Resamp_HMD_ang_z,TimeQuest(:,1),90);

[ResampAgent_x,TimeRes]=resample(ResampAgent_x,TimeQuest(:,1),90);
[ResampAgent_y,TimeRes]=resample(ResampAgent_y,TimeQuest(:,1),90);
[ResampAgent_z,TimeRes]=resample(ResampAgent_z,TimeQuest(:,1),90);


%% Reorientation spatiale 
 
%int�grer ici les d�placements spatiaux des donn�es avec de rotate...
[PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = trigrotate(Resamp_HMDx,Resamp_HMDy,Resamp_HMDz,ResampAgent_x,ResampAgent_y,ResampAgent_z,-45);
%%
[PPx,PPy,PPz,VAPx,VAPy,VAPz] = trigrotate(HMDx,HMDx,HMDx,Agent_position_x,Agent_position_y,Agent_position_z,-45);
%% Alignement temporel avec les donn�es Vicon
% if ~isnan(Vicon_Head_position(300,2))
% [istart,istop]=findsignal(PPymod,Vicon_Head_position(300:500,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
% istart=istart-400;
% istop=istart+size(Vicon_Head_position,1);
% else 
% [istart,istop]=findsignal(PPymod,Vicon_Head_position(600:800,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
% istart=istart-600;
% istop=istart+size(Vicon_Head_position,1);
% end
%% Output variables
HMD_position=[PPxmod PPymod PPzmod];
% try
% HMD_position_cut=HMD_position(istart:istop,:);
% catch
%     HMD_position_cut='tant pis'; 
% end
HMD_position_raw=[PPx PPy PPz];

HMD_rotation=[Resamp_HMD_ang_x, Resamp_HMD_ang_y, Resamp_HMD_ang_z];

Agent_position=[VAPxmod,VAPymod,VAPzmod];

cutoff_vicon=3;


end

