function [mean_walking_speed,local_density,ShoulderAngle,AgentsCrossed,MinDistanceL,MinDistanceR, CollisionL,CollisionR,TrajectorySequence,Aperture_sizes, closestShoulder,translated_polygons,K,Agent_rotatedposition,AgentName] = Copy_of_FindClearance_Crowd(Rep,HMD_position,HMD_Velocity_scalaire)
    % Initialize
    collision = 0;
    closestShoulder=[];
    Agent_rotatedposition=[];
    humanShoulders=[HMD_position(:,1)-0.5,HMD_position(:,2:3),HMD_position(:,1)+0.5,HMD_position(:,2:3)];
    
    % Step 1: Read the data from the polygons file
    try
        fileID = fopen('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\TryExtr\polygons.txt', 'r');
        lines = textscan(fileID, '%s', 'Delimiter', '\n');
        lines = lines{1};
        fclose(fileID);
    catch
        disp("The file 'polygons.txt' was not found.");
        return;
    end

    %% Step 2: Parse the polygons data
    polygons = {};
    AgentName = {}; 
    a=1;
    for i = 1:length(lines)
        line = lines{i};
        if contains(line, 'SM_')
            AgentName{a}=line;
            a=a+1;
        end
        if contains(line, 'VertexData')
            startIdx = strfind(line, '((X=') + 1;
            endIdx = strfind(line, ')),IndexData');
            if isempty(startIdx) || isempty(endIdx)
                disp(['Error parsing line: ', line]);
                continue;
            end
            vertex_data = strsplit(line(startIdx:endIdx-1), '),(');
            polygon = [];
            for j = 1:length(vertex_data)
                vertex = vertex_data{j};
                try
                    % Remove any extraneous characters and split by comma
                    coords = str2double(strsplit(strrep(strrep(strrep(strrep(strrep(vertex, 'X=',''), 'Y=', ''), 'Z=', ''), '(', ''), ')', ''), ','));
                    if length(coords) == 3 && all(~isnan(coords))
                        polygon = [polygon; coords / 100]; % Convert to meters
                    else
                        disp(['Error converting coordinates to float: ', vertex]);
                    end
                catch
                    disp(['Error converting coordinates to float: ', vertex]);
                    continue;
                end
            end
            polygons{end+1} = polygon;
        end
    end

    %% Step 3: Read and parse the crowd disposition file to get agent positions
    try
    if strcmp(Rep, '01') || strcmp(Rep, '09') || strcmp(Rep, '12') || strcmp(Rep, '13') || strcmp(Rep, '26') || strcmp(Rep, '28') || strcmp(Rep, '34') || strcmp(Rep, '35')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\LowCrowdAnalyzed.xml';
    elseif strcmp(Rep, '02') || strcmp(Rep, '05') || strcmp(Rep, '07') || strcmp(Rep, '08') || strcmp(Rep, '11') || strcmp(Rep, '20') || strcmp(Rep, '25') || strcmp(Rep, '39')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\HighCrowdAnalyzed.xml';
    elseif strcmp(Rep, '03') || strcmp(Rep, '10') || strcmp(Rep, '36')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\HighCrowdSham1.xml';
    elseif strcmp(Rep, '15') || strcmp(Rep, '31') || strcmp(Rep, '33')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\HighCrowdSham2.xml';
    elseif strcmp(Rep, '16') || strcmp(Rep, '27') || strcmp(Rep, '37')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\HighCrowdSham3.xml';
    elseif strcmp(Rep, '19') || strcmp(Rep, '24') || strcmp(Rep, '29')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\LowCrowdSham1.xml';
    elseif strcmp(Rep, '18') || strcmp(Rep, '23') || strcmp(Rep, '38')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\LowCrowdSham2.xml';
    elseif strcmp(Rep, '17') || strcmp(Rep, '21') || strcmp(Rep, '32')
        Condition = 'C:\Users\joris\OneDrive - Université Laval\Documents\UMANS\agents\LowCrowdSham3.xml';
    end
    
    tree = xmlread(Condition);
catch
    disp("The XML file was not found or could not be parsed.");
    return;
end

    % Extract positions of agents starting from the second one
    agent_positions = [];
    agent_goals = [];
    agents = tree.getElementsByTagName('Agent');
    for i = 1:agents.getLength
        if i == 1
            continue;
        end
        agent = agents.item(i-1);
        pos = agent.getElementsByTagName('pos').item(0);
        goal = agent.getElementsByTagName('goal').item(0);
        x_pos = str2double(pos.getAttribute('x'));
        y_pos = str2double(pos.getAttribute('y'));
        x_goal = str2double(goal.getAttribute('x'));
        y_goal = str2double(goal.getAttribute('y'));
        agent_positions = [agent_positions; x_pos, y_pos];
        agent_goals = [agent_goals; x_goal, y_goal];
    end

    % Calculate the direction vector from position to goal
    direction_vectors = agent_goals - agent_positions;

  %% Step 4: Translate and rotate the polygons based on agent positions
    translated_polygons = {};
 for i = 1:min(length(polygons), size(agent_positions, 1))
        % Translate agent positions
        x_offset = agent_positions(i, 2) - 1.71;
        y_offset = agent_positions(i, 1) + 4.57;

        % Calculate the orientation angle based on the direction vector
        angle = -45;
        sinagl = sind(angle);
        cosagl = cosd(angle);

        % Rotate agent positions
        x_rotated = x_offset * sinagl - y_offset * cosagl;
        y_rotated = x_offset * cosagl + y_offset * sinagl;
        
        Agent_rotatedposition{end+1}={x_rotated,y_rotated};
        % Translate polygons based on rotated positions
        polygon = polygons{i};
        translated_polygon = [polygon(:, 1) + x_rotated, polygon(:, 2) + y_rotated, polygon(:, 3)];
    % Step 4.5: Rotate the translated polygons around the Z-axis without changing their origins
    % Calculate the centroid of the polygon
    centroid = mean(translated_polygon(:, 1:2), 1);

    % Translate the polygon to the origin
    translated_polygon(:, 1:2) = translated_polygon(:, 1:2) - centroid;

    % Calculate the rotation angle based on the direction vector
    direction_vector = direction_vectors(i, :);
    angle2 = atan2d(direction_vector(2), direction_vector(1));
    rotation_matrix = [cosd(angle2), -sind(angle2), 0; sind(angle2), cosd(angle2), 0; 0, 0, 1];

    % Rotate the polygon
    translated_polygon = (rotation_matrix * translated_polygon')';

    % Translate the polygon back to its original position
    translated_polygon(:, 1:2) = translated_polygon(:, 1:2) + centroid;

    translated_polygons{end+1} = translated_polygon;
end



%% Step 5: Calculate clearance and check for collisions
for i = 1:length(translated_polygons)
    polygon = translated_polygons{i};
    K{i} = convhull(polygon(:, 1), polygon(:, 2), polygon(:, 3));
end
% Create a Quest Shoulder 
offsetL = humanShoulders(:, 1:3)-HMD_position;
offsetR = humanShoulders(:, 4:6)-HMD_position;

% Apply the offset to the humanShoulders to get the fictive shoulder positions
humanShoulders = [humanShoulders(:, 1:3) + offsetL, humanShoulders(:, 4:6) - offsetR];

% Iterate through each row of humanShoulders
for row = 1:size(humanShoulders, 1)
    % Extract left and right shoulder positions
    left_shoulder = humanShoulders(row, 1:3);
    right_shoulder = humanShoulders(row, 4:6);

    % Initialize collision flags for this row
    collision_flags = false(1, length(translated_polygons));

    % Check clearance and collisions for left shoulder
    for i = 1:length(translated_polygons)
        if collision_flags(i)
            continue; % Skip if collision already counted for this polygon
        end

        polygon = translated_polygons{i};
        min_distance = inf;

        % Calculate distances from left shoulder to each vertex of the polygon
        for j = 1:size(polygon, 1)
            vertex = polygon(j, 1:3);
            distance = sqrt(sum((left_shoulder - vertex) .^ 2));
            if distance < min_distance
                min_distance = distance;
            end
        end

            clearanceL(row, i) = min_distance;
        
    end

    % Check clearance and collisions for right shoulder
    for i = 1:length(translated_polygons)
        if collision_flags(i)
            continue; % Skip if collision already counted for this polygon
        end

        polygon = translated_polygons{i};
        min_distance = inf;

        % Calculate distances from right shoulder to each vertex of the polygon
        for j = 1:size(polygon, 1)
            vertex = polygon(j, 1:3);
            distance = sqrt(sum((right_shoulder - vertex) .^ 2));
            if distance < min_distance
                min_distance = distance;
            end
        end
            clearanceR(row, i) = min_distance;
        
    end
end

MinDistanceL=min(clearanceL);
MinDistanceR=min(clearanceR);
CollisionL=sum(MinDistanceL(:) < 0);
CollisionR=sum(MinDistanceR(:) < 0);

%% Step 6  Delaunay Triangle 
% Assuming humanShoulders and Agent_rotatedposition are already defined

% Extract 2D agent positions from the structure
agent_positions_2d = zeros(length(Agent_rotatedposition), 2);
for i = 1:length(Agent_rotatedposition)
    agent_positions_2d(i, :) = cell2mat(Agent_rotatedposition{1,i});
end

% Calculate Delaunay triangulation
tri = delaunay(agent_positions_2d(:, 1), agent_positions_2d(:, 2));

% % Plot the 2D agent positions and Delaunay triangulation
% figure;
% hold on;
% triplot(tri, agent_positions_2d(:, 1), agent_positions_2d(:, 2), 'k');
% plot(agent_positions_2d(:, 1), agent_positions_2d(:, 2), 'ko', 'MarkerFaceColor', 'k');
% 
% % Plot the trajectory of the HMD
% plot(HMD_position(1:end-200, 1), HMD_position(1:end-200, 2), 'b', 'LineWidth', 2);
% plot(humanShoulders(1:end-200,1),humanShoulders(1:end-200,2),'r',"LineWidth",2);
% plot(humanShoulders(1:end-200,4),humanShoulders(1:end-200,5),'r',"LineWidth",2)
% 
% % Add labels and legend
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% legend('Delaunay Triangulation', 'Agent Positions', 'HMD Trajectory');
% title('HMD Trajectories Through a Field of Agent Positions');

% Place the number of each aperture on the plot
for i = 1:size(tri, 1)
    % Get the vertices of the triangle
    vertices = agent_positions_2d(tri(i, :), :);
    
    % Calculate the centroid of the triangle
    centroid = mean(vertices, 1);
    
    % Place the number of the aperture at the centroid
%     text(centroid(1), centroid(2), num2str(i), 'Color', 'r', 'FontSize', 12, 'FontWeight', 'bold');
end

% Determine which Delaunay triangles (apertures) have been crossed in order
TrajectorySequence = [];
local_density=[];
mean_walking_speed=[];
for t = 1:find(HMD_position(:,2)>5.9,1,'first')
    % Get the current HMD position
    current_position = HMD_position(t, 1:2);
    
    % Check which triangle the current position is in
    for i = 1:size(tri, 1)
        % Get the vertices of the triangle
        vertices = agent_positions_2d(tri(i, :), :);
        
        % Check if the current position is inside the triangle
        in_triangle = inpolygon(current_position(1), current_position(2), vertices(:, 1), vertices(:, 2));
        if in_triangle
            % Add the triangle index to the sequence if it's not already the last one added
                        if isempty(TrajectorySequence) || TrajectorySequence(end) ~= i
                TrajectorySequence = [TrajectorySequence; i];
                local_density = [local_density;polyarea(vertices(:, 1), vertices(:, 2))/3];
                % Find the indices of HMD positions inside the triangle
                in_triangle_positions = inpolygon(HMD_position(1:find(HMD_position(:,2)>5.9,1,'first'), 1), HMD_position(1:find(HMD_position(:,2)>5.9,1,'first'), 2), vertices(:, 1), vertices(:, 2));
                % Calculate the mean walking speed in the triangle
                mean_walking_speed =[mean_walking_speed; mean(HMD_Velocity_scalaire(in_triangle_positions))];
            end
            
        break;
        end
    end
end

%% Step 7 Determine the crossed edges and their lengths
crossed_edges = [];
Aperture_sizes = [];
ShoulderAngle = [];
min_distance_poly_indices = [];
AgentsCrossed=[];
for k = 1:length(TrajectorySequence)-1
    tri1 = TrajectorySequence(k);
    tri2 = TrajectorySequence(k+1);
    
    % Get the vertices of the two triangles
    vertices1 = agent_positions_2d(tri(tri1, :), :);
    vertices2 = agent_positions_2d(tri(tri2, :), :);
    
    % Compare the edges to find the common edge
    for j = 1:3
        edge1_start = vertices1(j, :);
        edge1_end = vertices1(mod(j, 3) + 1, :);
        
        for l = 1:3
            edge2_start = vertices2(l, :);
            edge2_end = vertices2(mod(l, 3) + 1, :);
            
            % Check if the edges are the same (considering both directions)
            if (isequal(edge1_start, edge2_start) && isequal(edge1_end, edge2_end)) || ...
               (isequal(edge1_start, edge2_end) && isequal(edge1_end, edge2_start))
                % Calculate the length of the common edge
                edge_length = norm(edge1_start - edge1_end);

                % Store the crossed edge and its length
                crossed_edges = [crossed_edges;  tri1, tri2, j, l,edge_length];
                
                % Annotate the plot with the size of the crossed edge
                mid_point = (edge1_start + edge1_end) / 2;
%                 text(mid_point(1), mid_point(2), sprintf('%.2f', edge_length), 'Color', 'm', 'FontSize', 10, 'FontWeight', 'bold');
                
                % Find the exact crossing point
                crossing_point = (edge1_start + edge1_end) / 2;
                CenterShoulder=(humanShoulders(:,1:2)+humanShoulders(:,4:5))/2;
                
                % Interpolate shoulder positions at the crossing point
                % Assuming HMD_position and humanShoulders are synchronized and have the same length
                % Find the closest points in HMD_position to the crossing point
                distances = sqrt(sum((CenterShoulder - crossing_point).^2, 2));
                [~, closest_idx] = min(distances);


                % Find the corresponding polygons in translated_polygons
                polygon1 = [];
                polygon2 = [];
                for idx = 1:length(translated_polygons)
                    % Get the vertices of the polygon
                    poly_vertices = translated_polygons{idx}(:, 1:2);
                    
                    % Create the convex hull of the polygon
                    k = convhull(poly_vertices(:, 1), poly_vertices(:, 2));
                    
                    % Check if edge1_start is inside the convex hull
                    if inpolygon(edge1_start(1), edge1_start(2), poly_vertices(k, 1), poly_vertices(k, 2))
                        polygon1 = translated_polygons{idx};
                        min_distance_poly_indices1 = idx;
                    end
                    
                    % Check if edge1_end is inside the convex hull
                    if inpolygon(edge1_end(1), edge1_end(2), poly_vertices(k, 1), poly_vertices(k, 2))
                        polygon2 = translated_polygons{idx};
                        min_distance_poly_indices2 = idx;
                    end
                end
                
                   % Initialize variables to store the minimum distance and clearances
                    min_distance = inf;
                    min_point1 = [];
                    min_point2 = [];
        
                    for m = 1:size(polygon1, 1)
                        for n = 1:size(polygon2, 1)
                            distance = norm(polygon1(m, 1:3) - polygon2(n, 1:3));
                            if distance < min_distance
                                min_distance = distance;
                                min_point1 = polygon1(m, 1:3);
                                min_point2 = polygon2(n, 1:3);
                            end
                        end
                    end
        
                    % Store the aperture size
                    Aperture_sizes = [Aperture_sizes; min_distance];
        
                    % Store the indices of the polygons contributing to the minimum distance
                    AgentsCrossed = [AgentsCrossed; min_distance_poly_indices1; min_distance_poly_indices2];
                    
                    Min_point = (min_point1(1, 1:2) + min_point2(1, 1:2)) / 2;
        
                    % Annotate the plot with the size of the aperture
%                      text(Min_point(1), Min_point(2) - 0.1, sprintf('%.2f', min_distance), 'Color', 'g', 'FontSize', 10, 'FontWeight', 'bold');
        
                    % Interpolate the shoulder positions
                    left_shoulder = humanShoulders(closest_idx, 1:3);
                    right_shoulder = humanShoulders(closest_idx, 4:6);
        
                    % Calculate the shoulder vector at the crossing point
                    shoulder_vector = left_shoulder(1, 1:2) - right_shoulder(1, 1:2);
        
                    % Calculate the vectors representing the edges using the minimum distance points
                    edge_vector = min_point2(1, 1:2) - min_point1(1, 1:2);
        
                    % Normalize the vectors
                    shoulder_vector = shoulder_vector / norm(shoulder_vector);
                    edge_vector = edge_vector / norm(edge_vector);
        
                    % Calculate the angles between the shoulder vectors and the edge vectors
                    angle = acosd(dot(shoulder_vector, edge_vector));
        
                    ShoulderAngle = [ShoulderAngle; angle];
        
                    % Visualize the shoulder vector
%                     quiver(Min_point(1), Min_point(2), shoulder_vector(1), shoulder_vector(2), 0.5, 'r', 'LineWidth', 2, 'MaxHeadSize', 2);
        
                    % Annotate the angle on the plot, slightly offset to avoid overlap
%                     text(Min_point(1), Min_point(2) - 0.2, sprintf('%.2f°', angle), 'Color', 'b', 'FontSize', 10, 'FontWeight', 'bold');
  
                
                break;
            end
        end
    end
end
end