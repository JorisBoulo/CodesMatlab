function [humanShoulders,percentage_goal_direction,percentage_shorter_edge,local_densities_voronoi,local_densities_circle,mean_walking_speed,local_density,ShoulderAngle,AgentsCrossed, CollisionL,CollisionR,TrajectorySequence,Aperture_sizes, closestShoulder,translated_polygons,K,Agent_rotatedposition,AgentName,ShoulderWidth,RelativeApertureSize] = FindClearance_Crowd(HMD_position_cut,humanShoulders, AgentsPositions,HMD_position,Head_position,HMD_Velocity_scalaire,CrowdConditionName,w)
    % Initialize
    collision = 0;
    closestShoulder=[];
    Agent_rotatedposition=[];
    
    % Step 1: Read the data from the polygons file
    try
        fileID = fopen("C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\Participant\polygons.txt", 'r');
        lines = textscan(fileID, '%s', 'Delimiter', '\n');
        lines = lines{1};
        fclose(fileID);
    catch
        disp("The file 'polygons.txt' was not found.");
        return;
    end

    %% Step 2: Parse the polygons data
    polygons = {};
    AgentName = {}; 
    a=1;
    for i = 1:length(lines)
        line = lines{i};
        if contains(line, 'SM_')
            AgentName{a}=line;
            a=a+1;
        end
        if contains(line, 'VertexData')
            startIdx = strfind(line, '((X=') + 1;
            endIdx = strfind(line, ')),IndexData');
            if isempty(startIdx) || isempty(endIdx)
                disp(['Error parsing line: ', line]);
                continue;
            end
            vertex_data = strsplit(line(startIdx:endIdx-1), '),(');
            polygon = [];
            for j = 1:length(vertex_data)
                vertex = vertex_data{j};
                try
                    % Remove any extraneous characters and split by comma
                    coords = str2double(strsplit(strrep(strrep(strrep(strrep(strrep(vertex, 'X=',''), 'Y=', ''), 'Z=', ''), '(', ''), ')', ''), ','));
                    if length(coords) == 3 && all(~isnan(coords))
                        polygon = [polygon; coords / 100]; % Convert to meters
                    else
                        disp(['Error converting coordinates to float: ', vertex]);
                    end
                catch
                    disp(['Error converting coordinates to float: ', vertex]);
                    continue;
                end
            end
            polygons{end+1} = polygon;
        end
    end

    %% Step 3: Read and parse the crowd disposition file to get agent positions
    
    % Extract positions of agents starting from the second one
    agent_positions = [];
    agent_goals = [];
    agents = AgentsPositions.getElementsByTagName('Agent');
    for i = 1:agents.getLength
        if i == 1
            continue;
        end
        agent = agents.item(i-1);
        pos = agent.getElementsByTagName('pos').item(0);
        goal = agent.getElementsByTagName('goal').item(0);
        x_pos = str2double(pos.getAttribute('x'));
        y_pos = str2double(pos.getAttribute('y'));
        x_goal = str2double(goal.getAttribute('x'));
        y_goal = str2double(goal.getAttribute('y'));
        agent_positions = [agent_positions; x_pos, y_pos];
        agent_goals = [agent_goals; x_goal, y_goal];
    end

    % Calculate the direction vector from position to goal
    direction_vectors = agent_goals - agent_positions;

  %% Step 4: Translate and rotate the polygons based on agent positions
    translated_polygons = {};
 for i = 1:min(length(polygons), size(agent_positions, 1))
        % Translate agent positions
        x_offset = agent_positions(i, 2) - 1.71;
        y_offset = agent_positions(i, 1) + 4.57;

        % Calculate the orientation angle based on the direction vector
        angle = -45;
        sinagl = sind(angle);
        cosagl = cosd(angle);

        % Rotate agent positions
        x_rotated = x_offset * sinagl - y_offset * cosagl;
        y_rotated = x_offset * cosagl + y_offset * sinagl;
        
        Agent_rotatedposition{end+1}={x_rotated,y_rotated};
        % Translate polygons based on rotated positions
        polygon = polygons{i};
        translated_polygon = [polygon(:, 1) + x_rotated, polygon(:, 2) + y_rotated, polygon(:, 3)];
    % Step 4.5: Rotate the translated polygons around the Z-axis without changing their origins
    % Calculate the centroid of the polygon
    centroid = mean(translated_polygon(:, 1:2), 1);

    % Translate the polygon to the origin
    translated_polygon(:, 1:2) = translated_polygon(:, 1:2) - centroid;

    % Calculate the rotation angle based on the direction vector
    direction_vector = direction_vectors(i, :);
    angle2 = atan2d(direction_vector(2), direction_vector(1));
    rotation_matrix = [cosd(angle2), -sind(angle2), 0; sind(angle2), cosd(angle2), 0; 0, 0, 1];

    % Rotate the polygon
    translated_polygon = (rotation_matrix * translated_polygon')';

    % Translate the polygon back to its original position
    translated_polygon(:, 1:2) = translated_polygon(:, 1:2) + centroid;

    translated_polygons{end+1} = translated_polygon;
end



%% Step 5: Calculate clearance and check for collisions
for i = 1:length(translated_polygons)
    polygon = translated_polygons{i};
    K{i} = convhull(polygon(:, 1), polygon(:, 2), polygon(:, 3));
end


try
        % Create a Quest Shoulder 
    offsetL = humanShoulders(:, 1:3)-Head_position;
    offsetR = humanShoulders(:, 4:6)-Head_position;
    humanShoulders = [HMD_position_cut(1:length(humanShoulders),:) + offsetL, HMD_position_cut(1:length(humanShoulders),:) + offsetR];
catch
    humanShoulders = HMD_position;
end
% Initialize collision counters
CollisionL = 0;
CollisionR = 0;

% Initialize logical arrays to track collisions
leftCollided = false(1, length(translated_polygons));
rightCollided = false(1, length(translated_polygons));

for row = 1:size(humanShoulders, 1)
    left_shoulder = humanShoulders(row, 1:3);
    try
        right_shoulder = humanShoulders(row, 4:6);
    catch
        right_shoulder = humanShoulders(row, 1:3);
    end 

    for i = 1:length(translated_polygons)
        polygon = translated_polygons{i};
        vertices = polygon(:, 1:3);
        faces = K{i}; % Convex hull faces

        % Check if left shoulder is inside the polyhedron and hasn't collided before
        if ~leftCollided(i) && inpolyhedron(faces, vertices, left_shoulder)
            CollisionL = CollisionL + 1;
            leftCollided(i) = true; % Mark this polyhedron as collided
        end

        % Check if right shoulder is inside the polyhedron and hasn't collided before
        if ~rightCollided(i) && inpolyhedron(faces, vertices, right_shoulder)
            CollisionR = CollisionR + 1;
            rightCollided(i) = true; % Mark this polyhedron as collided
        end
    end
end

%% Step 6  Delaunay Triangle 
% Assuming humanShoulders and Agent_rotatedposition are already defined

% Extract 2D agent positions from the structure
agent_positions_2d = zeros(length(Agent_rotatedposition), 2);
for i = 1:length(Agent_rotatedposition)
    agent_positions_2d(i, :) = cell2mat(Agent_rotatedposition{1,i});
end

% Calculate Delaunay triangulation
tri = delaunay(agent_positions_2d(:, 1), agent_positions_2d(:, 2));

% % Plot the 2D agent positions and Delaunay triangulation
figure;
hold on;
triplot(tri, agent_positions_2d(:, 1), agent_positions_2d(:, 2), 'k');
plot(agent_positions_2d(:, 1), agent_positions_2d(:, 2), 'ko', 'MarkerFaceColor', 'k');
colors = jet(length(translated_polygons)); 
    % Plot polygons
    for i = 1:length(translated_polygons)
        polygon = translated_polygons{i};
        trisurf(K{i},polygon(:, 1), polygon(:, 2), polygon(:, 3),'FaceColor', colors(i, :), 'EdgeColor',colors(i, :), 'FaceAlpha', 0.25);
    end

% Plot the trajectory of the HMD
plot(HMD_position(1:end, 1), HMD_position(1:end, 2), 'b', 'LineWidth', 2);
plot(humanShoulders(1:end,1),humanShoulders(1:end,2),'r',"LineWidth",2);
plot(humanShoulders(1:end,4),humanShoulders(1:end,5),'r',"LineWidth",2);
plot(Head_position(1:end, 1), Head_position(1:end, 2), 'LineWidth', 2);

% Add labels and legend
xlabel('X');
ylabel('Y');
zlabel('Z');
legend('Delaunay Triangulation', 'Agent Positions', 'HMD Trajectory');
title(w);
% Initialize variables to store the results

% Place the number of each aperture on the plot
for i = 1:size(tri, 1)
    % Get the vertices of the triangle
    vertices = agent_positions_2d(tri(i, :), :);
    
    % Calculate the centroid of the triangle
    centroid = mean(vertices, 1);
    
    % Place the number of the aperture at the centroid
%      text(centroid(1), centroid(2), num2str(i), 'Color', 'r', 'FontSize', 12, 'FontWeight', 'bold');
end

% Determine which Delaunay triangles (apertures) have been crossed in order
TrajectorySequence = [];
local_density=[];
mean_walking_speed=[];
if w >= 1 && w <= 30
     if contains(CrowdConditionName,'(1)')||contains(CrowdConditionName,"2")
         Stop=find(HMD_position(:,2)>3,1,'first');
     elseif contains(CrowdConditionName,'3')||contains(CrowdConditionName,"4")
         Stop=find(HMD_position(:,2)>5,1,'first');
     elseif contains(CrowdConditionName,'5')||contains(CrowdConditionName,"6")
         Stop=find(HMD_position(:,1)>4,1,'first');
     end
else
    Stop=find(HMD_position(:,2)>5,1,'first');
end
% Initialize variables to store the results
shorter_edge_choices = 0;
total_choices = 0;
half_turns = 0;
goal_direction_choices = 0;

% Iterate through each position in HMD_position until STOP
for t = 1:Stop
    % Get the current HMD position
    current_position = HMD_position(t, 1:2);
    
    % Check which triangle the current position is in
    for i = 1:size(tri, 1)
        % Get the vertices of the triangle
        vertices = agent_positions_2d(tri(i, :), :);
        
        % Check if the current position is inside the triangle
        in_triangle = inpolygon(current_position(1), current_position(2), vertices(:, 1), vertices(:, 2));
        if in_triangle
            % Add the triangle index to the sequence if it's not already the last one added
            if isempty(TrajectorySequence) || TrajectorySequence(end) ~= i
                TrajectorySequence = [TrajectorySequence; i];
                local_density = [local_density; polyarea(vertices(:, 1), vertices(:, 2)) / 3];
                
                % Find the indices of HMD positions inside the triangle
                if w >= 1 && w <= 30
                    if contains(CrowdConditionName, '(1)') || contains(CrowdConditionName, "2")
                        in_triangle_positions = inpolygon(HMD_position(1:find(HMD_position(:, 2) > 3, 1, 'first'), 1), HMD_position(1:find(HMD_position(:, 2) > 3, 1, 'first'), 2), vertices(:, 1), vertices(:, 2));
                        ParticipantGoal = [-3, 3];
                    elseif contains(CrowdConditionName, '3') || contains(CrowdConditionName, "4")
                        in_triangle_positions = inpolygon(HMD_position(1:find(HMD_position(:, 2) > 5, 1, 'first'), 1), HMD_position(1:find(HMD_position(:, 2) > 5, 1, 'first'), 2), vertices(:, 1), vertices(:, 2));
                        ParticipantGoal = [-1, 6];
                    elseif contains(CrowdConditionName, '5') || contains(CrowdConditionName, "6")
                        in_triangle_positions = inpolygon(HMD_position(1:find(HMD_position(:, 1) > 4, 1, 'first'), 1), HMD_position(1:find(HMD_position(:, 1) > 4, 1, 'first'), 2), vertices(:, 1), vertices(:, 2));
                        ParticipantGoal = [4.4, 0.4];
                    end
                else
                    in_triangle_positions = inpolygon(HMD_position(1:find(HMD_position(:, 2) > 5, 1, 'first'), 1), HMD_position(1:find(HMD_position(:, 2) > 5, 1, 'first'), 2), vertices(:, 1), vertices(:, 2));
                    ParticipantGoal = [-1, 6];
                end
                
                % Calculate the mean walking speed in the triangle
                mean_walking_speed = [mean_walking_speed; mean(HMD_Velocity_scalaire(in_triangle_positions))];
            end
            
            break;
        end
    end
end



% Calculate the percentage of time the participant chose the shorter edge
for i = 2:length(TrajectorySequence)-1
    if length(TrajectorySequence) > 1
        tri1 = TrajectorySequence(i-1);
        tri2 = TrajectorySequence(i);
        
        % Get the vertices of the two triangles
        vertices1 = agent_positions_2d(tri(tri1, :), :);
        vertices2 = agent_positions_2d(tri(tri2, :), :);
        
        % Compare the edges to find the common edge
        common_edge = [];
        for j = 1:3
            edge1_start = vertices1(j, :);
            edge1_end = vertices1(mod(j, 3) + 1, :);
            
            for l = 1:3
                edge2_start = vertices2(l, :);
                edge2_end = vertices2(mod(l, 3) + 1, :);
                
                % Check if the edges are the same (considering both directions)
                if (isequal(edge1_start, edge2_start) && isequal(edge1_end, edge2_end)) || ...
                   (isequal(edge1_start, edge2_end) && isequal(edge1_end, edge2_start))
                    common_edge = [edge1_start; edge1_end];
                    break;
                end
            end
            if ~isempty(common_edge)
                break;
            end
        end
        
        % If a common edge is found, proceed with the calculations
        if ~isempty(common_edge)
            % Calculate the lengths of the other two edges in the first triangle
            other_edges1 = setdiff(vertices1, common_edge, 'rows');
            if size(other_edges1, 1) == 1
                edge1_length1 = norm(other_edges1(1, :) - common_edge(1, :));
                edge2_length1 = norm(other_edges1(1, :) - common_edge(2, :));
                edge_took = norm(common_edge(1, :) - common_edge(2, :));
            else
                edge1_length1 = inf;
                edge2_length1 = inf;
            end
            
            % Calculate the lengths of the other two edges in the second triangle
            other_edges2 = setdiff(vertices2, common_edge, 'rows');
            if size(other_edges2, 1) == 1
                edge1_length2 = norm(other_edges2(1, :) - common_edge(1, :));
                edge2_length2 = norm(other_edges2(1, :) - common_edge(2, :));
            else
                edge1_length2 = inf;
                edge2_length2 = inf;
            end
            
            % Determine the shorter edge in the first triangle
            shorter_edge1 = min([edge1_length1, edge2_length1, edge_took]);
            
            % Determine the shorter edge in the second triangle
            shorter_edge2 = min([edge1_length2, edge2_length2, edge_took]);
            
            % Check if the participant chose the shorter edge
            if shorter_edge1 == shorter_edge2
                shorter_edge_choices = shorter_edge_choices + 1;
            end
            
            % Check if the participant made a half-turn and took the edge they came from
            if isequal(common_edge, [vertices1(1, :); vertices1(2, :)]) || isequal(common_edge, [vertices1(2, :); vertices1(1, :)])
                half_turns = half_turns + 1;
            end
            
                        
            % Calculate the vectors
            EdgeTook = common_edge(1, :) - common_edge(2, :);
            EdgeTook = EdgeTook / norm(EdgeTook); % Normalize
            
            OtherEdge1 =  other_edges1(1, :) - common_edge(1, :);
            OtherEdge1 =  OtherEdge1 / norm(OtherEdge1); 

            OtherEdge2 =  other_edges1(1, :) - common_edge(1, :);
            OtherEdge2 =  OtherEdge1 / norm(OtherEdge1); 

            Goal_vector = mean(common_edge, 1)-ParticipantGoal;
            Goal_vector = Goal_vector / norm(Goal_vector);

            % Calculate the perpendicular vectors of the remaining edges
            perp_edgetook1 = [-EdgeTook(2), EdgeTook(1)];
            perp_edgetook2 = [EdgeTook(2), -EdgeTook(1)];

            perp_other1edge1 = [-OtherEdge1(2), OtherEdge1(1)];
            perp_other1edge2 = [OtherEdge1(2), -OtherEdge1(1)];

            perp_other2edge1 = [-OtherEdge2(2), OtherEdge2(1)];
            perp_other2edge2 = [OtherEdge2(2), -OtherEdge2(1)];
            
            % Calculate the angles between the goal vector and the perpendicular vectors
            angle(1) = acosd(dot(Goal_vector, perp_edgetook1));
            angle(2) = acosd(dot(Goal_vector, perp_edgetook2));
            angle(3) = acosd(dot(Goal_vector, perp_other1edge1));
            angle(4) = acosd(dot(Goal_vector, perp_other1edge2));
            angle(5) = acosd(dot(Goal_vector, perp_other2edge1));
            angle(6) = acosd(dot(Goal_vector, perp_other2edge2));

            % Check if the chosen edge is in the direction of ParticipantGoal
            if ismember(min(angle), angle(:,1:2))
                goal_direction_choices = goal_direction_choices + 1;
            end
        end
        total_choices = total_choices + 1;
    end
end

% Calculate the percentage of times the participant chose the shorter edge
percentage_shorter_edge = (shorter_edge_choices / total_choices) * 100;

% Calculate the percentage of times the participant chose the edge in the direction of ParticipantGoal
percentage_goal_direction = (goal_direction_choices / total_choices) * 100;



%% STEP 6.5 Voronoi and circle local density 
numericData = [];

% Loop through each cell and extract the numeric data
for i = 1:length(Agent_rotatedposition)
    numericData = [numericData; cell2mat(Agent_rotatedposition{i})];
end
positions = [numericData; HMD_position(:,1:2)]; % Nx2 matrix of (x, y) coordinates
% Calculate Voronoi diagram
[V, C] = voronoin(positions);

% Find the Voronoi cell of the participant
participant_cell = C{end};

% Calculate the area of the participant's Voronoi cell
cell_vertices = V(participant_cell, :);
cell_area = polyarea(cell_vertices(:, 1), cell_vertices(:, 2));

% Adjust for corridor boundaries (example: rectangular corridor)
corridor_x = [-5, 5, 5, -5];
corridor_y = [-2, -3, 6, 6];
% Initialize list to store local densities through time
local_densities_voronoi = [];

% Loop through each HMD position to calculate Voronoi density
for t = 1:size(HMD_position, 1)
    HMD_Pos = HMD_position(t, 1:2);
    
    % Combine agent positions and current participant position
    positions = [numericData; HMD_Pos]; % Nx2 matrix of (x, y) coordinates
    
    % Calculate Voronoi diagram
    [V, C] = voronoin(positions);
    
    % Find the Voronoi cell of the participant
    participant_cell = C{end};
    
    if all(participant_cell ~= 1)  % If the cell is bounded
        cell_vertices = V(participant_cell, :);
        
        % Adjust for corridor boundaries
        [in, on] = inpolygon(cell_vertices(:, 1), cell_vertices(:, 2), corridor_x, corridor_y);
        adjusted_area = polyarea(cell_vertices(in | on, 1), cell_vertices(in | on, 2));
        
        % Compute local density
        local_density_voronoi = 1 / adjusted_area;
    else
        local_density_voronoi = inf;
    end
    
    local_densities_voronoi = [local_densities_voronoi; local_density_voronoi];
end
local_densities_circle=[];
% Loop through each HMD position to calculate circle density
for t = 1:size(HMD_position, 1)
    HMD_pos =  HMD_position(t, 1:2);
    
    % Combine agent positions and current participant position
    positions = [numericData; HMD_pos]; % Nx2 matrix of (x, y) coordinates
    
    % Define circle parameters
    circle_radius = 1.0; % 1 meter radius
    theta = linspace(0, 2 * pi, 100);
    circle_x = HMD_pos(1) + circle_radius * cos(theta);
    circle_y = HMD_pos(2) + circle_radius * sin(theta);
    
    % Adjust for corridor boundaries
    [in, on] = inpolygon(circle_x, circle_y, corridor_x, corridor_y);
    adjusted_area = polyarea(circle_x(in | on), circle_y(in | on));
    
    % Count agents within the circle
    distances = sqrt(sum((numericData - HMD_pos) .^ 2, 2));
    num_agents_in_circle = sum(distances < circle_radius);
    
    % Compute local density
    local_density_circle = (num_agents_in_circle + 1) / adjusted_area; % WE ADD ONE FOR THE PARTICIPANT 
    
    local_densities_circle = [local_densities_circle; local_density_circle];
end


%% Step 7 Determine the crossed edges and their lengths
crossed_edges = [];
Aperture_sizes = [];
ShoulderAngle = [];
ShoulderWidth=[];
RelativeApertureSize = [];
AgentsCrossed=[];
for k = 1:length(TrajectorySequence)-1
    tri1 = TrajectorySequence(k);
    tri2 = TrajectorySequence(k+1);
    
    % Get the vertices of the two triangles
    vertices1 = agent_positions_2d(tri(tri1, :), :);
    vertices2 = agent_positions_2d(tri(tri2, :), :);
    
    % Compare the edges to find the common edge
    for j = 1:3
        edge1_start = vertices1(j, :);
        edge1_end = vertices1(mod(j, 3) + 1, :);
        
        for l = 1:3
            edge2_start = vertices2(l, :);
            edge2_end = vertices2(mod(l, 3) + 1, :);
            
            % Check if the edges are the same (considering both directions)
            if (isequal(edge1_start, edge2_start) && isequal(edge1_end, edge2_end)) || ...
               (isequal(edge1_start, edge2_end) && isequal(edge1_end, edge2_start))
                % Calculate the length of the common edge
                edge_length = norm(edge1_start - edge1_end);

                % Store the crossed edge and its length
                crossed_edges = [crossed_edges;  tri1, tri2, j, l,edge_length];
                
                % Annotate the plot with the size of the crossed edge
                mid_point = (edge1_start + edge1_end) / 2;
%                 text(mid_point(1), mid_point(2), sprintf('%.2f', edge_length), 'Color', 'm', 'FontSize', 10, 'FontWeight', 'bold');
                
                % Find the exact crossing point
                crossing_point = (edge1_start + edge1_end) / 2;
                try
                CenterShoulder=(humanShoulders(:,1:2)+humanShoulders(:,4:5))/2;
                catch
                CenterShoulder=HMD_position;
                end

                % Interpolate shoulder positions at the crossing point
                % Assuming HMD_position and humanShoulders are synchronized and have the same length
                % Find the closest points in HMD_position to the crossing point
                try
                distances = sqrt(sum((CenterShoulder - crossing_point).^2, 2));
                [~, closest_idx] = min(distances);
                catch
                    distances =[]; 
                end 



                % Find the corresponding polygons in translated_polygons
                polygon1 = [];
                polygon2 = [];
                for idx = 1:length(translated_polygons)
                    % Get the vertices of the polygon
                    poly_vertices = translated_polygons{idx}(:, 1:2);
                    
                    % Create the convex hull of the polygon
                    k = convhull(poly_vertices(:, 1), poly_vertices(:, 2));
                    
                    % Check if edge1_start is inside the convex hull
                    if inpolygon(edge1_start(1), edge1_start(2), poly_vertices(k, 1), poly_vertices(k, 2))
                        polygon1 = translated_polygons{idx};
                        min_distance_poly_indices1 = idx;
                    end
                    
                    % Check if edge1_end is inside the convex hull
                    if inpolygon(edge1_end(1), edge1_end(2), poly_vertices(k, 1), poly_vertices(k, 2))
                        polygon2 = translated_polygons{idx};
                        min_distance_poly_indices2 = idx;
                    end
                end
                
                   % Initialize variables to store the minimum distance and clearances
                    min_distance = inf;
                    min_point1 = [];
                    min_point2 = [];
        
                    for m = 1:size(polygon1, 1)
                        for n = 1:size(polygon2, 1)
                            distance = norm(polygon1(m, 1:3) - polygon2(n, 1:3));
                            if distance < min_distance
                                min_distance = distance;
                                min_point1 = polygon1(m, 1:3);
                                min_point2 = polygon2(n, 1:3);
                            end
                        end
                    end
        
                    % Store the aperture size
                    Aperture_sizes = [Aperture_sizes; min_distance];
        
                    % Store the indices of the polygons contributing to the minimum distance
                    AgentsCrossed = [AgentsCrossed; min_distance_poly_indices1; min_distance_poly_indices2];
                    
                    Min_point = (min_point1(1, 1:2) + min_point2(1, 1:2)) / 2;
        
                    % Annotate the plot with the size of the aperture
%                      text(Min_point(1), Min_point(2) - 0.1, sprintf('%.2f', min_distance), 'Color', 'g', 'FontSize', 10, 'FontWeight', 'bold');
                    try        
                    % Interpolate the shoulder positions
                    left_shoulder = humanShoulders(closest_idx, 1:3);
                    right_shoulder = humanShoulders(closest_idx, 4:6);
        
                    % Calculate the shoulder vector at the crossing point
                    shoulder_vector = left_shoulder(1, 1:2) - right_shoulder(1, 1:2);
        
                    % Calculate the vectors representing the edges using the minimum distance points
                    edge_vector = min_point2(1, 1:2) - min_point1(1, 1:2);
        
                    % Normalize the vectors
                    shoulder_vector2 = shoulder_vector / norm(shoulder_vector);
                    edge_vector = edge_vector / norm(edge_vector);
        
                    % Calculate the angles between the shoulder vectors and the edge vectors
                    angle = acosd(dot(shoulder_vector2, edge_vector));
        
                    ShoulderAngle = [ShoulderAngle; angle];
                    ShoulderWidth = [ShoulderWidth;norm(shoulder_vector)];
                    RelativeApertureSize =[RelativeApertureSize;min_distance/norm(shoulder_vector)];
                    catch
                    end
                    % Visualize the shoulder vector
%                     quiver(Min_point(1), Min_point(2), shoulder_vector(1), shoulder_vector(2), 0.5, 'r', 'LineWidth', 2, 'MaxHeadSize', 2);
        
                    % Annotate the angle on the plot, slightly offset to avoid overlap
%                     text(Min_point(1), Min_point(2) - 0.2, sprintf('%.2f°', angle), 'Color', 'b', 'FontSize', 10, 'FontWeight', 'bold');
  
                
                break;
            end
        end
    end
end
end