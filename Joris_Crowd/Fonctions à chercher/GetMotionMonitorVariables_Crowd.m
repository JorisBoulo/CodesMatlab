function [Head_position,Body_position,Body_Vel,Probed_point_contact,title_probed_point_contact,Reorientation_var,...
    title_reorientation_var]...
    = GetMotionMonitorVariables_Crowd(file_MM)
%%
load(file_MM);
%%
[NameRow, colInd] = find(strcmp(MMData(:,1), 'Frame #'));
[~,column_MMData]=size(MMData);
Title=MMData(NameRow,:);
MMData=str2double(string(MMData(NameRow+1:end,:)));
for i=1:column_MMData
%variables du tronc (utile pour les points de d�viation et la fluidit�
if ~isempty(strfind(Title{1,i},'Body_X'))
    Pos_Body_X=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Body_Y'))
    Pos_Body_Y=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Body_Z'))
    Pos_Body_Z=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Head_X_Vel')) 
    Body_X_velocity=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Head_Y_Vel'))
    Body_Y_velocity=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Body_Vel_Z'))
    Body_Z_velocity=MMData(:,i);
 elseif ~isempty(strfind(Title{1,12},'Body_X_Acc'))
     Body_X_acc=MMData(:,12);
 elseif ~isempty(strfind(Title{1,i},'Body_Y_Vel'))
    Body_Y_acc=MMData(:,13);
 elseif ~isempty(strfind(Title{1,i},'Body_Z_Vel'))
    Body_Z_acc=MMData(:,14);
 %variable des extr�mit�s pour les contacts
elseif ~isempty(strfind(Title{1,i},'Accro_left_X')) %% Creer Un point pour les accromions ...
    L_Accro_X=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Accro_left_Y'))
     L_Accro_Y=MMData(:,i);
 elseif ~isempty(strfind(Title{1,i},'Accro_left_Z'))
     L_Accro_Z=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Accro_Right_X'))
    R_Accro_X=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Accro_Right_Y'))
    R_Accro_Y=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Accro_Right_Z'))
    R_Accro_Z=MMData(:,i);
%Variable li� � la s�quence de r�orientation 
% elseif ~isempty(strfind(Title{1,i},'Angle_Head_Z'))
    Head_Rot_Z_G=MMData(:,32);   
elseif ~isempty(strfind(Title{1,i},'Angle_Thorax_Z'))
    Trunk_Rot_Z_G=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Angle_Pelvis_Z'))  
    Pelvis_Rot_Z_G=MMData(:,i);
    %Ligne � modifier!!
% elseif ~isempty(strfind(Title{1,i},'Angle_Head_Y'))
    Head_Rot_Y_G=MMData(:,31);   
elseif ~isempty(strfind(Title{1,i},'Angle_Thorax_Y'))
    Trunk_Rot_Y_G=MMData(:,i);   
elseif ~isempty(strfind(Title{1,i},'Angle_Pelvis_Z'))
    Pelvis_Rot_Y_G=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Head_Angular_Vel_Z'))
    Head_Angular_Vel_Z=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Trunk_Angular_Vel_Z'))
    Trunk_Angular_Vel_Z=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Pelvis_Angular_Vel_Z'))
    Pelvis_Angular_Vel_Z=MMData(:,i); 
    %position Head
elseif ~isempty(strfind(Title{1,i},'Pos_Head_X'))
    Pos_Head_X=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Pos_Head_Y'))
    Pos_Head_Y=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Pos_Head_Z'))
    Pos_Head_Z=MMData(:,i);
elseif ~isempty(strfind(Title{1,i},'Head_Pos_X'))
    Pos_Head_X=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Head_Pos_Y'))
    Pos_Head_Y=MMData(:,i); 
elseif ~isempty(strfind(Title{1,i},'Head_Pos_Z'))
    Pos_Head_Z=MMData(:,i);
  

end
end
Head_position=[Pos_Head_X Pos_Head_Y Pos_Head_Z];
Body_position=[Pos_Body_X Pos_Body_Y Pos_Body_Z];
Body_Vel=[Body_X_velocity Body_Y_velocity Body_Z_velocity];
% Bdy_Acc=[Body_X_acc Body_Y_acc Body_Z_acc];
Probed_point_contact=[L_Accro_X L_Accro_Y  L_Accro_Z R_Accro_X R_Accro_Y R_Accro_Z];
title_probed_point_contact={'L_Accro_X' 'L_Accro_Y' 'L_Accro_Z' 'R_Accro_X' 'R_Accro_Y' 'R_Accro_Y'};

Reorientation_var=[Head_Rot_Z_G];%, Trunk_Rot_Z_G, Pelvis_Rot_Z_G, Head_Angular_Vel_Z, Trunk_Angular_Vel_Z, Pelvis_Angular_Vel_Z];
 title_reorientation_var={'Head_Rot_Z_G'};%, 'Trunk_Rot_Z_G', 'Pelvis_Rot_Z_G', 'Head_Angular_Vel_Z', 'Trunk_Angular_Vel_Z', 'Pelvis_Angular_Vel_Z'};


