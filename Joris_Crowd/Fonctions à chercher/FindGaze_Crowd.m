function [output_gaze,DistanceParticipantAgent] =FindGaze_Crowd(Gaze,AgentsPositions,HMD_position,StartCrowd,AgentName,CrowdConditionName,w)




Gaze_Ground=contains(Gaze,"Ground");
Gaze_Crowd=contains(Gaze,"SM_");
Gaze_Goal=contains(Gaze,"Panel");
Gaze_Environment=contains(Gaze,"Envi");
CrowdAgents=extractAfter(Gaze,": SM_"); % On utilise contains car Panel change selon le mot marquÃ© sur le paneau 
AgentNameShort=erase(AgentName,'SM_');
%% Calcul de la distance entre le SM et le participant 
AgentPosition=[]; 
DistanceParticipantAgent=[];
for i=1:size(Gaze,1)
    AgentsPositionColon= find(contains(string(AgentNameShort),CrowdAgents(i))); 
    if size(AgentsPositionColon,2)==1
        % Extract the positions and calculate the distance
        x_diff = cell2mat(AgentsPositions {1,AgentsPositionColon(1)}(1,1));
        y_diff = cell2mat(AgentsPositions {1,AgentsPositionColon(1)}(1,2));
        AgentPosition = [x_diff, y_diff];
        DistanceParticipantAgent = [DistanceParticipantAgent;sqrt((AgentPosition(:, 2) - HMD_position(i, 2)).^2 + (AgentPosition(:, 1) - HMD_position(i, 1)).^2)];
    end
    
end   

 if w >= 1 && w <= 30
     if contains(CrowdConditionName,'(1)')||contains(CrowdConditionName,"2")
         Time_Gaze_Ground=(sum(Gaze_Ground(StartCrowd:find(HMD_position(:,2)>3,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>3,1,'first')),2))*100;
         Time_Gaze_Goal=(sum(Gaze_Goal(StartCrowd:find(HMD_position(:,2)>3,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>3,1,'first')),2))*100;
         Time_Gaze_Environment=(sum(Gaze_Environment(StartCrowd:find(HMD_position(:,2)>3,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>3,1,'first')),2))*100;
         Time_Gaze_Crowd=(sum(Gaze_Crowd(StartCrowd:find(HMD_position(:,2)>3,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>3,1,'first')),2))*100;

     elseif contains(CrowdConditionName,'3')||contains(CrowdConditionName,"4")
         Time_Gaze_Ground=(sum(Gaze_Ground(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
         Time_Gaze_Goal=(sum(Gaze_Goal(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
         Time_Gaze_Environment=(sum(Gaze_Environment(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
         Time_Gaze_Crowd=(sum(Gaze_Crowd(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
     
     elseif contains(CrowdConditionName,'5')||contains(CrowdConditionName,"6")
         Time_Gaze_Ground=(sum(Gaze_Ground(StartCrowd:find(HMD_position(:,1)>4,1,'first'),1))/size((StartCrowd:find(HMD_position(:,1)>4,1,'first')),2))*100;
         Time_Gaze_Goal=(sum(Gaze_Goal(StartCrowd:find(HMD_position(:,1)>4,1,'first'),1))/size((StartCrowd:find(HMD_position(:,1)>4,1,'first')),2))*100;
         Time_Gaze_Environment=(sum(Gaze_Environment(StartCrowd:find(HMD_position(:,1)>4,1,'first'),1))/size((StartCrowd:find(HMD_position(:,1)>4,1,'first')),2))*100;
         Time_Gaze_Crowd=(sum(Gaze_Crowd(StartCrowd:find(HMD_position(:,1)>4,1,'first'),1))/size((StartCrowd:find(HMD_position(:,1)>4,1,'first')),2))*100;

     end
else
    Time_Gaze_Ground=(sum(Gaze_Ground(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
    Time_Gaze_Goal=(sum(Gaze_Goal(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
    Time_Gaze_Environment=(sum(Gaze_Environment(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;
    Time_Gaze_Crowd=(sum(Gaze_Crowd(StartCrowd:find(HMD_position(:,2)>5,1,'first'),1))/size((StartCrowd:find(HMD_position(:,2)>5,1,'first')),2))*100;

 end  

output_gaze=[Time_Gaze_Ground,Time_Gaze_Goal,Time_Gaze_Environment,Time_Gaze_Crowd];

end

