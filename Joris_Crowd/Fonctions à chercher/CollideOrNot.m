% Helper function to check if a string can be converted to a float

% Step 1: Read the data from the polygons file
try
    fileID = fopen('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 4\TryExtr\polygons.txt', 'r');
    lines = textscan(fileID, '%s', 'Delimiter', '\n');
    lines = lines{1};
    fclose(fileID);
catch
    disp("The file 'polygons.txt' was not found.");
    return;
end

% Step 2: Parse the polygons data
polygons = {};

for i = 1:length(lines)
    line = lines{i};
    if contains(line, 'VertexData')
        startIdx = strfind(line, '((X=') + 1;
        endIdx = strfind(line, ')),IndexData');
        if isempty(startIdx) || isempty(endIdx)
            disp(['Error parsing line: ', line]);
            continue;
        end
        vertex_data = strsplit(line(startIdx:endIdx-1), '),(');
        polygon = [];
        for j = 1:length(vertex_data)
            vertex = vertex_data{j};
            try
                % Remove any extraneous characters and split by comma
                coords = str2double(strsplit(strrep(strrep(strrep(strrep(strrep(vertex, 'X=',''), 'Y=', ''), 'Z=', ''), '(', ''), ')', ''), ','));
                if length(coords) == 3 && all(~isnan(coords))
                    polygon = [polygon; coords / 100]; % Convert to meters
                else
                    disp(['Error converting coordinates to float: ', vertex]);
                end
            catch
                disp(['Error converting coordinates to float: ', vertex]);
                continue;
            end
        end
        polygons{end+1} = polygon;
    end
end           

% Step 3: Read and parse the HighCrowd.xml file to get agent positions
try
    tree = xmlread('C:\UMANS Wouter\UMANS Wouter\UMANS\agents\LowCrowdAnalyzed.xml');
catch
    disp("The file 'HighCrowd.xml' was not found or could not be parsed.");
    return;
end

% Extract positions of agents starting from the second one
agent_positions = [];
agents = tree.getElementsByTagName('Agent');
for i = 1:agents.getLength
    if i == 1
        continue;
    end
    agent = agents.item(i-1);
    pos = agent.getElementsByTagName('pos').item(0);
    x = str2double(pos.getAttribute('x'));
    y = str2double(pos.getAttribute('y'));
    agent_positions = [agent_positions; x, y];
end

% Step 4: Translate the polygons based on agent positions
translated_polygons = {};
for i = 1:min(length(polygons), size(agent_positions, 1))
    x_offset = agent_positions(i, 1);
    y_offset = agent_positions(i, 2);
    ConvexHull{i}=convhull(polygon);
    polygon = polygons{i};
    translated_polygon = [polygon(:, 1) + x_offset, polygon(:, 2) + y_offset, polygon(:, 3)];
    translated_polygons{end+1} = translated_polygon;
end

% % Step 5: Read the DensityHigh CSV file to get head positions
% head_positions = [];
% try
%     fid = fopen('DENSITYHIGH_ANALYZED-2024-08-15T14-08-07.958341.CSV', 'r');
%     if fid == -1
%         error('File not found');
%     end
%     % Skip header
%     fgetl(fid);
%     while ~feof(fid)
%         line = fgetl(fid);
%         head_pos = str2double(strsplit(strrep(strrep(line, '[', ''), ']', ''), ','));
%         if length(head_pos) == 3 && all(~isnan(head_pos))
%             % Convert to meters
%             head_positions = [head_positions; head_pos / 100];
%         else
%             disp(['Non-numeric or incomplete data found in row: ', line]);
%         end
%     end
%     fclose(fid);
% catch ME
%     if strcmp(ME.identifier, 'File not found')
%         disp("The file 'DENSITYHIGH_ANALYZED-2024-08-15T14-08-07.958341.CSV' was not found.");
%     else
%         rethrow(ME);
%     end
%     return;
% end
% Your existing code before the function definitions

% Step 6: Plot the translated polygons and head trajectory in 3D
figure;
hold on;
colors = jet(length(translated_polygons)); % 'lines' colormap with distinct colors

% Plot polygons
for i = 1:length(translated_polygons)
    polygon = translated_polygons{i};
    trisurf(ConvexHull{i}, polygon(:, 1), polygon(:, 2), polygon(:, 3), 'FaceColor', colors(i, :), 'EdgeColor', colors(i, :), 'FaceAlpha', 0.25);
end

% Plot head trajectory
% if ~isempty(head_positions)
%     plot3(head_positions(:, 1), head_positions(:, 2), head_positions(:, 3), 'o-', 'Color', 'blue', 'MarkerFaceColor', 'blue', 'DisplayName', 'Head Trajectory');
% end

% Define the start and goal points
% start_point = [-1.50, -0.5, 0];
% goal_point = [-8.57, 6.50, 0];
% 
% % Find the optimal path
% optimal_path = astar(start_point, goal_point, translated_polygons);
% 
% % Plot the optimal path
% if ~isempty(optimal_path)
%     plot3(optimal_path(:, 1), optimal_path(:, 2), optimal_path(:, 3), 'r-', 'LineWidth', 2, 'DisplayName', 'Optimal Path');
% end
% 
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% legend;
% hold off;

% % Function definitions
% function path = astar(start_point, goal_point, translated_polygons)
%     % Initialize open and closed lists
%     open_list = {start_point};
%     closed_list = [];
%     came_from = containers.Map('KeyType', 'char', 'ValueType', 'any');
%     g_score = containers.Map('KeyType', 'char', 'ValueType', 'double');
%     f_score = containers.Map('KeyType', 'char', 'ValueType', 'double');
%     
%     % Initialize g_score and f_score
%     g_score(mat2str(start_point)) = 0;
%     f_score(mat2str(start_point)) = heuristic(start_point, goal_point);
%     
%     while ~isempty(open_list)
%         % Get the node with the lowest f_score
%         [~, idx] = min(cellfun(@(x) f_score(mat2str(x)), open_list));
%         current = open_list{idx};
%         
%         % If the goal is reached, reconstruct the path
%         if isequal(current, goal_point)
%             path = reconstruct_path(came_from, current);
%             return;
%         end
%         
%         % Move current node from open to closed list
%         open_list(idx) = [];
%         closed_list = [closed_list; current];
%         
%         % Get neighbors of the current node
%         neighbors = get_neighbors(current, translated_polygons);
%         
%         for i = 1:length(neighbors)
%             neighbor = neighbors{i};
%             
%             % If neighbor is in closed list, skip it
%             if ismember(neighbor, closed_list, 'rows')
%                 continue;
%             end
%             
%             % Calculate tentative g_score
%             tentative_g_score = g_score(mat2str(current)) + distance(current, neighbor);
%             
%             % If neighbor is not in open list or new g_score is lower
%             if ~isKey(g_score, mat2str(neighbor)) || tentative_g_score < g_score(mat2str(neighbor))
%                 % Update g_score and f_score
%                 came_from(mat2str(neighbor)) = current;
%                 g_score(mat2str(neighbor)) = tentative_g_score;
%                 f_score(mat2str(neighbor)) = tentative_g_score + heuristic(neighbor, goal_point);
%                 
%                 % Add neighbor to open list if not already present
%                 if ~any(cellfun(@(x) isequal(x, neighbor), open_list))
%                     open_list = [open_list; {neighbor}];
%                 end
%             end
%         end
%     end
%     
%     % If no path is found, return empty
%     path = [];
% end
% 
% % Heuristic function (Euclidean distance)
% function h = heuristic(point, goal)
%     h = sqrt(sum((point - goal).^2));
% end
% 
% % Distance function (Euclidean distance)
% function d = distance(point1, point2)
%     d = sqrt(sum((point1 - point2).^2));
% end
% 
% % Reconstruct path from came_from map
% function path = reconstruct_path(came_from, current)
%     path = current;
%     while isKey(came_from, mat2str(current))
%         current = came_from(mat2str(current));
%         path = [current; path];
%     end
% end
% 
% % Get neighbors of a point (for simplicity, consider 8-connected grid)
% function neighbors = get_neighbors(point, translated_polygons)
%     step_size = 0.4; % Step size for the person (40 cm)
%     directions = [1, 0, 0; -1, 0, 0; 0, 1, 0; 0, -1, 0; 1, 1, 0; -1, -1, 0; 1, -1, 0; -1, 1, 0];
%     neighbors = {};
%     for i = 1:size(directions, 1)
%         neighbor = point + step_size * directions(i, :);
%         if is_valid(neighbor, translated_polygons)
%             neighbors{end+1} = neighbor;
%         end
%     end
% end
% 
% % Check if a point is valid (not colliding with any polygon)
% function valid = is_valid(point, translated_polygons)
%     valid = true;
%     for i = 1:length(translated_polygons)
%         polygon = translated_polygons{i};
%         if inpolygon3d(point, polygon)
%             valid = false;
%             break;
%         end
%     end
% end
% 
% % Check if a point is inside a 3D polygon (simplified version)
% function inside = inpolygon3d(point, polygon)
%     % Simplified check: point should not be inside the bounding box of the polygon
%     min_coords = min(polygon);
%     max_coords = max(polygon);
%     inside = all(point >= min_coords) && all(point <= max_coords);
% end