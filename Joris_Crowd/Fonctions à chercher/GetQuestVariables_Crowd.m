function [HMD_position, HMD_rotation, Agent_position, Gaze,HMD_position_raw,HMD_position_cut]=GetQuestVariables_Crowd(file_Quest,Vicon_Head_position)
%This function is used to extract the variables that are collected by the QuestPro in a .csv file 
%via the LogVS application 


%Open Quest csv file
data_quest=load(file_Quest);
if size(data_quest.DataQuest,2)<17
    RawDataFolder=strrep(file_Quest, '2_TransformData', '1_RawData');
    RawDataFolder = RawDataFolder(1:strfind(RawDataFolder, '1_RawData\')+9);

    files = dir(fullfile(RawDataFolder, '*.csv'));
    
    % Cherche les fichiers qui contiennent 'P01_38' dans leur nom
    [~, name, ~] = fileparts(file_Quest);
    tokens = regexp(name, '(P\d+_\d+)', 'tokens');
    pattern = tokens{1}{1};
    
    matchingFiles = {};
    
    for i = 1:length(files)
        if contains(files(i).name, pattern)
            matchingFiles{end+1} = fullfile(RawDataFolder, files(i).name);
        end
    end
    fileContent = importdata(matchingFiles{1,1});
    Data=[];
    try
    for i = 1:length(fileContent)
            cleanedContent = erase(fileContent{i}, {'"', '[', ']'});
            numCommas = count(cleanedContent, ',');
            if numCommas == 17
            lineData2 = strsplit(cleanedContent, ',');
            end
            Data=[Data;lineData2];

    end
    catch
        for i = 1:length(fileContent.textdata)
            cleanedContent = erase(fileContent.textdata{i}, {'"', '[', ']'});
            numCommas = count(cleanedContent, ',');
            if numCommas == 17
            lineData2 = strsplit(cleanedContent, ',');
            end
            Data=[Data;lineData2];

        end
    end

    data_quest.DataQuest=cell(Data);
end 

sizeQuestData = find(string(data_quest.DataQuest(:,7))~='false'); % Initial loop size
for i = sizeQuestData
        disp 'Pouet'
        data_quest.DataQuest(i,:)=[];
        sizeQuestData=sizeQuestData-1;
end

%% Extract raw variable

Gaze=string(data_quest.DataQuest(2:end,18));
%%
% Resampling

frame_time=str2double(string(data_quest.DataQuest(2:end,14)));

a=0;
for i=1:size(frame_time,1)-1
    if isnan(frame_time(i,1))
        a=[a];
    else
    a=[a;a(i,1)+(frame_time(i+1,1)-frame_time(i,1))];
    end
end
TimeQuest=a;% Mettre en secondes 

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
HMDx=(str2double(string(data_quest.DataQuest(2:end,2))))/100; 
HMDy=(str2double(string(data_quest.DataQuest(2:end,1))))/100; 
HMDz=(str2double(string(data_quest.DataQuest(2:end,3))))/100; 
%v�rifier les angles de rotation, car Joris ne les a jamais pris. D'un autre c�t�, je ne suis pas suppos� en avoir besoin. 
HMDangx=(str2double(string(data_quest.DataQuest(2:end,4))));
HMDangy=-(str2double(string(data_quest.DataQuest(2:end,5))));
HMDangz=(str2double(string(data_quest.DataQuest(2:end,6))))+90;

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
Agent_position_x=(str2double(string(data_quest.DataQuest(2:end,2))))/100; 
Agent_position_y=(str2double(string(data_quest.DataQuest(2:end,1))))/100; 
Agent_position_z=(str2double(string(data_quest.DataQuest(2:end,3))))/100;

% Remove incoherent data
thresholdup = 10; 
thresholddown = -10;     % Define a threshold for incoherent data
incoherent_indices = HMDx > thresholdup | HMDy > thresholdup | HMDz > thresholdup |HMDx < thresholddown | HMDy < thresholddown | HMDz < thresholddown;

% Remove rows with incoherent data
HMDx(incoherent_indices) = [];
HMDy(incoherent_indices) = [];
HMDz(incoherent_indices) = [];
HMDangx(incoherent_indices) = [];
HMDangy(incoherent_indices) = [];
HMDangz(incoherent_indices) = [];
Agent_position_x(incoherent_indices) = [];
Agent_position_y(incoherent_indices) = [];
Agent_position_z(incoherent_indices) = [];
TimeQuest(incoherent_indices) = [];
Gaze(incoherent_indices) = [];
%% Filter les donn�es cin�matiques du casque
[Resamp_HMDx]=Filtrate(HMDx,2,9,90);
[Resamp_HMDy]=Filtrate(HMDy,2,9,90);
[Resamp_HMDz]=Filtrate(HMDz,2,9,90);

[Resamp_HMD_ang_x]=Filtrate(HMDangx,2,9,90);
[Resamp_HMD_ang_y]=Filtrate(HMDangy,2,9,90);
[Resamp_HMD_ang_z]=Filtrate(HMDangz,2,9,90);

[ResampAgent_x]=Filtrate(Agent_position_x,2,9,90);
[ResampAgent_y]=Filtrate(Agent_position_y,2,9,90);
[ResampAgent_z]=Filtrate(Agent_position_z,2,9,90);

%%Resample
[Resamp_HMDx,TimeRes]=resample(Resamp_HMDx,TimeQuest(:,1),90);
[Resamp_HMDy,TimeRes]=resample(Resamp_HMDy,TimeQuest(:,1),90);
[Resamp_HMDz,TimeRes]=resample(Resamp_HMDz,TimeQuest(:,1),90);

[Resamp_HMD_ang_x,TimeRes]=resample(Resamp_HMD_ang_x,TimeQuest(:,1),90);
[Resamp_HMD_ang_y,TimeRes]=resample(Resamp_HMD_ang_y,TimeQuest(:,1),90);
[Resamp_HMD_ang_z,TimeRes]=resample(Resamp_HMD_ang_z,TimeQuest(:,1),90);

[ResampAgent_x,TimeRes]=resample(ResampAgent_x,TimeQuest(:,1),90);
[ResampAgent_y,TimeRes]=resample(ResampAgent_y,TimeQuest(:,1),90);
[ResampAgent_z,TimeRes]=resample(ResampAgent_z,TimeQuest(:,1),90);


%% Reorientation spatiale 
 
%int�grer ici les d�placements spatiaux des donn�es avec de rotate...
[PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = trigrotate(Resamp_HMDx,Resamp_HMDy,Resamp_HMDz,ResampAgent_x,ResampAgent_y,ResampAgent_z,-45);
%%
[PPx,PPy,PPz,VAPx,VAPy,VAPz] = trigrotate(HMDx,HMDx,HMDx,Agent_position_x,Agent_position_y,Agent_position_z,-45);
%% Find the longest nonNan segment
% Initialize variables
longestSegment = [];
currentSegment = [];
maxLength = 0;
startIndex = 0;
currentStartIndex = 0;
try
% Loop through the data to find non-NaN segments
for i = 1:size(Vicon_Head_position, 1)
    if all(~isnan(Vicon_Head_position(i, :)))
        if isempty(currentSegment)
            currentStartIndex = i; % Mark the start of a new segment
        end
        currentSegment = [currentSegment; Vicon_Head_position(i, :)];
    else
        if size(currentSegment, 1) > maxLength
            maxLength = size(currentSegment, 1);
            longestSegment = currentSegment;
            startIndex = currentStartIndex;
        end
        currentSegment = [];
    end
end

% Check the last segment
if size(currentSegment, 1) > maxLength
    longestSegment = currentSegment;
    startIndex = currentStartIndex;
end


%% Alignement temporel avec les donn�es Vicon

[istart,istop]=findsignal(PPymod,longestSegment(:,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
istart=istart-startIndex-1;
istop=istart+size(Vicon_Head_position,1);
catch
end 

%% Output variables
HMD_position=[PPxmod PPymod PPzmod];

try
HMD_position_cut=HMD_position(istart:istop,:);
catch
    HMD_position_cut='tant pis'; 
end
HMD_position_raw=[PPx PPy PPz];

HMD_rotation=[Resamp_HMD_ang_x, Resamp_HMD_ang_y, Resamp_HMD_ang_z];

Agent_position=[VAPxmod,VAPymod,VAPzmod];



% Convert words to categorical numbers
gazeDataCategorical = categorical(Gaze);
gazeDataNumeric = double(gazeDataCategorical);

% Resample the data
[gazeDataResampled, TimeRes] = resample(gazeDataNumeric, TimeQuest(:,1), 90);

% Convert back to words
gazeDataCategories = categories(gazeDataCategorical);

% Round the resampled data to the nearest integer
gazeDataResampledRounded = round(gazeDataResampled);

% Ensure indices are within valid range
gazeDataResampledRounded(gazeDataResampledRounded < 1) = 1;
gazeDataResampledRounded(gazeDataResampledRounded > numel(gazeDataCategories)) = numel(gazeDataCategories);

% Map back to words
Gaze = gazeDataCategories(gazeDataResampledRounded);
end

