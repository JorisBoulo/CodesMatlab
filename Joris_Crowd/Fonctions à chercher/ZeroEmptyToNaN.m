function [Data]=ZeroEmptyToNaN(DataBefore)
if isempty(DataBefore)
    Data=nan;
else
    Data=DataBefore;
end
if (Data==0)
    Data=nan;
else
    Data=Data;
end