function isInside = inpolyhedron(faces, vertices, point)
    % Check if a point is inside a 3D polyhedron using the ray-casting method
    % faces: Nx3 or Nx4 matrix of indices into vertices defining the polyhedron faces
    % vertices: Mx3 matrix of vertex coordinates
    % point: 1x3 vector of the point to check

    % Initialize the count of intersections
    numIntersections = 0;

    % Define a ray direction (arbitrary)
    rayDirection = [1, 0, 0];

    % Loop through each face of the polyhedron
    for i = 1:size(faces, 1)
        face = faces(i, :);
        faceVertices = vertices(face, :);

        % Check if the ray intersects with the face
        if rayIntersectsFace(point, rayDirection, faceVertices)
            numIntersections = numIntersections + 1;
        end
    end

    % If the number of intersections is odd, the point is inside the polyhedron
    isInside = mod(numIntersections, 2) == 1;
end

function intersects = rayIntersectsFace(point, rayDirection, faceVertices)
    % Check if a ray intersects with a face of the polyhedron
    % point: 1x3 vector of the ray origin
    % rayDirection: 1x3 vector of the ray direction
    % faceVertices: Nx3 matrix of the face vertex coordinates

    % Define the plane of the face
    v0 = faceVertices(1, :);
    v1 = faceVertices(2, :);
    v2 = faceVertices(3, :);
    normal = cross(v1 - v0, v2 - v0);
    normal = normal / norm(normal);

    % Check if the ray is parallel to the plane
    dotProduct = dot(normal, rayDirection);
    if abs(dotProduct) < 1e-6
        intersects = false;
        return;
    end

    % Find the intersection point of the ray with the plane
    t = dot(normal, v0 - point) / dotProduct;
    if t < 0
        intersects = false;
        return;
    end

    intersectionPoint = point + t * rayDirection;

    % Check if the intersection point is inside the face
    intersects = inpolygon3d(intersectionPoint, faceVertices);
end

function isInside = inpolygon3d(point, faceVertices)
    % Check if a point is inside a 3D polygon (face)
    % point: 1x3 vector of the point to check
    % faceVertices: Nx3 matrix of the face vertex coordinates

    % Project the 3D polygon and the point onto a 2D plane
    normal = cross(faceVertices(2, :) - faceVertices(1, :), faceVertices(3, :) - faceVertices(1, :));
    normal = normal / norm(normal);
    [~, maxIdx] = max(abs(normal));
    if maxIdx == 1
        plane = [2, 3];
    elseif maxIdx == 2
        plane = [1, 3];
    else
        plane = [1, 2];
    end

    projectedPoint = point(plane);
    projectedVertices = faceVertices(:, plane);

    % Use the inpolygon function to check if the projected point is inside the projected polygon
    isInside = inpolygon(projectedPoint(1), projectedPoint(2), projectedVertices(:, 1), projectedVertices(:, 2));
end