% Generate synthetic data for 2x5 participants doing 8 trials each
rng(42); % For reproducibility

num_groups = 2;
num_participants_per_group = 5;
num_trials = 8;

data = [];

for group = 1:num_groups
    for participant = 1:num_participants_per_group
        for trial = 1:num_trials
            if group == 1
                speed = 2 + (10-2).*rand();
                aperture_size = 1 + (3-1).*rand();
            else
                speed = 1 + (5-1).*rand();
                aperture_size = 4 + (6-4).*rand();
            end
            data = [data; group, participant, trial, speed, aperture_size];
        end
    end
end

% Convert to table
data = array2table(data, 'VariableNames', {'Group', 'Participant', 'Trial', 'Speed', 'ApertureSize'});

% Feature extraction: Calculate average speed and average aperture size for each participant
features = varfun(@mean, data, 'InputVariables', {'Speed', 'ApertureSize'}, ...
                  'GroupingVariables', {'Group', 'Participant'});

% Normalize the features
features.mean_Speed = (features.mean_Speed - mean(features.mean_Speed)) / std(features.mean_Speed);
features.mean_ApertureSize = (features.mean_ApertureSize - mean(features.mean_ApertureSize)) / std(features.mean_ApertureSize);

% Apply K-means clustering
X = [features.mean_Speed, features.mean_ApertureSize];
[idx, C] = kmeans(X, 2, 'Replicates', 5);

% Plot the clusters
figure;
gscatter(features.mean_Speed, features.mean_ApertureSize, idx, 'bg', 'xo');
xlabel('Normalized Speed');
ylabel('Normalized Aperture Size');
title('Clusters of Participants Based on Speed and Aperture Size');
legend('Cluster 1', 'Cluster 2');
grid on;

% Display the features with cluster labels
features.Cluster = idx;
disp(features);
