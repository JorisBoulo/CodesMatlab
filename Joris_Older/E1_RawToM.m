clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'1_RawData');
outputpath=fullfile(subject_path,'2_TransformData');
 
numParticipant=erase(subject_path,'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\')
Rep={'01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15';'16';'17';'18';'19';'20';'21';'22';'23';'24';'25';'26';'27';'28';'29';'30';'31';'32';'33';'34';'35';'36';'37';'38';'39';'40'};
Sensor={'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'};
Error=1; 
A=1;
%% for EMG 
    for j=1:25
        NamesEMG{1,j}=sprintf('%s_%s.c3d',numParticipant,Rep{j});
        inputfilepath=fullfile(inputpath,NamesEMG{1,j});
        if exist(inputfilepath, 'file')==2
        cd(inputpath);
        Fc3d = btkReadAcquisition(NamesEMG{1,j});
        Emg=btkGetAnalogs(Fc3d);
        EmgName={'SternoLeft','ErSpRight','ObliqRight','GluMedRight','AccZLeftTA','AccZLeftTA'};
        for k=1:11
            EMG(k).Sensor = sprintf ('Emg.Sensor_%d_EMG%d',k,k);  
            EMG(k).Data=eval(EMG(k).Sensor);
        end
        HeelAceleration=[Emg.Sensor_1_ACCZ1,Emg.Sensor_2_ACCZ2]; 
        A=EMG(1).Data;B=EMG(2).Data;C=EMG(3).Data;D=EMG(4).Data;
        EmgData=[A B C D HeelAceleration];
        EmgData=num2cell(EmgData);
        DataEMG=[EmgName;EmgData];
        name=sprintf('%s_%s_EMG.mat',numParticipant,Rep{j});
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'DataEMG')
        else 
            Error= Error+1; 
        end 
    end 
%% for MM 
     for j=1:25
        NamesMM{1,j}=sprintf('%s_%s.xlsx',numParticipant,Rep{j});
        inputfilepath=fullfile(inputpath,NamesMM{1,j});
        if exist(inputfilepath, 'file')==2
        MMData=readcell(inputfilepath);
        name=sprintf('%s_%s_MM.mat',numParticipant,Rep{j});
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'MMData')
        else 
            Error= Error+1; 
        end 
     end 

%% for Quest
cd(inputpath);
fileList = dir('*.csv');
for j=1:size(fileList,1)
        DataQuest=readcell (fileList(j).name);
        Split=split(fileList(j).name,'-');
        name=cell2char(strcat(Split(1),'_Quest.mat'));
        outputfilepath=fullfile(outputpath,name);
        save(outputfilepath,'DataQuest')
end 
