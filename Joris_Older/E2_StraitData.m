% This script allows you to create the mean and std of muscle inactivity of each muscle for each participant  
%% *Open files*
% Choose Participant 
clearvars;clc;

subject_path=uigetdir('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\','Select Participant');
if isequal(subject_path,0)
    return;
end
inputpath=fullfile(subject_path,'2_TransformData');
outputpath=fullfile(subject_path,'3_Output');
 
numParticipant=erase(subject_path,'C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\');



%% Imorting EMG data
filecatch1=fullfile(inputpath,sprintf('%s_02_EMG.mat',numParticipant));filecatch2=fullfile(inputpath,sprintf('%s_06_EMG.mat',numParticipant));filecatch3=fullfile(inputpath,sprintf('%s_10_EMG.mat',numParticipant));
filecatch4=fullfile(inputpath,sprintf('%s_16_EMG.mat',numParticipant));filecatch5=fullfile(inputpath,sprintf('%s_23_EMG.mat',numParticipant));

catchdata1=load(filecatch1); catchdata2=load(filecatch2);catchdata3=load(filecatch3);
catchdata4=load(filecatch4);catchdata5=load(filecatch5);
catchEMGtxt=catchdata1.DataEMG(1,:);

catchdata1=catchdata1.DataEMG(2:end,1:6);catchdata2=catchdata2.DataEMG(2:end,1:6);catchdata3=catchdata3.DataEMG(2:end,1:6);
catchdata4=catchdata4.DataEMG(2:end,1:6);catchdata5=catchdata5.DataEMG(2:end,1:6);

catchdata1=cell2mat(catchdata1);catchdata2=cell2mat(catchdata2);catchdata3=cell2mat(catchdata3);
catchdata4=cell2mat(catchdata4);catchdata5=cell2mat(catchdata5);

% Find acceleration of TA EMG for Heel strike  
Acc1=catchdata1(:,5:6);Acc2=catchdata2(:,5:6);Acc3=catchdata3(:,5:6);
Acc4=catchdata4(:,5:6);Acc5=catchdata5(:,5:6);

% Filtrate and move mean of the EMG 
catchdata1=FiltrateEMG(catchdata1,4,20,450);catchdata2=FiltrateEMG(catchdata2,4,20,450);catchdata3=FiltrateEMG(catchdata3,4,20,450);
catchdata4=FiltrateEMG(catchdata4,4,20,450);catchdata5=FiltrateEMG(catchdata5,4,20,450);

catchdata1=movmean(abs(catchdata1-mean(catchdata1,1)),100);catchdata2=movmean(abs(catchdata2-mean(catchdata2,1)),100);catchdata3=movmean(abs(catchdata3-mean(catchdata3,1)),100);
catchdata4=movmean(abs(catchdata4-mean(catchdata4,1)),100);catchdata5=movmean(abs(catchdata5-mean(catchdata5,1)),100);

%% Imorting EMG data
filecatch1=fullfile(inputpath,sprintf('%s_02_MM.mat',numParticipant));filecatch2=fullfile(inputpath,sprintf('%s_06_MM.mat',numParticipant));filecatch3=fullfile(inputpath,sprintf('%s_10_MM.mat',numParticipant));
filecatch4=fullfile(inputpath,sprintf('%s_16_MM.mat',numParticipant));filecatch5=fullfile(inputpath,sprintf('%s_23_MM.mat',numParticipant));

[Head_position1,Heels_position1,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(filecatch1);

[Head_position2,Heels_position2,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(filecatch2);

[Head_position3,Heels_position3,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(filecatch3);

[Head_position4,Heels_position4,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(filecatch4);

[Head_position5,Heels_position,Body_position5,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(filecatch5);
%% Importing Quest data 
FilePath1=fullfile(inputpath,sprintf('%s_02_Quest.mat',numParticipant));FilePath2=fullfile(inputpath,sprintf('%s_06_Quest.mat',numParticipant));FilePath3=fullfile(inputpath,sprintf('%s_10_Quest.mat',numParticipant));
FilePath4=fullfile(inputpath,sprintf('%s_16_Quest.mat',numParticipant));FilePath5=fullfile(inputpath,sprintf('%s_23_Quest.mat',numParticipant));

[HMD_position1, HMD_rotation1, Agent_position1]=GetQuestVariables_older(FilePath1,Head_position1);
[HMD_position2, HMD_rotation2, Agent_position2]=GetQuestVariables_older(FilePath2,Head_position2);
[HMD_position3, HMD_rotation3, Agent_position3]=GetQuestVariables_older(FilePath3,Head_position3);
[HMD_position4, HMD_rotation4, Agent_position4]=GetQuestVariables_older(FilePath4,Head_position4);
[HMD_position5, HMD_rotation5, Agent_position5]=GetQuestVariables_older(FilePath5,Head_position5);

%% Find heel strike 
[~,HeelStrikeL1]=findpeaks(Acc1(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL2]=findpeaks(Acc2(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL3]=findpeaks(Acc3(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL4]=findpeaks(Acc4(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeL5]=findpeaks(Acc5(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelStrikeR1]=findpeaks(Acc1(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeR2]=findpeaks(Acc2(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeR3]=findpeaks(Acc3(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeR4]=findpeaks(Acc4(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelStrikeR5]=findpeaks(Acc5(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);

%% Question to confirm that the cycle are cut correctly
figure
findpeaks(Acc1(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);
hold on 
findpeaks(Acc1(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);
fig = gcf;
fig.Position=[1,1,1440,500];
answer = questdlg('Do you want to continu ?', 'Continu the script', 'Yes', 'Find For left(BLue)','Find For Right(Orange)','Yes');
switch answer 
    case 'Yes'
        close (fig)
    case 'Find For left(BLue)'
        [x,y]=ginput(2);
        [value,Row]=max(cell2mat(DataEMG(round(x(1,1)):round(x(2,1)),end)));
        HeelLFrames=[HeelLFrames;round(x(1,1))+Row];
        HeelLFrames=sort(HeelLFrames);
        close (fig)
    case 'Find For Right(Orange)'
        [x,y]=ginput(2);
        [Row,value]=max(cell2mat(DataEMG(round(x(1,1)):round(x(2,1)),12)));
        HeelRFrames=[HeelRFrames;round(x(1,1))+Row];
        HeelRFrames=sort(HeelRFrames);
        close (fig)
end
                
%% Resampling Cycle 1   
Timeline=(1:1:size(catchdata1(HeelStrikeL1(2,1):HeelStrikeL1(3,1),:)));
Cycle1_Data1=resample(catchdata1(HeelStrikeL1(2,1):HeelStrikeL1(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata2(HeelStrikeL2(2,1):HeelStrikeL2(3,1),:)));
Cycle1_Data2=resample(catchdata2(HeelStrikeL2(2,1):HeelStrikeL2(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata3(HeelStrikeL3(2,1):HeelStrikeL3(3,1),:)));
Cycle1_Data3=resample(catchdata3(HeelStrikeL3(2,1):HeelStrikeL3(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata4(HeelStrikeL4(2,1):HeelStrikeL4(3,1),:)));
Cycle1_Data4=resample(catchdata4(HeelStrikeL4(2,1):HeelStrikeL4(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata5(HeelStrikeL5(2,1):HeelStrikeL5(3,1),:)));
Cycle1_Data5=resample(catchdata5(HeelStrikeL5(2,1):HeelStrikeL5(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling Cycle 2
Timeline=(1:1:size(catchdata1(HeelStrikeL1(3,1):HeelStrikeL1(4,1),:)));
Cycle2_Data1=resample(catchdata1(HeelStrikeL1(3,1):HeelStrikeL1(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata2(HeelStrikeL2(3,1):HeelStrikeL2(4,1),:)));
Cycle2_Data2=resample(catchdata2(HeelStrikeL2(3,1):HeelStrikeL2(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata3(HeelStrikeL3(3,1):HeelStrikeL3(4,1),:)));
Cycle2_Data3=resample(catchdata3(HeelStrikeL3(3,1):HeelStrikeL3(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata4(HeelStrikeL4(3,1):HeelStrikeL4(4,1),:)));
Cycle2_Data4=resample(catchdata4(HeelStrikeL4(3,1):HeelStrikeL4(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata5(HeelStrikeL5(3,1):HeelStrikeL5(4,1),:)));
Cycle2_Data5=resample(catchdata5(HeelStrikeL5(3,1):HeelStrikeL5(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling Cycle 3
Timeline=(1:1:size(catchdata1(HeelStrikeL1(4,1):HeelStrikeL1(5,1),:)));
Cycle3_Data1=resample(catchdata1(HeelStrikeL1(4,1):HeelStrikeL1(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata2(HeelStrikeL2(4,1):HeelStrikeL2(5,1),:)));
Cycle3_Data2=resample(catchdata2(HeelStrikeL2(4,1):HeelStrikeL2(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata3(HeelStrikeL3(4,1):HeelStrikeL3(5,1),:)));
Cycle3_Data3=resample(catchdata3(HeelStrikeL3(4,1):HeelStrikeL3(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata4(HeelStrikeL4(4,1):HeelStrikeL4(5,1),:)));
Cycle3_Data4=resample(catchdata4(HeelStrikeL4(4,1):HeelStrikeL4(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
Timeline=(1:1:size(catchdata5(HeelStrikeL5(4,1):HeelStrikeL5(5,1),:)));
Cycle3_Data5=resample(catchdata5(HeelStrikeL5(4,1):HeelStrikeL5(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Cycles 
Cycles1=[Cycle1_Data1;Cycle2_Data1;Cycle3_Data1]; 
Cycles2=[Cycle1_Data2;Cycle2_Data2;Cycle3_Data2]; 
Cycles3=[Cycle1_Data3;Cycle2_Data3;Cycle3_Data3]; 
Cycles4=[Cycle1_Data4;Cycle2_Data4;Cycle3_Data4]; 
Cycles5=[Cycle1_Data5;Cycle2_Data5;Cycle3_Data5]; 


%% Extract Max Amplitude 
MaxEMG=(max([Cycle1_Data1;Cycle2_Data1;Cycle3_Data1])+max([Cycle1_Data2;Cycle2_Data2;Cycle3_Data2])+max([Cycle1_Data3;Cycle2_Data3;Cycle3_Data3])+max([Cycle1_Data4;Cycle2_Data4;Cycle3_Data4])++max([Cycle1_Data5;Cycle2_Data5;Cycle3_Data5]))/5;

%% Co-contraction Strait

t = linspace(0, 1, 3000);
CoCo1=[2*(trapz(t,min(Cycles1(:,1),Cycles1(:,3)))/(trapz(t,Cycles1(:,1))+trapz(t,Cycles1(:,3))))*100,   2*(trapz(t,min(Cycles1(:,2),Cycles1(:,4)))/(trapz(t,Cycles1(:,2))+trapz(t,Cycles1(:,4))))*100];
CoCo2=[2*(trapz(t,min(Cycles2(:,1),Cycles2(:,3)))/(trapz(t,Cycles2(:,1))+trapz(t,Cycles2(:,3))))*100,   2*(trapz(t,min(Cycles2(:,2),Cycles2(:,4)))/(trapz(t,Cycles2(:,2))+trapz(t,Cycles2(:,4))))*100];
CoCo3=[2*(trapz(t,min(Cycles3(:,1),Cycles3(:,3)))/(trapz(t,Cycles3(:,1))+trapz(t,Cycles3(:,3))))*100,   2*(trapz(t,min(Cycles3(:,2),Cycles3(:,4)))/(trapz(t,Cycles3(:,2))+trapz(t,Cycles3(:,4))))*100];
CoCo4=[2*(trapz(t,min(Cycles4(:,1),Cycles4(:,3)))/(trapz(t,Cycles4(:,1))+trapz(t,Cycles4(:,3))))*100,   2*(trapz(t,min(Cycles4(:,2),Cycles4(:,4)))/(trapz(t,Cycles4(:,2))+trapz(t,Cycles4(:,4))))*100];
CoCo5=[2*(trapz(t,min(Cycles5(:,1),Cycles5(:,3)))/(trapz(t,Cycles5(:,1))+trapz(t,Cycles5(:,3))))*100,   2*(trapz(t,min(Cycles5(:,2),Cycles5(:,4)))/(trapz(t,Cycles5(:,2))+trapz(t,Cycles5(:,4))))*100];

nameCoco=['Right','Left']; 
AllCoco=[CoCo1;CoCo2;CoCo3;CoCo4;CoCo5]; 

%% Extract head Trajectory 
% Heel strike for Vicon 
HeelStrike_ViconL1=round((HeelStrikeL1/2250)*90);
HeelStrike_ViconL2=round((HeelStrikeL2/2250)*90);
HeelStrike_ViconL3=round((HeelStrikeL3/2250)*90);
HeelStrike_ViconL4=round((HeelStrikeL4/2250)*90);
HeelStrike_ViconL5=round((HeelStrikeL5/2250)*90);
%Cut in order to have the same lenght
Straitdata1=Head_position1(HeelStrike_ViconL1(1,1):HeelStrike_ViconL1(5,1),:);Straitdata2=Head_position2(HeelStrike_ViconL2(1,1):HeelStrike_ViconL2(5,1),:);Straitdata3=Head_position3(HeelStrike_ViconL3(1,1):HeelStrike_ViconL3(5,1),:);Straitdata4=Head_position4(HeelStrike_ViconL4(1,1):HeelStrike_ViconL4(5,1),:);Straitdata5=Head_position5(HeelStrike_ViconL5(1,1):HeelStrike_ViconL5(5,1),:);
[s1 i1]=size(Straitdata1);[s2 i2]=size(Straitdata2);[s3 i3]=size(Straitdata3);[s4 i4]=size(Straitdata4);[s5 i5]=size(Straitdata5);
coupure=[s1,s2,s3,s4,s5];coupure=min(coupure);
Straitdata1=Straitdata1(1:coupure,:);Straitdata2=Straitdata2(1:coupure,:);Straitdata3=Straitdata3(1:coupure,:);Straitdata4=Straitdata4(1:coupure,:);Straitdata5=Straitdata5(1:coupure,:);

%Combine 
XHeadStrait=[Straitdata1(:,1),Straitdata2(:,1),Straitdata3(:,1),Straitdata4(:,1),Straitdata5(:,1)];
YHeadStrait=[Straitdata1(:,2),Straitdata2(:,2),Straitdata3(:,2),Straitdata4(:,2),Straitdata5(:,2)];

%% Position 
%for X
meanXHead=mean(XHeadStrait')';
sdXHead=abs(std(XHeadStrait')');
sdXHead_up=meanXHead+2*sdXHead;sdXHead_low=meanXHead-2*sdXHead;
%for Y
meanYHead=mean(YHeadStrait')';
sdYHead=abs(std(YHeadStrait')');
sdYHead_up=meanYHead+2*sdYHead;sdYHead_low=meanYHead-2*sdYHead;

%% Velocity 
XHead_Vel_Strait=derivate(XHeadStrait,1/90.023);
meanHead_Vel_X=mean(XHead_Vel_Strait')';
sdXHead_Vel=(std(XHead_Vel_Strait')');sdXHead_Vel=abs(sdXHead_Vel);
sdXHead_Vel_up=meanHead_Vel_X+2*sdXHead_Vel;sdXHead_Vel_low=meanHead_Vel_X-2*sdXHead_Vel;
YHead_Vel_Strait=derivate(YHeadStrait,1/90.023);
meanHead_Vel_Y=mean(YHead_Vel_Strait')';
sdYHead_Vel=abs(std(YHead_Vel_Strait')');
sdYHead_Vel_up=meanHead_Vel_Y+2*sdYHead_Vel;sdYHead_Vel_low=meanHead_Vel_Y-2*sdYHead_Vel;

%% Save strait data
StraitDATA.MaxEMG=MaxEMG; 
StraitDATA.XStrait=XHeadStrait; 
StraitDATA.YStrait=YHeadStrait; 
StraitDATA.Coco=AllCoco; 
StraitDATA.CocoNames=nameCoco;
StraitDATA.meanXHead=meanXHead;
StraitDATA.meanYHead=meanYHead;
StraitDATA.sdXHead_up=sdXHead_up;
StraitDATA.sdYHead_up=sdYHead_up;
StraitDATA.sdXHead_low=sdXHead_low;
StraitDATA.sdYHead_low=sdYHead_low;
StraitDATA.meanXHead_Vel=meanHead_Vel_X;
StraitDATA.meanYHead_Vel=meanHead_Vel_Y;
StraitDATA.sdXHead_up_Vel=sdXHead_Vel_up;
StraitDATA.sdYHead_up_Vel=sdYHead_Vel_up;
StraitDATA.sdXHead_low_Vel=sdXHead_Vel_low;
StraitDATA.sdYHead_low_Vel=sdYHead_Vel_low;
StraitDATA.HeelStrikeL1=HeelStrikeL1;
StraitDATA.HeelStrikeL2=HeelStrikeL2;
StraitDATA.HeelStrikeL3=HeelStrikeL3;
StraitDATA.HeelStrikeL4=HeelStrikeL4;
StraitDATA.HeelStrikeL5=HeelStrikeL5;

FilepathName=fullfile(outputpath,'StraitData.mat');
save(FilepathName,'StraitDATA')