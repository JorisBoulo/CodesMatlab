function [Clearance,Side_Circumvention]=FindClearance(Probed_point_contact,Avatar_Position,HMD_position,Head_position,AvatarGender )

Offset=HMD_position-Head_position; 
% With Left Shoulder :
ClearOffsetLShoulder=Probed_point_contact(:,1:2)-Offset(:,1:2);
if strcmp(char(AvatarGender),'m')==1
    ClearOffsetLShoulder(:,1)=ClearOffsetLShoulder(:,1)-0.18;
else
    ClearOffsetLShoulder(:,1)=ClearOffsetLShoulder(:,1)-0.14;
end
ClearanceLeftS=min(sqrt(sum((ClearOffsetLShoulder(:,1:2)-Avatar_Position(:,1:2)).^2, 2)));

% With Right Shoulder :
ClearOffsetRShoulder=Probed_point_contact(:,3:4)-Offset(:,1:2);
if strcmp(char(AvatarGender),'m')==1
    ClearOffsetRShoulder(:,1)=ClearOffsetRShoulder(:,1)-0.18;
else
    ClearOffsetRShoulder(:,1)=ClearOffsetRShoulder(:,1)-0.14;
end
ClearanceRightS=min(sqrt(sum((ClearOffsetRShoulder(:,1:2)-Avatar_Position(:,1:2)).^2, 2)));

if ClearanceLeftS<ClearanceRightS
    Clearance=ClearanceLeftS;
    Side_Circumvention='Right'; 
else 
    Clearance=ClearanceRightS;
    Side_Circumvention='Left'; 
end