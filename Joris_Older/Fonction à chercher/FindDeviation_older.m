function [dev,frame_dev1, frame_dev2,Scalar_Velocity] = FindDeviation_older(Head_position,Head_Vel,Agent_position,frame_at_crossing,subject_path,DataEMG,Side_Circumvention)

%% Deviation points identification
close all
addpath(subject_path)
load('StraitData.mat')
n=numel(Head_position(:,1));zero=zeros(n,1);
[~,HeelLFramesEMG]=findpeaks(DataEMG(2:end,5),'MinPeakHeight',-3,'MinPeakDistance',1700);
[~,HeelRFramesEMG]=findpeaks(DataEMG(2:end,6),'MinPeakHeight',-3,'MinPeakDistance',1700);
HeelLFrames=round((HeelLFramesEMG/2250)*90);
HeelRFrames=round((HeelRFramesEMG/2250)*90);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     For the Right side      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isequal(Side_Circumvention,'Right') 

%% Find Dev1  
plot(Head_position(:,2),Head_position(:,1))
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up,'--') %% Il faut faire en sorte que ça commence à Heelframe1 pour la position et la vitesse de devition
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low,'--')
plot(Head_position(:,2),zero,':r')
[x, ~]=ginput(2);
b1=find(Head_position(:,2)>x(2),1);
b2=find(Head_position(:,2)<x(1));b2=b2(end);
cut_Head_position=Head_position(b1:b2,:);  
[Head_min, frame_dev1]=min(cut_Head_position(:,1));
frame_dev1=frame_dev1+b1;
plot(Head_position(frame_dev1,2),Head_position(frame_dev1,1),'*');
graph1=figure;

%% Find Dev 2

graph2=figure;
plot(Head_position(:,2),Head_Vel(:,1))
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up_Vel,'--')
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low_Vel,'--')
plot(Head_position(:,2),zero,':r')
[x, ~]=ginput(2);%Identify the window within you want the program to search the ML velocity
b1=find(Head_position(:,2)>x(2),1);
b2=find(Head_position(:,2)<x(1));b2=b2(end);
cut_Head_Vel=Head_Vel(b1:b2,:);
Max_vel=max(Head_Vel(:,1));
[~,B]=min(cut_Head_Vel(:,1)); 
A=cut_Head_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(Head_position(:,2)>x(2),1);
 b2=find(Head_position(:,2)<x(1));b2=b2(end);
cut_Head_Vel=Head_Vel(b1:b2,1);
 B=find(cut_Head_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all


frame_dev1=frame_dev1;
frame_dev2=frame_dev2;
%% Deviation point calculated relatived to the avatar position

distance_dev1_avatar=Agent_position(frame_dev1,2)-Head_position(frame_dev1,2);
distance_dev2_avatar=Agent_position(frame_dev2,2)-Head_position(frame_dev2,2);
Time_dev1_passing=(frame_at_crossing-frame_dev1)/90;
Time_dev2_passing=(frame_at_crossing-frame_dev2)/90;
frameEXP_dev1=frame_dev1-1; %to get the real frame of EXP file that start to 0
frameEXP_dev2=frame_dev2-1; %to get the real frame of EXP file that start to 0

%% Complementary variables: Maximal velocity 


dev1_Head_X_vel=Head_Vel(frame_dev1:frame_dev2,1);
dev2_Head_X_vel=Head_Vel(frame_dev2:frame_at_crossing,1);

%absolute value to help to compare R and L circumv trials
vel_at_onset_X_dev1=abs(Head_Vel(frame_dev1,1));vel_at_onset_X_dev2=abs(Head_Vel(frame_dev2,1));
rate_of_ML_avoidance=abs(mean(Head_Vel(frame_dev1:frame_at_crossing,1))); 

max_dev1_X_vel=max(dev1_Head_X_vel);mean_dev1_X_vel=mean(dev1_Head_X_vel);
max_dev2_X_vel=max(dev2_Head_X_vel);mean_dev2_X_vel=mean(dev2_Head_X_vel);
min_dev1_X_vel=min(dev1_Head_X_vel);mean_dev1_X_vel=mean(dev1_Head_X_vel);
min_dev2_X_vel=min(dev2_Head_X_vel);mean_dev2_X_vel=mean(dev2_Head_X_vel);



%% Velocity calcul 
Scalar_Velocity=sqrt((Head_Vel(:,1)).^2+(Head_Vel(:,2)).^2+(Head_Vel(:,3)).^2);
mean_scalar_vel_dev1_crossing=mean(Scalar_Velocity(frame_dev1:frame_at_crossing));
mean_scalar_vel_dev1_dev2=mean(Scalar_Velocity(frame_dev1:frame_dev2));
mean_scalar_vel_dev2_crossing=mean(Scalar_Velocity(frame_dev2:frame_at_crossing));

%velocity 1.5m (pour enlever la phase d'acclration) to crossing
frame_start=find(Head_position(:,2)>=-3.3,1);
mean_scalar_vel_one_p_five_cros= mean(Scalar_Velocity(frame_start:frame_at_crossing));
max_scalar_vel_one_p_five_cros= max(Scalar_Velocity(frame_start:frame_at_crossing));
min_scalar_vel_one_p_five_cros= min(Scalar_Velocity(frame_start:frame_at_crossing));

%% Range of deviation

Max_dev=max(Head_position(frame_dev1:frame_at_crossing))-min(Head_position(frame_dev1:frame_at_crossing));


%% Exporter les variables
dev={'FrameEXP_dev1','Dev1_longitudinal_axis','Distance_dev1_avatar','Dev1_onset_VelocityX',...
    'Max_dev1_X_vel','Min_dev1_X_vel','Mean_dev1_X_vel','Time_dev1_passing','FrameEXP_dev2','Dev2_axe_longitudinal',...
    'Distance_dev2_avatar','Dev2_onset_VelocityX','Max_dev2_X_vel','Min_dev2_X_vel','Mean_dev2_X_vel','Time_dev2_passing',...
    'Rate_ML_avoidance','mean_scalar_vel_dev1_crossing','mean_scalar_vel_dev1_dev2','mean_scalar_vel_dev2_crossing','frame_dev1','frame_dev2'...
    'mean_scalar_vel_1.5_crossing','max_scalar_vel_1.5_crossing','min_scalar_vel_1.5_crossing','Max_dev(range)',;...
    frameEXP_dev1,Head_position(frame_dev1,2),distance_dev1_avatar,vel_at_onset_X_dev1,max_dev1_X_vel,min_dev1_X_vel,...
    mean_dev1_X_vel,Time_dev1_passing,frameEXP_dev2,Head_position(frame_dev2,2),distance_dev2_avatar,...
    vel_at_onset_X_dev2,max_dev2_X_vel,min_dev2_X_vel,mean_dev2_X_vel,Time_dev2_passing,rate_of_ML_avoidance,...
    mean_scalar_vel_dev1_crossing,mean_scalar_vel_dev1_dev2,mean_scalar_vel_dev2_crossing,frame_dev1,frame_dev2,...
    mean_scalar_vel_one_p_five_cros,max_scalar_vel_one_p_five_cros,min_scalar_vel_one_p_five_cros,Max_dev};
%% Montrer le graph pour verifier 
graph_dev=figure;
subplot(2,1,1)
plot(Head_position(:,2),Head_position(:,1))
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up,'--')
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low,'--')
plot(Head_position(:,2),zero,':r')
plot(Head_position(frame_dev1,2),Head_position(frame_dev1,1),'*')
plot(Head_position(frame_dev2,2),Head_position(frame_dev2,1),'o')
title('Head Position')
hold off
subplot(2,1,2)
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up_Vel,'--')
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low_Vel,'--')
plot(Head_position(:,2),Head_Vel(:,1))
plot(Head_position(:,2),zero,':r')
plot(Head_position(frame_dev1,2),Head_Vel(frame_dev1,1),'*')
plot(Head_position(frame_dev2,2),Head_Vel(frame_dev2,1),'o')
title('Head Velocity')
hold off



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     For the Left side      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    inverse_Head_position=[Head_position(:,1)*-1,Head_position(:,2:3)];
    inverse_Head_Vel=[Head_Vel(:,1)*-1,Head_Vel(:,2:3)];
%% Find Dev1  
plot(inverse_Head_position(:,2),inverse_Head_position(:,1))
hold on
plot(-StraitDATA.meanYHead,-StraitDATA.sdXHead_up,'--') %% Il faut faire en sorte que ça commence à Heelframe1 pour la position et la vitesse de devition
plot(-StraitDATA.meanYHead,-StraitDATA.sdXHead_low,'--')
plot(Head_position(:,2),zero,':r')
[x, ~]=ginput(2);
b1=find(inverse_Head_position(:,2)>x(2),1);
b2=find(inverse_Head_position(:,2)<x(1));b2=b2(end);
cut_Head_position=inverse_Head_position(b1:b2,:);  
[Head_min, frame_dev1]=min(cut_Head_position(:,1));
frame_dev1=frame_dev1+b1;
plot(inverse_Head_position(frame_dev1,2),inverse_Head_position(frame_dev1,1),'*');
graph1=figure;

%% Find Dev 2

graph2=figure;
plot(inverse_Head_position(:,2),inverse_Head_Vel(:,1))
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up_Vel,'--')
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low_Vel,'--')
plot(Head_position(:,2),zero,':r')
[x, ~]=ginput(2);%Identify the window within you want the program to search the ML velocity
b1=find(inverse_Head_position(:,2)>x(2),1);
b2=find(inverse_Head_position(:,2)<x(1));b2=b2(end);
cut_Head_Vel=inverse_Head_Vel(b1:b2,:);
Max_vel=max(inverse_Head_Vel(:,1));
[~,B]=max(cut_Head_Vel(:,1)); 
frame_dev2=(b1+B);
plot(inverse_Head_position(frame_dev2,2),inverse_Head_position(frame_dev2,1),'o')
close all


frame_dev1=frame_dev1;
frame_dev2=frame_dev2;
%% Deviation point calculated relatived to the avatar position

distance_dev1_avatar=Agent_position(frame_dev1,2)-Head_position(frame_dev1,2);
distance_dev2_avatar=Agent_position(frame_dev2,2)-Head_position(frame_dev2,2);
Time_dev1_passing=(frame_at_crossing-frame_dev1)/90;
Time_dev2_passing=(frame_at_crossing-frame_dev2)/90;
frameEXP_dev1=frame_dev1-1; %to get the real frame of EXP file that start to 0
frameEXP_dev2=frame_dev2-1; %to get the real frame of EXP file that start to 0

%% Complementary variables: Maximal velocity 


dev1_Head_X_vel=Head_Vel(frame_dev1:frame_dev2,1);
dev2_Head_X_vel=Head_Vel(frame_dev2:frame_at_crossing,1);

%absolute value to help to compare R and L circumv trials
vel_at_onset_X_dev1=abs(Head_Vel(frame_dev1,1));vel_at_onset_X_dev2=abs(Head_Vel(frame_dev2,1));
rate_of_ML_avoidance=abs(mean(Head_Vel(frame_dev1:frame_at_crossing,1))); 

max_dev1_X_vel=min(dev1_Head_X_vel);mean_dev1_X_vel=mean(dev1_Head_X_vel);
max_dev2_X_vel=min(dev2_Head_X_vel);mean_dev2_X_vel=mean(dev2_Head_X_vel);
min_dev1_X_vel=max(dev1_Head_X_vel);mean_dev1_X_vel=mean(dev1_Head_X_vel);
min_dev2_X_vel=max(dev2_Head_X_vel);mean_dev2_X_vel=mean(dev2_Head_X_vel);



%% Velocity calcul 
Scalar_Velocity=sqrt((Head_Vel(:,1)).^2+(Head_Vel(:,2)).^2+(Head_Vel(:,3)).^2);
mean_scalar_vel_dev1_crossing=mean(Scalar_Velocity(frame_dev1:frame_at_crossing));
mean_scalar_vel_dev1_dev2=mean(Scalar_Velocity(frame_dev1:frame_dev2));
mean_scalar_vel_dev2_crossing=mean(Scalar_Velocity(frame_dev2:frame_at_crossing));

%velocity 1.5m (pour enlever la phase d'acclration) to crossing
frame_start=find(Head_position(:,2)>=-3.3,1);
mean_scalar_vel_one_p_five_cros= mean(Scalar_Velocity(frame_start:frame_at_crossing));
max_scalar_vel_one_p_five_cros= min(Scalar_Velocity(frame_start:frame_at_crossing));
min_scalar_vel_one_p_five_cros= max(Scalar_Velocity(frame_start:frame_at_crossing));

%% Range of deviation

Max_dev=min(Head_position(frame_dev1:frame_at_crossing))-max(Head_position(frame_dev1:frame_at_crossing));


%% Exporter les variables
dev={'FrameEXP_dev1','Dev1_longitudinal_axis','Distance_dev1_avatar','Dev1_onset_VelocityX',...
    'Max_dev1_X_vel','Min_dev1_X_vel','Mean_dev1_X_vel','Time_dev1_passing','FrameEXP_dev2','Dev2_axe_longitudinal',...
    'Distance_dev2_avatar','Dev2_onset_VelocityX','Max_dev2_X_vel','Min_dev2_X_vel','Mean_dev2_X_vel','Time_dev2_passing',...
    'Rate_ML_avoidance','mean_scalar_vel_dev1_crossing','mean_scalar_vel_dev1_dev2','mean_scalar_vel_dev2_crossing','frame_dev1','frame_dev2'...
    'mean_scalar_vel_1.5_crossing','max_scalar_vel_1.5_crossing','min_scalar_vel_1.5_crossing','Max_dev(range)',;...
    frameEXP_dev1,Head_position(frame_dev1,2),distance_dev1_avatar,vel_at_onset_X_dev1,max_dev1_X_vel,min_dev1_X_vel,...
    mean_dev1_X_vel,Time_dev1_passing,frameEXP_dev2,Head_position(frame_dev2,2),distance_dev2_avatar,...
    vel_at_onset_X_dev2,max_dev2_X_vel,min_dev2_X_vel,mean_dev2_X_vel,Time_dev2_passing,rate_of_ML_avoidance,...
    mean_scalar_vel_dev1_crossing,mean_scalar_vel_dev1_dev2,mean_scalar_vel_dev2_crossing,frame_dev1,frame_dev2,...
    mean_scalar_vel_one_p_five_cros,max_scalar_vel_one_p_five_cros,min_scalar_vel_one_p_five_cros,Max_dev};
%% Montrer le graph pour verifier 
graph_dev=figure;
subplot(2,1,1)
plot(Head_position(:,2),Head_position(:,1))
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up,'--')
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low,'--')
plot(Head_position(:,2),zero,':r')
plot(Head_position(frame_dev1,2),Head_position(frame_dev1,1),'*')
plot(Head_position(frame_dev2,2),Head_position(frame_dev2,1),'o')
title('Head Position')
hold off
subplot(2,1,2)
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_up_Vel,'--')
hold on
plot(StraitDATA.meanYHead,StraitDATA.sdXHead_low_Vel,'--')
plot(Head_position(:,2),Head_Vel(:,1))
plot(Head_position(:,2),zero,':r')
plot(Head_position(frame_dev1,2),Head_Vel(frame_dev1,1),'*')
plot(Head_position(frame_dev2,2),Head_Vel(frame_dev2,1),'o')
title('Head Velocity')
hold off
end

