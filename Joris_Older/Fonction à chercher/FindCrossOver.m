function [CrossOver]=FindCrossOver(file_EXP,frame_dev2,CrossingFrame,direction)

load(file_EXP);
%%
[NameRow, colInd] = find(strcmp(MMData(:,1), 'Frame #'));
[~,column_MMData]=size(MMData);
Title=MMData(NameRow,:);
MMData=str2double(string(MMData(NameRow+1:end,:)));
for i=1:column_MMData
    %variables pour connaitre le cycle de marche 
    if ~isempty(strfind(Title{1,i},'R_Heel_X_C'))
        R_Heel_X_C=MMData(:,i);
    elseif ~isempty(strfind(Title{1,i},'L_Heel_X_C'))
        L_Heel_X_C=MMData(:,i);
    end
end

% Ensure frame_dev2 and CrossingFrame are integers
frame_dev2 = round(frame_dev2);
CrossingFrame = round(CrossingFrame);
if direction==1
    CrossOver = max(L_Heel_X_C(frame_dev2:CrossingFrame,:) > R_Heel_X_C(frame_dev2:CrossingFrame,:));
else
    CrossOver = max(L_Heel_X_C(frame_dev2:CrossingFrame,:) < R_Heel_X_C(frame_dev2:CrossingFrame,:));
end

