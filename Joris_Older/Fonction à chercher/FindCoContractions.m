function [CoCoDEV,CoCoBeforeDEV]=FindCoContractions(DataEMG,frame_dev1,CrossingFrame,subject_path)

addpath(subject_path)
load('StraitData.mat')

%% Emg filtration and movemean 

Filtrated=FiltrateEMG(DataEMG,4,20,450);
Movemean=movmean(abs(Filtrated-mean(Filtrated,1)),100);

%% Heelstrike detection 

Acc=DataEMG(:,5:6);

[~,HeelStrikeL]=findpeaks(Acc(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelStrikeR]=findpeaks(Acc(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);

%% Resampling 

% Resampling Left Cycle 1   
Timeline=(1:1:size(Movemean(HeelStrikeL(2,1):HeelStrikeL(3,1),:)));
CycleL1=resample(Movemean(HeelStrikeL(2,1):HeelStrikeL(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling Left Cycle 2
Timeline=(1:1:size(Movemean(HeelStrikeL(3,1):HeelStrikeL(4,1),:)));
CycleL2=resample(Movemean(HeelStrikeL(3,1):HeelStrikeL(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CycleL1;CycleL2];

if size(HeelStrikeL,1)>4
%Resampling Left Cycle 3
Timeline=(1:1:size(Movemean(HeelStrikeL(4,1):HeelStrikeL(5,1),:)));
CycleL3=resample(Movemean(HeelStrikeL(4,1):HeelStrikeL(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CycleL1;CycleL2;CycleL3];
end

if size(HeelStrikeL,1)>5
%Resampling Left Cycle 4
Timeline=(1:1:size(Movemean(HeelStrikeL(5,1):HeelStrikeL(6,1),:)));
CycleL4=resample(Movemean(HeelStrikeL(5,1):HeelStrikeL(6,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CyclesL;CycleL4];
end


% Resampling right Cycle 1   
Timeline=(1:1:size(Movemean(HeelStrikeR(2,1):HeelStrikeR(3,1),:)));
CycleR1=resample(Movemean(HeelStrikeR(2,1):HeelStrikeR(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling right Cycle 2
Timeline=(1:1:size(Movemean(HeelStrikeR(3,1):HeelStrikeR(4,1),:)));
CycleR2=resample(Movemean(HeelStrikeR(3,1):HeelStrikeR(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CycleR1;CycleR2];

if size(HeelStrikeR,1)>4
%Resampling right Cycle 3
Timeline=(1:1:size(Movemean(HeelStrikeR(4,1):HeelStrikeR(5,1),:)));
CycleR3=resample(Movemean(HeelStrikeR(4,1):HeelStrikeR(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CycleR1;CycleR2;CycleR3];
end

if size(HeelStrikeR,1)>5
%Resampling right Cycle 4
Timeline=(1:1:size(Movemean(HeelStrikeR(5,1):HeelStrikeR(6,1),:)));
CycleR4=resample(Movemean(HeelStrikeR(5,1):HeelStrikeR(6,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CyclesR;CycleR4];
end

%% Find DEV and Cross step
frame_dev1_EMG=(frame_dev1*2250)/90;
CrossingFrame_EMG=(CrossingFrame*2250)/90;

indexDev_L = find(HeelStrikeL > frame_dev1_EMG, 1);
SelectStrike_Dev_L= HeelStrikeL(indexDev_L); 
indexDev_R = find(HeelStrikeR > frame_dev1_EMG, 1);
SelectStrike_Dev_R= HeelStrikeR(indexDev_R); 

StepDev=SelectStrike_Dev_R>SelectStrike_Dev_L; 

indexCross_L = find(HeelStrikeL > CrossingFrame_EMG, 1);
SelectStrike_Cross_Dev_L= HeelStrikeL(indexCross_L); 
indexCross_R = find(HeelStrikeR > CrossingFrame_EMG, 1);
SelectStrike_Cross_R= HeelStrikeR(indexCross_R); 

StepCross=SelectStrike_Cross_R>SelectStrike_Cross_Dev_L;

%% Co-Contraction
try
if StepCross==1 % alors ça veut dire que le pas gauche était le pas juste après le croisement
    t=linspace(0,1,size((indexDev_L-3)*1000:(indexCross_L-2)*1000,2)); 
    CoCoDEV=[2*(trapz(t,min(CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,1),CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,3)))/(trapz(t,CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,1))+trapz(t,CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,3))))*100,...
        2*(trapz(t,min(CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,2),CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,4)))/(trapz(t,CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,2))+trapz(t,CyclesL((indexDev_L-3)*1000:(indexCross_L-2)*1000,4))))*100];
    t=linspace(0,1,size(1:(indexDev_L-3)*1000,2));
    CoCoBeforeDEV=[2*(trapz(t,min(CyclesL(1:(indexDev_L-3)*1000,1),CyclesL(1:(indexDev_L-3)*1000,3)))/(trapz(t,CyclesL(1:(indexDev_L-3)*1000,1))+trapz(t,CyclesL(1:(indexDev_L-3)*1000,3))))*100,...
        2*(trapz(t,min(CyclesL(1:(indexDev_L-3)*1000,2),CyclesL(1:(indexDev_L-3)*1000,4)))/(trapz(t,CyclesL(1:(indexDev_L-3)*1000,2))+trapz(t,CyclesL(1:(indexDev_L-3)*1000,4))))*100];
else 
    t=linspace(0,1,size((indexDev_R-3)*1000:(indexCross_R-2)*1000,2)); 
    CoCoDEV=[2*(trapz(t,min(CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,1),CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,3)))/(trapz(t,CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,1))+trapz(t,CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,3))))*100,...
        2*(trapz(t,min(CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,2),CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,4)))/(trapz(t,CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,2))+trapz(t,CyclesR((indexDev_R-3)*1000:(indexCross_R-2)*1000,4))))*100];
    t=linspace(0,1,size(1:(indexDev_R-3)*1000,2));
    CoCoBeforeDEV=[2*(trapz(t,min(CyclesR(1:(indexDev_R-3)*1000,1),CyclesR(1:(indexDev_R-3)*1000,3)))/(trapz(t,CyclesR(1:(indexDev_R-3)*1000,1))+trapz(t,CyclesR(1:(indexDev_R-3)*1000,3))))*100,...
        2*(trapz(t,min(CyclesR(1:(indexDev_R-3)*1000,2),CyclesR(1:(indexDev_R-3)*1000,4)))/(trapz(t,CyclesR(1:(indexDev_R-3)*1000,2))+trapz(t,CyclesR(1:(indexDev_R-3)*1000,4))))*100];

end 
catch
if StepCross==1 % alors ça veut dire que le pas gauche était le pas juste après le croisement
    t=linspace(0,1,size(frame_dev1_EMG:CrossingFrame_EMG,2)); 
    CoCoDEV=[2*(trapz(t,min(Movemean(frame_dev1_EMG:CrossingFrame_EMG,1),Movemean(frame_dev1_EMG:CrossingFrame_EMG,3)))/(trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,1))+trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,3))))*100,...
        2*(trapz(t,min(Movemean(frame_dev1_EMG:CrossingFrame_EMG,2),Movemean(frame_dev1_EMG:CrossingFrame_EMG,4)))/(trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,2))+trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,4))))*100];
    t=linspace(0,1,size(1:frame_dev1_EMG,2));
    CoCoBeforeDEV=[2*(trapz(t,min(Movemean(1:frame_dev1_EMG,1),Movemean(1:frame_dev1_EMG,3)))/(trapz(t,Movemean(1:frame_dev1_EMG,1))+trapz(t,Movemean(1:frame_dev1_EMG,3))))*100,...
        2*(trapz(t,min(Movemean(1:frame_dev1_EMG,2),Movemean(1:frame_dev1_EMG,4)))/(trapz(t,Movemean(1:frame_dev1_EMG,2))+trapz(t,Movemean(1:frame_dev1_EMG,4))))*100];
else 
    t=linspace(0,1,size(frame_dev1_EMG:CrossingFrame_EMG,2)); 
    CoCoDEV=[2*(trapz(t,min(Movemean(frame_dev1_EMG:CrossingFrame_EMG,1),Movemean(frame_dev1_EMG:CrossingFrame_EMG,3)))/(trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,1))+trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,3))))*100,...
        2*(trapz(t,min(Movemean(frame_dev1_EMG:CrossingFrame_EMG,2),Movemean(frame_dev1_EMG:CrossingFrame_EMG,4)))/(trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,2))+trapz(t,Movemean(frame_dev1_EMG:CrossingFrame_EMG,4))))*100];
    t=linspace(0,1,size(1:frame_dev1_EMG,2));
    CoCoBeforeDEV=[2*(trapz(t,min(Movemean(1:frame_dev1_EMG,1),Movemean(1:frame_dev1_EMG,3)))/(trapz(t,Movemean(1:frame_dev1_EMG,1))+trapz(t,Movemean(1:frame_dev1_EMG,3))))*100,...
        2*(trapz(t,min(Movemean(1:frame_dev1_EMG,2),Movemean(1:frame_dev1_EMG,4)))/(trapz(t,Movemean(1:frame_dev1_EMG,2))+trapz(t,Movemean(1:frame_dev1_EMG,4))))*100];

end
end
