function [HMD_position, HMD_rotation, Agent_position, CrossingFrame, Gaze, cutoff_vicon,OffsetFrame,HMD_position_raw,HMD_position_uncut]=GetQuestVariables_older(file_Quest,Vicon_Head_position)
%This function is used to extract the variables that are collected by the QuestPro in a .csv file 
%via the LogVS application 


%Open Quest csv file
data_quest=load(file_Quest);

%% Extract raw variable
HMD_position=cellfun(@str2num,data_quest.DataQuest(2:end,1),'UniformOutput',false);
HMD_position=cell2mat(HMD_position);
HMD_rotation=cellfun(@str2num,data_quest.DataQuest(2:end,2),'UniformOutput',false);
HMD_rotation=cell2mat(HMD_rotation);
Agent_position=cellfun(@str2num,data_quest.DataQuest(2:end,4),'UniformOutput',false);
Agent_position=cell2mat(Agent_position);
Agent_rotation=cellfun(@str2num,data_quest.DataQuest(2:end,5),'UniformOutput',false);
Agent_rotation=cell2mat(Agent_rotation);
Gaze=data_quest.DataQuest(2:end,10);
%%
OffsetFrame=cell2mat(data_quest.DataQuest(2:end,7));

%% Resampling

frame_time=cell2mat(data_quest.DataQuest(2:end,7));

a=0;
for i=1:size(frame_time,1)
a=[a;a(i,1)+(frame_time(i,1))];
end
TimeQuest=a/1000;% Mettre en secondes 

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
HMDx=(HMD_position(:,2))/100; 
HMDy=(HMD_position(:,1))/100; 
HMDz=(HMD_position(:,3))/100;
%v�rifier les angles de rotation, car Joris ne les a jamais pris. D'un autre c�t�, je ne suis pas suppos� en avoir besoin. 
HMDangx=HMD_rotation(:,1);
HMDangy=-HMD_rotation(:,2);
HMDangz=HMD_rotation(:,3)+90;

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
Agent_position_x=(Agent_position(:,2))/100; 
Agent_position_y=(Agent_position(:,1))/100; 
Agent_position_z=(Agent_position(:,3))/100;

[Resamp_HMDx,TimeRes]=resample(HMDx,TimeQuest(2:end,1),90.023);
[Resamp_HMDy,TimeRes]=resample(HMDy,TimeQuest(2:end,1),90.023);
[Resamp_HMDz,TimeRes]=resample(HMDz,TimeQuest(2:end,1),90.023);

[Resamp_HMD_ang_x,TimeRes]=resample(HMDangx,TimeQuest(2:end,1),90.023);
[Resamp_HMD_ang_y,TimeRes]=resample(HMDangy,TimeQuest(2:end,1),90.023);
[Resamp_HMD_ang_z,TimeRes]=resample(HMDangz,TimeQuest(2:end,1),90.023);

[ResampAgent_x,TimeRes]=resample(Agent_position_x,TimeQuest(2:end,1),90.023);
[ResampAgent_y,TimeRes]=resample(Agent_position_y,TimeQuest(2:end,1),90.023);
[ResampAgent_z,TimeRes]=resample(Agent_position_z,TimeQuest(2:end,1),90.023);

%% Filter les donn�es cin�matiques du casque
[Resamp_HMDx]=Filtrate(Resamp_HMDx,2,9,90);
[Resamp_HMDy]=Filtrate(Resamp_HMDy,2,9,90);
[Resamp_HMDz]=Filtrate(Resamp_HMDz,2,9,90);

[Resamp_HMD_ang_x]=Filtrate(Resamp_HMD_ang_x,2,9,90);
[Resamp_HMD_ang_y]=Filtrate(Resamp_HMD_ang_y,2,9,90);
[Resamp_HMD_ang_z]=Filtrate(Resamp_HMD_ang_z,2,9,90);

[ResampAgent_x]=Filtrate(ResampAgent_x,2,9,90);
[ResampAgent_y]=Filtrate(ResampAgent_y,2,9,90);
[ResampAgent_z]=Filtrate(ResampAgent_z,2,9,90);


%% Reorientation spatiale 
 
%int�grer ici les d�placements spatiaux des donn�es avec de rotate...
[PPxmod,PPymod,PPzmod,VAPxmod,VAPymod,VAPzmod] = trigrotate(Resamp_HMDx,Resamp_HMDy,Resamp_HMDz,ResampAgent_x,ResampAgent_y,ResampAgent_z,-45);
%%
[PPx,PPy,PPz,VAPx,VAPy,VAPz] = trigrotate(HMDx,HMDx,HMDx,Agent_position_x,Agent_position_y,Agent_position_z,-45);
%% Alignement temporel avec les donn�es Vicon
[istart,istop,~]=findsignal(PPymod,Vicon_Head_position(100:end-100,2),'TimeAlignment','fixed','Metric','absolute','MaxNumSegments',1);            
CrossingFrameQuest=min(find(VAPymod<PPymod));
istart=istart-99;
istop=istop+100; 
%% Output variables
HMD_position=[PPxmod PPymod PPzmod];
HMD_position_uncut=[PPxmod PPymod PPzmod];
HMD_position_raw=[PPx PPy PPz];
HMD_position=HMD_position(istart:istop,:);

HMD_rotation=[Resamp_HMD_ang_x, Resamp_HMD_ang_y, Resamp_HMD_ang_z];
HMD_rotation=HMD_rotation(istart:istop,:);

Agent_position=[VAPxmod,VAPymod,VAPzmod];
Agent_position=Agent_position(istart:istop,:);

CrossingFrame=CrossingFrameQuest-istart;
cutoff_vicon=istart;

Gaze=Gaze(istart:istop);

end

