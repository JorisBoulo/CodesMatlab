clearvars;clc;clear all

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\';
Input='\2_TransformData\';
Output='\3_Output\';

Parti={'31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'};
Rep={'04';'07';'09';'11';'14';'15';'17';'18';'21';'24'};

ParticipantData=readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\Variables_Questionnaire_Etude3.xlsx');
AvatarGender=table2cell(ParticipantData(:,15));


for t=26:29
for j=1:10
fullNames_Quest{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_Quest.mat',Parti{t},Rep{j}))));
fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_MM.mat',Parti{t},Rep{j}))));
fullNames_EMG{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_EMG.mat',Parti{t},Rep{j}))));
fullNames_DataTrail{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Output,(sprintf('P%s_%s_DataTrial.mat',Parti{t},Rep{j}))));
if exist(fullNames_DataTrail{j,t}, 'file')==2 
%import data
 file_Quest=fullNames_Quest{j,t};
 file_EXP=fullNames_MM{j,t};
 file_EMG=fullNames_EMG{j,t};

 input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
 output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);
 load(file_EMG)
 titleEMG=DataEMG(1,:);
 DataEMG=cell2mat(DataEMG(2:end,:));

 load (fullNames_DataTrail{j,t})

%% % *Importing variable from motion monitor and Quest files*

%MotionMonitor variables 
[Head_position,Heels_position,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(file_EXP);

%Quest variables
[HMD_position, HMD_rotation, Agent_position, CrossingFrame, Gaze, cutoff_vicon,OffsetFrame,HMD_position_raw,HMD_position_uncut]=GetQuestVariables_older(file_Quest, Head_position);

%% Clearance

[Clearance,Side_Circumvention]=FindClearance(Probed_point_contact, Agent_position,HMD_position,Head_position,AvatarGender(t));

%% Deviation 

frame_dev1=cell2mat(DataTrial.Dev(2,1));
%% Walking speed and Maximal ML speed 
Gait_onset=find(HMD_position(:,2)>HMD_position(1,2)+0.2,1,'first');
SpeedBeforeDev=(sqrt(sum((Body_position(frame_dev1-Gait_onset,1:2)-Body_position(frame_dev1,1:2)).^2, 2))/(Gait_onset*1/90));
SpeedDev=(sqrt(sum((Body_position(frame_dev1,1:2)-Body_position(CrossingFrame,1:2)).^2, 2))/((CrossingFrame-frame_dev1)*1/90)); 

MaxMLspeed_Dev=max(sqrt(sum((Body_position(frame_dev1,1)-Body_position(CrossingFrame,1)).^2, 2))/((CrossingFrame-frame_dev1)*1/90));

%% Acces to cross over 

[CrossOver]=FindCrossOver(file_EXP,frame_dev1,CrossingFrame);

%% Gaze analysis 

[output_gaze,title_gaze]=FindGaze(Gaze,OffsetFrame,cutoff_vicon,frame_dev1,CrossingFrame,HMD_position,HMD_position_raw,HMD_position_uncut); 

%% Co-contraction 

[CoCoDEV,CoCoBeforeDEV]=FindCoContractions(DataEMG,frame_dev1,CrossingFrame,output_folder);

%% Saving the data 
DataTrial.MaxMLspeed_Dev=MaxMLspeed_Dev;
DataTrial.SpeedDev=SpeedDev;
DataTrial.Gaze=output_gaze;
DataTrial.CoCoDEV=CoCoDEV;
DataTrial.CoCoBeforeDEV=CoCoBeforeDEV;

FilepathName=fullfile(output_folder,(sprintf('P%s_%s_DataTrial_Gaze_Coco.mat',Parti{t},Rep{j})));
save(FilepathName,'DataTrial')

end
end
end

