clearvars;clc;

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\';
Input='\3_Output\';
Input2='\2_TransformData\';

Parti={'31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','53','54','55','56','57','58','59'};
Rep={'04';'07';'09';'11';'14';'15';'17';'18';'21';'24'};

for t=1:28
for j=1:10
    
fullNames_DataTrail{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_DataTrial.mat',Parti{t},Rep{j}))));
fullNames_DataTrailGazeCoco{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_DataTrial_Gaze_Coco.mat',Parti{t},Rep{j}))));
fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input2,(sprintf('P%s_%s_MM.mat',Parti{t},Rep{j}))));
if exist(fullNames_DataTrail{j,t}, 'file')==2 && exist(fullNames_DataTrailGazeCoco{j,t}, 'file')==2
    load (fullNames_DataTrail{j,t})
    load (fullNames_DataTrailGazeCoco{j,t})
    file_EXP=fullNames_MM{j,t};


    % Determine the direction of circumvention
    HMD_position_x = DataTrial.HMD_position(:, 1);
    max_peak = max(HMD_position_x);
    min_peak = min(HMD_position_x);

    if abs(max_peak) > abs(min_peak)
        direction{t,j} = 1; %Right
    else
        direction{t,j} = 0; %Left
    end

    CrossingFrame=min(find(DataTrial.HMD_position(:,2)>DataTrial.Agent_position(:,2)));

    CrossOver{t,j}=FindCrossOver(file_EXP,cell2mat(DataTrial.Dev(2,3)),CrossingFrame,direction{t,j});
    
%     Dev{t,j}=cell2mat(DataTrial.Dev(2,3));
% %     Gaze(t,j)=DataTrial.Gaze;
%     try
%     SpeedPreDev{t,j}=DataTrial.SpeedBeforeDev;
%     SpeedDevCross{t,j}=DataTrial.SpeedDev;
%     MaxMLSpeed{t,j}=DataTrial.MaxMLspeed_Dev;
%     CoCoDEV_G{t,j}=DataTrial.CoCoDEV(1,1);
%     CoCoDEV_D{t,j}=DataTrial.CoCoDEV(1,2);
%     CoCoBeforeDEV_G{t,j}=DataTrial.CoCoBeforeDEV(1,1);
%     CoCoBeforeDEV_D{t,j}=DataTrial.CoCoBeforeDEV(1,2);
%     catch
%     SpeedPreDev{t,j}=[];
%     SpeedDevCross{t,j}=[];
%     MaxMLSpeed{t,j}=[];
%     CoCoDEV_G{t,j}=[];
%     CoCoDEV_D{t,j}=[];
%     CoCoBeforeDEV_G{t,j}=[];
%     CoCoBeforeDEV_D{t,j}=[];
%     end
%     try
%     Clearancetry{t,j}=DataTrial.Clearance;
%     catch
%         a=1;
%     end
%     if Dev(t,j)==0
%         Dev(t,j)=99999;
%     end
%     if SpeedPreDev(t,j)==0
%         SpeedPreDev(t,j)=99999;
%     end
%     if SpeedDevCross(t,j)==0
%         SpeedDevCross(t,j)=99999;
%     end
%     if MaxMLSpeed(t,j)==0
%         MaxMLSpeed(t,j)=99999;
%     end
%     if CoCoDEV_G(t,j)==0
%         CoCoDEV_G(t,j)=99999;
%     end
%     if CoCoDEV_D(t,j)==0
%         CoCoDEV_D(t,j)=99999;
%     end
%     if CoCoBeforeDEV_G(t,j)==0
%         CoCoBeforeDEV_G(t,j)=99999;
%     end
%     if CoCoBeforeDEV_D(t,j)==0
%         CoCoBeforeDEV_D(t,j)=99999;
%     end

% 
% 
end 
end 
end

%%
%     MeanPreDevGround=mean(PreDevGround,2);
%     MeanPreDevLeg=mean(PreDevLeg,2);
%     MeanPreDevUpperBody=mean(PreDevUpperBody,2);
%     MeanPreDevHead=mean(PreDevHead,2);
%     MeanPreDevEnvi=mean(PreDevEnvi,2);
%     MeanPreDevGoal=mean(PreDevGoal,2);
%     MeanPreDevVirtualAgent=mean(PreDevVirtualAgent,2);
% 
%     MeanConcatenatedPreDev=[MeanPreDevGround,MeanPreDevLeg,MeanPreDevUpperBody,MeanPreDevHead,MeanPreDevEnvi,MeanPreDevGoal,MeanPreDevVirtualAgent];
%     
%     MeanDevGround=mean(DevCrossGround,2);
%     MeanDevLeg=mean(DevCrossLeg,2);
%     MeanDevUpperBody=mean(DevCrossUpperBody,2);
%     MeanDevHead=mean(DevCrossHead,2);
%     MeanDevEnvi=mean(DevCrossEnvi,2);
%     MeanDevGoal=mean(DevCrossGoal,2);
%     MeanDevVirtualAgent=mean(DevCrossVirtualAgent,2);
% 
%     MeanConcatenatedDevCross=[MeanDevGround,MeanDevLeg,MeanDevUpperBody,MeanDevHead,MeanDevEnvi,MeanDevGoal,MeanDevVirtualAgent];
% 
% %%
%     stdPreDevGround=std(PreDevGround',1);
%     stdPreDevLeg=std(PreDevLeg',1);
%     stdPreDevUpperBody=std(PreDevUpperBody',1);
%     stdPreDevHead=std(PreDevHead',1);
%     stdPreDevEnvi=std(PreDevEnvi',1);
%     stdPreDevGoal=std(PreDevGoal',1);
%     stdPreDevVirtualAgent=std(PreDevVirtualAgent',1);
% 
%     stdConcatenatedPreDev=[stdPreDevGround',stdPreDevLeg',stdPreDevUpperBody',stdPreDevHead',stdPreDevEnvi',stdPreDevGoal',stdPreDevVirtualAgent'];
%     
%     stdDevGround=std(DevCrossGround',1);
%     stdDevLeg=std(DevCrossLeg',1);
%     stdDevUpperBody=std(DevCrossUpperBody',1);
%     stdDevHead=std(DevCrossHead',1);
%     stdDevEnvi=std(DevCrossEnvi',1);
%     stdDevGoal=std(DevCrossGoal',1);
%     stdDevVirtualAgent=std(DevCrossVirtualAgent',1);
% 
%     stdConcatenatedDevCross=[stdDevGround',stdDevLeg',stdDevUpperBody',stdDevHead',stdDevEnvi',stdDevGoal',stdDevVirtualAgent'];
% 
