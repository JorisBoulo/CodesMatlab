clearvars;clc;

%% % Settings 

subject_path='C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\';
Input='\2_TransformData\';
Output='\3_Output\';

Parti={'31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'};
Rep={'02';'06';'10';'16';'23'};

ParticipantData=readtable('C:\Users\joris\OneDrive - Université Laval\Documents\Doctorat\Etude 3\Participants\Variables_Questionnaire_Etude3.xlsx');
AvatarGender=table2cell(ParticipantData(:,15));


for t=1:29
for j=1:5
fullNames_MM{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_MM.mat',Parti{t},Rep{j}))));
fullNames_EMG{j,t}=(fullfile(subject_path,(sprintf('P%s',Parti{t})),Input,(sprintf('P%s_%s_EMG.mat',Parti{t},Rep{j}))));
if  exist(fullNames_MM{j,t}, 'file')==2 && exist(fullNames_EMG{j,t}, 'file')==2
%import data
 file_EXP=fullNames_MM{j,t};
 file_EMG=fullNames_EMG{j,t};

 input_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Input);
 output_folder=fullfile(subject_path,(sprintf('P%s',Parti{t})),Output);
 load(file_EMG)
 titleEMG=DataEMG(1,:);
 DataEMG=cell2mat(DataEMG(2:end,:));

%% CO-contraction 
Filtrated=FiltrateEMG(DataEMG,4,20,450);
Movemean=movmean(abs(Filtrated-mean(Filtrated,1)),100);

% Heelstrike detection 

Acc=DataEMG(:,5:6);

[~,HeelStrikeL]=findpeaks(Acc(:,2),'MinPeakHeight',-3,'MinPeakDistance',1700);

[~,HeelStrikeR]=findpeaks(Acc(:,1),'MinPeakHeight',-3,'MinPeakDistance',1700);

% Resampling 

% Resampling Left Cycle 1   
Timeline=(1:1:size(Movemean(HeelStrikeL(2,1):HeelStrikeL(3,1),:)));
CycleL1=resample(Movemean(HeelStrikeL(2,1):HeelStrikeL(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling Left Cycle 2
Timeline=(1:1:size(Movemean(HeelStrikeL(3,1):HeelStrikeL(4,1),:)));
CycleL2=resample(Movemean(HeelStrikeL(3,1):HeelStrikeL(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CycleL1;CycleL2];

if size(HeelStrikeL,1)>4
%Resampling Left Cycle 3
Timeline=(1:1:size(Movemean(HeelStrikeL(4,1):HeelStrikeL(5,1),:)));
CycleL3=resample(Movemean(HeelStrikeL(4,1):HeelStrikeL(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CycleL1;CycleL2;CycleL3];
end

if size(HeelStrikeL,1)>5
%Resampling Left Cycle 4
Timeline=(1:1:size(Movemean(HeelStrikeL(5,1):HeelStrikeL(6,1),:)));
CycleL4=resample(Movemean(HeelStrikeL(5,1):HeelStrikeL(6,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesL=[CyclesL;CycleL4];
end


% Resampling right Cycle 1   
Timeline=(1:1:size(Movemean(HeelStrikeR(2,1):HeelStrikeR(3,1),:)));
CycleR1=resample(Movemean(HeelStrikeR(2,1):HeelStrikeR(3,1),:),Timeline,(1/(Timeline(end,end)/1000)));

%Resampling right Cycle 2
Timeline=(1:1:size(Movemean(HeelStrikeR(3,1):HeelStrikeR(4,1),:)));
CycleR2=resample(Movemean(HeelStrikeR(3,1):HeelStrikeR(4,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CycleR1;CycleR2];

if size(HeelStrikeR,1)>4
%Resampling right Cycle 3
Timeline=(1:1:size(Movemean(HeelStrikeR(4,1):HeelStrikeR(5,1),:)));
CycleR3=resample(Movemean(HeelStrikeR(4,1):HeelStrikeR(5,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CycleR1;CycleR2;CycleR3];
end

if size(HeelStrikeR,1)>5
%Resampling right Cycle 4
Timeline=(1:1:size(Movemean(HeelStrikeR(5,1):HeelStrikeR(6,1),:)));
CycleR4=resample(Movemean(HeelStrikeR(5,1):HeelStrikeR(6,1),:),Timeline,(1/(Timeline(end,end)/1000)));
CyclesR=[CyclesR;CycleR4];
end

Z=linspace(0,1,size(CyclesL,1)); 
CoCoL{j,t}=2*(trapz(Z,min(CyclesL(:,1),CyclesL(:,3))))/(trapz(Z,CyclesL(:,1))+trapz(Z,CyclesL(:,3)))*100;
CoCoR{j,t}=2*(trapz(Z,min(CyclesL(:,2),CyclesL(:,4))))/(trapz(Z,CyclesL(:,2))+trapz(Z,CyclesL(:,4)))*100;

%% Walking Speed

[Head_position,Heels_position,Body_position,Head_Vel,Probed_point_contact,title_probed_point_contact,...
    Reorientation_var,title_reorientation_var,Toe_Position]...
            = GetMotionMonitorVariables_older(file_EXP);

Gait_onset=find(Body_position(:,2)>Body_position(1,2)+0.2,1,'first');
Gait_offset=find(Body_position(:,2)<Body_position(end,2)+4,1,'last');

WalkingSpeed{j,t}=(sqrt(sum((Body_position(Gait_offset-Gait_onset,1:2)-Body_position(Gait_offset,1:2)).^2, 2))/(Gait_onset*1/90));

%% Gaze analysis 

% [output_gaze,title_gaze]=FindGaze(Gaze,OffsetFrame,cutoff_vicon,frame_dev1,CrossingFrame,HMD_position,HMD_position_raw,HMD_position_uncut); 

% 
% 
% FilepathName=fullfile(output_folder,(sprintf('P%s_%s_DataTrial.mat',Parti{t},Rep{j})));
% save(FilepathName,'DataTrial')

end
end
end