function [reorientation_seq,reorientation_Magnitude] = Seq_rotation(reorientation_var,title_reorientation_var,frame_dev1,frame_dev1_HMD,CrossingFrame,output_folder,subject_path,file,side,Trunk_position)
%This function is to calculate the reorientation sequencies of the body and
%the bike
load('Mean_5_CatchTrials.mat')
if frame_dev1>0 
%% Define Variable
Head_Rot_Z_G=reorientation_var(:,1);
Head_Angular_Vel_Z=reorientation_var(:,10);
Trunk_Rot_Z_G=reorientation_var(:,2);
Trunk_Angular_Vel_Z=reorientation_var(:,11);
Trunk_Rot_Y_G=reorientation_var(:,3);
Trunk_Angular_Vel_Y=reorientation_var(:,12);
HandBar_Rot_Z_G=reorientation_var(:,4);
HandBar_Angular_Vel_Z=reorientation_var(:,13);
Frame_Rot_Z_G=reorientation_var(:,5);
Frame_Angular_Vel_Z=reorientation_var(:,14);

[frame ~]=size(Head_Rot_Z_G);frame=(1:frame)';

% Standardized right side of circumvention 
%Onset identification will always seem to be right sided circumvention to
%facilitate coding and visual confirmation. After that, you will see the
%real side in the graph.

if isequal(side,'Right')
   
    STDR_Head_Rot_Z_G=Head_Rot_Z_G;
    STDR_Head_Angular_Vel_Z=Head_Angular_Vel_Z;
    STDR_Trunk_Rot_Z_G=Trunk_Rot_Z_G;
    STDR_Trunk_Angular_Vel_Z=Trunk_Angular_Vel_Z;
    STDR_Trunk_Rot_Y_G=Trunk_Rot_Y_G;
    STDR_Trunk_Angular_Vel_Y=Trunk_Angular_Vel_Y;
    STDR_HandBar_Rot_Z_G=HandBar_Rot_Z_G;
    STDR_HandBar_Angular_Vel_Z=HandBar_Angular_Vel_Z;
    STDR_Frame_Rot_Z_G=Frame_Rot_Z_G;
    STDR_Frame_Angular_Vel_Z=Frame_Angular_Vel_Z;

else
    STDR_Head_Rot_Z_G=Head_Rot_Z_G*-1;
    STDR_Head_Angular_Vel_Z=Head_Angular_Vel_Z*-1;
    STDR_Trunk_Rot_Z_G=Trunk_Rot_Z_G*-1;
    STDR_Trunk_Angular_Vel_Z=Trunk_Angular_Vel_Z*-1;
    STDR_Trunk_Rot_Y_G=Trunk_Rot_Y_G*-1;
    STDR_Trunk_Angular_Vel_Y=Trunk_Angular_Vel_Y*-1;
    STDR_HandBar_Rot_Z_G=HandBar_Rot_Z_G*-1;
    STDR_HandBar_Angular_Vel_Z=HandBar_Angular_Vel_Z*-1;
    STDR_Frame_Rot_Z_G=Frame_Rot_Z_G*-1;
    STDR_Frame_Angular_Vel_Z=Frame_Angular_Vel_Z*-1;
end

%% Head rotation onset

graph3=figure;

response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Head_Rot_Z_G,'-')
    hold on
    title('Head')
    plot(frame,STDR_Head_Angular_Vel_Z,':')
    plot(frame(frame_dev1),Head_Rot_Z_G(frame_dev1),'o')
    plot(sdHead_Rot_Z_low,'--')
    plot(sdHead_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_head_onset=find(STDR_Head_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_head_onset=frame_head_onset+b1;
    if isempty(frame_head_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_head_onset= 'NaN'
                
                  case 'Yes'
                  frame_head_onset='Error'
                  disp('Error occurs in the onset selection')
                  
              end        
    end

hold off
break
end
%% Trunk rotation onset

response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_Trunk_Rot_Z_G,'-')
    hold on
    title('Trunk')
    plot(frame,STDR_Trunk_Angular_Vel_Z,':')
    plot(frame(frame_dev1),Trunk_Rot_Z_G(frame_dev1),'o')
    plot(sdTrunk_Rot_Z_low,'--')
    plot(sdTrunk_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_trunk_onset=find(STDR_Trunk_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_trunk_onset=frame_trunk_onset+b1;
    if isempty(frame_trunk_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_trunk_onset= 'NaN';
                  case 'Yes'
                  frame_trunk_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end

%% HandBar rotation onset

response = 'Yes';
while (strcmp(response,'Yes'));
    plot(frame,STDR_HandBar_Rot_Z_G,'-')
    hold on
    title('HandBar')
    plot(frame,STDR_HandBar_Angular_Vel_Z,':')
    plot(frame(frame_dev1),HandBar_Rot_Z_G(frame_dev1),'o')
    plot(sdHandBar_Rot_Z_low,'--')
    plot(sdHandBar_Rot_Z_up,'--')
    [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
    b1=find(frame>x(2),1);
    b2=find(frame<x(1),1,'last');
    frame_HandBar_onset=find(STDR_HandBar_Angular_Vel_Z(b1:b2,1)>0,1,'last');
    frame_HandBar_onset=frame_HandBar_onset+b1;
    if isempty(frame_HandBar_onset);
    response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
              switch (response)
                  case 'No'
                 frame_HandBar_onset= 'NaN';
                  case 'Yes'
                  frame_HandBar_onset='Error'
                  disp('Error occurs in the onset selection')
                  break
              end
    end

hold off
break
end

%% trunk roll

response = 'Yes';
% while (strcmp(response,'Yes'));
%     plot(frame,STDR_Trunk_Rot_Y_G,'-')
%     hold on
%     title('Trunk')
%     plot(frame,STDR_Trunk_Angular_Vel_Y,':')
%     plot(frame(frame_dev1),Trunk_Rot_Y_G(frame_dev1),'o')
%     plot(sdTrunk_Rot_Y_low,'--')
%     plot(sdTrunk_Rot_Y_up,'--')
%     [x, ~]=ginput(2); %si les �cart-types ne guident pas vers un onset... s�lectionner une zone comportant que des valeurs n�gatives pour la s�q de r�orientation
%     b1=find(frame>x(2),1);
%     b2=find(frame<x(1),1,'last');
%     [~ , frame_trunk_roll_onset]=max(STDR_Trunk_Rot_Y_G(b1:b2));
%     frame_trunk_roll_onset=frame_trunk_roll_onset+b1;
%     if isempty(frame_trunk_roll_onset);
%     response=questdlg('There is no point above 0 in this selection. Would you like chose other points?','Yes','No''No');
%               switch (response)
%                   case 'No'
%                  frame_trunk_roll_onset= 'NaN';
%                   case 'Yes'
%                   frame_trunk_roll_onset='Error'
%                   disp('Error occurs in the onset selection')
%                   break
%               end
%     end
% 
% hold off
% break
% end
frame_trunk_roll_onset= 'NaN';
close all

%% Figure
%Principal variable figure

rot_graph=figure;
subplot(2,3,1)
plot(frame,Head_Rot_Z_G,'-')
hold on
plot(frame,Head_Angular_Vel_Z,':')
plot(frame(frame_dev1),Head_Rot_Z_G(frame_dev1),'o')
if isnumeric(frame_head_onset)==1
   plot(frame(frame_head_onset),Head_Rot_Z_G(frame_head_onset),'*')
end
title('Head Yaw')
plot(sdHead_Rot_Z_low,'--')
plot(sdHead_Rot_Z_up,'--')
%legend({'position','velocity','COM onset','Rot Onset','catch','catch'})
hold off


subplot(2,3,2)
plot(frame,Trunk_Rot_Z_G,'-')
hold on
plot(frame,Trunk_Angular_Vel_Z,':')
plot(frame(frame_dev1),Trunk_Rot_Z_G(frame_dev1),'o')
if isnumeric(frame_trunk_onset)==1
   plot(frame(frame_trunk_onset),Trunk_Rot_Z_G(frame_trunk_onset),'*')
end
title('Trunk Yaw')
plot(sdTrunk_Rot_Z_low,'--')
plot(sdTrunk_Rot_Z_up,'--')
hold off

subplot(2,3,3)
plot(frame,HandBar_Rot_Z_G,'-')
hold on
plot(frame,HandBar_Angular_Vel_Z,':')
plot(frame(frame_dev1),HandBar_Rot_Z_G(frame_dev1),'o')
plot(frame(frame_HandBar_onset),HandBar_Rot_Z_G(frame_HandBar_onset),'*')
if isnumeric(frame_HandBar_onset)==1
   plot(frame(frame_HandBar_onset),Trunk_Rot_Z_G(frame_HandBar_onset),'*')
end
title('HandBar Yaw')
plot(sdHandBar_Rot_Z_low,'--')
plot(sdHandBar_Rot_Z_up,'--')
hold off

subplot(2,3,5)
plot(frame,Trunk_Rot_Y_G,'-')
hold on
plot(frame,Trunk_Angular_Vel_Y,':')
plot(frame(frame_dev1),Trunk_Rot_Y_G(frame_dev1),'o')
if isnumeric(frame_trunk_roll_onset)==1
   plot(frame(frame_trunk_roll_onset),Trunk_Rot_Y_G(frame_trunk_roll_onset),'*')
end
title('Trunk Roll')
plot(sdTrunk_Rot_Y_low,'--')
plot(sdTrunk_Rot_Y_up,'--')
hold off

subplot(2,3,6)
plot(frame,Trunk_position(:,1))
hold on
plot(frame(frame_dev1),Trunk_position(frame_dev1,1),'o')
title('Trunk Displacement')
plot(sdXtrunk_low,'--')
plot(sdXtrunk_up,'--')
hold off

figure_reorientation_file=fullfile(output_folder,strrep(file, '.xlsx', '_rot_Yaw'));
saveas(rot_graph,figure_reorientation_file,'png');
close all 
%% Onset relative to the time of crossing
if isnumeric(frame_head_onset)==1
    onset_time_head_yaw=(CrossingFrame-frame_head_onset)/90;
else
    onset_time_head_yaw='NaN';
end

if isnumeric(frame_trunk_onset)==1
    onset_time_trunk_yaw=(CrossingFrame-frame_trunk_onset)/90;
else
   onset_time_trunk_yaw='NaN';
end

if isnumeric(frame_HandBar_onset)==1    
    onset_time_HandBar_yaw=(CrossingFrame-frame_HandBar_onset)/90;
else
    onset_time_HandBar_yaw='NaN';
end



trunk_ML_COM_onset=(CrossingFrame-frame_dev1)/90;
frame_trunk_ML_COM_onset=frame_dev1; 

if isnumeric(frame_trunk_roll_onset)==1
    onset_time_trunk_roll=(CrossingFrame-frame_trunk_roll_onset)/90;
else
   onset_time_trunk_roll='NaN';
end

%% Magnitude 

magnitude=[Head_Rot_Z_G,Trunk_Rot_Z_G,HandBar_Rot_Z_G,Trunk_Rot_Y_G];
cut_magnitude=magnitude(1:CrossingFrame,:);

mean_rot=mean(abs(cut_magnitude));

%range: Le range est calcul� pour l'amplitude de rotation, car il y a
%souvent un biais dans la rotation (constant entre les essais), la moyenne
%du d�but jusqu'au onset de rotation est donc calcul� pour corriger le
%bias, pour avoir l'angle max de rotation relative � l'angle de d�part.


 [max_rot frame_max_rot]=min(cut_magnitude);
 
 if isnumeric(frame_head_onset)==1  
     range_rot_head_yaw=abs(min(cut_magnitude(:,1))-mean(cut_magnitude(1:frame_head_onset,1)));
 else 
     range_rot_head_yaw=0;
 end
 
 if isnumeric(frame_trunk_onset)==1
    range_rot_trunk_yaw=abs(min(cut_magnitude(:,2))-mean(cut_magnitude(1:frame_trunk_onset,2)));
 else
     range_rot_trunk_yaw=0;
 end
 
 if isnumeric(frame_HandBar_onset)==1
         range_rot_HandBar_yaw=abs(min(cut_magnitude(:,3))-mean(cut_magnitude(1:frame_HandBar_onset,3)));
 else
         range_rot_HandBar_yaw=0;
 end
 
 if isnumeric(frame_trunk_roll_onset)==1
        range_rot_trunk_roll=abs(min(cut_magnitude(:,4))-max(cut_magnitude(1:frame_trunk_roll_onset,4)));
 else 
        range_rot_trunk_roll=0;
 end
    range_rot=[range_rot_head_yaw,range_rot_trunk_yaw,range_rot_HandBar_yaw,range_rot_trunk_roll];


else
display('Analyse de la s�quence de r�orientation impossible, car absence des donn�es vicon au d�but de la d�viation')
range_rot=[0 0 0 0];
onset_time_head_yaw=0;
onset_time_trunk_yaw=0;
onset_time_HandBar_yaw=0;
trunk_ML_COM_onset=0;
onset_time_trunk_roll=0;
frame_head_onset=0;
frame_trunk_onset=0;
frame_HandBar_onset=0;
frame_trunk_ML_COM_onset=0;
frame_trunk_roll_onset=0;
end

%% Export to main code
reorientation_Magnitude={'range_rot_head_yaw','range_rot_trunk_yaw','range_rot_HandBar_yaw',...
   'range_rot_trunk_roll';range_rot(1),range_rot(2),range_rot(3),range_rot(4)};
reorientation_seq={'onset_time_head_yaw','onset_time_trunk_yaw','onset_time_HandBar_yaw','trunk_ML_COM_onset','onset_time_trunk_roll','frame_head_yaw_onset','frame_trunk_yaw_onset','frame_HandBar_yaw_onset','frame_trunk_ML_COM_onset,','frame_trunk_roll_onset',...
               ;onset_time_head_yaw,onset_time_trunk_yaw,onset_time_HandBar_yaw,trunk_ML_COM_onset,onset_time_trunk_roll,           frame_head_onset,      frame_trunk_onset,     frame_HandBar_onset,frame_trunk_ML_COM_onset          ,frame_trunk_roll_onset};
end

