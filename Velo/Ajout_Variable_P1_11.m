%Ce programme sert simplement � ajuster la variable clearance qui contient
%davantage d'informations que dans les analyses r�alis�s pour le REPAR,
%soit les participants 1 � 11.
clearvars;
clc;
%% Ouverture des fichiers

list={'Young Man','Young Female'};
sex=listdlg('PromptString','Select the virtual pedestrian:','SelectionMode','single','Name','VP','Liststring',list);
switch sex
    case 'Young Man'
        sex=1;
    case 'Young Female'
        sex=2;
end

subject_path=uigetdir('C:\Data\Data_Velo','Select the participant folder');
if isequal(subject_path,0)
    return;
end
addpath(subject_path);

%Ouvrir les fichiers Motion Monitor
[file_EXP, EXP_path]=uigetfile({'*.xlsx';'*.*'},'S�lectionnez les fichiers EXP des essais de contournement',subject_path,'MultiSelect', 'on');
if (isequal(file_EXP, 0))
    return;
end
file_EXP=fullfile(EXP_path,file_EXP);

%Ouvrir les fichiers Matlab
[file_mat, Mat_path]=uigetfile({'*.mat';'*.*'},'S�lectionnez les fichiers Matlab des essais de contournement',subject_path,'MultiSelect', 'on');
if (isequal(file_EXP, 0))
    return;
end
file_mat=fullfile(Mat_path,file_mat);

%Ouvrir les fichiers results Excel
[file, ~]=uigetfile({'*.xlsx';'*.*'},'S�lectionnez les fichiers Res des essais de contournement',Mat_path,'MultiSelect', 'on');
if (isequal(file_EXP, 0))
    return;
end
file_res=fullfile(Mat_path,file);

%% D�but boucle
for n=1:length(file_EXP)

%MotionMonitor variables 

[Head_position,~,~,~,~,~,Frame_position,...
    ~,Acromions,Acromions_title, ~,~,Frame_markers,... 
    Frame_markers_titles] = GetMotionMonitorVariables(file_EXP{n});

%Matlab variables

load(file_mat{n}); 


%Clearance
Clearance{n}=FindClearance(Acromions,Frame_markers,Agent_position,HMD_position,Head_position,Frame_position,sex,CrossingFrame);

%Correction du fichier excel
Variable=readcell(file_res{n});
Variable_corrected=[Clearance{n},Variable(:,7:end)];
mask=cellfun(@(x) any(isa(x,'missing')), Variable_corrected); %enlever les missing sinon tout bug
Variable_corrected(mask) = {0};
filename=fullfile(subject_path,strrep(file{n}, '.xlsx', '_2.xlsx'));
writecell(Variable_corrected,filename);
end
