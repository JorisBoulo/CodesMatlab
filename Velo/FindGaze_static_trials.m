function [output_gaze,title_gaze] = FindGaze_static_trials(Gaze,HMD_position,HMD_pos_uncut,HMD_position_raw,cutoff_vicon,CrossingFrame,OffsetFrame)
%FindGaze This function is used to calculate the percentage of time looking
%at different element of the virtual environment during specif phase of
%circumvention. 

%%Trouver les frames relatifs dans les donn�es non coup�es correspondant au frame_dev1_HMD, CrossingFrame

% initiation du mouvement, retirer le premier 20 cm
Gait_onset=find(HMD_position_raw(:,2)>HMD_position_raw(1,2)+0.2,1,'first');
% Crossing Frame_Gaze: L'agent est toujours � la m�me position, donc facile
% � calculer ici
CrossingFrame_Gaze=find(HMD_position_raw(:,2)>2.47,1,'first');



%% Logicals 

Gaze_Ground=contains(Gaze,"Sol");
Gaze_VA_Leg=contains(Gaze,"Agent: LowerBody");
Gaze_VA_Body=contains(Gaze,"Agent: UpperBody");
Gaze_VA_Head=contains(Gaze,"Agent: Head");
Gaze_Environment=contains(Gaze,"Environment");
Gaze_Goal=contains(Gaze,"Panel");% On utilise contains car Panel change selon le mot marqué sur le paneau 

Gaze_VirtualAgent=Gaze_VA_Leg+Gaze_VA_Body+Gaze_VA_Head;

%Between Onset and Crossing
Time_Gaze_Ground=(sum(Gaze_Ground(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Leg=(sum(Gaze_VA_Leg(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Body=(sum(Gaze_VA_Body(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Head=(sum(Gaze_VA_Head(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_Environment=(sum(Gaze_Environment(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_Goal=(sum(Gaze_Goal(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VirtualAgent=(sum(Gaze_VirtualAgent(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;

Time_Gaze=[Time_Gaze_Ground,Time_Gaze_VA_Leg,Time_Gaze_VA_Body,Time_Gaze_VA_Head,Time_Gaze_Environment,Time_Gaze_Goal,Time_Gaze_VirtualAgent];


%Warning si pas de panel pour v�rifier que le regard fonctionne bien
if sum(Gaze_Goal==0)
    warning('Le panneau n a jamais �t� regard�')
end


title_gaze={'Ground','VA_Leg','VA_Body','VA_Head','Environment','Goal','VirtualAgent'};
output_gaze=[Time_Gaze];

end

