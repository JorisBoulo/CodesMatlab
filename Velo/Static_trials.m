%L'objectif de ce programme est de calculer la clearance lors des essais o�
%le pi�ton virtuel reste statique. Ce programme est donc limit� aux essais
%de contournement statique. Comme la d�viation de trajectoire survient hors
%champs du vicon, les seules variables d'int�r�t sont la clearance et le
%regard. Puisqu'il n'y a pas d'identification d'onset de d�viation de la
%trajectoire, l'analyse de tous les essais sera fait en m�me temps via une
%boucle. 

clearvars;
clc;
%% Ouverture des fichiers 

list={'Young Man','Young Female'};
sex=listdlg('PromptString','Select the virtual pedestrian:','SelectionMode','single','Name','VP','Liststring',list);
switch sex
    case 'Young Man'
        sex=1;
    case 'Young Female'
        sex=2;
end

subject_path=uigetdir('C:\Data\Data_Velo','Select the participant folder');
if isequal(subject_path,0)
    return;
end
addpath(subject_path);

%Ouvrir les fichiers Motion Monitor
[file_EXP, EXP_path]=uigetfile({'*.xlsx';'*.*'},'S�lectionnez les fichiers EXP des essais de contournement static',subject_path,'MultiSelect', 'on');
if (isequal(file_EXP, 0))
    return;
end
file_EXP=fullfile(EXP_path,file_EXP);


%Ouvrir les fichiers du Quest
[file_Quest, Quest_path]=uigetfile({'*.csv';'*.*'},'s�lectionnez les fichiers Quest des essais de contournement static',subject_path,'MultiSelect', 'on');
if (isequal(file_Quest, 0))
    return;
end
file_Quest=fullfile(Quest_path,file_Quest);

%% D�but boucle
for n=1:length(file_Quest)
%% Importer les donn�es de MotionMonitor et du Quest    

%MotionMonitor variables 

[Head_position,Head_Vel,Trunk_position,Trunk_Vel,HandBar_position,HandBar_Vel,Frame_position,...
    Frame_Vel,Acromions,Acromions_title, reorientation_var,title_reorientation_var,Frame_markers,... 
    Frame_markers_titles] = GetMotionMonitorVariables(file_EXP{n});

%Quest variables
[HMD_position, HMD_rotation, Agent_position, CrossingFrame, Gaze, cutoff_vicon,HMD_pos_uncut,Agent_pos_uncut,HMD_position_raw,OffsetFrame]=GetQuestVariables(file_Quest{n}, Head_position);
    
%% Calcul de clearance et du regard       
Clearance=FindClearance(Acromions,Frame_markers,Agent_position,HMD_position,Head_position,Frame_position,sex,CrossingFrame);
output_clearance{n}=cell2mat(Clearance(2,:));    
[output_gaze{n},title_gaze] = FindGaze_static_trials(Gaze,HMD_position,HMD_pos_uncut,HMD_position_raw,cutoff_vicon,CrossingFrame,OffsetFrame);
end

%% Exporter les donn�es
output_clearance=cell2mat(output_clearance');    
output_gaze=cell2mat(output_gaze');    
%trier les output selon le c�t� de contournement, cette fonction place les
%donn�es dans l'ordre, en fonction de la colonne 10 (variable side) 
output_clearance=sortrows(output_clearance,10);
%ajout de la colonne side � output_gaze pour r�organiser les donn�es
output_gaze=[output_gaze,output_clearance(:,10)];
output_gaze=sortrows(output_gaze,8);
display('Pour terminer et enrigistrer les r�sultats, copier-coller les r�sultats de la variable output_clearance dans le fichier excel Analyse_v�lo et out_gaze dans le fichier Analyse_Gaze_V�lo') 