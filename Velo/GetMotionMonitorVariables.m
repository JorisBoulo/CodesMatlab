function [Head_position,Head_Vel,Trunk_position,Trunk_Vel,HandBar_position,HandBar_Vel,Frame_position,Frame_Vel,Acromions,Acromions_title,...
    reorientation_var,title_reorientation_var,Frame_markers,Frame_markers_titles]...
    = GetMotionMonitorVariables(fileName)

data_vicon=readcell(fileName);

Title_line=9;
[line_data_vicon column_data_vicon]=size(data_vicon);
for i=1:column_data_vicon
    
%Position, vitesse et acc�l�ration    
    if ~isempty(strfind(data_vicon{Title_line,i},'Head_X_position'));
    Head_X_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Y_position'));
    Head_Y_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Z_position'));
    Head_Z_position=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_X_position'));
    Trunk_X_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Y_position'));
    Trunk_Y_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Z_position'));
    Trunk_Z_position=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_X_position'));
    HandBar_X_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Y_position'));
    HandBar_Y_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Z_position'));
    HandBar_Z_position=cell2mat(data_vicon(Title_line+1:end,i)); 

elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_X_position'));
    Frame_X_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_Y_position'));
    Frame_Y_position=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_Z_position'));
    Frame_Z_position=cell2mat(data_vicon(Title_line+1:end,i));    

elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_X_vel'));
    Head_X_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Y_vel'));
    Head_Y_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Z_vel'));
    Head_Z_vel=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_X_vel'));
    Trunk_X_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Y_vel'));
    Trunk_Y_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Z_vel'));
    Trunk_Z_vel=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_X_vel'));
    HandBar_X_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Y_vel'));
    HandBar_Y_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Z_vel'));
    HandBar_Z_vel=cell2mat(data_vicon(Title_line+1:end,i)); 

elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_X_vel'));
    Frame_X_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_Y_vel'));
    Frame_Y_vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_Z_vel'));
    Frame_Z_vel=cell2mat(data_vicon(Title_line+1:end,i));      

elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_X_acc'));
    Head_X_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Y_acc'));
    Head_Y_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Z_acc'));
    Head_Z_acc=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_X_acc'));
    Trunk_X_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Y_acc'));
    Trunk_Y_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Z_acc'));
    Trunk_Z_acc=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_X_acc'));
    HandBar_X_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Y_acc'));
    HandBar_Y_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Z_acc'));
    HandBar_Z_acc=cell2mat(data_vicon(Title_line+1:end,i)); 

elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_X_acc'));
    Frame_X_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_Y_acc'));
    Frame_Y_acc=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Framer_Z_acc'));
    Frame_Z_acc=cell2mat(data_vicon(Title_line+1:end,i));      
    

%Variable li� � la s�quence de r�orientation 
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_X_angle'));
    Head_X_angle=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Y_angle'));
    Head_Y_angle=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Z_angle'));
    Head_Z_angle=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_X_angle'));
    Trunk_X_angle=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Y_angle'));
    Trunk_Y_angle=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Z_angle'));
    Trunk_Z_angle=cell2mat(data_vicon(Title_line+1:end,i));
    

elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Z_angle'));
    HandBar_Z_angle=cell2mat(data_vicon(Title_line+1:end,i)); 
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_Z_angle'));
    Frame_Z_angle=cell2mat(data_vicon(Title_line+1:end,i));      
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Trunk_angle'));
    Head_Trunk_angle=cell2mat(data_vicon(Title_line+1:end,i));  
elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Frame_angle'));
   HandBar_Frame_angle=cell2mat(data_vicon(Title_line+1:end,i));  
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Handbar_angle'));
    Trunk_Handbar_angle=cell2mat(data_vicon(Title_line+1:end,i));  
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Frame_angle'));
   Trunk_Frame_angle=cell2mat(data_vicon(Title_line+1:end,i));  
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_X_ang_Vel'));
    Head_X_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Y_ang_Vel'));
    Head_Y_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Head_Z_ang_Vel'));
    Head_Z_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
    
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_X_ang_Vel'));
    Trunk_X_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Y_ang_Vel'));
    Trunk_Y_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Trunk_Z_ang_Vel'));
    Trunk_Z_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));
    

elseif ~isempty(strfind(data_vicon{Title_line,i},'HandBar_Z_ang_Vel'));
    HandBar_Z_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i)); 
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_Z_ang_Vel'));
    Frame_Z_ang_Vel=cell2mat(data_vicon(Title_line+1:end,i));  

%Get acromion position
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_left_X'));
    Acr_left_X=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_left_Y'));
    Acr_left_Y=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_left_Z'));
    Acr_left_Z=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_right_X'));
    Acr_right_X=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_right_Y'));
    Acr_right_Y=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Acr_right_Z'));
    Acr_right_Z=cell2mat(data_vicon(Title_line+1:end,i)); 
    
%Get frame marker to calculate clearance
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_01_X'));
    Frame_L_X=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_01_Y'));
    Frame_L_Y=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_01_Z'));
    Frame_L_Z=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_04_X'));
    Frame_R_X=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_04_Y'));
    Frame_R_Y=cell2mat(data_vicon(Title_line+1:end,i));
elseif ~isempty(strfind(data_vicon{Title_line,i},'Frame_04_Z'));
    Frame_R_Z=cell2mat(data_vicon(Title_line+1:end,i)); 
    
end
end

Head_position=[Head_X_position Head_Y_position Head_Z_position];
Head_Vel=[Head_X_vel Head_Y_vel Head_Z_vel];
Head_Acc=[Head_X_acc Head_Y_acc Head_Z_acc];

Trunk_position=[Trunk_X_position Trunk_Y_position Trunk_Z_position];
Trunk_Vel=[Trunk_X_vel Trunk_Y_vel Trunk_Z_vel];
Trunk_Acc=[Trunk_X_acc Trunk_Y_acc Trunk_Z_acc];

Acromions=[Acr_left_X Acr_left_Y Acr_left_Z Acr_right_X  Acr_right_Y  Acr_right_Z ];
Acromions_title=[{'Acr_left_X' 'Acr_left_Y' 'Acr_left_Z' 'Acr_right_X' 'Acr_right_Y' 'Acr_right_Z'}];


Frame_markers_titles=[{'Frame_L_X' 'Frame_L_Y' 'Frame_L_Z' 'Frame_R_X' 'Frame_R_Y' 'Frame_R_Z'}];
%La boucle ci-dessus permet de g�n�rer des variables Not a number ou des
%vecteurs zero pour les variables qui concernent le v�lo lors des essais
%sans v�lo. 

if isnumeric(HandBar_X_position)==1
HandBar_position=[HandBar_X_position HandBar_Y_position HandBar_Z_position];
HandBar_Vel=[HandBar_X_vel HandBar_Y_vel HandBar_Z_vel];
HandBar_Acc=[HandBar_X_acc HandBar_Y_acc HandBar_Z_acc];

Frame_position=[Frame_X_position Frame_Y_position Frame_Z_position];
Frame_Vel=[Frame_X_vel Frame_Y_vel Frame_Z_vel];
Frame_Acc=[Frame_X_acc Frame_Y_acc Frame_Z_acc];

reorientation_var=[Head_Z_angle, Trunk_Z_angle, Trunk_Y_angle, HandBar_Z_angle, Frame_Z_angle, Head_Trunk_angle, HandBar_Frame_angle, Trunk_Handbar_angle, Trunk_Frame_angle, Head_Z_ang_Vel,Trunk_Z_ang_Vel,Trunk_Y_ang_Vel,HandBar_Z_ang_Vel,Frame_Z_ang_Vel];
title_reorientation_var=[{'Head_Z_angle', 'Trunk_Z_angle', 'Trunk_Y_angle', ' HandBar_Z_angle', 'Frame_Z_angle', 'Head_Trunk_angle', 'HandBar_Frame_angle', 'Trunk_Handbar_angle', 'Trunk_Frame_angle', 'Head_Z_ang_Vel','Trunk_Z_ang_Vel','Trunk_Y_ang_Vel','HandBar_Z_ang_Vel','Frame_Z_ang_Vel'}];

Frame_markers=[Frame_L_X Frame_L_Y Frame_L_Z Frame_R_X Frame_R_Y Frame_R_Z];

else
    
HandBar_position=[NaN];
HandBar_Vel=[NaN];
HandBar_Acc=[NaN];
Frame_position=[NaN];
Frame_Vel=[NaN];
Frame_Acc=[NaN];
reorientation_var=[Head_Z_angle, Trunk_Z_angle, Trunk_Y_angle, zeros([size(Head_Z_angle) 1]), zeros([size(Head_Z_angle) 1]), Head_Trunk_angle, zeros([size(Head_Z_angle) 1]), zeros([size(Head_Z_angle) 1]), zeros([size(Head_Z_angle) 1]), Head_Z_ang_Vel,Trunk_Z_ang_Vel,Trunk_Y_ang_Vel,zeros([size(Head_Z_angle) 1]),zeros([size(Head_Z_angle) 1])];
title_reorientation_var=[{'Head_Z_angle', 'Trunk_Z_angle', 'Trunk_Y_angle', ' HandBar_Z_angle', 'Frame_Z_angle', 'Head_Trunk_angle', 'HandBar_Frame_angle', 'Trunk_Handbar_angle', 'Trunk_Frame_angle', 'Head_Z_ang_Vel','Trunk_Z_ang_Vel','Trunk_Y_ang_Vel','HandBar_Z_ang_Vel','Frame_Z_ang_Vel'}];
Frame_markers=[NaN];
end




end

