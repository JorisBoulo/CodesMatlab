function [dev,frame_dev1_HMD,frame_dev1, frame_dev2,Scalar_Velocity,CrossingFrame_vicon] = Calcul_Deviation_Velo(Trunk_position,Trunk_Vel,... 
    HMD_position,HMD_pos_uncut, cutoff_vicon,Agent_position,Agent_pos_uncut,CrossingFrame,side,subject_path,output_folder,file)
%This function is used to calculate the deviation point and the variable
%associated to it. Identification of the deviation point is using Vicon
%data. For variables calculating distances between participant and the
%virtual agent, the Quest data is used to prevent error due to the spatial
%shift between the virtual and the real world
%   When graph appear, you have to select a zone to indicate where to
%   search a local minimum to identify deviation point. First clic to the
%   right, and second clic to the left. 


addpath(subject_path)
load Mean_5_CatchTrials.mat
n=numel(Trunk_position(:,1));zero=zeros(n,1);
HMD_pos_uncut=HMD_pos_uncut(1:(CrossingFrame+cutoff_vicon),:);

%% Deviation point identification

if isequal(side,'Right')

response = 'Yes I mess up';
while (strcmp(response,'Yes I mess up'));
    plot(Trunk_position(:,2),Trunk_position(:,1))
    hold on
    plot(HMD_pos_uncut(:,2),HMD_pos_uncut(:,1))
    plot(meanYtrunk,sdXtrunk_up,'--')
    plot(meanYtrunk,sdXtrunk_low,'--')
    plot(Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2); 
   b1=find(Trunk_position(:,2)>x(2),1);
   b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
   cut_trunk_position=Trunk_position(b1:b2,:);  
   [Trunk_min, frame_dev1]=min(cut_trunk_position(:,1));
   frame_dev1=frame_dev1+b1;
   plot(Trunk_position(frame_dev1,2),Trunk_position(frame_dev1,1),'*');
   graph1=figure;
  %S'il n'y a aucun minimum dans la courbe de position, par exemple, si le
  %participant se dirige un peu vers le c�t� de contournement depuis le
  %d�but, la courbe de vitesse aidera � trouver le moment o� la d�viation
  %s'emplifie. Cette option ne devrait pas servir fr�quemment.
   if cut_trunk_position(1,1)==Trunk_min; %le local min n'a pas pu �tre identifi� par le vicon
     response = questdlg('The minimum detected is equal to the second click.','error','Yes I want that','I know','I know'); 
     switch response
         case 'Yes I want that'
             frame_dev1=frame_dev1
         case 'I know'
      frame_dev1=0;
     end
   else
       break
   end  
end
   
    plot(HMD_pos_uncut(:,2),HMD_pos_uncut(:,1))
    hold on
    plot(meanYtrunk,sdXtrunk_up,'--')
    plot(meanYtrunk,sdXtrunk_low,'--')
    plot(Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2); 
   b1=find(HMD_pos_uncut(:,2)>x(2),1);
   b2=find(HMD_pos_uncut(:,2)<x(1));b2=b2(end);
   cut_HMD_pos=HMD_pos_uncut(b1:b2,:);  
   [Trunk_min, frame_dev1_HMD]=min(cut_HMD_pos(:,1));
   frame_dev1_HMD=frame_dev1_HMD+b1-cutoff_vicon;
   plot(HMD_pos_uncut(frame_dev1_HMD+cutoff_vicon,2),HMD_pos_uncut(frame_dev1_HMD+cutoff_vicon,1),'*');
   
   if cut_HMD_pos(1,1)==Trunk_min; %le local min n'a pas pu �tre identifi� par le vicon
     response = questdlg('The minimum detected is equal to the second click.','error','Yes I want that','I know','I know'); 
     switch response
         case 'Yes I want that'
             frame_dev1_HMD=frame_dev1_HMD
         case 'I know'
      frame_dev1_HMD=0;
     end
   end  
         %Avant, lorsqu'il n'y avait pas de minimum local, on prenait la
         %vitesse, maintenant, nous prendrons plut�t la courbe position du
         %HMD qui permet d'avoir de l'info beaucoup plus t�t.
%          plot(Trunk_position(:,2),Trunk_Vel(:,1))
%          hold on
%          plot(meanYtrunk,sdXtrunk_Vel_up,'--')
%          plot(meanYtrunk,sdXtrunk_Vel_low,'--')
%          plot(Trunk_position(:,2),zero,':r')
%          [x, ~]=ginput(2);
%          b1=find(Trunk_position(:,2)>x(2),1);
%          b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
%          cut_trunk_Vel=Trunk_Vel(b1:b2,:);
%          [A, B]=min(cut_trunk_Vel(:,1));
%          %S'il n'y a pas de minimum local, on prend alors le premier point
%          %o� la vitesse en X va dans le sens du contournement
%          if A<0
%              [x, ~]=ginput(2);
%              b1=find(Trunk_position(:,2)>x(2),1);
%              b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
%              cut_trunk_Vel=Trunk_Vel(b1:b2,:);
%              B=find(cut_trunk_Vel(:,1)>=0,1,'first');
%          end
%          hold off
%          frame_dev1=(b1+B);

graph2=figure;
plot(Trunk_position(:,2),Trunk_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),zero,':r')
%Identify the window within you want the program to search the ML velocity
%local min
[x, ~]=ginput(2);
b1=find(Trunk_position(:,2)>x(2),1);
b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=Trunk_Vel(b1:b2,:);
Max_vel=max(Trunk_Vel(:,1));
[B,~]=find(islocalmin(cut_trunk_Vel(:,1),'MinProminence',0.05*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_trunk_Vel(:,1)),1,'last')
end

%Local min must have a proeminence of 5% of max ML velocity to make sure
%it is not only a lack of fluidity
A=cut_trunk_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(Trunk_position(:,2)>x(2),1);
 b2=find(Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=Trunk_Vel(b1:b2,:);
 B=find(cut_trunk_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all    

elseif isequal(side,'Left')
    
%Afin de faciliter et optimiser la vitesse d'identification des points, le
%contournement est artificiellement mis du c�t� droit. Il sera pivot� �
%nouveau du c�t� gauche pour la production du graphique qui sera enregistr�. 
 
    inverse_Trunk_position=[Trunk_position(:,1)*-1,Trunk_position(:,2:3)];
    inverse_Trunk_Vel=[Trunk_Vel(:,1)*-1,Trunk_Vel(:,2:3)];
    inverse_HMD_pos_uncut=[HMD_pos_uncut(:,1)*-1,HMD_pos_uncut(:,2:3)];
    
response = 'Yes I mess up';
while (strcmp(response,'Yes I mess up'));
    plot(inverse_Trunk_position(:,2),inverse_Trunk_position(:,1))
    hold on
    plot(inverse_HMD_pos_uncut(:,2),inverse_HMD_pos_uncut(:,1))
    plot(meanYtrunk,sdXtrunk_up,'--')
    plot(meanYtrunk,sdXtrunk_low,'--')
    plot(inverse_Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2); 
   b1=find(inverse_Trunk_position(:,2)>x(2),1);
   b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
   cut_trunk_position=inverse_Trunk_position(b1:b2,:);  
   [Trunk_min, frame_dev1]=min(cut_trunk_position(:,1));
   frame_dev1=frame_dev1+b1;
   plot(inverse_Trunk_position(frame_dev1,2),inverse_Trunk_position(frame_dev1,1),'*');
   graph1=figure;
  %S'il n'y a aucun minimum dans la courbe de position, par exemple, si le
  %participant se dirige un peu vers le c�t� de contournement depuis le
  %d�but, la courbe de vitesse aidera � trouver le moment o� la d�viation
  %s'emplifie. Cette option ne devrait pas servir fr�quemment.
   if cut_trunk_position(1,1)==Trunk_min; %le local min n'a pas pu �tre identifi� par le vicon
     response = questdlg('The minimum detected is equal to the second click.','error','Yes I want that','I know','I know'); 
     switch response
         case 'Yes I want that'
             frame_dev1=frame_dev1
         case 'I know'
      frame_dev1=0;
     end
   else
       break
   end  
end
   
    plot(inverse_HMD_pos_uncut(:,2),inverse_HMD_pos_uncut(:,1))
    hold on
    plot(meanYtrunk,sdXtrunk_up,'--')
    plot(meanYtrunk,sdXtrunk_low,'--')
    plot(Trunk_position(:,2),zero,':r')
   [x, ~]=ginput(2); 
   b1=find(inverse_HMD_pos_uncut(:,2)>x(2),1);
   b2=find(inverse_HMD_pos_uncut(:,2)<x(1));b2=b2(end);
   cut_HMD_pos=inverse_HMD_pos_uncut(b1:b2,:);  
   [Trunk_min, frame_dev1_HMD]=min(cut_HMD_pos(:,1));
   frame_dev1_HMD=frame_dev1_HMD+b1-cutoff_vicon;
   plot(HMD_pos_uncut(frame_dev1_HMD+cutoff_vicon,2),HMD_pos_uncut(frame_dev1_HMD+cutoff_vicon,1),'*');
   
   if cut_HMD_pos(1,1)==Trunk_min; %le local min n'a pas pu �tre identifi� par le vicon
     response = questdlg('The minimum detected is equal to the second click.','error','Yes I want that','I know','I know'); 
     switch response
         case 'Yes I want that'
             frame_dev1_HMD=frame_dev1_HMD
         case 'I know'
      frame_dev1_HMD=0;
     end
   end  

graph2=figure;
plot(inverse_Trunk_position(:,2),inverse_Trunk_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(inverse_Trunk_position(:,2),zero,':r')
%Identify the window within you want the program to search the ML velocity
%local min
[x, ~]=ginput(2);
b1=find(inverse_Trunk_position(:,2)>x(2),1);
b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
Max_vel=max(inverse_Trunk_Vel(:,1));
[B,~]=find(islocalmin(cut_trunk_Vel(:,1),'MinProminence',0.05*Max_vel(1,1)),1,'last');
if isempty(B)
    [B,~]=find(islocalmin(cut_trunk_Vel(:,1)),1,'last')
end

%Local min must have a proeminence of 5% of max ML velocity to make sure
%it is not only a lack of fluidity
A=cut_trunk_Vel(B,1);
%if local min is negative, take the first point it become positive
if A<0
 [x, ~]=ginput(2);
 b1=find(inverse_Trunk_position(:,2)>x(2),1);
 b2=find(inverse_Trunk_position(:,2)<x(1));b2=b2(end);
cut_trunk_Vel=inverse_Trunk_Vel(b1:b2,:);
 B=find(cut_trunk_Vel(:,1)>=0,1,'first');
end
hold off 
frame_dev2=(b1+B);
close all    

end

%% Produce graph

graph_dev=figure;
subplot(2,1,1)
plot(Trunk_position(:,2),Trunk_position(:,1))
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
plot(Trunk_position(:,2),zero,':r')
plot(HMD_pos_uncut(:,2),HMD_pos_uncut(:,1))
plot(HMD_pos_uncut((frame_dev1_HMD+cutoff_vicon),2),HMD_pos_uncut((frame_dev1_HMD+cutoff_vicon),1),'*')
if isnumeric(frame_dev2)
plot(Trunk_position(frame_dev2,2),Trunk_position(frame_dev2,1),'o')
end
title('Trunk and HMD Position')
hold off
subplot(2,1,2)
plot(Trunk_position(:,2),Trunk_Vel(:,1))
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
plot(Trunk_position(:,2),zero,':r')
if frame_dev1_HMD>0
plot(Trunk_position(frame_dev1_HMD,2),Trunk_Vel(frame_dev1_HMD,1),'*')
end
if isnumeric(frame_dev2)
plot(Trunk_position(frame_dev2,2),Trunk_Vel(frame_dev2,1),'o')
end
title('Trunk Velocity')
hold off
figure_dev_file=fullfile(output_folder,strrep(file, '.xlsx', '_dev'));
saveas(graph_dev,figure_dev_file,'png');

%% Deviation point calcul
% Deviation point calculated relatived to the agent position. To avoid the
% spatial offset between vicon and the quest, the distance is calculated
% only with the Quest data.
if isempty(frame_dev2)
    frame_dev2=0
else %corrected to put in the reference timeline of the Quest.
   frame_dev2=frame_dev2-(frame_dev1-frame_dev1_HMD); 
end

%La premi�re variable calcul�e est � l'�preuve d'une d�viation trop t�t,
%car elle n'est pas affect�e par la coupure vicon ni par un probl�me d'orientation.
distance_dev1_agent_HMD=Agent_pos_uncut(frame_dev1_HMD+cutoff_vicon,2)-HMD_pos_uncut((frame_dev1_HMD+cutoff_vicon),2);

if frame_dev1>0 %si HMD est plus petit que z�ro, les donn�es vicon sont manquantes et il sera impossible de calculer la d�viation avec le vicon (ancienne m�thode).
distance_dev1_agent_trunk=Agent_position(frame_dev1,2)-HMD_position(frame_dev1,2);
else
distance_dev1_agent_trunk=0
end

if frame_dev2>0 & frame_dev1_HMD>0 %si on n'a pas le dev2, pas de calcul � faire. Sans le dev1 du vicon, on ne peut pas calculer car l'offset n'est pas connu.
distance_dev2_agent=Agent_position(frame_dev2,2)-HMD_position(frame_dev2,2);
else 
distance_dev2_agent=0;
end

Time_dev1_passing_HMD=(CrossingFrame-frame_dev1_HMD)/90;
if frame_dev1_HMD>0
Time_dev1_passing_trunk=(CrossingFrame-frame_dev1)/90;
else 
Time_dev1_passing_trunk=0
end
if frame_dev2>0
Time_dev2_passing=(CrossingFrame-frame_dev2)/90; 
else
Time_dev2_passing=0
end


%% Complementary variables: Maximal velocity and more
%La vitesse se calcul dans le r�f�rant du vicon, mais le crossing frame a
%�t� calcul� avec le quest. Il faut donc cr�er un crossing frame adapt� au vicon. Si on n'a pas le dev1
% sans le quest, on ne peut pas avoir le dev 2 avec pr�cision.
CrossingFrame_vicon=CrossingFrame-(frame_dev1_HMD-frame_dev1);

if frame_dev1>0
    dev_Trunk_X_vel=Trunk_Vel(frame_dev1:CrossingFrame_vicon,1);
else 
    dev_Trunk_X_vel=0;
end


if frame_dev1>0
    dev1_Trunk_X_vel=Trunk_Vel(frame_dev1:frame_dev2,1);
else 
    dev1_Trunk_X_vel=0;
end

if frame_dev2>0
   dev2_Trunk_X_vel=Trunk_Vel(frame_dev2:CrossingFrame_vicon,1);
else
   dev2_Trunk_X_vel=0;
end

%absolute value to help to compare R and L circumv trials
if frame_dev1>0
    rate_of_ML_avoidance=abs(mean(Trunk_Vel(frame_dev1:CrossingFrame_vicon,1))); 
else 
    rate_of_ML_avoidance=0;
end

if isequal(side,'Right')
max_dev_X_vel=max(dev_Trunk_X_vel);mean_dev_X_vel=mean(dev_Trunk_X_vel);
max_dev1_X_vel=max(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
max_dev2_X_vel=max(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
min_dev_X_vel=min(dev_Trunk_X_vel);
min_dev1_X_vel=min(dev1_Trunk_X_vel);
min_dev2_X_vel=min(dev2_Trunk_X_vel);
elseif isequal(side,'Left') %les vitesses minimales deviennent max et vice-versa du chgmt de sens de contournement
min_dev_X_vel=max(dev_Trunk_X_vel);mean_dev_X_vel=mean(dev_Trunk_X_vel);
min_dev1_X_vel=max(dev1_Trunk_X_vel);mean_dev1_X_vel=mean(dev1_Trunk_X_vel);
min_dev2_X_vel=max(dev2_Trunk_X_vel);mean_dev2_X_vel=mean(dev2_Trunk_X_vel);
max_dev_X_vel=min(dev_Trunk_X_vel);
max_dev1_X_vel=min(dev1_Trunk_X_vel);
max_dev2_X_vel=min(dev2_Trunk_X_vel);
end


% Velocity calcul 
Scalar_Velocity=sqrt((Trunk_Vel(:,1)).^2+(Trunk_Vel(:,2)).^2+(Trunk_Vel(:,3)).^2);
if frame_dev1>0
mean_scalar_vel_dev1_crossing=mean(Scalar_Velocity(frame_dev1:CrossingFrame));
if frame_dev2>0
mean_scalar_vel_dev1_dev2=mean(Scalar_Velocity(frame_dev1:frame_dev2));
mean_scalar_vel_dev2_crossing=mean(Scalar_Velocity(frame_dev2:CrossingFrame));
else 
mean_scalar_vel_dev1_dev2=0;
mean_scalar_vel_dev2_crossing=0;
end

else
mean_scalar_vel_dev1_crossing=0;
mean_scalar_vel_dev1_dev2=0;
mean_scalar_vel_dev2_crossing=0;
end

% Range of deviation
%pour un contournement � droite
if isequal(side,'Right') & frame_dev1>0
Max_dev=max(Trunk_position(frame_dev1:CrossingFrame))-min(Trunk_position(frame_dev1:CrossingFrame));
elseif isequal(side,'Left') & frame_dev1>0
%pour un contournement � gauche
Max_dev=min(Trunk_position(frame_dev1:CrossingFrame))-max(Trunk_position(frame_dev1:CrossingFrame));
Max_dev=abs(Max_dev);
else
Max_dev=0
end
%% Exporter les variables
dev={'distance_dev1_agent_HMD','distance_dev1_agent_trunk','distance_dev2_agent','frame_dev1_HMD',...
    'frame_dev1','frame_dev2','CrossingFrame_vicon','mean_scalar_vel_dev1_crossing','Max_dev', ...
    'rate_ML_avoidance','max_dev_X_Vel','max_dev1_X_Vel','max_dev2_X_Vel','min_dev_X_vel',...
    'min_dev2_X_vel','min_dev2_X_vel','mean_dev_X_vel','mean_dev1_X_vel','mean_dev2_X_vel';...
    distance_dev1_agent_HMD,distance_dev1_agent_trunk,distance_dev2_agent,frame_dev1_HMD,...
    frame_dev1,frame_dev2,CrossingFrame_vicon,mean_scalar_vel_dev1_crossing,Max_dev, ...
    rate_of_ML_avoidance,max_dev_X_vel,max_dev1_X_vel,max_dev2_X_vel,min_dev_X_vel,min_dev1_X_vel,...
    min_dev2_X_vel,mean_dev_X_vel,mean_dev1_X_vel,mean_dev2_X_vel};
end

