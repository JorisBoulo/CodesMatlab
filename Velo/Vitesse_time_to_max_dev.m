%Ajout de la variable de vitesse en lien avec le casque et le time to max
%deviation
clearvars;
clc;
%% Ouverture des fichiers 
subject_path=uigetdir('C:\Data\Data_Velo','Select the participant folder');
if isequal(subject_path,0)
    return;
end
addpath(subject_path);

%Ouvrir les fichiers Matlab
[file_mat, Mat_path]=uigetfile({'*.mat';'*.*'},'S�lectionnez les fichiers Matlab des essais de contournement',subject_path,'MultiSelect', 'on');
if (isequal(file_mat, 0))
    return;
end
file_mat=fullfile(Mat_path,file_mat);
%% Boucle 

for n=1:length(file_mat)
load(file_mat{n}); 

if max(Trunk_position(:,1))> abs(min(Trunk_position(:,1)))
    side="Right" %enregistr� plus loin comme �gal � 1
else
    side="Left" %enregistr� plus loin comme �gal � 2
end

%Calcul de la vitesse bas� sur le casque, onset de la marche jusqu'au
%croisement
Gait_onset=find(HMD_pos_uncut(:,2)>HMD_pos_uncut(1,2)+0.2,1,'first');
HMD_Velocity=diff(HMD_pos_uncut)./(1/90);
HMD_Velocity_scalaire=sqrt((HMD_Velocity(:,1)).^2+(HMD_Velocity(:,2)).^2+(HMD_Velocity(:,3)).^2);
HMD_Velocity_onset_cross{n}=mean(HMD_Velocity_scalaire(Gait_onset:(CrossingFrame+cutoff_vicon)));

%Calcul de la vitesse bas� sur le casque, point de d�v jusqu'au croisement
%(� fin de comparateur seulement)
if frame_dev1>0
HMD_Velocity_cut=diff(HMD_position)./(1/90);
HMD_Velocity_cut_scalaire=sqrt((HMD_Velocity_cut(:,1)).^2+(HMD_Velocity_cut(:,2)).^2+(HMD_Velocity_cut(:,3)).^2);
HMD_Velocity_dev1_cross{n}=mean(HMD_Velocity_cut_scalaire(frame_dev1:CrossingFrame));
else 
HMD_Velocity_dev1_cross{n}=0
end

%Time_to_max_deviation


Trunk_position_X=(Trunk_position(:,1));
    
%Range of deviation (d�j� enregistr� dans les variables principales;
%seulement le frame de l'�v�nement qui ne l'est pas)
%pour un contournement � droite
if isequal(side,'Right') & frame_dev1>0

Max_dev{n}=max(Trunk_position_X(frame_dev1:CrossingFrame))-min(Trunk_position_X(frame_dev1:CrossingFrame)); %correspond au calcul de base
[max_1 time_1]=max(Trunk_position_X(frame_dev1:CrossingFrame));
time_to_max_dev{n}=(time_1)*(1/90) %donne le temps en secondes

elseif isequal(side,'Left') & frame_dev1>0
%pour un contournement � gauche
Max_dev{n}=abs(min(Trunk_position_X(frame_dev1:CrossingFrame))-max(Trunk_position_X(frame_dev1:CrossingFrame)));
[max_1 time_1]=min(Trunk_position_X(frame_dev1:CrossingFrame))
time_to_max_dev{n}=(time_1)*(1/90) %donne le temps en secondes

else
Max_dev{n}=0
time_to_max_dev{n}=0
end

end

%% Output_file

HMD_Velocity_onset_cross=cell2mat(HMD_Velocity_onset_cross)'
HMD_Velocity_dev1_cross=cell2mat(HMD_Velocity_dev1_cross)'
time_to_max_dev=cell2mat(time_to_max_dev)'
Max_dev=cell2mat(Max_dev)'
output=[HMD_Velocity_onset_cross,HMD_Velocity_dev1_cross,time_to_max_dev,Max_dev];
output_title=({'HMD_Velocity_onset_cross','HMD_Velocity_dev1_cross','time_to_max_dev','Max_dev'});
filename=fullfile(Mat_path,'variable_res2')
save(filename,'output','output_title')
