clearvars;clc;

%% % *Open files*
%Before running this script, you have to run the E1_StraitData_velo before.

subject_path=uigetdir('C:\Data\Data_Velo');
if isequal(subject_path,0)
    return;
end

output_folder=fullfile(subject_path,'\matlab')

%subject_path='C:\Data\Data_Velo\Velo_24';
% output_folder = uigetdir(subject_path, 'Select Results Output Folder');
% if (isequal(output_folder, 0))
%     return;
% end
% output_folder='C:\Data\Data_Velo\Velo_24\Matlab';
%Virtual pedestrian have different width depending sex of the VP selected, which impact the
%clearance. The selection of the sexe appears before selection of the
%trials because it will concern every trials. 

list={'Young Man','Young Female'};
sex=listdlg('PromptString','Select the virtual pedestrian:','SelectionMode','single','Name','VP','Liststring',list);
switch sex
    case 'Young Man'
        sex=1;
    case 'Young Female'
        sex=2;
end
%  sex=2
%open motion monitor file
[file, EXP_path]=uigetfile({'*.xlsx';'Excel Files (*.xlsx)';'*.*'},'Select Motion Monitor File',subject_path);
if (isequal(file, 0))
    return;
end
file_EXP=fullfile(EXP_path,file);

%open Quest file
[file2, Quest_path]=uigetfile({'*.csv';'*.*'},'Select Quest File',subject_path);
if (isequal(file, 0))
    return;
end
file_Quest=fullfile(Quest_path,file2);



%% % *Importing variable from motion monitor and Quest files*


%MotionMonitor variables 

[Head_position,Head_Vel,Trunk_position,Trunk_Vel,HandBar_position,HandBar_Vel,Frame_position,...
    Frame_Vel,Acromions,Acromions_title, reorientation_var,title_reorientation_var,Frame_markers,... 
    Frame_markers_titles] = GetMotionMonitorVariables(file_EXP);

%Quest variables
[HMD_position, HMD_rotation, Agent_position, CrossingFrame, Gaze, cutoff_vicon,HMD_pos_uncut,Agent_pos_uncut,HMD_position_raw,OffsetFrame]=GetQuestVariables(file_Quest, Head_position);

alignement=figure;
plot(1:numel(Head_position(:,1)),Head_position(:,1),1:numel(HMD_position(:,1)),HMD_position(:,1)... 
    ,1:numel(Head_position(:,2)),Head_position(:,2),1:numel(HMD_position(:,2)),HMD_position(:,2))
figure_alignement=fullfile(output_folder,strrep(file, '.xlsx', '_align'));
saveas(alignement,figure_alignement,'png');
waitfor(alignement)

%% Deviation point

%identify the side of circumvention
if max(Trunk_position(:,1))> abs(min(Trunk_position(:,1)))
    side="Right" %enregistr� plus loin comme �gal � 1
else
    side="Left" %enregistr� plus loin comme �gal � 2
end
%side="Right" %Si jamais on a besoin d'un bypass
%calcul of deviation point
[dev,frame_dev1_HMD,frame_dev1, frame_dev2,Scalar_Velocity,CrossingFrame_vicon] = Calcul_Deviation_Velo(Trunk_position,Trunk_Vel,... 
    HMD_position, HMD_pos_uncut,cutoff_vicon,Agent_position,Agent_pos_uncut,CrossingFrame,side,subject_path,output_folder,file);


%% Clearance 
Clearance=FindClearance(Acromions,Frame_markers,Agent_position,HMD_position,Head_position,Frame_position,sex,CrossingFrame);

%% Reorientation
[reorientation_seq,reorientation_Magnitude] = Seq_rotation(reorientation_var,title_reorientation_var,frame_dev1,frame_dev1_HMD,CrossingFrame_vicon,output_folder,subject_path,file,side,Trunk_position);


%% Gaze analysis 
[output_gaze,title_gaze] = FindGaze(Gaze,HMD_position,HMD_pos_uncut,HMD_position_raw,cutoff_vicon,frame_dev1_HMD,CrossingFrame,OffsetFrame);
gaze_percent={'T-Ground','T-VA_Leg','T-VA_Body','T_VA_Head','T_Environment','T-Goal','T-VA',...
    'PreD-Ground','PreD-VA_Leg','PreD-VA_Body','PreD-VA_Head','PreD-Environment','PreD-Goal','PreD-VA'... 
    'PostD-Ground','PostD-VA_Leg','PostD-VA_Body','PostD-VA_Head','PostD-Environment','PostD-Goal','PostD-VA';...
    output_gaze(1),output_gaze(2),output_gaze(3),output_gaze(4),output_gaze(5),output_gaze(6),output_gaze(7),output_gaze(8),output_gaze(9)...
    output_gaze(10),output_gaze(11),output_gaze(12),output_gaze(13),output_gaze(14),output_gaze(15),output_gaze(16),output_gaze(17),output_gaze(18),output_gaze(19),output_gaze(20),output_gaze(21)};

%% Export the main variables
%fichier matlab regroupant les princales variables utilent � la r�alisation
%du code pour le regard.
filename=fullfile(output_folder,strrep(file, '.xlsx', '_res.mat'));
save(filename,'Agent_pos_uncut','Agent_position','HMD_pos_uncut','HMD_rotation','HMD_position','HMD_position_raw','frame_dev1_HMD','frame_dev1','frame_dev2','CrossingFrame','CrossingFrame_vicon','cutoff_vicon','Gaze','Trunk_position');

%fichier excel
filename=fullfile(output_folder,strrep(file, '.xlsx', '_res.xlsx'));
variables=[Clearance,dev,reorientation_seq,reorientation_Magnitude,gaze_percent];
writecell(variables,filename)
