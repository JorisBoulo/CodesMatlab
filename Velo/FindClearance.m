function clearance=FindClearance(Acromions,Frame_markers,Agent_position,HMD_position,Head_position,Frame_position,sex,CrossingFrame)
%This function is used to calculate the minimal clearance between the
%Virtual Agent and the shoulder of the participant, or the lateral
%extremity of the bike.
if sex==1
    av_rayon=0.21; %Correspond � la moiti� de la largeur de l'agent virtuel homme
elseif sex==2
    av_rayon=0.15; %Correspond � la moiti� de la largeur de l'agent virtuel femme
end
% if CrossingFrame>=numel(Agent_position)/3
%     CrossingFrame=numel(Agent_position)/3
% end
%The calcul of clearance will change depending of if the bike is there or
%not. 
if isnan(Frame_position)
    bike=1;
else
    bike=2;
end

%Clearance CoM to CoM 
dist_CoM_center=sqrt(sum((HMD_position(:,1:2)-Agent_position(:,1:2)).^2,2));
[Clearance_CoM, frame_clearance_com]=min(dist_CoM_center(1:CrossingFrame));
Distance_at_crossing_CoM=dist_CoM_center(CrossingFrame);

%% clearance shoulder-shoulder

%� partir du participant v�lo 12 (apr�s REPAR), la clearance Shoulder-Shoulder est calcul�e m�me en v�lo, sur demande de Brad.

%Pour calculer cette clearance, il faut cr�er un vecteur entre la position
%du Quest et de l'acromion et appliquer cette distance � position du Quest pour cr�er une acromion virtuelle 
%bas�e sur le Quest. Avec le v�lo, le concept est le m�me, mais avec la position du Frame.


%Nouvelle m�thode    
%cr�ation d'une position virtuelle de l'acromion bas�e sur la position du
%casque.

%With right acromions
Vecteur_acrR_tete=Acromions(:,4:6)-Head_position;
Quest_AcrR=HMD_position+Vecteur_acrR_tete;
Agent_position_shoulder=[Agent_position(:,1)-av_rayon, Agent_position(:,2:3)];
Dist_AcrR=sqrt(sum((Quest_AcrR(:,1:2)-Agent_position_shoulder(:,1:2)).^2,2));
[Clearance_shoulder_R, frame_clearance_shoulder_R]=min(Dist_AcrR(1:CrossingFrame));
Distance_at_crossing_shoulder_R=Dist_AcrR(CrossingFrame);

%With left acromions
Vecteur_acrL_tete=Acromions(:,1:3)-Head_position;
Quest_AcrL=HMD_position+Vecteur_acrL_tete;
Agent_position_shoulder=[Agent_position(:,1)+av_rayon, Agent_position(:,2:3)];
Dist_AcrL=sqrt(sum((Quest_AcrL(:,1:2)-Agent_position_shoulder(:,1:2)).^2,2));
[Clearance_shoulder_L, frame_clearance_shoulder_L]=min(Dist_AcrL(1:CrossingFrame));
Distance_at_crossing_shoulder_L=Dist_AcrL(CrossingFrame);


if Clearance_shoulder_L<Clearance_shoulder_R 
    Clearance_shoulder=Clearance_shoulder_L;
    Frame_clearance_shoulder=frame_clearance_shoulder_L;
    side=1 %signifie contournement � droite (donc clearance avec l'�paule gauche)
else 
    Clearance_shoulder=Clearance_shoulder_R;
    Frame_clearance_shoulder=frame_clearance_shoulder_R; 
    side=2 %signifie contournement � gauche (donc clearance avec l'�paule droite)
end
%distance at crossing
Distance_at_crossing_shoulder=min(Distance_at_crossing_shoulder_R, Distance_at_crossing_shoulder_L);

%% clearance bike 

if bike==1
 Clearance_bike=0;
 Frame_clearance_bike=0;
 Distance_at_crossing_bike=0;
else
% With Right side of the bike :
Vecteur_FrameR_tete=Frame_markers(:,4:6)-Head_position;
Quest_FrameR=HMD_position+Vecteur_FrameR_tete;
Agent_position_shoulder=[Agent_position(:,1)-av_rayon, Agent_position(:,2:3)];
Dist_FrameR=sqrt(sum((Quest_FrameR(:,1:2)-Agent_position_shoulder(:,1:2)).^2,2));
[Clearance_bike_R, frame_clearance_bike_R]=min(Dist_FrameR(1:CrossingFrame));
Distance_at_crossing_bike_R=Dist_FrameR(CrossingFrame);
Clearance_bike_R=Clearance_bike_R-0.10; %Le 10 cm est enlev� car, la roue occupe 10 cm suppl�mentaire  
% With Left side of the bike :
Vecteur_FrameL_tete=Frame_markers(:,1:3)-Head_position;
Quest_FrameL=HMD_position+Vecteur_FrameL_tete;
Agent_position_shoulder=[Agent_position(:,1)+av_rayon, Agent_position(:,2:3)];
Dist_FrameL=sqrt(sum((Quest_FrameL(:,1:2)-Agent_position_shoulder(:,1:2)).^2,2));
[Clearance_bike_L, frame_clearance_bike_L]=min(Dist_FrameL(1:CrossingFrame));
Distance_at_crossing_bike_L=Dist_FrameL(CrossingFrame);
Clearance_bike_L=Clearance_bike_L-0.10; %Le 10 cm est enlev� car, la roue occupe 10 cm suppl�mentaire  

if Clearance_bike_L<Clearance_bike_R 
    Clearance_bike=Clearance_bike_L;
    Frame_clearance_bike=frame_clearance_bike_L;
else 
    Clearance_bike=Clearance_bike_R;
    Frame_clearance_bike=frame_clearance_bike_R; 
end

%distance at crossing
Distance_at_crossing_bike=min(Distance_at_crossing_bike_R, Distance_at_crossing_bike_L);
end

%% identification d'un contact
if bike==1

    if Clearance_shoulder<=0 
    contact=1;
    warndlg('There was a contact between the participant and the virtual agent. Look at the Quest file to see if a verbal feedback was sent to the participant','Warning')
    else
    contact=0;
    end

else
    if Clearance_bike<=0 
    contact=1;
    warndlg('There was a contact between the participant and the virtual agent. Look at the Quest file to see if a verbal feedback was sent to the participant','Warning')
    else
    contact=0;
end
end
%Distance entre les 2 acromions donnent la largeur du participant
%Largeur_participant=mean(sqrt((Acromions(:,5)-Acromions(:,2)).^2+(Acromions(:,4)-Acromions(:,1)).^2+(Acromions(:,6)-Acromions(:,3)).^2));
%Largeur_velo=mean(sqrt((Frame_markers(:,5)-Frame_markers(:,2)).^2+(Frame_markers(:,4)-Frame_markers(:,1)).^2+(Frame_markers(:,6)-Frame_markers(:,3)).^2));

%% Exportation des variables

clearance={'Clearance_shoulder','Frame_clearance_shoulder','Distance_at_crossing_shoulder','Clearance_bike','Frame_clearance_bike',...
    'Distance_at_crossing_bike','Clearance_CoM','frame_clearance_com','Distance_at_crossing_CoM','side','contact';...
            Clearance_shoulder,Frame_clearance_shoulder,Distance_at_crossing_shoulder,Clearance_bike,Frame_clearance_bike,...
     Distance_at_crossing_bike,Clearance_CoM, frame_clearance_com,Distance_at_crossing_CoM,side,contact};
end

%Ancien code laiss� ici
% Offset=HMD_position-Head_position; 
% % With Left Shoulder :
% ClearOffsetLShoulder=Acromions(:,1:2)-Offset(:,1:2);
% Agent_position_Right_shoulder=Agent_position; Agent_position_Right_shoulder(:,1)=Agent_position(:,1)+av_rayon;
% Left_clearance_dist=sqrt(sum((ClearOffsetLShoulder(:,1:2)-Agent_position_Right_shoulder(:,1:2)).^2, 2));
% [ClearanceLeftS,Frame_clearanceL]=min(Left_clearance_dist(1:CrossingFrame));
% 
% 
% % With Right Shoulder :
% ClearOffsetRShoulder=Acromions(:,4:5)-Offset(:,1:2);
% Agent_position_Left_shoulder=Agent_position; Agent_position_Left_shoulder(:,1)=Agent_position(:,1)-av_rayon;
% Right_clearance_dist=sqrt(sum((ClearOffsetRShoulder(:,1:2)-Agent_position_Left_shoulder(:,1:2)).^2, 2));
% [ClearanceRightS,Frame_clearanceR]=min(Right_clearance_dist(1:CrossingFrame));

% 
% ClearOffsetLShoulder=Frame_position(:,1:2)-Offset(:,1:2);
% ClearOffsetLShoulder(:,1)=ClearOffsetLShoulder(:,1)-0.305; %pour avoir le c�t� gauche du v�lo ; MODIFIER ET FAIRE LA M�ME CHOSE QUE ACROMIONS AVEC MARQUEUR DU FRAME
% Agent_position_Right_shoulder=Agent_position; Agent_position_Right_shoulder(:,1)=Agent_position(:,1)+av_rayon;
% Left_clearance_dist=sqrt(sum((ClearOffsetLShoulder(:,1:2)-Agent_position_Right_shoulder(:,1:2)).^2, 2));
% [ClearanceLeftS,Frame_clearanceL]=min(Left_clearance_dist(1:CrossingFrame));
% 
% ClearOffsetRShoulder=Frame_position(:,1:2)-Offset(:,1:2);
% ClearOffsetRShoulder(:,1)=ClearOffsetRShoulder(:,1)+0.305; %pour avoir le c�t� droit du v�lo
% Agent_position_Left_shoulder=Agent_position; Agent_position_Left_shoulder(:,1)=Agent_position(:,1)-av_rayon;
% Right_clearance_dist=sqrt(sum((ClearOffsetRShoulder(:,1:2)-Agent_position_Left_shoulder(:,1:2)).^2, 2));
% [ClearanceRightS,Frame_clearanceR]=min(Right_clearance_dist(1:CrossingFrame));