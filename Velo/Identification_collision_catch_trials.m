% Programme dans le but de voir s'il y a des collisions lors des catchs
% trials ou des inversions d'ordre de passage. Tant qu'� le faire la clearance CoM to CoM est calcul�e
clearvars;
clc;

rayon_participant=inputdlg("Entrer le rayon du participant (en m�tre)","Rayon du participant"); %incrire la moiti� de la largeur entre les deux marqueurs acromions
rayon_participant=str2num(rayon_participant{1});

list={'Young Man','Young Female'};
sex=listdlg('PromptString','Select the virtual pedestrian:','SelectionMode','single','Name','VP','Liststring',list);
switch sex
    case 'Young Man'
        sex=1;
    case 'Young Female'
        sex=2;
end

list={'Marche','Velo'};
velo=listdlg('PromptString','Select the virtual pedestrian:','SelectionMode','single','Name','VP','Liststring',list);
switch sex
    case 'Marche'
        velo=1;
    case 'Velo'
        velo=2;
end


%% Ouverture des fichiers
subject_path=uigetdir('C:\Data\Data_Velo','Select the matlab output file folder');
if isequal(subject_path,0)
    return;
end
addpath(subject_path);


%Ouvrir les fichiers du Quest
[file_Quest, Quest_path]=uigetfile({'*.csv';'*.*'},'s�lectionnez les essais du Quest correspondant au catch trials',subject_path,'MultiSelect', 'on');
if (isequal(file_Quest, 0))
    return;
end
file_Quest=fullfile(Quest_path,file_Quest);

%% D�but boucle
for n=1:length(file_Quest)
%% Ouverture du fichier et extraction des variables d'int�r�ts
%Open Quest csv file
data_quest=readcell(file_Quest{n});

%Extract raw variable
HMD_position=cellfun(@str2num,data_quest(:,1),'UniformOutput',false);
HMD_position=cell2mat(HMD_position);
Agent_position=cellfun(@str2num,data_quest(:,4),'UniformOutput',false);
Agent_position=cell2mat(Agent_position);
Collision_Quest=data_quest(2:end,3);

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
HMDx=(HMD_position(:,2))/100; 
HMDy=(HMD_position(:,1))/100; 
HMDz=(HMD_position(:,3))/100;

%le /100 permet de ramener les donn�es en m�tre et on change les Y en X pour mettre dans le m�me syst�me de r�f�rence.
Agent_position_x=(Agent_position(:,2))/100; 
Agent_position_y=(Agent_position(:,1))/100; 
Agent_position_z=(Agent_position(:,3))/100;

%Mettre dans le syst�me de r�f�rence du labo
[HMDx,HMDy,HMDz, Agent_position_x, Agent_position_y, Agent_position_z] = trigrotate(HMDx,HMDy,HMDz,Agent_position_x,Agent_position_y,Agent_position_z,-45);

HMD_position=[HMDx,HMDy,HMDz];
Agent_position=[Agent_position_x, Agent_position_y, Agent_position_z];
%% Collision identifi� par le quest (donc feedback sonore re�u)

%il faut couper les donn�es au croisement, car on ne veut pas identifier
%une collision qui n'est jamais arriv�e.Par exemple, le fichier identifie
%toujours une collision � la bo�te qui trig le d�but de la marche du pi�ton virtuel.

CrossingFrameQuest=min(find(Agent_position(:,2)<HMD_position(:,2)));
%concentrer la recherche de la collision autour du croisement pour ne pas
%identifier de faux positif
collision=contains(Collision_Quest(CrossingFrameQuest-78:CrossingFrameQuest+77),"true"); % la collsion est recherch�e sur un range de 1.5 secondes au croisement
collision_frame=find(collision==1); 
if isempty(collision_frame);
    collision=0;
else
    collision=1;
    display("Collision selon le Quest!")
end

%% Calcul de la clearance 
%Puisque le vicon n'a pas �t� trait� lors des catchs. Seul les donn�es du
%quest peuvent servir, et par cons�quant, seul la clearance CoM-Participant
%� centre-pi�ton peut �tre calcul�e. Il faut consid�rer la largeur du participant/v�lo
%et la largaur du pi�ton virtuel pour voir s'il y a une collision.

%largeur du participant estim� � 40 cm pour la largeur des acromions
%largeur du v�lo (frame) est de 38 cm, et la roue occupe un 10 cm
%suppl�mentaire. 
if velo==2
    rayon_participant=0.29;
end

if sex==1
    av_rayon=0.21; %Correspond � la moiti� de la largeur de l'agent virtuel homme
elseif sex==2
    av_rayon=0.15; %Correspond � la moiti� de la largeur de l'agent virtuel femme
end


dist_CoM_center=sqrt(sum((HMD_position(:,1:2)-Agent_position(:,1:2)).^2,2));
[Clearance_CoM, frame_clearance_com]=min(dist_CoM_center(1:CrossingFrameQuest));
Distance_at_crossing_CoM=dist_CoM_center(CrossingFrameQuest);

if Clearance_CoM<(av_rayon+rayon_participant)
    collision_clearance=1;
else
    collision_clearance=0;
end

largeur_participant_VA=av_rayon+rayon_participant;

%% Ordre de passage 
%Enlever l'offset de la position de d�part de l'agent pour le ramener �
%z�ro en X. Le m�me principe n'est pas n�cessaire pour le participant, car
%l'offset avec l'origine est moindre et il d�vie dans une plus grande
%amplitude.

Agent_position = [Agent_position(:,1)-Agent_position(1,1),Agent_position(:,2),Agent_position(:,3)];

if abs(min(Agent_position(1:CrossingFrameQuest,1)))>abs(max(Agent_position(1:CrossingFrameQuest,1)))     & HMD_position(CrossingFrameQuest,1)>Agent_position(CrossingFrameQuest,1) 
    inversion = 0; % correspond � un catch droit avec le participant qui va en effet � droite
elseif abs(min(Agent_position(1:CrossingFrameQuest,1)))>abs(max(Agent_position(1:CrossingFrameQuest,1))) & HMD_position(CrossingFrameQuest,1)<Agent_position(CrossingFrameQuest,1) 
    inversion = 1; % correspond � un catch droit, mais que le participant a forc� pour passer quand m�me � gauche
elseif abs(min(Agent_position(1:CrossingFrameQuest,1)))<abs(max(Agent_position(1:CrossingFrameQuest,1))) & HMD_position(CrossingFrameQuest,1)>Agent_position(CrossingFrameQuest,1)
    inversion = 1; % correspond � un catch gauche mais que le participant a forc� pour passer quand m�me � droite
elseif abs(min(Agent_position(1:CrossingFrameQuest,1)))<abs(max(Agent_position(1:CrossingFrameQuest,1))) & HMD_position(CrossingFrameQuest,1)<Agent_position(CrossingFrameQuest,1)
    inversion = 0; % correspond � un catch gauche avec le participant qui va en effet � gauche
end
%% Exporter les variables cl�es 
output{n}=[Clearance_CoM,Distance_at_crossing_CoM,collision,collision_clearance,largeur_participant_VA,inversion,CrossingFrameQuest];

end %Fin de la boucle

output=cell2mat(output');
output_title=[{'Clearance_CoM','Distance_at_crossing_CoM','collision','collision_clearance','largeur_participant_VA','inversion','CrossingFrameQuest'}];
file_Quest=file_Quest';


display('Finit!')