function [output_gaze,title_gaze] = FindGaze(Gaze,HMD_position,HMD_pos_uncut,HMD_position_raw,cutoff_vicon,frame_dev1_HMD,CrossingFrame,OffsetFrame)
%FindGaze This function is used to calculate the percentage of time looking
%at different element of the virtual environment during specif phase of
%circumvention. 

%%Trouver les frames relatifs dans les donn�es non coup�es correspondant au frame_dev1_HMD, CrossingFrame

% initiation du mouvement, retirer le premier 20 cm
Gait_onset=find(HMD_position_raw(:,2)>HMD_position_raw(1,2)+0.2,1,'first');

if frame_dev1_HMD<=0
frame_dev_gaze=find(HMD_position_raw(:,2)<HMD_pos_uncut(cutoff_vicon+frame_dev1_HMD,2),1,'last');
else 
frame_dev_gaze=find(HMD_position_raw(:,2)<HMD_position(frame_dev1_HMD,2),1,'last');
end

if length(HMD_position)>=CrossingFrame
CrossingFrame_Gaze=find(HMD_position_raw(:,2)>HMD_position(CrossingFrame,2),1,'first');
else
CrossingFrame_Gaze=length(HMD_position);
display('Il manque le crossing Frame!!!')
end

%si le frame_dev_gaze est apr�s, c'est qui a �t� identifi� sur le retour, ceci corrige l'erreur 
if frame_dev_gaze>CrossingFrame_Gaze
    frame_dev_gaze=find(HMD_position_raw(1:CrossingFrame_Gaze,2)<HMD_pos_uncut(cutoff_vicon+frame_dev1_HMD,2),1,'last');
end



%% Logicals 

Gaze_Ground=contains(Gaze,"Sol");
Gaze_VA_Leg=contains(Gaze,"Agent: LowerBody");
Gaze_VA_Body=contains(Gaze,"Agent: UpperBody");
Gaze_VA_Head=contains(Gaze,"Agent: Head");
Gaze_Environment=contains(Gaze,"Environment");
Gaze_Goal=contains(Gaze,"Panel");% On utilise contains car Panel change selon le mot marqué sur le paneau 

Gaze_VirtualAgent=Gaze_VA_Leg+Gaze_VA_Body+Gaze_VA_Head;

%Between Onset and Crossing
Time_Gaze_Ground=(sum(Gaze_Ground(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Leg=(sum(Gaze_VA_Leg(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Body=(sum(Gaze_VA_Body(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VA_Head=(sum(Gaze_VA_Head(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_Environment=(sum(Gaze_Environment(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_Goal=(sum(Gaze_Goal(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;
Time_Gaze_VirtualAgent=(sum(Gaze_VirtualAgent(Gait_onset:CrossingFrame_Gaze,1).*OffsetFrame(Gait_onset:CrossingFrame_Gaze,1))/sum(OffsetFrame(Gait_onset:CrossingFrame_Gaze,1)))*100;

Time_Gaze=[Time_Gaze_Ground,Time_Gaze_VA_Leg,Time_Gaze_VA_Body,Time_Gaze_VA_Head,Time_Gaze_Environment,Time_Gaze_Goal,Time_Gaze_VirtualAgent];


%Between Onset et DEV

TimePreDEV_Gaze_Ground=(sum(Gaze_Ground(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_VA_Leg=(sum(Gaze_VA_Leg(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_VA_Body=(sum(Gaze_VA_Body(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_VA_Head=(sum(Gaze_VA_Head(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_Environment=(sum(Gaze_Environment(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_Goal=(sum(Gaze_Goal(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;
TimePreDEV_Gaze_VirtualAgent=(sum(Gaze_VirtualAgent(Gait_onset:frame_dev_gaze,1).*OffsetFrame(Gait_onset:frame_dev_gaze,1))/sum(OffsetFrame(Gait_onset:frame_dev_gaze,1)))*100;

TimePreDev_Gaze=[TimePreDEV_Gaze_Ground,TimePreDEV_Gaze_VA_Leg,TimePreDEV_Gaze_VA_Body,TimePreDEV_Gaze_VA_Head,TimePreDEV_Gaze_Environment,TimePreDEV_Gaze_Goal,TimePreDEV_Gaze_VirtualAgent];

% Between DEV et Crossing

TimeDevCross_Gaze_Ground=(sum(Gaze_Ground(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_VA_Leg=(sum(Gaze_VA_Leg(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_VA_Body=(sum(Gaze_VA_Body(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_VA_Head=(sum(Gaze_VA_Head(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_Environment=(sum(Gaze_Environment(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_Goal=(sum(Gaze_Goal(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;
TimeDevCross_Gaze_VirtualAgent=(sum(Gaze_VirtualAgent(frame_dev_gaze:CrossingFrame_Gaze,1).*OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1))/sum(OffsetFrame(frame_dev_gaze:CrossingFrame_Gaze,1)))*100;

TimeDevCross_Gaze=[TimeDevCross_Gaze_Ground,TimeDevCross_Gaze_VA_Leg,TimeDevCross_Gaze_VA_Body,TimeDevCross_Gaze_VA_Head,TimeDevCross_Gaze_Environment,TimeDevCross_Gaze_Goal,TimeDevCross_Gaze_VirtualAgent];

%Warning si pas de panel pour v�rifier que le regard fonctionne bien
if sum(Gaze_Goal==0)
    warning('Le panneau n a jamais �t� regard�')
end


title_gaze={'Ground','VA_Leg','VA_Body','VA_Head','Environment','Goal','VirtualAgent'};
output_gaze=[Time_Gaze,TimePreDev_Gaze,TimeDevCross_Gaze];

end

