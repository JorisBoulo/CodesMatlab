function [lowpassed]=Filtrate(Signal,Order,FreqLow,FreqAcquis)
%Seccond order low-passed butterworth filter 9hz
[b,a]=butter(Order,FreqLow/(FreqAcquis/2),'low');
lowpassed = filtfilt(b,a,Signal);
end

