% The goal of this scrpit is to produced a mean catch trial curve that will 
% be used to guide visually the identification of the two deviation points 
% and reorientation sequencies. 
% 
% *Note that some part of the script is "write in french". 
% 
% *This script is need to be run before the main code to produce the "mean5catchtrials" 
% file that is needed to start the main code.*
% 
%% *Open files*

clearvars;clc;

subject_path=uigetdir('C:\Data\Data_Velo');
if isequal(subject_path,0)
    return;
end

output_folder = fullfile(subject_path,'Matlab');



%% Imorting data
%essai 2,6,9,10,16
[filecatch path]=uigetfile({'*.xlsx';'Excel Files (*.xlsx)';'*.*'},'Select 5 catch trials','MultiSelect','on',subject_path);


if numel(filecatch)~=5
    warndlg=('You didn''t select five catch trials')
    return
end

filecatch1=cell2mat(fullfile(path,filecatch(1)));filecatch2=cell2mat(fullfile(path,filecatch(2)));
filecatch3=cell2mat(fullfile(path,filecatch(3)));filecatch4=cell2mat(fullfile(path,filecatch(4)));
filecatch5=cell2mat(fullfile(path,filecatch(5)));

[Head_position_1,Head_Vel_1,Trunk_position_1,Trunk_Vel_1,~,~,~,~,~,~,reorientation_var_1,title_reorientation_var]...
     = GetMotionMonitorVariables(filecatch1);
[Head_position_2,Head_Vel_2,Trunk_position_2,Trunk_Vel_2,~,~,~,~,~,~,reorientation_var_2,title_reorientation_var]...
     = GetMotionMonitorVariables(filecatch2);
[Head_position_3,Head_Vel_3,Trunk_position_3,Trunk_Vel_3,~,~,~,~,~,~,reorientation_var_3,title_reorientation_var]...
     = GetMotionMonitorVariables(filecatch3);
[Head_position_4,Head_Vel_4,Trunk_position_4,Trunk_Vel_4,~,~,~,~,~,~,reorientation_var_4,title_reorientation_var]...
     = GetMotionMonitorVariables(filecatch4);
[Head_position_5,Head_Vel_5,Trunk_position_5,Trunk_Vel_5,~,~,~,~,~,~,reorientation_var_5,title_reorientation_var]...
     = GetMotionMonitorVariables(filecatch5);
 
%% Verify the catch trials
% *Display of the 250 first frameof the 5 catchs trials *Verify if it is representative 
% trials, if it seems to have an outlier position, note it and remove the
% trial

plot(1:250,Trunk_position_1(1:250,1))
hold on
plot(1:250,Trunk_position_2(1:250,1))
plot(1:250,Trunk_position_3(1:250,1))
plot(1:250,Trunk_position_4(1:250,1))
plot(1:250,Trunk_position_5(1:250,1))
title 'Catch trials M-L COM Trunk position'
legend('1','2','3','4','5')
hold off
answer=questdlg('Are the 5 catch trials seem representative?','Verify catch trials','Yes','No','Yes')
switch answer
    case 'Yes'
        representative=1
    case 'No'
        representative=0
end

if isequal(representative,0)
    return
end

%% *Cut the trials to make sure every five trials have the same length*

Ytrunk1=Trunk_position_1(:,2);Ytrunk2=Trunk_position_2(:,2);Ytrunk3=Trunk_position_3(:,2);Ytrunk4=Trunk_position_4(:,2);Ytrunk5=Trunk_position_5(:,2);

minYtrunk1= min(Ytrunk1);minYtrunk2= min(Ytrunk2);minYtrunk3= min(Ytrunk3);minYtrunk4= min(Ytrunk4);minYtrunk5= min(Ytrunk5);
minYtrunk=[minYtrunk1,minYtrunk2,minYtrunk3,minYtrunk4,minYtrunk5];
[maxYtrunk ind]=max(minYtrunk)

%regroupement de toutes les variables ensemble pour pouvoir toutes les
%couper en m�me tem
catchdata1=[Head_position_1,Head_Vel_1,Trunk_position_1,Trunk_Vel_1,reorientation_var_1];
catchdata2=[Head_position_2,Head_Vel_2,Trunk_position_2,Trunk_Vel_2,reorientation_var_2];
catchdata3=[Head_position_3,Head_Vel_3,Trunk_position_3,Trunk_Vel_3,reorientation_var_3];
catchdata4=[Head_position_4,Head_Vel_4,Trunk_position_4,Trunk_Vel_4,reorientation_var_4];
catchdata5=[Head_position_5,Head_Vel_5,Trunk_position_5,Trunk_Vel_5,reorientation_var_5];

if ind==1
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==2
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==3
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==4
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);
y5=Ytrunk5<maxYtrunk; f5=find(y5==1);mf5=max(f5);catchdata5=catchdata5(mf5:end,:);
elseif ind==5
y1=Ytrunk1<maxYtrunk; f1=find(y1==1);mf1=max(f1);catchdata1=catchdata1(mf1:end,:);
y2=Ytrunk2<maxYtrunk; f2=find(y2==1);mf2=max(f2);catchdata2=catchdata2(mf2:end,:);
y3=Ytrunk3<maxYtrunk; f3=find(y3==1);mf3=max(f3);catchdata3=catchdata3(mf3:end,:);1
y4=Ytrunk4<maxYtrunk; f4=find(y4==1);mf4=max(f4);catchdata4=catchdata4(mf4:end,:);
end
[s1 i1]=size(catchdata1);[s2 i2]=size(catchdata2);[s3 i3]=size(catchdata3);[s4 i4]=size(catchdata4);[s5 i5]=size(catchdata5);
coupure=[s1,s2,s3,s4,s5];coupure=min(coupure)
catchdata1=catchdata1(1:coupure,:);catchdata2=catchdata2(1:coupure,:);catchdata3=catchdata3(1:coupure,:);catchdata4=catchdata4(1:coupure,:);catchdata5=catchdata5(1:coupure,:);

%% *Mean catch trial vector and 2 SD curves*
Trunk_X_pos_Ind=7;
Trunk_Y_pos_Ind=8;
Trunk_X_vel_Ind=10;
Trunk_Y_vel_Ind=11;
Trunk_Z_vel_Ind=12;
Head_Rot_Z_G_Ind=13;
Trunk_Rot_Z_G_Ind=14;
Trunk_Rot_Y_G_Ind=15;
HandBar_Z_angle_Ind=16;
Frame_Z_angle_Ind=17;
HandBar_Frame_Z_angle_Ind=19;
     
XtrunkCatch=[catchdata1(:,Trunk_X_pos_Ind),catchdata2(:,Trunk_X_pos_Ind),catchdata3(:,Trunk_X_pos_Ind),catchdata4(:,Trunk_X_pos_Ind),catchdata5(:,Trunk_X_pos_Ind)];
meanXtrunk=mean(XtrunkCatch')';
sdXtrunk=std(XtrunkCatch')';
sdXtrunk_up=meanXtrunk+2*sdXtrunk;sdXtrunk_low=meanXtrunk-2*sdXtrunk;
YtrunkCatch=[catchdata1(:,Trunk_Y_pos_Ind),catchdata2(:,Trunk_Y_pos_Ind),catchdata3(:,Trunk_Y_pos_Ind),catchdata4(:,Trunk_Y_pos_Ind),catchdata5(:,Trunk_Y_pos_Ind)];
meanYtrunk=mean(YtrunkCatch')';
sdYtrunk=std(YtrunkCatch')';
sdYtrunk_up=meanYtrunk+2*sdYtrunk;sdYtrunk_low=meanYtrunk-2*sdYtrunk;


Xtrunk_Vel_Catch=[catchdata1(:,Trunk_X_vel_Ind),catchdata2(:,Trunk_X_vel_Ind),catchdata3(:,Trunk_X_vel_Ind),catchdata4(:,Trunk_X_vel_Ind),catchdata5(:,Trunk_X_vel_Ind)];
meantrunk_Vel_X=mean(Xtrunk_Vel_Catch')';
sdXtrunk_Vel=std(Xtrunk_Vel_Catch')';
sdXtrunk_Vel_up=meantrunk_Vel_X+2*sdXtrunk_Vel;sdXtrunk_Vel_low=meantrunk_Vel_X-2*sdXtrunk_Vel;
Ytrunk_Vel_Catch=[catchdata1(:,Trunk_Y_vel_Ind),catchdata2(:,Trunk_Y_vel_Ind),catchdata3(:,Trunk_Y_vel_Ind),catchdata4(:,Trunk_Y_vel_Ind),catchdata5(:,Trunk_Y_vel_Ind)];
meantrunk_Vel_Y=mean(Ytrunk_Vel_Catch')';
sdYtrunk_Vel=std(Ytrunk_Vel_Catch')';
sdYtrunk_Vel_up=meantrunk_Vel_Y+2*sdYtrunk_Vel;sdYtrunk_Vel_low=meantrunk_Vel_Y-2*sdYtrunk_Vel;
Ztrunk_Vel_Catch=[catchdata1(:,Trunk_Z_vel_Ind),catchdata2(:,Trunk_Z_vel_Ind),catchdata3(:,Trunk_Z_vel_Ind),catchdata4(:,Trunk_Z_vel_Ind),catchdata5(:,Trunk_Z_vel_Ind)];
meantrunk_Vel_Z=mean(Ztrunk_Vel_Catch')';
sdZtrunk_Vel=std(Ztrunk_Vel_Catch')';


Head_Rot_Z_Catch=[catchdata1(:,Head_Rot_Z_G_Ind),catchdata2(:,Head_Rot_Z_G_Ind),catchdata3(:,Head_Rot_Z_G_Ind),catchdata4(:,Head_Rot_Z_G_Ind),catchdata5(:,Head_Rot_Z_G_Ind)];
meanHead_Rot_Z=mean(Head_Rot_Z_Catch')';
sdHead_Rot_Z=std(Head_Rot_Z_Catch')';
sdHead_Rot_Z_up=meanHead_Rot_Z+2*sdHead_Rot_Z;sdHead_Rot_Z_low=meanHead_Rot_Z-2*sdHead_Rot_Z;

Trunk_Rot_Z_Catch=[catchdata1(:,Trunk_Rot_Z_G_Ind),catchdata2(:,Trunk_Rot_Z_G_Ind),catchdata3(:,Trunk_Rot_Z_G_Ind),catchdata4(:,Trunk_Rot_Z_G_Ind),catchdata5(:,Trunk_Rot_Z_G_Ind)];
meanTrunk_Rot_Z=mean(Trunk_Rot_Z_Catch')';
sdTrunk_Rot_Z=std(Trunk_Rot_Z_Catch')';
sdTrunk_Rot_Z_up=meanTrunk_Rot_Z+2*sdTrunk_Rot_Z;sdTrunk_Rot_Z_low=meanTrunk_Rot_Z-2*sdTrunk_Rot_Z;
Trunk_Rot_Y_Catch=[catchdata1(:,Trunk_Rot_Y_G_Ind),catchdata2(:,Trunk_Rot_Y_G_Ind),catchdata3(:,Trunk_Rot_Y_G_Ind),catchdata4(:,Trunk_Rot_Y_G_Ind),catchdata5(:,Trunk_Rot_Y_G_Ind)];
meanTrunk_Rot_Y=mean(Trunk_Rot_Y_Catch')';
sdTrunk_Rot_Y=std(Trunk_Rot_Y_Catch')';
sdTrunk_Rot_Y_up=meanTrunk_Rot_Y+2*sdTrunk_Rot_Y;sdTrunk_Rot_Y_low=meanTrunk_Rot_Y-2*sdTrunk_Rot_Y;

HandBar_Rot_Z_Catch=[catchdata1(:,HandBar_Z_angle_Ind),catchdata2(:,HandBar_Z_angle_Ind),catchdata3(:,HandBar_Z_angle_Ind),catchdata4(:,HandBar_Z_angle_Ind),catchdata5(:,HandBar_Z_angle_Ind)];
meanHandBar_Rot_Z=mean(HandBar_Rot_Z_Catch')';
sdHandBar_Rot_Z=std(HandBar_Rot_Z_Catch')';
sdHandBar_Rot_Z_up=meanHandBar_Rot_Z+2*sdHandBar_Rot_Z;sdHandBar_Rot_Z_low=meanHandBar_Rot_Z-2*sdHandBar_Rot_Z;

Frame_Rot_Z_Catch=[catchdata1(:,Frame_Z_angle_Ind),catchdata2(:,Frame_Z_angle_Ind),catchdata3(:,Frame_Z_angle_Ind),catchdata4(:,Frame_Z_angle_Ind),catchdata5(:,Frame_Z_angle_Ind)];
meanFrame_Rot_Z=mean(Frame_Rot_Z_Catch')';
sdFrame_Rot_Z=std(Frame_Rot_Z_Catch')';
sdFrame_Rot_Z_up=meanFrame_Rot_Z+2*sdFrame_Rot_Z;sdFrame_Rot_Z_low=meanFrame_Rot_Z-2*sdFrame_Rot_Z;

HandBar_Frame_Rot_Z_Catch=[catchdata1(:,HandBar_Frame_Z_angle_Ind),catchdata2(:,HandBar_Frame_Z_angle_Ind),catchdata3(:,HandBar_Frame_Z_angle_Ind),catchdata4(:,HandBar_Frame_Z_angle_Ind),catchdata5(:,HandBar_Frame_Z_angle_Ind)];
meanHandBar_Frame_Rot_Z=mean(HandBar_Frame_Rot_Z_Catch')';
sdHandBar_Frame_Rot_Z=std(HandBar_Frame_Rot_Z_Catch')';
sdHandBar_Frame_Rot_Z_up=meanHandBar_Frame_Rot_Z+2*sdHandBar_Frame_Rot_Z;sdHandBar_Frame_Rot_Z_low=meanHandBar_Frame_Rot_Z-2*sdHandBar_Frame_Rot_Z;

Scalar_Vel_Catch=[sqrt(catchdata1(:,Trunk_X_vel_Ind).^2+catchdata1(:,Trunk_Y_vel_Ind).^2+catchdata1(:,Trunk_Z_vel_Ind).^2)...
    ,sqrt(catchdata2(:,Trunk_X_vel_Ind).^2+catchdata2(:,Trunk_Y_vel_Ind).^2+catchdata2(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata3(:,Trunk_X_vel_Ind).^2+catchdata3(:,Trunk_Y_vel_Ind).^2+catchdata3(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata4(:,Trunk_X_vel_Ind).^2+catchdata4(:,Trunk_Y_vel_Ind).^2+catchdata4(:,Trunk_Z_vel_Ind).^2) ...
,sqrt(catchdata5(:,Trunk_X_vel_Ind).^2+catchdata5(:,Trunk_Y_vel_Ind).^2+catchdata5(:,Trunk_Z_vel_Ind).^2)];
mean_Scalar_Vel=mean(Scalar_Vel_Catch')';
SD_Scalar_Vel=std(Scalar_Vel_Catch')';
SD_Scalar_Vel_up=mean_Scalar_Vel+2*SD_Scalar_Vel;SD_Scalar_Vel_low=mean_Scalar_Vel-2*SD_Scalar_Vel;

%% *Graphic representation to verify the catch trial average curve *
graph=figure
plot(meanYtrunk,meanXtrunk,'m-')
hold on
plot(YtrunkCatch(:,1),XtrunkCatch(:,1))
plot(YtrunkCatch(:,2),XtrunkCatch(:,2))
plot(YtrunkCatch(:,3),XtrunkCatch(:,3))
plot(YtrunkCatch(:,4),XtrunkCatch(:,4))
plot(YtrunkCatch(:,5),XtrunkCatch(:,5))
title 'Trunk COM M-L displacement during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML Trunk displacement (m)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_X');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meantrunk_Vel_X,'m-')
hold on
plot(YtrunkCatch(:,1),Xtrunk_Vel_Catch(:,1))
plot(YtrunkCatch(:,2),Xtrunk_Vel_Catch(:,2))
plot(YtrunkCatch(:,3),Xtrunk_Vel_Catch(:,3))
plot(YtrunkCatch(:,4),Xtrunk_Vel_Catch(:,4))
plot(YtrunkCatch(:,5),Xtrunk_Vel_Catch(:,5))
title 'Trunk M-L velocity during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML velocity (m/s)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_VelX');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanHead_Rot_Z,'m-')
hold on
plot(YtrunkCatch(:,1),Head_Rot_Z_Catch(:,1))
plot(YtrunkCatch(:,2),Head_Rot_Z_Catch(:,2))
plot(YtrunkCatch(:,3),Head_Rot_Z_Catch(:,3))
plot(YtrunkCatch(:,4),Head_Rot_Z_Catch(:,4))
plot(YtrunkCatch(:,5),Head_Rot_Z_Catch(:,5))
title 'Head Yaw during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'Head yaw (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Head_Rot_Z');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanTrunk_Rot_Y,'m-')
hold on
plot(YtrunkCatch(:,1),Trunk_Rot_Y_Catch(:,1))
plot(YtrunkCatch(:,2),Trunk_Rot_Y_Catch(:,2))
plot(YtrunkCatch(:,3),Trunk_Rot_Y_Catch(:,3))
plot(YtrunkCatch(:,4),Trunk_Rot_Y_Catch(:,4))
plot(YtrunkCatch(:,5),Trunk_Rot_Y_Catch(:,5))
title 'Trunk Roll during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'Trunk Roll (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Trunk_Rot_Y');
saveas(graph,figure_name,'png')
hold off

if mean(HandBar_Rot_Z_Catch)~=0

graph=figure
plot(meanYtrunk,meanHandBar_Rot_Z,'m-')
hold on
plot(YtrunkCatch(:,1),HandBar_Rot_Z_Catch(:,1))
plot(YtrunkCatch(:,2),HandBar_Rot_Z_Catch(:,2))
plot(YtrunkCatch(:,3),HandBar_Rot_Z_Catch(:,3))
plot(YtrunkCatch(:,4),HandBar_Rot_Z_Catch(:,4))
plot(YtrunkCatch(:,5),HandBar_Rot_Z_Catch(:,5))
title 'HandBar Yaw during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'HandBar Yaw (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_HandBar_Rot_Z');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanFrame_Rot_Z,'m-')
hold on
plot(YtrunkCatch(:,1),Frame_Rot_Z_Catch(:,1))
plot(YtrunkCatch(:,2),Frame_Rot_Z_Catch(:,2))
plot(YtrunkCatch(:,3),Frame_Rot_Z_Catch(:,3))
plot(YtrunkCatch(:,4),Frame_Rot_Z_Catch(:,4))
plot(YtrunkCatch(:,5),Frame_Rot_Z_Catch(:,5))
title 'Frame Yaw during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'Frame Yaw (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_Frame_Rot_Z');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk,meanHandBar_Frame_Rot_Z,'m-')
hold on
plot(YtrunkCatch(:,1),HandBar_Frame_Rot_Z_Catch(:,1))
plot(YtrunkCatch(:,2),HandBar_Frame_Rot_Z_Catch(:,2))
plot(YtrunkCatch(:,3),HandBar_Frame_Rot_Z_Catch(:,3))
plot(YtrunkCatch(:,4),HandBar_Frame_Rot_Z_Catch(:,4))
plot(YtrunkCatch(:,5),HandBar_Frame_Rot_Z_Catch(:,5))
title 'HandBar relative to Frame Yaw during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'HandBar relative to frame Yaw (�)'
legend('mean','1','2','3','4','5')
figure_name=fullfile(output_folder,'5Catch_HandBar_Frame_Rot_Z');
saveas(graph,figure_name,'png')
hold off
end

%% *Cr�ation de courbe moyenne avec les SD (2SD affich�e)*
graph=figure
plot(meanYtrunk,meanXtrunk)
hold on
plot(meanYtrunk,sdXtrunk_up,'--')
plot(meanYtrunk,sdXtrunk_low,'--')
title 'Mean Trunk COM M-L displacement during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML trunk displacemen (m)'
figure_name=fullfile(output_folder,'Catch_Trunk_X');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanYtrunk)
hold on
plot(sdYtrunk_up,'--')
plot(sdYtrunk_low,'--')
title 'Mean Trunk COM longitudinal displacement during catch trials'
xlabel 'Frame'
ylabel 'Longitudinal trunk displacement(m)'
figure_name=fullfile(output_folder,'Catch_Trunk_Y');
saveas(graph,figure_name,'png')
hold off


graph=figure
plot(meanYtrunk,meantrunk_Vel_X)
hold on
plot(meanYtrunk,sdXtrunk_Vel_up,'--')
plot(meanYtrunk,sdXtrunk_Vel_low,'--')
title 'Mean Trunk COM M-L velocity during catch trials'
xlabel 'Longitudinal trunk displacement(m)'
ylabel 'ML trunk velocity(m/s)'
figure_name=fullfile(output_folder,'Catch_Trunk_VelX');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meantrunk_Vel_Y)
hold on
plot(sdYtrunk_Vel_up,'--')
plot(sdYtrunk_Vel_low,'--')
title 'Mean Trunk longitudinal velocity during catch trials'
xlabel 'Frame'
ylabel 'Longitudinal trunk velocity(m/s)'
figure_name=fullfile(output_folder,'Catch_Trunk_VelY');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanHead_Rot_Z)
hold on
plot(sdHead_Rot_Z_up,'--')
plot(sdHead_Rot_Z_low,'--')
title 'Mean Head Yaw during catch trials'
xlabel 'Frame'
ylabel 'Head Yaw (�)'
figure_name=fullfile(output_folder,'Catch_Head_Yaw');
saveas(graph,figure_name,'png')
hold off


graph=figure
plot(meanTrunk_Rot_Z)
hold on
plot(sdTrunk_Rot_Z_up,'--')
plot(sdTrunk_Rot_Z_low,'--')
title 'Mean Trunk Yaw during catch trials'
xlabel 'Frame'
ylabel 'Trunk Yaw (�)'
figure_name=fullfile(output_folder,'Catch_Trunk_Yaw');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanTrunk_Rot_Y)
hold on
plot(sdTrunk_Rot_Y_up,'--')
plot(sdTrunk_Rot_Y_low,'--')
title 'Mean Trunk Roll during catch trials'
xlabel 'Frame'
ylabel 'Trunk Roll (�)'
figure_name=fullfile(output_folder,'Catch_Trunk_Roll');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(mean_Scalar_Vel)
hold on
plot(SD_Scalar_Vel_up,'--')
plot(SD_Scalar_Vel_low,'--')
title 'Mean scalar velocity'
xlabel 'Frame'
ylabel 'Velocity (m/s)'
figure_name=fullfile(output_folder,'Catch_Scalar_Velocity');
saveas(graph,figure_name,'png')
hold off

if mean(HandBar_Rot_Z_Catch)~=0
    
graph=figure
plot(meanHandBar_Rot_Z)
hold on
plot(sdHandBar_Rot_Z_up,'--')
plot(sdHandBar_Rot_Z_low,'--')
title 'Mean HandBar rotation during catch trials'
xlabel 'Frame'
ylabel 'Angle (�)'
figure_name=fullfile(output_folder,'Catch_HandBar_Yaw');
saveas(graph,figure_name,'png')
hold off

graph=figure
plot(meanFrame_Rot_Z)
hold on
plot(sdFrame_Rot_Z_up,'--')
plot(sdFrame_Rot_Z_low,'--')
title 'Mean Bike Frame rotation during catch trials'
xlabel 'Frame'
ylabel 'Angle (�)'
figure_name=fullfile(output_folder,'Catch_Frame_Yaw');
saveas(graph,figure_name,'png')
hold off

end
%% Calculs de variables 

%Value: mean and SD of each variables calculated 
Value_Head_Rot_Z_Mean=mean(mean(Head_Rot_Z_Catch));
Value_Head_Rot_Z_SD=mean(std(Head_Rot_Z_Catch));

Value_Trunk_Rot_Z_Mean=mean(mean(Trunk_Rot_Z_Catch));
Value_Trunk_Rot_Z_SD=mean(std(Trunk_Rot_Z_Catch));
Value_Trunk_Rot_Y_Mean=mean(mean(Trunk_Rot_Y_Catch));
Value_Trunk_Rot_Y_SD=mean(std(Trunk_Rot_Y_Catch));

Value_Mean_Scalar_Velocity=mean(mean(Scalar_Vel_Catch));
Value_SD_Scalar_Velocity=std(mean(Scalar_Vel_Catch));

Value_HandBar_Rot_Z_Mean=mean(mean(HandBar_Rot_Z_Catch));
Value_HeadBar_Rot_Z_SD=mean(std(HandBar_Rot_Z_Catch));
Value_Frame_Rot_Z_Mean=mean(mean(Frame_Rot_Z_Catch));
Value_Frame_Rot_Z_SD=mean(std(Frame_Rot_Z_Catch));

Value_Out=[Value_Head_Rot_Z_Mean,Value_Head_Rot_Z_SD,Value_Trunk_Rot_Z_Mean,Value_Trunk_Rot_Z_SD... 
    Value_Trunk_Rot_Y_Mean,Value_Trunk_Rot_Y_SD,Value_Mean_Scalar_Velocity,Value_SD_Scalar_Velocity,Value_HandBar_Rot_Z_Mean...
    Value_HeadBar_Rot_Z_SD,Value_Frame_Rot_Z_Mean,Value_Frame_Rot_Z_SD];
Value_Out_title={'Value_Head_Rot_Z_Mean','Value_Head_Rot_Z_SD','Value_Trunk_Rot_Z_Mean','Value_Trunk_Rot_Z_SD'... 
    'Value_Trunk_Rot_Y_Mean','Value_Trunk_Rot_Y_SD','Value_Mean_Scalar_Velocity','Value_SD_Scalar_Velocity','Value_HandBar_Rot_Z_Mean'...
    'Value_HeadBar_Rot_Z_SD','Value_Frame_Rot_Z_Mean','Value_Frame_Rot_Z_SD'};
value_sheet=[Value_Out_title;num2cell(Value_Out)];
%data
mean_SD_curve=[meanXtrunk,sdXtrunk_up,sdXtrunk_low,meanYtrunk,sdYtrunk_up,sdYtrunk_low,...
    meantrunk_Vel_X,sdXtrunk_Vel_up,sdXtrunk_Vel_low,meantrunk_Vel_Y,sdYtrunk_Vel_up,sdYtrunk_Vel_low...
    meanHead_Rot_Z,sdHead_Rot_Z_up,sdHead_Rot_Z_low,...
    meanTrunk_Rot_Z,sdTrunk_Rot_Z_up,sdTrunk_Rot_Z_low,meanTrunk_Rot_Y,sdTrunk_Rot_Y_up,sdTrunk_Rot_Y_low...
    meanHandBar_Rot_Z,sdHandBar_Rot_Z_up,sdHandBar_Rot_Z_low,meanFrame_Rot_Z,sdFrame_Rot_Z_up,sdFrame_Rot_Z_low ...
    mean_Scalar_Vel,SD_Scalar_Vel_up,SD_Scalar_Vel_low]
mean_SD_curve_title={'meanXtrunk','2SD_up','2SD_low','meanYtrunk','2SD_up','2SD_low'...
    'meantrunk_Vel_X','2SD_up','2SD_low','meantrunk_Vel_Y','2SD_up','2SD_low'...
    'meanHead_Rot_Z','2SD_up','2SD_low',...
    'meanTrunk_Rot_Z','2SD_up','2SD_low','meanTrunk_Rot_Y','2SD_up','2SD_low'...
    'meanHandBar_Rot_Z','2SD_up','2SD_low','meanFrame_Rot_Z','2SD_up','2SD_low','mean_Scalar_Vel','2SD_up','2SD_low'}
data_sheet=[mean_SD_curve_title;num2cell(mean_SD_curve)];

%% 
% *Enregistrement des variables*

%fichier excel
Value=cell2table(value_sheet);
Data=cell2table(data_sheet);
excel_filename=fullfile(output_folder,'mean5CatchTrials.xlsx');
writetable(Value,excel_filename,'sheet',1);
writetable(Data,excel_filename,'sheet',2);

%fichier matlab
filename='Mean_5_CatchTrials.mat'
filename=fullfile(output_folder,filename);
save(filename,'meanXtrunk','sdXtrunk_up','sdXtrunk_low','meanYtrunk','sdYtrunk_up','sdYtrunk_low',...
    'meantrunk_Vel_X','sdXtrunk_Vel_up','sdXtrunk_Vel_low','meantrunk_Vel_Y','sdYtrunk_Vel_up','sdYtrunk_Vel_low'...
    ,'meanHead_Rot_Z','sdHead_Rot_Z_up','sdHead_Rot_Z_low','meanTrunk_Rot_Z','sdTrunk_Rot_Z_up','sdTrunk_Rot_Z_low'...
    ,'meanTrunk_Rot_Y','sdTrunk_Rot_Y_up','sdTrunk_Rot_Y_low','meanHandBar_Rot_Z','sdHandBar_Rot_Z_up'...
    ,'sdHandBar_Rot_Z_low','meanFrame_Rot_Z','sdFrame_Rot_Z_up','sdFrame_Rot_Z_low', 'mean_Scalar_Vel'...
    ,'SD_Scalar_Vel_up','SD_Scalar_Vel_low');
